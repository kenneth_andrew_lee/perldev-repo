#!/bin/perl

use DateTime;
use DateTime::TimeZone;

use Time::Local;

@t = localtime(time);
$gmt_offset_in_seconds = timegm(@t) - timelocal(@t);
$gmt_offset_in_seconds /= 3600;

printf "%02.2f\n\n\n",$gmt_offset_in_seconds;



my $tz = DateTime::TimeZone->new( name => 'America/Denver' );

my $dt = DateTime->now();
my $offset = $tz->offset_for_datetime($dt);

print "$tz, $dt, $offset\n\n";