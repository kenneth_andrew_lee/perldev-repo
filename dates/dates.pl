#!/usr/bin/perl
use strict;
use warnings;
use lib qw(/home/klee/perl5/lib/perl5);
use Date::Calc qw(Add_Delta_Days);
use POSIX qw(strftime);

my $year;
my $month;
my $day;

($year, $month, $day) = Add_Delta_Days(2013,2,1,-1);
print "$year $month $day\n";
printf qq{%d\n}, time/86400;

print "\n\n\n";
print "localtime:\n";
my $local_time = localtime;
my $now_string = strftime "%a %b %d %H:%M:%S %Y %z", localtime;
#$now_string = strftime "%a, %d %b %y %T %z", localtime;  #RFC 822-compliant date format
#$now_string = strftime "%a, %d %b %Y %T %z", localtime;  #RFC 2822-compliant date format
$now_string = strftime "%Y%m%d%H%M%S", localtime;
print "$now_string\n";
