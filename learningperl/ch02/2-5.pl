#!/usr/bin/perl
# Write a program that prompts for and reads a sting and a number (on separate
# lines of input) and prints out the string the number of times indicated by 
# the number on separate lines. (Hint: use the x operator.) If the user enters "
# fred" and "3", the output should be three lines, each saying "fred".  If the
# user enters "fred" and "299792", there may be a lot of output.

print "\nEnter a the first value: ";
$first = <STDIN>;
print "\nEnter a the second value: ";
chomp($second = <STDIN>);
print "\n";
print $first x $second;
 
