#!/usr/bin/perl
# Write a program that prompts for and reads two numbers (on separate lines
# of input) and prints out the product of the two numbers multiplied together.
# 

use Math::Trig;
print "\nEnter a the first value: ";
chomp($first = <STDIN>);
print "\nEnter a the second value: ";
chomp($second = <STDIN>);
$sum = $first * $second;
print "\n" . 'Sum = ' . $sum . "\n";
 
