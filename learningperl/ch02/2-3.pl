#!/usr/bin/perl
# Modify the program from the previous exercise so that, if the user enters a
# number less than zero, the reported circumference will be zero, rather than
# negative

use Math::Trig;
print "Enter a value for the radius: ";
chomp($radius = <STDIN>);
if ($radius < 0) {
	$radius = 0;
}
$circumference = 2 * pi * $radius;
print "\n" . 'Circumference = ' . $circumference . "\n";
 
