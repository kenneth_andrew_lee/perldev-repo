use strict;
use warnings;
use Data::Dumper;

#anonymous array
my $arrayOfHashes = [ { orderid => 100,
                        cost => 2000,
                        quantity => 3 },
                      { name => "Test",
                        address => "Some place" },
                      { brandName => "Geeks",
                        vendorName => "newWave",
                        carrierName => "bluedart" }  
                    ];

print @{$arrayOfHashes}[0], "\n";


my @test = @{$arrayOfHashes}[1];
print Dumper(@test), "\n";
#my @keys = keys @test;
#print @keys, "\n";
#print keys @{$arrayOfHashes}[1], "\n";

print @{$arrayOfHashes}[1]->{"name"}, "\n";

__END__
