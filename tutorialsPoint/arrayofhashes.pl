#!/usr/bin/perl
use strict;
use warnings;

my @arrayOfHashes = ( { orderid => 100,
                        cost => 2000,
                        quantity => 3 },
                      { name => "Test",
                        address => "Some place" },
                      { brandName => "Geeks",
                        vendorName => "newWave",
                        carrierName => "bluedart" }  
                    );

print $arrayOfHashes[0], "\n";

print keys %{$arrayOfHashes[1]}, "\n";

print $arrayOfHashes[1]->{"name"}, "\n";

__END__
HASH(0x563d9647b780)
nameaddress
Test
