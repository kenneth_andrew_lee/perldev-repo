#!/usr/bin/perl

#use strict;
use warnings;

use POSIX qw(strftime);


print header_data() . transaction_data(), "||||\n";

sub header_data {
	# $eh_app_data_len can be either 00000454 or 00000728
	my $eh_app_data_len = "00000728";

	my $eh_ext_hdr_len = "0000";

	# 100000101 = Priority scoring; authorization data from external system; response required
	# 100000102 = Standard scoring; authorization or settlement/posting data from external system; response required
	# 000000102 = Standard scoring; authorization or settlement/posting data from external systems; no response
	# 100000104 = Priority scoring; authorization data from external system; no profile update; response required
	# 100000105 = Standard scoring; authorization or settlement/posting data from external system; response required
	# 000000105 = Standard scoring; authorization or settlement/posting data from external systems and local database; no response
	# 000000107 = Profile update; authorization or settlement/posting data from external systems; no scoring; no response
	# 000000108 = Profile update; authorization or settlement/posting data from external systems and local databases; no scoring; no response
	# 100000109 = Debit transaction (for use in rules); data from external systems; response required
	# 000000109 = Debit transaction (for use in rules); data from external systems; no response
	# 100000110 = Nonmonetary transaction; data from external systems; no scoring; response required
	# 000000110 = Nonmonetary transaction; data from external systems; no scoring; no response
	# 000000111 = Reserved
	# 000000112 = Reserved
	# 000000113 = Reserved
	# 100000121 = Reserved
	my $eh_tran_code = "100000102"; # 9 bytes

	my $eh_source = "JMETER    "; # 10 bytes
	my $eh_dest = "FALCON    "; # 10 bytes
	my $eh_error = "0000000000"; # 10 bytes
	my $eh_filler = " "; # 1 byte			
	my $eh_ext_hdr = ""; # Not using this

	return $eh_app_data_len . $eh_ext_hdr_len . $eh_tran_code . $eh_source . $eh_dest . $eh_error . $eh_filler . $eh_ext_hdr;
}

sub transaction_data {
	my %sic_cds = ( '5812' => 'M',  # Eating Places, Restaurants
		             '5311' => 'M',  # Department Store
		             '5411' => 'M',  # Grocery Stores, Supermarkets
		             '5542' => 'M',  # Automated Fuel Dispenser
		             '5814' => 'M',  # Fast Food Restaurants
		             '6011' => 'C',  # ATM
		             '5732' => 'M',  # Electronics Store
		             '5734' => 'M',  # Computer Software Store
		             '5944' => 'M'   # Jewlery
	);

	my @sic_keys = keys %sic_cds;
	my @nom_sic = ( '5812', '5311', '5411', '5542', '5814', '6011' );

	my $fmt_date = strftime("%Y%m%d", localtime(time));
	my $fmt_time = strftime("%H%M%S", localtime(time));

	my $mer_cat;
	if (rand(5) < 1) {
		$mer_cat=$sic_keys[int(rand(9))];
	} else {
		$mer_cat=@nom_sic[int(rand(6))];
	}

	my $trans_amt=rand(200) + 3;
	if (($mer_cat eq '5732' || $mer_cat eq '5944' ) && rand(100) < 8) {
		$trans_amt=rand(1500) + 1000;
	}
	$trans_amt=(rand(30) + 2) if ($mer_cat eq '5814');
	if ($mer_cat eq '5542' ) {
		if ( rand(4) > 2 ) {
	  		$trans_amt=rand(75);
		}
		$trans_amt=rand(75);
	}

	$trans_amt = sprintf "+%012.2f", $trans_amt;
	#print "\$trans_amt = $trans_amt\n\n"; # 13 bytes with prepended + sign.
	#print "\$sic_cds{$mer_cat} = $sic_cds{$mer_cat}","\n";


	my $eh_tran_code = "100000102";
	my $expl_threshold = " " x 4;
	my $susp_threshold = " " x 4;


	my $acct_nbr  = get_account();
	my $auth_post_flag  = "A";
	my $card_post_code  = "801290000";  # 9 bytes Cardholder ZIP or postal code. Do not truncate leading zeros.
	my $card_country_cd  = "840";
	my $open_date  = "19950101";  #  8 bytes ccyymmdd  Make 19950101
	my $plastic_issue_date  = "20050101";
	my $plastic_issue_type  = " ";
	my $acct_exp_date = "20990101"; #  8 bytes ccyymmdd  Make "        "
	my $card_exp_date = "20200101"; #  8 bytes ccyymmdd  Make 20200101
	my $auth_daily_limit  = "+000000000";
	my $auth_daily_cash_limit  = "+000000000";
	my $gender_cd  = " ";
	my $birth_date  = "19700101";
	my $num_card  = "001";
	my $income = "+000000000";
	my $card_type  = " ";
	my $card_use  = " ";
	my $trans_date  = "$fmt_date";
	my $trans_time  = "$fmt_time";
	#my $trans_amt  = "";  # Calculated above
	my $trans_curr_code = "840";
	my $trans_curr_conv = "+00001.000000";
	my $auth_decision_cd  = "A";
	my $tran_typ = $sic_cds{$mer_cat};
	#my $mer_cat  = ""; Calculated above
	my $merch_post_code  = "801110000";
	my $merch_country_cd  = "840";
	my $pin_verify_cd  = "V";
	my $cvv_verify_cd  = "V";
	my $pos_entry_mode  = "S";
	my $post_date  = " " x 8;
	my $post_authed_cd  = " ";
	my $mismatch_ind  = " ";
	my $case_cre_ind  = " ";
	my $user_ind_1  = " ";
	my $user_ind_2  = " ";
	my $user_data_1  = " " x 10;
	my $user_data_2  = " " x 10;
	my $merch_id = " " x 10;
	my $merch_data_avail  = "Y";
	my $card_data_avail  = "Y";
	my $ext_score1  = "0000";
	my $ext_score2  = "0000";
	my $ext_score3  = "0000";
	my $cust_present_ind  = "Y";
	my $cat_type  = " ";
	my $random_digits  = "  ";
	my $portfolio  = " " x 14;
	my $client_id  = "PRC595        ";  # 14 bytes
	my $ica_nbr  = " " x 6;
	my $merch_nm  = 'MERCHANT #1234';
	$merch_nm = sprintf "%-40s", $merch_nm;
	my $merch_city  = "DENVER";
	$merch_city = sprintf "%-30s", $merch_city;
	my $merch_st  = "CO ";
	my $cas_supr_ind  = " ";
	my $user_ind_3  = " " x 5;
	my $user_ind_4  = " " x 5;
	my $user_data_3  = " " x 15;
	my $user_data_4  = " " x 20;
	my $user_data_5  = " " x 40;
	my $rltm_req  = " ";
	my $pad_resp  = " ";
	my $pad_action_until  = " " x 8;

	# Start API2.x fields
	my $card_master_acct_nbr  = " " x 19;
	my $card_aip_static  = " ";
	my $card_aip_dynamic  = " " x 2;
	my $card_aip_verify  = " ";
	my $card_aip_risk  = " ";
	my $card_aip_issuer_auth  = " ";
	my $card_aip_application_cryptogram  = " ";
	my $card_delinq_cycles  = "0";
	my $card_tot_delinq_amt  = "+000000000.00";
	my $card_cash_balance  = "+000000000.00";
	my $card_merch_balance = "+000000000.00";
	my $card_payment_history  = " " x 12;
	my $card_media_type = " ";
	my $cvv2_present = "0";
	my $cvv2_response = "P";
	my $avs_response = " ";
	my $transaction_category = "P";
	my $acquirer_id = " " x 12;
	my $acquirer_country = "840";
	my $terminal_id = " " x 16;
	my $terminal_type  = "U";
	my $terminal_entry_cap  = "U";
	my $pos_condition_code  = "00";
	my $atm_network_id  = " ";
	my $create_case  = " ";
	my $check_number  = " " x 6;
	my $term_verif_results  = " " x 10;
	my $card_verif_results  = " " x 10;
	my $cryptogram_valid  = " ";
	my $atc_card  = " " x 5;
	my $atc_host  = " " x 5;
	my $last_online_atc  = " " x 2;
	my $offline_limit_inc  = " " x 2;
	my $offline_limit_tocard  = " " x 2;
	my $recurring_auth_frequency  = " " x 2;
	my $recur_auth_exp_date  = " " x 8;
	my $card_association  = "C";
	my $card_incentive  = " ";
	my $card_status  = " " x 2;
	my $card_status_date  = " " x 8;
	my $process_reason_code  = " " x 5;
	my $transaction_advice  = " ";
	my $acquirer_merchant_id  = " " x 16;
	my $card_order  = " ";
	my $cashback_amount  = "0000000000.00";
	my $user_data_6  = "0" x 13;
	my $user_data_7  = " " x 40;


#return "";

	return 	$eh_tran_code . $expl_threshold . $susp_threshold .
		$acct_nbr . $auth_post_flag . $card_post_code . $card_country_cd . 
		$open_date . $plastic_issue_date . $plastic_issue_type . $acct_exp_date . 
		$card_exp_date . $auth_daily_limit . $auth_daily_cash_limit . $gender_cd . 
		$birth_date . $num_card . $income . $card_type . $card_use . $trans_date . 
		$trans_time . $trans_amt . $trans_curr_code . $trans_curr_conv . 
		$auth_decision_cd . $tran_typ . $mer_cat . $merch_post_code . 
		$merch_country_cd . $pin_verify_cd . $cvv_verify_cd . $pos_entry_mode . 
		$post_date . $post_authed_cd . $mismatch_ind . $case_cre_ind . $user_ind_1 . 
		$user_ind_2 . $user_data_1 . $user_data_2 . $merch_id . $merch_data_avail . 
		$card_data_avail . $ext_score1 . $ext_score2 . $ext_score3 . $cust_present_ind . 
		$cat_type . $random_digits . $portfolio . $client_id . $ica_nbr . $merch_nm . 
		$merch_city . $merch_st . 
		$cas_supr_ind . $user_ind_3 . $user_ind_4 . $user_data_3 . 
		$user_data_4 . $user_data_5 . $rltm_req . $pad_resp . 
		$pad_action_until . $card_master_acct_nbr . $card_aip_static . $card_aip_dynamic . 
		$card_aip_verify . $card_aip_risk . $card_aip_issuer_auth . $card_aip_application_cryptogram . 
		$card_delinq_cycles . $card_tot_delinq_amt . $card_cash_balance . $card_merch_balance . 
		$card_payment_history . $card_media_type . $cvv2_present . $cvv2_response . 
		$avs_response . $transaction_category . $acquirer_id . $acquirer_country . 
		$terminal_id . $terminal_type . $terminal_entry_cap . $pos_condition_code . 
		$atm_network_id . $create_case . $check_number . $term_verif_results . 
		$card_verif_results . $cryptogram_valid . $atc_card . $atc_host . 
		$last_online_atc . $offline_limit_inc . $offline_limit_tocard . 
		$recurring_auth_frequency . $recur_auth_exp_date . $card_association . 
		$card_incentive . $card_status . $card_status_date . $process_reason_code . 
		$transaction_advice . $acquirer_merchant_id . $card_order . $cashback_amount . 
		$user_data_6 . $user_data_7;
}

sub get_account {
	# return a 19 byte card number with spaces at the right end.
    #$acct_nbr=sprintf "781346%010.10d", ((int(rand(10)) + 1) * 10) + 55000000;
    # $acct_nbr=sprintf "876543%010.10d", 100002;
    #my $acct_nbr = sprintf "400140%010.10d", 40004001;
    my $acct_nbr = "4" . "0" x 9 . sprintf("%06.6d", rand(100000)) . "   ";

}

__END__
print $acct_nbr,"\n";
print $auth_post_flag,"\n";
print $card_post_code,"\n";
print $card_country_cd,"\n";
print $open_date,"\n";
print $plastic_issue_date,"\n";
print $plastic_issue_type,"\n";
print $acct_exp_date,"\n";
print $card_exp_date,"\n";
print $auth_daily_limit,"\n";
print $auth_daily_cash_limit,"\n";
print $gender_cd,"\n";
print $birth_date,"\n";
print $num_card,"\n";
print $income,"\n";
print $card_type,"\n";
print $card_use,"\n";
print $trans_date,"\n";
print $trans_time,"\n";
print $trans_amt,"\n";
print $trans_curr_code,"\n";
print $trans_curr_conv,"\n";
print $auth_decision_cd,"\n";
print $$tran_typ,"\n";
print $mer_cat,"\n";
print $merch_post_code,"\n";
print $merch_country_cd,"\n";
print $pin_verify_cd,"\n";
print $cvv_verify_cd,"\n";
print $pos_entry_mode,"\n";
print $post_date,"\n";
print $post_authed_cd,"\n";
print $mismatch_ind,"\n";
print $case_cre_ind,"\n";
print $user_ind_1,"\n";
print $user_ind_2,"\n";
print $user_data_1,"\n";
print $user_data_2,"\n";
print $merch_id,"\n";
print $merch_data_avail,"\n";
print $card_data_avail,"\n";
print $ext_score1,"\n";
print $ext_score2,"\n";
print $ext_score3,"\n";
print $cust_present_ind,"\n";
print $cat_type,"\n";
print $random_digits,"\n";
print $portfolio,"\n";
print $client_id,"\n";
print $ica_nbr,"\n";
print $merch_nm,"\n";
print $merch_city,"\n";
print $merch_st,"\n";
print $cas_supr_ind,"\n";
print $user_ind_3,"\n";
print $user_ind_4,"\n";
print $user_data_3,"\n";
print $user_data_4,"\n";
print $user_data_5,"\n";
print $rltm_req,"\n";
print $pad_resp,"\n";
print $pad_action_until,"\n";
print $card_master_acct_nbr,"\n";
print $card_aip_static,"\n";
print $card_aip_dynamic,"\n";
print $card_aip_verify,"\n";
print $card_aip_risk,"\n";
print $card_aip_issuer_auth,"\n";
print $card_aip_application_cryptogram,"\n";
print $card_delinq_cycles,"\n";
print $card_tot_delinq_amt,"\n";
print $card_cash_balance,"\n";
print $card_merch_balance,"\n";
print $card_payment_history,"\n";
print $card_media_type,"\n";
print $cvv2_present,"\n";
print $cvv2_response,"\n";
print $avs_response,"\n";
print $transaction_category,"\n";
print $acquirer_id,"\n";
print $acquirer_country,"\n";
print $terminal_id,"\n";
print $terminal_type,"\n";
print $terminal_entry_cap,"\n";
print $pos_condition_code,"\n";
print $atm_network_id,"\n";
print $create_case,"\n";
print $check_number,"\n";
print $term_verif_results,"\n";
print $card_verif_results,"\n";
print $cryptogram_valid,"\n";
print $atc_card,"\n";
print $atc_host,"\n";
print $last_online_atc,"\n";
print $offline_limit_inc,"\n";
print $offline_limit_tocard,"\n";
print $recurring_auth_frequency,"\n";
print $recur_auth_exp_date,"\n";
print $card_association,"\n";
print $card_incentive,"\n";
print $card_status,"\n";
print $card_status_date,"\n";
print $process_reason_code,"\n";
print $transaction_advice,"\n";
print $acquirer_merchant_id,"\n";
print $card_order,"\n";
print $cashback_amount,"\n";
print $user_data_6,"\n";
print $user_data_7,"\n";


000007280000100000102VISADPS   FALCON    0000000000 
100000102        
4959993999999952   A2260300008401995010120050101 2099010120200101+000000000+000000000 19630101001+000000000  20160125161612+000000324.24840+00001.000000AM5411801110000840VVS                                           YY000000000000Y                 PRCC95              MERCHANT #1234                          DENVER                        CO                                                                                                                           0+000000000.00+000000000.00+000000000.00             0P P            840                UU00                                                       C                                  0000000000.000000000000000                                        |






4959993999999952   A2260300008401995010120050101 2099010120200101+000000000+000000000 19630101001+000000000  20160125161612+000000324.24840+00001.000000AM5411801110000840VVS                                           YY000000000000Y                 PRCC95              MERCHANT #1234                          DENVER                        CO                                                                                                                           0+000000000.00+000000000.00+000000000.00             0P P            840                UU00                                                       C                                  0000000000.000000000000000                                        |
4959993999999952   A2260300008401995010120050101 2099010120200101+000000000+000000000 19630101001+000000000  20160125161612+000000324.24840+00001.000000AM5411801110000840VVS                                           YY000000000000Y                 PRCC95              MERCHANT #1234                          DENVER                        CO                                                                                                                           0+000000000.00+000000000.00+000000000.00             0P P            840                UU00                                                       C                                  0000000000.000000000000000                                        |






"000007280000100000102VISADPS   FALCON    0000000000 100000102        ${acct_nbr}A${card_post_code}8401995010120050101 2099010120200101+000000000+000000000 ${birth_date}001+000000000  ${trans_date}${trans_time}${tran_amt}840+00001.000000AM5411${merch_post_code}840VVS                                           YY000000000000Y                 ${client_id}      MERCHANT #1234                          DENVER                        CO                                                                                                                           0+000000000.00+000000000.00+000000000.00             0P P            840                UU00                                                       C                                  0000000000.000000000000000                                        "

acct_nbr,card_post_code,birth_date,trans_date,trans_time,tran_amt,merch_post_code,client_id