#!/bin/perl
################################################################################
# clientlist.pl
# KALee
# Friday 11-Mar-2016
# Prints tenant, then PRC and PROC_NM for each prc under the tenant from the
# file clientlist that is located in Falcon 52c in the instance home
# directory.
################################################################################
use strict;
use warnings;
use DBI;

# database credentials
#my $database = 'DBI:Oracle:FHDB03';
my $database = 'DBI:Oracle:FHDB03Q';
my $username = 'reporter';
#my $password = 'so_billme';  # reporter or so_billme
my $password = 'reporter';  # reporter or so_billme

# connection to the database
my $dbh = DBI->connect($database, $username, $password) or die "Can't make database connect: $DBI::errstr\n";    
my $sth = $dbh -> prepare(
  qq( 
      select distinct proc_nm 
      from client_dim
      where 1=1
        and ora_falcon_crd_client_id = :client_id
        and ora_falcon_schema = :schema
    )
);

while (<DATA>) {
  chomp;
  my ($tenant, $tenantid, $prclist) = split /:/, $_;
  my @prclist = split /,/, $prclist;
  print "$tenant\n";
  foreach my $prc (@prclist) {
    $sth->bind_param(":client_id", $prc);
    $sth->bind_param(":schema", $tenant);
    $sth->execute();
    while (my $row = $sth->fetchrow_array) {   
      print "$prc -> $row\n";
    }
  }
  print "\n\n";
}
warn "Data fetching terminated early by error: $DBI::errstr\n" if $DBI::err;

# Close Connection
$dbh->disconnect();


#use Data::Dumper;
  #print Dumper($tenant, @prclist), "\n";
  #print "\$tenant = $tenant, \$tenantid = $tenantid, \@prclist = @prclist\n\n";
#FALU740:5105:PRC740

__DATA__
FALUCALL:5101:PRC200,PRC202,PRC206,PRC208,PRC215,PRC222,PRC225,PRC239,PRC246,PRC254,PRC255,PRC257,PRC261,PRC272,PRC273,PRC282,PRC284,PRC287,PRC294,PRC312,PRC323,PRC338,PRC350,PRC372,PRC378,PRC392,PRC402,PRC420,PRC450,PRC480,PRC560,PRC564,PRC584,PRC616,PRC640,PRC715,PRC720,PRC726,PRC750,PRC784,PRC860,PRC869,PRC930,PRC354,PRC670,PRC656,PRC924,PRC645,PRC600,PRC510,PRC890,PRC260,PRC508,PRC396,PRC0X0,PRC224,PRC735,PRC0X1,PRC470,PRC962,PRC216,PRC695,PRC681,PRC682,PRC745,PRC725,PRC390,PRC635,PRC717,PRC733,PRC132,PRC864,PRC892,PRC233,PRC760,PRC244,PRC324,PRC769,PRC763,PRC663,PRC826,PRC352,PRC445,PRC281,PRC327,PRC824,PRC235,PRC328,PRC765,PRC697,PRC966,PRC632,PRC662,PRC665,PRC692,PRC968,PRC451,PRC675,PRC618,PRC766,PRC418,PRC531,PRC454,PRC291,PRC552,PRC602,PRC318,PRC339,PRC347,PRC428,PRC140,PRC607,PRC828,PRC558,PRC376,PRC603,PRC866,PRC457,PRC862,PRC756,PRC633,PRC863,PRC823,PRC370,PRC921,PRCZAG,PRC879,PRC923,PRC825,PRC724,PRC356,PRC829,PRC881,PRC599,PRC819,PRC807,PRC975,PRC931,PRC815,PRC927,PRC853,PRC961,PRC989,PRC817,PRC956,PRC704,PRC580,PRC885,PRC541,PRC579,PRC532
FALUPRE:5103:PRC562,PRC629,PRC809,PRC0X2,PRC456,PRC446,PRC608,PRC253,PRC468,PRC699,PRC855,PRC636,PRC513,PRC689,PRC252,PRC598,PRC967
FALU120:5104:PRC120,PRC122
FALU683:5106:PRC683
FALU597:5108:PRC597
FALU899:5109:PRC899
FALU595:5110:PRC595
FALU127:5112:PRC127
FALU734:5113:PRC734
FALU761:5114:PRC761