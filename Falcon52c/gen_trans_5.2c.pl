## $HDR_FMT = "%19.19s%1.1s%12.12s%8.8s%2.2s%12.12s%8.8s%6.6s%9.9s%360.360s";
## $FTR_FMT = "%19.19s%1.1s%12.12s%8.8s%2.2s%12.12s%8.8s%6.6s%9.9s%360.360s";
use Time::HiRes qw(usleep);

$zip='802220000';
$open_dt='19900101';
$iss_dt='20070101';
$a_exp_dt='20990101';
$c_exp_dt='20090131';
$dob='19700701';
$tran_dt_base='201103';
$tran_typ='M';
$mer_cat='5812';
$mer_zip='801110000';
$merch_id='331234';
$clnt_id='PRC140';
$mer_name='MERCHANT #1234';
$mer_city='DENVER';
$mer_st='CO';

%sic_cds = ( '5812' => 'M',  # Eating Places, Restaurants
             '5311' => 'M',  # Department Store
             '5411' => 'M',  # Grocery Stores, Supermarkets
             '5542' => 'M',  # Automated Fuel Dispenser
             '5814' => 'M',  # Fast Food Restaurants
             '6011' => 'C',  # ATM
             '5732' => 'M',  # Electronics Store
             '5734' => 'M',  # Computer Software Store
             '5944' => 'M'   # Jewlery
);

@sic_keys = keys %sic_cds;
@nom_sic = ( '5812', '5311', '5411', '5542', '5814', '6011' );

format TRAN_DATA =
@<<<<<<<<<<<<<<<<<<A@<<<<<<<<840@<<<<<<<@<<<<<<< @<<<<<<<@<<<<<<<+000000000+000000000 @<<<<<<<001+000000000D @<<<<<<<@<<<<<+@0#######.##840+@0###.######A@@<<<@<<<<<<<<840VVS             @<<<<<<<<<@<<<<<<<<<@<<<<<<<<<YY000000000000Y 00@<<<<<<<<<<<<<@<<<<<<<<<<<<<@<<<<<@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<@<<@>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>                          0+000000000.00+000000000.00+000000000.00             0P P                               UU00                                                       C                                  0000000000.000000000000000                                       .
$acct_nbr,           $zip,      $open_dt, $iss_dt, $a_exp_dt, $c_exp_dt,              $dob,                   $tran_dt, $tran_tm, $tran_amt, 1.0, $tran_typ, $mer_cat, $mer_zip, undef, undef, $mer_id, "FALCON TEST", $clnt_id, undef, $mer_name, $mer_city, $mer_st, ' '
.

$~='TRAN_DATA';
for $day (9..9) {
  $tran_dt = sprintf "%04.4d%02.2d", $tran_dt_base , $day;
#  HOUR: for $hr (0..23) {
  HOUR: for $hr (13..14) {
    next HOUR if(rand(12) > 11);
    if ($hr <= 7 || $hr >= 22) {
      next HOUR if(rand(20) > 19);
      usleep(int(rand(850)+150));
    } 
    $min = 0;
    MIN: while ($min < 60) {
      next MIN if(rand(12) > 11);
      $min += int(rand(rand(50))) + 1;
      if ($min >= 60) {
        next MIN;
      }
      
      if (rand(5) < 1) {
        $mer_cat=$sic_keys[int(rand(9))];
      } else {
        $mer_cat=@nom_sic[int(rand(6))];
      }
      
      $tran_typ=$sic_cds{$mer_cat};
      #$acct_nbr=sprintf "781346%010.10d", ((int(rand(10)) + 1) * 10) + 55000000;
      # $acct_nbr=sprintf "876543%010.10d", 100002;
      $acct_nbr=sprintf "400140%010.10d", 40004001;
      $tran_amt=rand(200) + 3;
      if (($mer_cat eq '5732' || $mer_cat eq '5944' ) && rand(100) < 8) {
        $tran_amt=rand(1500) + 1000;
      }
      $tran_amt=(rand(30) + 2) if ($mer_cat eq '5814');
      if ($mer_cat eq '5542' ) {
        if ( rand(4) > 2 ) {
          $tran_tm=sprintf "%02d%02d%02d", $hr, ($min <= 59 ? $min : 59) , int(rand(29));
          $tran_amt=rand(75);
          write;
        }
        $tran_tm=sprintf "%02d%02d%02d", $hr, $min, 30 + int(rand(29));
        $tran_amt=rand(75);
      }
      write;
      $tran_tm=sprintf "%02d%02d%02d", $hr, ($min <= 59 ? $min : 59), 30 + int(rand(29));
##      $acct_base = 1;  
##      while ($acct_base <= 35) {
##        $acct_nbr=sprintf "987123%010.10d", $acct_base;
##        write;
##        $acct_base++;
##      }
    }
  }
}

