#!/usr/bin/perl
use strict;
use warnings;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# Static Variables
#my $acct_nbr             = '4959993999999955';   
my $card_post_code       = '226030000';              
my $birth_date           = '19630101';               
#my $trans_date           = '20130628';           
#my $trans_time           = '080000';             
#my $tran_amt             = '+000000010.01';      
my $merch_post_code      = '801110000';          
my $merch_id             = '    331234';         
my $ext_score1           = '0025';               
my $portfolio            = 'FALCON TEST TR';     
my $client_id            = 'PRCC95';             
my $merch_nm             = 'MERCHANT #1234';     
my $merch_city           = 'DENVER';             
my $merch_st             = 'CO';                 
my $card_master_acct_nbr = '1111222233334444';
my $recur_auth_exp_date  = '20161231';           
my $card_status_date     = '20161231';           


my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
my $trans_date = $year+1900 . (sprintf "%02d", $mon+1) . sprintf "%02d", $mday;

# Create xxx cards and 100,000 amounts: 10000
for (1..1000) {
	my $acct_nbr = '49599939999999' . sprintf "%02d", int(rand(100));
	$acct_nbr = sprintf "%-19.19s", $acct_nbr;
	for ( 1..10) {
		($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);	
		my $trans_time =  sprintf "%-2.2d", $hour . sprintf "%-2.2d", $min . sprintf "%-2.2d", $sec;
		my $tran_amt = (rand(99999.00)/100);	
		$tran_amt = sprintf "+%012.2f", $tran_amt;
		#printf "%-19sA%-9s8401996081720010101 2099010120201231+000000000+000000000 %-8s001+000000000D %-8s%-6s%-13s840+000000001.00AM5310%-9s840VVS             00                  %-10sYY%-4s00000000Y 00%-14s%-14s      %-40s%-30s%-3s                                                                                                %-19s                                                                                                                                                  %-8s    %-8s             ",$acct_nbr, $card_post_code, $birth_date, $trans_date, $trans_time, $tran_amt, $merch_post_code, $merch_id, $ext_score1, $portfolio, $client_id, $merch_nm, $merch_city, $merch_st, $card_master_acct_nbr, $recur_auth_exp_date, $card_status_date;
		#printf "\"%-19s\",\"%-9s\",\"%-8s\",\"%-8s\",\"%-6s\",\"%-13s\",\"%-9s\",\"%-10s\",\"%-4s\",\"%-14s\",\"%-14s\",\"%-40s\",\"%-30s\",\"%-3s\",\"%-19s\",\"%-8s\",\"%-8s\"", $acct_nbr, $card_post_code, $birth_date, $trans_date, $trans_time, $tran_amt, $merch_post_code, $merch_id, $ext_score1, $portfolio, $client_id, $merch_nm, $merch_city, $merch_st, $card_master_acct_nbr, $recur_auth_exp_date, $card_status_date;
		printf "\"%-19s\",\"%-9s\",\"%-8s\",\"%-8s\",\"%-6s\",\"%-13s\",\"%-9s\",\"%-14s\"", $acct_nbr, $card_post_code, $birth_date, $trans_date, $trans_time, $tran_amt, $merch_post_code, $client_id;
		print "\n";
	}
}

__END__


acct_nbr,card_post_code,birth_date,trans_date,trans_time,tran_amt,merch_post_code,client_id

acct_nbr,card_post_code,birth_date,trans_date,trans_time,tran_amt,merch_post_code,merch_id,ext_score1,portfolio,client_id,merch_nm,merch_city,merch_st,card_master_acct_nbr,recur_auth_exp_date,card_status_date

acct_nbr, card_post_code, birth_date, trans_date, trans_time, tran_amt, merch_post_code, merch_id, ext_score1, portfolio, client_id, merch_nm, merch_city, merch_st, card_master_acct_nbr, recur_auth_exp_date, card_status_date
Need a header

Field Name 			Field Format  					Description 																																																	Position
EH_APP_DATA_LEN		Numeric, nnnnnnnn (8 bytes). 	Length (in bytes) of data following the external header and extended header. For a handshaking message, the only valid value is 00000000. 																		1–8
EH_EXT_HDR_LEN		Numeric, nnnn (4 bytes).		Length of extended-header information that follows the external header. Should be set to zero for handshaking messages. No extended header information is returned even when the setting is not set to zero.	9–12
EH_TRAN_CODE		Numeric, n00000nnn (9 bytes).	Transaction codes. A handshaking message from the Falcon Server to an external application includes one of the following transaction codes:																		13–21
													100000050 = Stop sending transactions
													100000051 = Start sending transactions
													100000052 = Falcon is shutting down
													100000053 = Falcon is unavailable
													A handshaking message from the external application to the Falcon Server includes the following transaction code:
													100000060 = Is Falcon ready for transactions?

EH_SOURCE			(10 bytes).						Name of source application.																																														22–31
EH_DEST				(10 bytes).						Name of destination application.																																												32–41				
													Note: A header accompanies both a message from a sender to a recipient, and a response from the recipient to the sender. In the response, the EH_SOURCE and EH_DEST values are reversed. In either case, the value for the external application should match the EXTERNALDEVICE option in the Falcon Server configuration file.
EH_ERROR			Numeric, 0000000000 (10 bytes). Not implemented; fill with zeros																																												42–51
Falcon Fraud Manager Developer’s Guide
EH_FILLER			Text (1 byte).					For use only by clients who use Tandem computers. If the field is used, include the line HEADERSIZE = 52 in the Falcon configuration file and leave the field blank. (If the field is not used, the external header totals 51 bytes plus extended header data.)	52
EH_EXT_HDR 			Text or binary data, size determined by EH_EXT_HDR_LEN. Should not be present in handshaking messages. No extended header information is returned with handshaking messages regardless of settings.												53–nn


000000000000100000051FALCON    driver    0000000000 ||||

000009950000100000102TESTAPP001PMAX0000010000000000                  ||||
000000000000100000050FALCON    driver    0000000000 

000007280000100000105VISADPS   FALCON    0000000000 4888881100038639   A                                     20170401+000000000+                  001+000000000D 20150329234709+000000000.04840+00001.000000DR601280163    840 VE             00        000   101 Visa/ PLUYN02" ?0 ?0 "000 0000Y 00              PRC822        762793PO Box 26001                            CO                            CO    700               4888881100038639           000000000000000000000000000000000000000            M  YP59000000877 840 TERMID01       OM05     L O00000000U   0000000000121 Visa / PLUS      000000000000000000        315089046291265

Trans Header
"000007280000100000102VISADPS   FALCON    0000000000 "
"000000000000100000102VISADPS   FALCON    0000000000 "
"${acct_nbr}A${card_post_code}8401996081720010101 2099010120201231+000000000+000000000 ${birth_date}001+000000000D ${trans_date}${trans_time}${tran_amt}840+000000001.00AM5310${merch_post_code}840VVS             00                  ${merch_id}YY${ext_score1}00000000Y 00${portfolio}${client_id}      ${merch_nm}${merch_city}${merch_st}                                                                                                ${card_master_acct_nbr}                                                                                                                                                  ${recur_auth_exp_date}    ${card_status_date}            "

"000007280000100000102VISADPS   FALCON    0000000000 100000102        ${acct_nbr}A${card_post_code}8401995010120050101 2099010120200101+000000000+000000000 ${birth_date}001+000000000  ${trans_date}${trans_time}${tran_amt}840+00001.000000AM5411${merch_post_code}840VVS                                           YY000000000000Y                 ${client_id}      MERCHANT #1234                          DENVER                        CO                                                                                                                            0+000000000.00+000000000.00+000000000.00             0P P            840                UU00                                                       C                                  0000000000.000000000000000                                        "








tcpdump -s0 -vv -A '(port 49201)' | egrep "FALCON"

print $trans_date, "\n";
print $mon, "\n";
print $mon+1, "\n";
printf "%02d\n", $mon+1;
print $year+1900, "\n";

#my $acct = sprintf "%-16.16d", int(rand(20000000));  # 0000000000000001 - 0000000020000000   # 0000000000000001 - 0000000020000000
#my acct = $prefix . sprintf "%-10.10d", int(rand(20000000));
#my $pan = sprintf "%-19.19d", int(rand(20000000));
#4959993999999955
#49599939999999??

printf "%-13s\n", $tran_amt;
#print $tran_amt, "\n";
#${acct_nbr}A${card_post_code}8401996081720010101 2099010120201231+000000000+000000000 ${birth_date}001+000000000D ${trans_date}${trans_time}${tran_amt}840+000000001.00AM5310${merch_post_code}840VVS             00                  ${merch_id}YY${ext_score1}00000000Y 00${portfolio}${client_id}      ${merch_nm}${merch_city}${merch_st}                                                                                                ${card_master_acct_nbr}                                                                                                                                                  ${recur_auth_exp_date}    ${card_status_date}             ||||


4959993999999955                            19 bytes $acct_nbr 
22603                                       9  bytes $card_post_code
1963                                        8  bytes $birth_date
20130628                                    8  bytes $trans_date
080000                                      6  bytes $trans_time
+000000010.01                               13 bytes $tran_amt
801110000                                   9  bytes $merch_post_code
    331234                                  10 bytes $merch_id
0025                                        4  bytes $ext_score1
FALCON TEST TR                              14 bytes $portfolio
PRCC95                                      14 bytes $client_id
MERCHANT #1234                              40 bytes $merch_nm
DENVER                                      30 bytes $merch_city
CO                                          3  bytes $merch_st
1111222233334444                            19 bytes $card_master_acct_nbr
20131231                                    8  bytes $recur_auth_exp_date
20131231                                    8  bytes $card_status_date

%06.2f,%08s,%06s,%03s


customerAccountNumber	40 bytes
pan						19 bytes
amt 					9 bytes
date 					8 bytes
time					6 bytes
milli					3 bytes
