#!/usr/bin/perl


################################################################################
# get_opt_data.pl
# get isam data and write to file "isam_data_" . $dstamp . ".dat"
# write out the following fields:
#  "$card_cus_nm|$card_cus_lst_nm|$card_client_id|$card_wrk_phon_2|$md5_checksum|$card_acct_id\n"
#
#
#
#
#
#
#
#
#
#
################################################################################



use strict;
use warnings;

use Digest::MD5 qw(md5_hex);
use File::Basename;
use Cwd;
use POSIX;

#--------------------------------------------------------------------------
#       Define Variables
#--------------------------------------------------------------------------
my @date   = (localtime);
my $dstamp = sprintf(
    "%4d%02d%02d%02d%02d%02d",
    $date[5] + 1900,
    $date[4] + 1,
    $date[3], $date[2], $date[1], $date[0]
);
my $rectime
    = sprintf( "%4d%02d%02d", $date[5] + 1900, $date[4] + 1, $date[3] );

# default configuration file unless other specified on the command line
use constant LCFG       => "/home/falcon/config";
use constant CONFIGFILE => LCFG . "/fal_adeptra_sms.cfg";

use constant {

    # record length of a card ISAM record
    RECLENGTH => 538,

    # ISAM header length
    HDLENGTH => 3228,

    # output file record length (including the newline)
    OUT_RECLENGTH => 118,

    # String values for different messages printed out by this script:
    ERROR_MSG => "ERROR:",
    INFO_MSG  => "INFO:",
    DEBUG_MSG => "DEBUG:",
    WARN_MSG  => "WARNING:"
};

# The following variables are defaults
my $TARGETDIR     = "/tmp";
my $PRCLIST_FILE  = LCFG . "/AdeptraSMSPRC.lst";

# The following are command line defaults
my $FAL_INST = "";
#my $DEBUG    = 0;
my $NO_FTP   = 0;

sub parse_config {
    open( CFG, CONFIGFILE )
        or warn "Cannot open configuration file : CONFIGFILE, $!\n"
        . "Using script defaults.\n" && return;
    while (<CFG>) {

        # Skip commented and blank lines
        if ( !m/^#/ && !m/^\s*$/ ) {
            my $buf .= $_;
            eval $buf;
            if ( !$@ ) {
                $buf = "";
            }
        }
    }
    close CFG;
}


sub usage {
    my $PROG = basename($0);
    print "Usage\n$PROG \n" . "$PROG [-f] [-d [1]] [-n] \n\n";
    print "-f Falcon instance    Required name of Falcon instance.\n"
        . "-d int        Debug level. None given, default (0). Debug level 1\n"
        . "          adds ability to run the script on any given day.\n\n"
        . "-n            Do no attempt to FTP the output files. Use for\n"
        . "          diagnostics purposes. \n\n";
}

# Print a stderr output seperator - for if/when stderr is captured into a log file
print STDERR "=" x 80, "\n$0: Date of run - $dstamp\n";

# Read the configuration file and set variables thereof
&parse_config;
my $FAL_INST = "p1dps";
$FAL_INST = lc($FAL_INST);
my $OUTFILE    = $TARGETDIR . "/isam_data_" . $dstamp . ".dat";
my $IN_FILEPATH = "/" . $FAL_INST . "isamdat/" . $FAL_INST . "card*.dat";
my @IN_FILES    = glob("$IN_FILEPATH");
my $TOTAL_FILES = @IN_FILES;

print "\$IN_FILEPATH = $IN_FILEPATH\n";
print "\$OUTFILE = $OUTFILE\n";
# Check for files
(@IN_FILES) || die usage();

# Change working directory to the target directory spec'd in the config file
chdir $TARGETDIR
    or die print STDERR "ERROR_MSG Cannot change to directory: "
    . "$TARGETDIR, $!\n";

# Create array of participating PRC values
open( PRCFILE, "$PRCLIST_FILE" );
my @PRCLIST = <PRCFILE>;

# Remove whitespace from PRC values
for (@PRCLIST) { s/\s+$//; }

open( OFD, ">$OUTFILE" )
    or die print STDERR "ERROR_MSG Cannot open output file: $OUTFILE, $!\n";
my $TOTAL_RECS  = 0;
my $FILE_NUMBER = 0;

foreach my $INFILE (@IN_FILES) {

    # Count of records for current file
    my $RECS       = 0;
    my $HEADER     = "";
    my $REC        = "";
    my $READ_C     = 0;
    my $NUMRECORDS = 0;
    my $NUMBYTES   = 0;

    open( IFD, "<$INFILE" )
        or die print STDERR
        "ERROR_MSG Cannot open input file $INFILE for rec count: $INFILE, $!\n";

    # Strip off the header
    read IFD, $HEADER, HDLENGTH;

COUNT: while ( $NUMBYTES = read IFD, $REC, RECLENGTH ) {
        $READ_C++;
        if ( substr( $REC, 0, 1 ) =~ /\xff/ ) { next COUNT; }
        $NUMRECORDS++;
    }
    close IFD;

    # If first or last file, output has an extra record
    if ( $FILE_NUMBER == 0 || $FILE_NUMBER == ( $TOTAL_FILES - 1 ) ) {
        $NUMRECORDS++;
    }


    open( IFD, "<$INFILE" )
        or die print STDERR "ERROR_MSG Cannot open input file: $INFILE, $!\n";

    # Strip off the ISAM header
    read IFD, $HEADER, HDLENGTH;

RECORD: while ( $NUMBYTES = read IFD, $REC, RECLENGTH ) {

        # If the first byte is hex delete
        if ( substr( $REC, 0, 1 ) =~ /\xff/ ) {
            next RECORD;
        }

        # If the card number is not a 'valid' card number, disregard it
        # and go on to the next card. A 'valid' card is an integer in
        # 11 to 19 positions
        my $CHECK_ACCOUNT = substr( $REC, 2, 19 );

        # Strip off leading spaces
        $CHECK_ACCOUNT =~ s/^ *//;
        if ( $CHECK_ACCOUNT !~ /^[0-9]{11,19}/ ) {
            next RECORD;
        }

        # Card number
        my $card_acct_id = substr( $REC, 2, 19 );

        # Strip off leading spaces, if any
        $card_acct_id =~ s/^ *//;

        # Make it a 19 character string
        $card_acct_id = sprintf( "%-19s", $card_acct_id );

        my $card_cus_nm     = substr( $REC, 21, 30 );
        my $card_cus_lst_nm = substr( $REC, 51, 25 );
        my $card_client_id = uc( substr( $REC, 417, 14 ) );
        my $card_wrk_phon_2 = substr( $REC, 285, 16 );

        # Generate unique key using MD5 checksum on PRC and acct number
        my $prc_uuid     = uc( md5_hex($card_client_id) );
        my $md5_checksum = uc( md5_hex( $prc_uuid . $card_acct_id ) );

        my $t_client_id = $card_client_id;
        $t_client_id =~ s/\s+$//;

        # If work phone is populated with non-blanks, write to output file
        if ( $card_wrk_phon_2 ne "                " && ( grep { $_ eq $t_client_id } @PRCLIST ) ) {
            $card_cus_nm =~ s/\s+//g;
            $card_cus_lst_nm =~ s/\s+//g;
            $card_client_id =~ s/\s+//g;
            $card_wrk_phon_2 =~ s/\s+//g;
            $md5_checksum =~ s/\s+//g;
            $card_acct_id =~ s/\s+//g;
            print OFD
                "$card_cus_nm|$card_cus_lst_nm|$card_client_id|$card_wrk_phon_2|$md5_checksum|$card_acct_id\n";
            $RECS++;
        }
    }

    $TOTAL_RECS += $RECS;
    close IFD;

    $FILE_NUMBER++;
}

close OFD;

exit 0;

