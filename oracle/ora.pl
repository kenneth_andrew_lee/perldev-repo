#!/usr/bin/perl
use strict;
use DBI;
my $db = DBI->connect( "dbi:Oracle:xe", "hr", "hr" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;
my $SEL = "select employee_id, first_name, last_name from employees";
my $sth = $db->prepare($SEL);
$sth->execute();
 
while ( my @row = $sth->fetchrow_array() ) {
    foreach (@row) {
        $_ = "\t" if !defined($_);
        print "$_\t";
    }
    print "\n";
}
 
END {
    $db->disconnect if defined($db);
}

