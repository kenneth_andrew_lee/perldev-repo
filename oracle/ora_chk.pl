#!/usr/bin/perl
#
#
# Sample Call
# perl ora_chk.pl 2015100100 201511010000
#
use strict;
use warnings;

use DBI;
use DBD::Oracle qw(:ora_types);

# Pull parameters off of command line
my ($start_date, $end_date) = @ARGV;
die "Need Start Date\n" if (not defined $start_date);
die "Need End Date\n" if (not defined $end_date);

# connection to the database
my $dbh = DBI->connect('DBI:Oracle:fhdbd','reporter','reporter') or die "Can't make database connect: $DBI::errstr\n";    
my $curref;
my $sth_cursor;
my $sth = $dbh -> prepare("begin falcon_esp_jobs.get_risk_factor_comments (:start_date_in, :end_date_in, :curref); end;");
$sth->bind_param(":start_date_in", $start_date);
$sth->bind_param(":end_date_in", $end_date);
$sth->bind_param_inout(":curref", \$sth_cursor, 0, {ora_type => ORA_RSET });
$sth->execute();

### Retrieve the returned rows of data
my @row;
while(@row = map { defined($_) ? $_ : ' ***N/A*** '} $sth_cursor->fetchrow_array()) {
	print "Row: @row\n";
}
warn "Data fetching terminated early by error: $DBI::errstr\n" if $DBI::err;


# Close Connection
$dbh->disconnect();
