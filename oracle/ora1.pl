#!/usr/bin/perl

#
# Set up tunnels to use this:
#
#    nohup trusted.tunnels.sh < /dev/null 2> /dev/null > /dev/null &
#

use strict;
use warnings;
use DBI;

my $dbh = DBI->connect(
    'dbi:Oracle:host=127.0.0.1;sid=XE;port=1521',
    'hr', 'hr', # Your credentials go here
    {
        RaiseError => 1,
        AutoCommit => 0,
    }
);

my $ary_ref = $dbh->selectall_arrayref("select employee_id, first_name, last_name from employees");

foreach my $row(@{$ary_ref})  {
    print defined $_ ? "\"$_\"" : 'NULL', ',' foreach @$row;
    print "\n";
}

$dbh->disconnect;

