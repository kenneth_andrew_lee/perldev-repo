#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my %hash=();
my $data=();
my @vars = ();
my @vals = ();

#Load Data and parse string into variables using regular expressions
@vars[0] = <<'END_MESSAGE';
Thirteen Reasons Why by Jay Asher (2011, Paperback)
Jay Asher
 Paperback, 2011
$1.48
END_MESSAGE

@vals = $vars[0] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";

@vars[1] = <<'END_MESSAGE';
The Shack : Where Tragedy Confronts Eternity by William Paul Young (2008, Paperback, Large Type)
William Paul Young
 Paperback, 2008
$0.99
END_MESSAGE

@vals = $vars[1] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";


@vars[2] = <<'END_MESSAGE';
Milk and Honey by Rupi Kaur (2015, Paperback)
rupi kaur
 Paperback, 2015
$6.90 (Save 53%)
END_MESSAGE

@vals = $vars[2] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";


@vars[3] = <<'END_MESSAGE';
Classic Seuss: Oh, the Places You\'ll Go! by Dr. Seuss (1990, Hardcover, Special)
Dr. Seuss
 Hardcover, 12FA660
$1.96
END_MESSAGE

@vals = $vars[3] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";


@vars[4] = <<'END_MESSAGE';
The Handmaid\'s Tale by Margaret Atwood (1998, Paperback)
Margaret Atwood
 Paperback, 1998
$5.26
END_MESSAGE

@vals = $vars[4] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\d+|(?:\w+)())\s*\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";


@vars[5] = <<'END_MESSAGE';
Publication Manual of the American Psychological Association: Publication Manual of the American Psychological Association by American Psychological Association Staff (2009, Paperback, 6th Edition)
American Psychological Association Staff
 Paperback, 2009
$7.99 (Save 72%)
END_MESSAGE

@vals = $vars[5] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";


@vars[6] = <<'END_MESSAGE';
The Declaration of Independence and the Constitution of the United States of America (2002, Paperback)

 
$68.13
END_MESSAGE

@vals = $vars[6] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)\r*\n(.*)\r*\n/;
print Dumper(@vals), "\n";




exit;



#my @vals = $_ =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\w+)(?:\r*\n)(.*)/
#\s*([^,]+),\s+(\w+)(?:\r*\n)(.*)/;
#my @vals = $_ =~ /^(.+)\r*\n(.*)\r*\n/;
#my @vals = $_ =~ /^(.+)\r*\n(\w*)/;
#my @vals = $_ =~ /^(.+)\r*\n(.*)\r*\n(.*)\r*\n/;
#\s*([^,]+),\s+(\w+)(?:\r*\n)
#my @vals = $_ =~ /(.*)\s+?([^,]+),\s+?(\w+)\s+?(.*)/;

#my @vals = $_ =~ /^(.+)\s+?(.*)\s+?  (?: ([^,]+),\s+?(\w+)|(\s+?) )  (\s+?)\s+?(.*)/;
#my @vals = $_ =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\r*\n(\w+)/;
#(\s+?)\s+?(.*)/;
#([^,]+),\s+?(\w+)|(\s+?)




#$hash{'1'}  = \('Thirteen Reasons Why by Jay Asher (2011, Paperback)', 'Jay Asher', 'Paperback', '2011', '$1.48');
#$hash{'2'}  = \('The Shack : Where Tragedy Confronts Eternity by William Paul Young (2008, Paperback, Large Type)','William Paul Young','Paperback','2008','$0.99');
#$hash{'3'}  = \('Milk and Honey by Rupi Kaur (2015, Paperback)','rupi kaur','Paperback','2015','$6.90 (Save 53%)');
#$hash{'4'}  = \('Classic Seuss: Oh, the Places You\'ll Go! by Dr. Seuss (1990, Hardcover, Special)','Dr. Seuss','Hardcover','12FA660','$1.96');
#$hash{'5'}  = \('The Handmaid\'s Tale by Margaret Atwood (1998, Paperback)','Margaret Atwood','Paperback','1998','$5.26');
#$hash{'6'}  = \('Publication Manual of the American Psychological Association: Publication Manual of the American Psychological Association by American Psychological Association Staff (2009, Paperback, 6th Edition)','American Psychological Association Staff','Paperback','2009','$7.99 (Save 72%)');
$hash{'10'} = \('Toltec Wisdom: The Four Agreements : A Practical Guide to Personal Freedom by Janet Mills and Don Miguel Ruiz (1997, Paperback)','Don Miguel Ruiz, Janet Mills','Paperback','1997','$0.75');
$hash{'11'} = \('Shattered : Inside Hillary Clinton\'s Doomed Campaign by Jonathan Allen and Amie Parnes (2017, Hardcover)','Jonathan Allen, Amie Parnes','Hardcover','2017','$11.00');
$hash{'12'} = \('Everything, Everything by Nicola Yoon (2017, Paperback)','Nicola Yoon','Paperback','2017','$6.67');
$hash{'13'} = \('Diagnostic and Statistical Manual of Mental Disorders - DSM-5 by American Psychiatric Association Staff and Kernberg (2013, Paperback, Revised)','American Psychiatric Association Staff, kernberg','Paperback','2013','$30.00');
$hash{'14'} = \('Astrophysics for People in a Hurry by Neil deGrasse Tyson (2017, Hardcover)','Neil DeGrasse Tyson','Hardcover','2017','$14.76 (Save 22%)');
$hash{'15'} = \('Thirteen Reasons Why by Jay Asher (2017, Paperback)','Jay Asher','Paperback','2017','$5.49');
$hash{'16'} = \('Hillbilly Elegy : A Memoir of a Family and Culture in Crisis by J. D. Vance (2016, Hardcover)','J. D. Vance','Hardcover','2016','$10.50 (Save 62%)');
$hash{'17'} = \('1984 Signet Classics by George Orwell','George Orwell','Paperback','1950','$1.74');
$hash{'20'} = \('Ben Comee : A Tale of Rogers\'s Rangers, 1758-59 by Michael Joseph Canavan (2010, Paperback)','Michael Joseph Canavan','Paperback','132B3A0','$19.83 (Save 28%)');




#print @{$hash{'1'}}, "\n";
#print @{$hash{'2'}}, "\n";

#print Dumper(%hash), "\n";
#for my $key (%hash) {
	#print $hash{$key}, "\n";
	#for my $vars in ($hash{$key}) {
		#for my $var in @($vars) {
		#	print "\$key:$key", "\$var:$var", "\n"; 
		#}
	#}
#}

exit(0);
