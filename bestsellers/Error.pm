package Error;

use Mail::Sendmail;
use Carp;

use strict;
use constant FAILURE => 0;
use constant SUCCESS => 1;

our $error;
#my $DN = 'prometheus';  # set this
my $DN = 'klsoftware.com';  # set this

sub new {
    my ($class, $recipients, $opts) = @_;

    unless(@$recipients){
        $error = 'No email recipients specified';
        return FAILURE;
    }

    $class = ref($class) || $class;
    my $self = bless {}, $class;
	$self->{opts} = $opts if ref $opts eq 'HASH';
	$self->{recipients} = $recipients;
    $self->_init;
    return $self;
}

sub _init {
    my $self = shift;

    $self->_verify_address;
	$self->reset;
}

sub reset {
	my $self = shift;

    @$self{qw+From Subject Message+} = ($self->{opts}{FROM}    || join('@',  $0, $ENV{HOSTNAME} || $DN),
                                        $self->{opts}{SUBJECT} || join('', 'Error Report (', $0, ') '),
                                        $self->{opts}{MESSAGE} || join(' ', $0, 'generated the following error(s):', "\n")
	);
	delete $self->{errors};
}

sub notify {
    my $self = shift;

	my $term = 1;
	if(@_ && ($_[$#_] eq 'NO_DIE')){
		$term = 0;
		pop @_;
	}

    $self->add_error(@_) if @_;
    return SUCCESS unless exists $self->{errors};
    $self->{Subject} .= ' ' . scalar(localtime);
	$self->{Message} .= delete $self->{errors};
    unless(sendmail(%$self)) {
        open(LOG, join('', '>', $0, '_', localtime, '.log'));
        print LOG 'Unable to email error notification: ', $Mail::Sendmail::error, "\n" x 2,
        'The following errors were encountered: ', $self->{Message};
        close LOG;
    }
	if($term){
		croak $0, ': fatal error encountered - email report sent. Error messages as follows: ', "\n", 
			$self->{Message}, "\n";
	}
	$self->reset;
}

sub add_error {
    my $self = shift;
    $self->{errors} .= join('', "\n\t", scalar localtime, ': ', @_) if @_;
}

sub _verify_address {
	my $self = shift;
    $_ = /@/ ? $_ : join('@', $_, $DN) for @{$self->{recipients}};
    $self->{To} = join(' ', @{$self->{recipients}});
}

sub err { return $error }

sub DESTROY {
    my $self = shift;
    $self->notify if $self->{errors};
}

SUCCESS;
