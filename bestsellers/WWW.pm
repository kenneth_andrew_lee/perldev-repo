package WWW;

# $Id: WWW.pm,v 1.12 2004/09/24 16:22:02 zthompso Exp $

require Exporter;
use LWP::UserAgent;
use HTTP::Cookies;
use HTML::TableExtract;
use HTML::Form;
use HTTP::Request::Common;

our @ISA = qw(Exporter);
our @EXPORT;
our @EXPORT_OK = qw(fetch_html extract_table extract_multitables get_form $wwwerr);
our %EXPORT_TAGS = ( STD => [qw=fetch_html
                                $wwwerr=],
                     ALL => [qw=fetch_html
                                extract_table
                                extract_multitables
                                get_form
                                $wwwerr=]
                    );

use strict;

use constant SUCCESS => 1;
use constant FAILURE => 0;
use constant PROXY   => ''; # <= May need to be updated!

our $VERSION = '1.0';

our $wwwerr;

{ # "Encapsulation" for our UserAgent. We will now only create one instance of
  # LWP::UserAgent which will also handle cookies by default.

    my $ua;

    # create one user agent per session; handle cookies by default
    sub _init_ua {
        $ua = LWP::UserAgent->new(requests_redirectable => [qw(GET POST)]);
        $ua->agent('Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040818 Firefox/0.9.3');
        $ua->cookie_jar(HTTP::Cookies->new);
#        $ua->proxy(http => PROXY)
    }

    sub _check_proxy {
        my $res = $ua->request(GET 'http://proxy_check');
        return $res->status_line =~ /^401 Unauthorized/ ? _add_proxy() : 0;
    }

    sub _add_proxy { $ua->proxy(http => PROXY) }

    # retrieve a url
    sub fetch_html {
        my ($request, %opts) = @_;

        my $type;
        unless($type = ref($request)){
            $wwwerr = 'Error: ' . $request . ' is not a reference';
            return FAILURE;
        }
        _init_ua() unless defined $ua;
        my $req;
		if($type eq 'ARRAY'){
			my $method = shift @$request;
			if($method eq 'GET'){ $req = GET @$request }
			elsif($method eq 'POST'){ $req = POST @$request }
		}
		else{ $req = $request }
		$req->authorization_basic(@{$opts{AUTH}}) if exists $opts{AUTH};
#        $req->proxy_authorization_basic(qw()) if $ua->proxy('http');

        my $attempts = $opts{TRIES} || 5;
        FETCH: {
            my $res = $ua->request($req);
            if($res->is_success) {
                return $opts{BASE} ? [$res->content, $res->base] : $res->content  }
            else { ## we'll try 5 times over the course of about 1.5 minutes
                if(--$attempts > 0){
                    sleep 5;
                    redo FETCH;
                }
                my $url = $type eq 'ARRAY' ? $request->[0] : $request->uri();
                $wwwerr = join('', 'Failed to retrieve ', $url, ': ', $res->status_line);
                return FAILURE;
            }
        }
    }
}

sub extract_table {
    my ($html, %opts) = @_;

    my $te = new HTML::TableExtract( %opts );
    $te->parse($html);
    my ($ts) = $te->table_states;
	unless($ts){
		my $opts_str;
		for my $k (sort keys %opts){
			$opts_str .= ', ' if $opts_str;
			$opts_str .= join(' => ', $k, $opts{$k});
		}
		$wwwerr = 'No table found in document using options: ' . $opts_str;
		return FAILURE;
	}
    return [$ts->rows];
}

sub extract_multitables {
    my ($html) = @_;

    my $te = new HTML::TableExtract();
    $te->parse($html);

    my @rows;
    for ($te->table_states) { push(@rows, ($_->rows)) }
	unless(@rows){
		$wwwerr = 'No tables found in document';
		return FAILURE;
	}

    return \@rows;
}

sub get_form {
    my ($url, $idx) = @_;

    return FAILURE unless my $html = fetch_html([GET => $url], BASE => 1);
    my @forms = HTML::Form->parse(@$html);
    unless(@forms){
        $wwwerr = 'No forms found at ' . $url;
        return FAILURE;
    }
    if(defined $idx){ # return a single form
        unless($idx < @forms){
            $wwwerr = join('', 'Max form index is ', $#forms, '. ', $idx, ' is out of bounds.');
            return FAILURE;
        }
        return $forms[$idx];
    }
    else { # return all forms
        return \@forms;
    }
}

SUCCESS;


__END__


=head1 NAME

Platts::WWW - Library of web routines

=head1 SYNOPSIS

    use Platts::WWW;

    fetch_html([GET => $url], %opts);
    extract_table($html, %opts);
    extract_multitables($html);
    get_form($url, $index);

=head1 DESCRIPTION

Platts::WWW provides a few subroutines to make processing web data
easier. Nothing is exported automatically and there are two tags the
module recognizes:

=over 4

=item *

B<:STD> - exports the most common subroutine C<fetch_html> as well as
the package error C<$wwwerr>.

=item *

B<:ALL> - gives you everything in package.

=back


All errors that generated will be available in the C<$Platts::WWW::wwwerr>
variable.

=head1 SUBROUTINES

=head2 fetch_html($request[, %options])

C<fetch_html> will retrieve the URL specified in the C<$request> and return
it in a string. The C<$request> can either be an actual HTTP::Request
object or simply an array reference with the I<method> and URL:

    use HTTP::Request;

    my $req = HTTP::Request->new(GET => $url);
    die $wwwerr unless my $html = fetch_html($request);

or simply

    die $wwwerr unless my $html = fetch_html([GET => $url]);

=head3 Options

C<fetch_html()> allows several options to be specified to accomodate
special functionality. These are passed in via C<%options>:

=over 4

=item *

B<TRIES> - explicitly specify the number of attempts that will be made
to retrieve the document (default = 5). Will sleep 5 seconds between each
attempt.

=item *

B<BASE> - request that the base URI, i.e. HTTP::Response::base,
be returned in addition to the document. This is useful for
C<get_form> which requires the base URI. Note that the return value
when C<BASE> is specified is an array reference of the form:

    [$html, $base_uri]

=item *

B<AUTH> - for sites that require basic authentication a username and password
can be specified in an array reference:

	[$user, $pass]

=back

Returns 0 on failure.

=head2 extract_table($html, %opts)

C<extract_table> is used to extract the data from an HTML table
using HTML::TableExtract. The return value is an array reference of
array references each of which is a row from the table:

    [
        [r1c1, r1c2, r1c3],
        [r2c1, r2c2, r2c3],
        .
        .
        .
        [ric1, ric2, ric3]
    ]

The C<%opts> are passed directly to the HTML::TableExtract object. For more
information on which options are available see the I<DESCRIPTION> section
of the HTML::TableExtract documentation.

A typical use of C<extract_table> is as follows:

    my %opts = (depth => 3, count => 4);
    die $wwwerr unless my $html = fetch_html([GET => $url]);
    die $wwwerr unless my $rows = extract_table($html, %opts);

    for my $row (@$rows){
        my @columns = @$row;
        ...process @columns here
    }

In this example we try to extract the fifth, i.e. count == 4,
an HTML table found in the fourth, i.e. depth == 3, nested layer
of the entire HTML page.

Returns 0 on failure.


=head2 extract_multitables( $html )


This subroutine is similar to C<extract_table> except that it returns
I<all> tables in the HTML page as if they were a single table and, thus,
does not recognize any options. The only difference is in the way it
is called:

    die $wwwerr unless my $rows = extract_multitable($html);


=head2 get_form($url[, $index])

C<get_form> uses HTML::Form to extract forms from HTML and returns
an HTML::Form object (see the doc on HTML::Form for what you can do
with the form object.)

Although forms can be posted by concatenating the option name/value
pairs directly into the url, there are several places using an HTML::Form
proves useful.

=over 4

item 1.

I<Extracting form parameters>. Sometimes it's tedious to wade through
the HTML looking for the form options. C<get_form> can be used to extract
them quickly:

    die $wwwerr unless my $html = fetch_html([GET => $url], BASE => 1);
    die $wwwerr unless my $form = get_form(@$html);
    $form->dump;

This will print name/value list of form parameters that you can scan
quickly.

=item 2.

I<"Hiding" unneeded, cumbersome, or otherwise troubling parameters>.
Some forms have hidden parameters that should simply remain hidden. For example, page long session keys
that the server uses to track the state of the client. If you are only
changing one or two parameters, e.g. a date, there's no need to hardcode
a session id or write a separate routine to parse it out. The form object
will parse and store it for you with out ever having to see it:

    die $wwwerr unless my $form = get_form(@$html);

    for my $day (@dates){
        $form->value(PostDate => $day);
        die $wwwerr unless my $html = fetch_html($form->click);
    }

Here the C<value> method of HTML::Form is used to set a date for the
file we wish to retrieve and C<click> creates an HTTP::Request from
the form that we send directly to C<fetch_html>.

=back

Returns 0 on failure

=head1 VERSION

Platts::WWW version 1.0

=head1 DEPENDENCIES

Perl v. 5.6+, LWP::UserAgent, HTTP::Cookies, HTML::TableExtract,
HTML::Form

=head1 AUTHOR

Zach Thompson zach_thompson@platts.com

=head1 COPYRIGHT

Platts, Inc. E<copy> 2002.  All rights reserved.





