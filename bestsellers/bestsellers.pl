#!/usr/bin/perl 

use DBI;
use lib '/data_collection/half.com';
use WWW qw':STD extract_table';
use Error;
#use Data::Dumper;


my @data;
my @email_list =  qw(perl perl@klsoftware.com); # first will go to the default domain in Error.pm  

my $posterr = Error->new(\@email_list); 

my ($base, $url) = qw(http://www.half.ebay.com /books-bestsellers);
do{ $url = process_link(join('', $base, $url)) }while($url);
check_ranks() if @data != 200;

## Leave following line uncommented to print the data without inserting
#print join('|', @$_), "\n" for @data;exit;

insert();
$posterr->notify();

sub process_link {
	my $url = shift;
	my ($pg) = $url =~ /(\d+)$/o;
	warn 'Processing page ', $pg || '1', "\n";
	my %opts = (depth => 1, count => 16);
	my $html = fetch_html([GET => $url]);
	$posterr->notify($wwwerr) unless $html;
	my ($next) = $html =~ m{<a href="([^"]+)">Next</a>}o;
	$next ||= 0;
	#goto WOW if $pg != 1; # a way to only process certain pages 

	##########################################################
	# Ucomment to exit; to check for depth/count
	#my $te = HTML::TableExtract->new;
   	#$te->parse($html);

   	# Examine all matching tables
   	#for my $ts ($te->tables) {
    #    print "Table (", join(',', $ts->coords), "):\n";
    #    for my $row ($ts->rows) {
    #    	print join(',', @$row), "\n";
    #    }
   	#}
	#exit;
	##########################################################


	my $rows = extract_table($html, %opts);
	
	$posterr->notify('Failed to extract table from ', $url, ': ', $wwwerr) unless $rows;
	my $i;
	for my $row (@$rows){
		next unless $row->[2];
		my ($r) = $row->[2] =~ /^(\d+)/o;
		my @vals = $row->[3] =~ /^(.+)\r*\n(.*)\r*\n\s*([^,]+),\s+(\d+)(?:\r*\n)+Buy:\s+\$([\.0-9]+)/o;
		unshift @vals, $r;
		#rank, description, author, binding, pub_year, price
		unless(@vals == 6){
			warn 'Expecting 6 values but extracted ', scalar(@vals), ': ', join('|', @vals), "\n";
			next;
		}
		my $isbn = get_isbn(@vals[0,1], \$html);
		if($isbn =~ /^Failed/o){
			$posterr->add_error(join(' ', @vals[0,1], $isbn));
			next;
		}
		push(@data, [@vals, $isbn]);
	}
	WOW: return $next;
}

sub get_isbn {
	my ($rank, $title, $html) = @_;

	$title =~ s/\W+/[^<]+/go;

	my ($full_page_link) = $$html =~ />$rank\..+?<a href="([^"]+)">$title/s;
	return 'Failed to extract ISBN link for ' . $desc unless $full_page_link; 
	my $full_page = fetch_html([GET => $full_page_link]) if $full_page_link;
	return $wwwerr unless $full_page;

	my $rows2 = extract_table($full_page, depth => 3, count => 3);
	return 'Failed to extract table from ' . $full_page_link . ': ' . $wwwerr unless $rows2;

	my ($isbn) = $rows2->[0][1] =~ /ISBN-10:\s+(\S+)/o;
	return 'Failed to extract ISBN for ' . $desc unless $isbn;

	$isbn =~ s/\W//go;
	return $isbn;
}

sub insert {
	
	eval {
		my $con = DBI->connect('dbi:mysql:db', 'bestseller', 'bestseller', { RaiseError => 1, PrintError => 0, AutoCommit => 1 });
		my @d = localtime;
		my $timestamp = join(".", $d[5]+1900, $d[4]+1, reverse @d[0..3]);
		my $ins = $con->prepare("insert into bestsellers (rank, description, author, binding, pub_year, price, isbn, capture_date)
			values(?,?,?,?,?,?,?,'$timestamp')");
		$ins->execute(@$_) for @data;
	};
	$posterr->notify('INSERT ERR: ', $@) if $@;
}

sub check_ranks {
	for(my ($i, $j) = (0,1);$i < @data;++$j){
		unless($data[$i][0] == $j){
			$posterr->add_error('Missing rank #', $j);
			next;
		}
		++$i;
	}
}
