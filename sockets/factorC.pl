#!/usr/local/bin/perl

use strict;
use IO::Socket;

my($server, $port, $n) = @ARGV;

my $client = new IO::Socket::INET Proto    => 'tcp',
                                  PeerAddr => $server,
                                  PeerPort => $port;

$client or die "Can't connect to $server:$port\n";

print $client "$n\n";
my $factors = <$client>;
print $factors;

__END__

=head1 NAME

factor - number factoring client

=head1 SYNOPSIS

B<factorC> I<host> I<port>

=head1 DESCRIPTION

B<factorC> is a number factoring client.
It connects to the server at I<host>:I<port>.

Once connected,
it sends a number from standard input to the server,
and prints the factors returned by the server
on standard output.

=head1 REQUIRES

=over 4

=item *

IO::Socket

=back

=head1 AUTHOR

Steven McDougall, swmcd@world.std.com

=head1 COPYRIGHT

Copyright 2000 by Steven McDougall. This program is free
software; you can redistribute it and/or modify it under the same
terms as Perl.
