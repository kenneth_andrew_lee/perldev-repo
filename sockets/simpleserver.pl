#!/usr/bin/perl

use strict;
use IO::Socket;
use Net::hostent;

my $server = new IO::Socket::INET Proto     => 'tcp',
                                  LocalPort => 51001,
                                  Listen    => SOMAXCONN;

$server or die "Can't create server\n";

print "Listening on ", $server->sockport, "\n";

for (;;)
{
    my $handler = $server->accept;
    $handler or die "accept: $!\n";
    
    my $peeraddr = $handler->peeraddr;
    my $hostinfo = gethostbyaddr($peeraddr);
    # printf "accept %s\n", $hostinfo->name || $handler->peerhost;

    my $message = <$handler>;
    printf "%s:%s\n", length($message), $message; # $hostinfo->name || $handler->peerhost;
    print $handler "@factors\n";
}
