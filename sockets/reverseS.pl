#!/usr/local/bin/perl

use strict;
use English;
use FileHandle;
use Socket;

my $proto = getprotobyname('tcp');
socket(SERVER, PF_INET, SOCK_STREAM, $proto) or die "socket: $!\n";
print "socket\n";

my $port     = 7200;
my $sockaddr = sockaddr_in($port, INADDR_ANY);
bind(SERVER, $sockaddr) or die "bind: $!\n";
print "bind\n";

listen(SERVER, SOMAXCONN) or die "listen: $!\n";
print "listen\n";

for (;;)
{
    my $sockaddr = accept(HANDLER, SERVER) or die "accept: $!\n";
    my($port, $ip_addr) = sockaddr_in($sockaddr);
    my $host = gethostbyaddr($ip_addr, AF_INET);
    print "accept $host:$port\n";

    Reverse();

    close HANDLER or die "close HANDLER: $!\n";
    print "close HANDLER\n";
}


sub Reverse
{
    autoflush HANDLER 1;

    while (<HANDLER>)
    {
  chomp;
  my $reverse = reverse $_;
  print HANDLER "$reverse\n";
    }
}

__END__

=head1 NAME

reverseS - line reverse server

=head1 SYNOPSIS

B<reverseS>

=head1 DESCRIPTION

B<reverseS> is a simple TCP server.
It listens on TCP port 7800 for incoming connections.

For each connection,
it reads lines from the client,
reverses the line,
and sends it back to the client.

=head1 REQUIRES

=over 4

=item *

English

=item *

FileHandle

=item *

Socket

=back

=head1 BUGS

=over 4

=item *

It can only handle one client at a time.

=item *

The only way to exit the server is to kill it, e.g. with ^C.

=item *

It is very silly.

=back

=head1 AUTHOR

Steven McDougall, swmcd@world.std.com

=head1 COPYRIGHT

Copyright 2000 by Steven McDougall. This program is free
software; you can redistribute it and/or modify it under the same
terms as Perl.

