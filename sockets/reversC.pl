#!/usr/local/bin/perl

use strict;
use English;
use FileHandle;
use Socket;

my($server, $port) = @ARGV;

my $proto      = getprotobyname('tcp');
socket(CLIENT, PF_INET, SOCK_STREAM, $proto) or die "socket: $!\n";
print "socket\n";

my $sockaddr = sockaddr_in(0, INADDR_ANY);
bind(CLIENT, $sockaddr) or die "bind: $!\n";
print "bind\n";

my $ip_addr  = inet_aton($server) or die "inet_aton: $!\n";
   $sockaddr = sockaddr_in($port, $ip_addr);
connect(CLIENT, $sockaddr) or die "connect: $!\n";
print "connect\n";

autoflush CLIENT 1;

while (<STDIN>)
{
    print CLIENT $_;
    my $reverse = <CLIENT>;
    print $reverse;
}

close CLIENT or die "close: $!\n";
print "close\n";

__END__

=head1 NAME

reverseC - line reverse client

=head1 SYNOPSIS

B<reverseC> I<host> I<port>

=head1 DESCRIPTION

B<reverseC> is a simple TCP client.
It connects to I<host>:I<port>.

Once connected,
it sends lines from standard input to the server,
and prints whatever comes back on standard output.

=head1 REQUIRES

=over 4

=item *

English

=item *

FileHandle

=item *

Socket

=back

=head1 AUTHOR

Steven McDougall, swmcd@world.std.com

=head1 COPYRIGHT

Copyright 2000 by Steven McDougall. This program is free
software; you can redistribute it and/or modify it under the same
terms as Perl.
