#!/usr/local/bin/perl

use strict;
use IO::Socket;
use Net::hostent;

my $server = new IO::Socket::INET Proto     => 'tcp',
                                  LocalPort => 7801,
                                  Listen    => SOMAXCONN;

$server or die "Can't create server\n";

print "Listening on ", $server->sockport, "\n";

for (;;)
{
    my $handler = $server->accept;
    $handler or die "accept: $!\n";
    
    my $peeraddr = $handler->peeraddr;
    my $hostinfo = gethostbyaddr($peeraddr);
    printf "accept %s\n", $hostinfo->name || $handler->peerhost;

    my $n = <$handler>;
    my @factors = Factor($n);
    print $handler "@factors\n";
}


sub Factor
{
    my $n = shift;

    my $sqrt = sqrt $n;
    for (my $f=2; $f<=$sqrt; $f++)
    {
  $n % $f and next;
  return $f, Factor($n / $f);
    }

    $n
}

=head1 NAME

factorS - number factoring server

=head1 SYNOPSIS

B<factorS>

=head1 DESCRIPTION

B<factorS> provides a number factoring service.
It listens on TCP port 7801 for incoming connections.

For each connection,
it reads numbers from the client,
factors them, 
and sends the list of factors back to the client.

=head1 REQUIRES

=over 4

=item *

IO::Socket

=item

Net::hostent

=back

=head1 BUGS

=over 4

=item *

It can only handle one client at a time.

=item *

The only way to exit the server is to kill it, e.g. with ^C.

=back

=head1 AUTHOR

Steven McDougall, swmcd@world.std.com

=head1 COPYRIGHT

Copyright 2000 by Steven McDougall. This program is free
software; you can redistribute it and/or modify it under the same
terms as Perl.


