use strict;
use warnings;

# Find the product of matrices
sub mat_mul {
  my ($mat1, $mat2) = @_;
  
  my $mat1_rows_length = $mat1->{rows_length};
  my $mat1_columns_length = $mat1->{columns_length};
  my $mat1_values = $mat1->{values};
  
  my $mat2_rows_length = $mat2->{rows_length};
  my $mat2_columns_length = $mat2->{columns_length};
  my $mat2_values = $mat2->{values};
  
  #Calculation of matrix product
  my $mat_out_values = [];
  for (my $row = 0; $row <$mat1_rows_length; $row ++) {
    for (my $col = 0; $col <$mat2_columns_length; $col ++) {
      for (my $incol = 0; $incol <$mat1_columns_length; $incol ++) {
        $mat_out_values->[$row + $col * $mat1_rows_length] += 
        $mat1_values->[$row + $incol * $mat1_rows_length] * 
        $mat2_values->[$incol + $col * $mat2_rows_length];
      }
    }
  }
  
  my $mat_out = {
    rows_length => $mat1_rows_length,
    columns_length => $mat2_columns_length,
    values => $mat_out_values,
  };;
  
  return $mat_out;
}

my $mat1 = {
  values => [3,6,9,3,10,6,10,9,13,1,1,4,8,5,15,11],
  rows_length => 4,
  columns_length => 4,
};;

my $mat2 = {
  values => [2,9,3,2,7,4,8,8,9,12,5,6,6,15,4,2],
  rows_length => 4,
  columns_length => 4,
};;



my $outputs_mul = mat_mul ($mat1, $mat2);

use Data::Dumper;
print Dumper $outputs_mul;

