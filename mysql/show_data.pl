#!/usr/bin/perl
use strict;
use warnings;

use DBI;

# Get list of 200 items from table that do not have JSON data.
my $db = DBI->connect( "dbi:mysql:db:192.168.2.21", "bestseller", "bestseller" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;



my $sql = "select pub_year, price, isbn, capture_date, description from bestsellers order by capture_date desc limit 10";
my $sth = $db->prepare($sql);
$sth->execute();

print "\nReturning Data Here\n";
while (my @row = $sth->fetchrow_array()) {
    foreach (@row) {
        print $_, "|";
    }
    print "\n";
    #print @row, "\n";
}

$db->disconnect if defined($db);
exit();

__END__


