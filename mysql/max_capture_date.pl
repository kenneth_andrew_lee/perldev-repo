#!/usr/bin/perl
use strict;
use warnings;

use DBI;

# Get list of 200 items from table that do not have JSON data.
my $db = DBI->connect( "dbi:mysql:db:192.168.2.21", "bestseller", "bestseller" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;
my $sql = "select max(capture_date) from bestsellers";
my $sth = $db->prepare($sql);
$sth->execute();

print "\nReturning Data Here\n";
while (my @row = $sth->fetchrow_array()) {
    print @row, "\n";
}

$db->disconnect if defined($db);
exit();

__END__


#How many records in isbn that are missing json about isbn.
select count(*) from isbn_list where v is null;

# How many missing isbns in isbn_list from bestsellers
select distinct isbn from bestsellers where isbn not in (select distinct k from isbn_list);


