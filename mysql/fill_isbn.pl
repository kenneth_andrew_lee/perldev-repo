#!/usr/bin/perl
use strict;
use warnings;

use LWP::Simple;                # From CPAN
use JSON;
use DBI;
use Data::Dumper;               # Perl core module

my %books =();
my $key;
my $value;

# Get list of 200 items from table that do not have JSON data.
my $db = DBI->connect( "dbi:mysql:db:192.168.2.65", "bestseller", "bestseller" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;
my $SEL = "select k, v from isbn_list where v is null limit 200";
my $sth = $db->prepare($SEL);
$sth->execute();

print "\nReturning Data Here\n";
my @row;
while (@row = $sth->fetchrow_array()) {
	$books{$row[0]} = (defined $row[1] ? $row[1] : "(undef)");
	#print "\$k = $row[0] and \$v = $books{$row[0]}", "\n";
}

my $bookurl = "http://isbndb.com/api/v2/json/1LFWBBIV/book/";
$sth = $db->prepare(qq{
	update isbn_list set v = ? where k = ?
});	
for (keys %books) {
	$bookurl = "http://isbndb.com/api/v2/json/1LFWBBIV/book/";
	$bookurl .= $_;
	print "\$bookurl = $bookurl", "\n";
	# 'get' is exported by LWP::Simple; install LWP from CPAN unless you have it.
	# You need it or something similar (HTTP::Tiny, maybe?) to get web pages.
	my $json = get($bookurl);
	die "Could not get $bookurl!" unless defined $json;
	#print "$_", ":", "$json", "\n";
	$sth->execute($json, $_);
}
$db->commit or die $db->errstr;

$db->disconnect if defined($db);
print "\nFinished!\n";
__END__


#How many records in isbn that are missing json about isbn.
select count(*) from isbn_list where v is null;

# How many missing isbns in isbn_list from bestsellers
select distinct isbn from bestsellers where isbn not in (select distinct k from isbn_list);

# fill missing isbn's
insert into isbn_list (k) (select distinct isbn from bestsellers where isbn not in (select distinct k from isbn_list));



#$SEL = "update isbn_list set v = :value where k = :key";
#$sth = $db->prepare($SEL);
	#$books{$_} = $json;


$sth = $dbh->prepare(qq{
    UPDATE people SET age = ? WHERE fullname = ?
  });
  $sth->execute(undef, "Joe Bloggs");



Returning Data Here
0007577311{
   "error" : "Unable to locate 0007577311"
}

Argument ":key" isn't numeric in subroutine entry at fill_isbn.pl line 44.
DBD::mysql::st bind_param failed: Illegal parameter number at fill_isbn.pl line 44.
DBD::mysql::st bind_param failed: Illegal parameter number at fill_isbn.pl line 44.
Issuing rollback() due to DESTROY without explicit disconnect() of DBD::mysql::db handle db:192.168.2.65 at fill_isbn.pl line 44.




if defined($row[1]) {
	$books{$row[0]} = $row[1];	
} else {
	$books{$row[0]} = "(undef)"
}




#while (@row = map { defined($_) ? $_ : ' ***N/A*** '} $sth->fetchrow_array() ) {
	#my $k = $row[0];
	#my $v = $row[1];
	#print "\$k = $k and \$v = $v", "\n";
#print "\n\nThis query returned " . $sth->rows . " rows.\n";
(defined $_ ? sprintf('%5.2f', $_) : "(undef)")


foreach (@row) {
$_ = "\t" if !defined($_);
print "$_\t";
}
print "\n";

### Retrieve the returned rows of data
my @row;
while(@row = map { defined($_) ? $_ : ' ***N/A*** '} $sth_cursor->fetchrow_array()) {
	print "Row: @row\n";
}
warn "Data fetching terminated early by error: $DBI::errstr\n" if $DBI::err;




# hash to store key we will search.  In XML, this would be the name of a branch or leaf, not sure what it is called in JSON.
my %title;
my %title1;


#my $wooturl = "http://api.woot.com/2/events.json?key=59b2672e52314d818728bab165c575b0";
#foreach (@sites) {
#	$wooturl .= "&site=" . $_
#}
#
#foreach (@eventTypes) {
#	$wooturl .= "&eventType=" . $_
#}
#
#print $wooturl, "\n\n";

my $wooturl = "http://isbndb.com/api/v2/json/1LFWBBIV/book/";
foreach (@sites) {
	$wooturl .= "&site=" . $_
}

foreach (@eventTypes) {
	$wooturl .= "&eventType=" . $_
}

$wooturl .= "&select=Title";


# 'get' is exported by LWP::Simple; install LWP from CPAN unless you have it.
# You need it or something similar (HTTP::Tiny, maybe?) to get web pages.
my $json = get($wooturl);
die "Could not get $wooturl!" unless defined $json;

print $json, "\n\n";

# Decode the entire JSON
my $data = decode_json($json);

#for my $report ( @{$data} ) {
foreach  my $report (@$data) {
	#print $report->{Title}, "\n";
	$title{$report->{Title}}++; 
}

# print distinct list
 while( my ($k, $v) = each %title ) {
        print "key: $k, value: $v.\n";
}

print "\n\n";

$wooturl = "http://api.woot.com/2/events.json?key=59b2672e52314d818728bab165c575b0";
$json = get($wooturl);
#print $json, "\n\n";
$data = decode_json($json);

foreach  my $report (@$data) {
	$title1{$report->{Title}}++; 
}

# print distinct list
 while( my ($k, $v) = each %title1 ) {
        print "key: $k, value: $v.\n";
}


