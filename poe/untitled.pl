#!/usr/bin/perl
# Load the modules we absolutely need
use warnings;
use strict;
use POE qw(Component::Server::TCP);

# Create and run the TCP server component.  It will accept connections, manage
# wheels and so forth.  POE""Component::Server::TCP is customizable through
# callbacks.  In its simplest usage, we only need to supply the function to
# handle input.
POE::Component::Server::TCP->new(
  Port        => 12345,
  ClientInput => \&client_input,
);
POE::Kernel->run();
exit;


# Finally we define the input handler
# Every client connection has been given its own POE::Session instance, so each 
# has its own heap to to store things in.  This simplifieds our code because
#their heaps track things for each connection, not us.  Eachs connections 
# ReadWrtie wheel is already placed into $heap->{client} for us.
sub client_input {
  my ($heap, $input) = @_[HEAP, ARG0];
  $heap->{client}->put($input);
}