#!/usr/bin/perl
use strict;
use warnings;

die "Usage: $0 data_files...\n" if ( @ARGV < 1);

# Input:
# February 12, 2011
#
# Output:
# YYYY-MM-DD
while (my $line = <>) {
	chomp $line;
	if ($line =~ /(\w+)\s+(\d{1,2}),\s+(\d{4})/) {
		my ($month, $day, $year) = ($1, $2, $3);
		my $monthval;
		if ($month eq "January") 		{ $monthval = 1; }
		elsif ($month eq "February")	{ $monthval = 2; }
		elsif ($month eq "March")		{ $monthval = 3; }
		elsif ($month eq "April")		{ $monthval = 4; }
		elsif ($month eq "May")			{ $monthval = 5; }
		elsif ($month eq "June")		{ $monthval = 6; }
		elsif ($month eq "July")		{ $monthval = 7; }
		elsif ($month eq "August")		{ $monthval = 8; }
		elsif ($month eq "September")	{ $monthval = 9; }
		elsif ($month eq "October")		{ $monthval = 10; }
		elsif ($month eq "November")	{ $monthval = 11; }
		elsif ($month eq "December")	{ $monthval = 12; }
		else {
			# print "'$line' not a valid date\n";  ## Show bad dates
			next;
	}

		printf "%04s-%02s-%02s\n", $year, $monthval, $day;
	} else {
		print "'$line' not a valid date\n";
	}
}
