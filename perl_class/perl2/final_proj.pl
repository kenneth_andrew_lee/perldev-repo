#!/usr/bin/perl
use strict;
use warnings;

my $order;
my @allow;
my @deny;
my %allow;
my %deny;

init(@ARGV);
run();

sub init {
    while (@_) {
        my $arg = shift;
        parse_file($arg);    
    }
    # Requirement: if no order, then die
    if (!defined $order) {
        die "Must have at least one order";
    }
    # fill allow array from hash
    for (keys %allow) {
        push @allow, $_;
    }
    # fill deny array from hash
    for (keys %deny) {
        push @deny, $_;
    }
}

sub run {
    for ( menu(); my $command = <STDIN>; menu() ) {
        chomp $command;
        act_on($command);
    }
}

sub menu {
    print "Input address to test: ";
}

sub act_on {
    my $command = shift;
    print "You entered: $command\n\n";
    #die "Invalid option" if $command !~ /\A\d+\z/ || !$options[$command];
    if ($command eq 'quit') {
        show_results();
        exit;
    }
}

sub show_results {
    print "\norder: $order\n";
    #for (@order) {
    #    print "$_\n";
    #}
    print "\nallow:\n";
    for (@allow) {
        print "$_\n";
    }
    print "\ndeny:\n";
    for (@deny) {
        print "$_\n";
    }
}

sub parse_file {
    my $filename = shift;
    my ( $column1, $column2, $column3 );
    if ( open my $fh, '<', $filename ) {
        while (<$fh>) {
            chomp;
            ( $column1, $column2, $column3 ) = split / /;
            if ($column1 eq 'order') {
                if ($column2 eq 'allow,deny' or $column2 eq 'deny,allow') {
                    if (defined $order) {
                        # print warning according to rule: More than one order 
                        # directive should generate a warning;
                        warn "More than one order\n";    
                    }
                    $order = $column2; # The last order is the one in effect
                } else {
                    warn "Invalid input for order: ";
                    print "\$column1: $column1  " if defined($column1);
                    print "\$column2: $column2  " if defined($column2);
                    print "\$column3: $column3  " if defined($column3);
                    print "\n";
                }
            }
            # work on filling allow and deny
            if ($column1 eq 'allow' and $column2 eq 'from' and defined($column3)) {
                # Add code to build hash of allows, and only push unique values onto the array for testing later.
                $allow{$column3} = $column3;
                #push @allow, $column3 ;
            }    
            
            # Original setup, does not have unique values.  Is that a must?  Not in the requrements.
            #push @deny,  $column3 if ($column1 eq 'deny'  and $column2 eq 'from' and defined($column3));
            if ($column1 eq 'deny' and $column2 eq 'from' and defined($column3)) {
                # Add code to build hash of denys, and only push unique values onto the array for testing later.
                $deny{$column3} = $column3;
                #push @allow, $column3 ;
            }    
        }
        # print "allow keys:\n";
        # print "$_\n" for keys %allow;
        #     
        # print "deny keys:\n";
        # print "$_\n" for keys %deny;
    }
}
## Order Rules #################################################################
# order = one of the following alternatives:
#   order = allow,deny
#   order = deny,allow
#   Anything other than those two alternatives should generate a warning. 
#   No spaces are allowed. 
#   More than one order directive should generate a warning; 
#   the last one will be the one that is in effect. If there is no order 
#   directive, the program should die with an error.
# 

## Address Rules ###############################################################
# address - one of the following alternatives:
#     A dotted IP address like n.n.n.n or a prefix thereof (an address that  
#     omits one or more of the components; for example, n.n.n). Example:  
#     10.41.123.45 is a full IP address; 10.41.123 is a prefix.
#     
#     A fully qualified host name like name.name.name or a suffix thereof. 
#     Example: www.oreilly.com is a fully qualified host name, oreilly.com is a 
#     suffix. But because host names can contain any number of periods (for 
#     example, mx.local.ca.cdc.gov), there is actually no distinction between a 
#     fully qualified name and a suffix. This will become clearer when you see 
#     the rest of the instructions.
# 
################################################################################
# Allow,Deny: First, all Allow directives are evaluated; at least one must 
# match, or the request is rejected. Next, all Deny directives are evaluated. 
# If any matches, the request is rejected. Last, any requests that do not match 
# an Allow or a Deny directive are denied by default.
# 
# Deny,Allow: First, all Deny directives are evaluated; if any match, the 
# request is denied unless it also matches an Allow directive. Any requests 
# that do not match any Allow or Deny directives are permitted.
# 
# So what constitutes a "match"? If the user enters a DNS name like 
# www.oreilly.com, that will match an allow or deny directive that contains 
# either www.oreilly.com, oreilly.com, or com. If the user enters a numeric 
# IP address like 10.41.123.24, that will match an allow or deny directive 
# that contains either 10.41.123.24, 10.41.123, 10.41, or 10.
#
################################################################################

# sub verify {
#     my ( $digest, $password ) = @_;
# 
#     return crypt( $password, $digest ) eq $digest;
# }
# 
# 
# sub write_file {
#     my ( $filename, %data ) = @_;
# 
#     open my $fh, '>', $filename
#         or die "Couldn't open $filename for writing: $!\n";
#     print {$fh} "$_:$data{$_}\n" for keys %data;
# }
