#!/usr/bin/perl
use strict;
use warnings;

#
# If Perl evaluates the $count{$word} on the right side of the = sign, and 
# that element does not yet exist in the hash, the result evaluates as the 
# undefined value (which has the name undef). In a numeric context (a subset 
# of scalar context, forced in this case by the + sign), undef is zero, and 
# so the expression becomes adding zero to one to get one, and assigning 
# that to $count{$word}. Normally, using undef in a numeric context also 
# results in a warning (if we have use warnings turned on), but in the 
# specific case where the numeric context occurs as a result of using the 
# ++ operator, that warning will not appear. (That design choice was made 
# specifically so that this coding idiom would work.)
#

my $text = <<'END_OF_TEXT';
Love, love me do.
You know I love you,
I'll always be true,
So please, love me do.
Whoa, love me do.

Love, love me do.
You know I love you,
I'll always be true,
So please, love me do.
Whoa, love me do.

Someone to love,
Somebody new.
Someone to love,
Someone like you.

Love, love me do.
You know I love you,
I'll always be true,
So please, love me do.
Whoa, love me do.

Love, love me do.
You know I love you,
I'll always be true,
So please, love me do.
Whoa, love me do.
Yeah, love me do.
Whoa, oh, love me do
END_OF_TEXT

my %count;
foreach my $line ( split "\n", $text) {
	foreach my $word ( split " ", $line) {
		$count{$word}++;	# shortcut for $count{word} = $count{$word} + 1
	}
}

print "'love' occurs $count{love} times\n";