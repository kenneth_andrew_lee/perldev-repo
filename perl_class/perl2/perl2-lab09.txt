Objective 1:

There is a file on your student machine (and all Unix/Linux machines) named /etc/passwd that contains information about the users of the machine. It consists of one line per user, with fields separated by colons, as follows:

    User name
    Unused ('x')
    User ID
    Group ID
    Full name
    Home directory
    Login shell

One line from this file will look like:

username:x:userid:groupid:fullname:homedir:shell

Create a one-liner to count and print the number of each shell occurring in that file, sorted in descending order by count.

When you are satisfied with your one-liner, create a text file named shells.sh like this, including your one-liner as shown:

#!/bin/sh
perl -one-liner code goes here

You can run this file from the shell to test it.

We'll ask for one-liners in this form for other projects. This enables the instructor to test your solution without needing to copy and paste it.

The result from the one-liner should look something like this:

/usr/bin/false: 48
/var/empty/usr/bin/false: 2
/bin/sh: 1
/usr/sbin/uucico: 1

solution:
perl -nle '($username,$unused,$userid,$groupid,$fullname,$homedir,$loginshell)=split(/:/) and $h{loginshell}++; END{print "$_: $h{$_}" foreach sort {$h{$b} <=> $h{$a}} keys %h} ' /etc/passwd


lab9Obj2.txt
Question 1:
perl -le 'print 42' works; perl -el 'print 42' does not. Why?
solution:
the -e command line option expects the next input to be the command to execute, not another parameter.

Question 2:
perl -le 'print 'Hello, Sailor!?'' does not work. Why? Which of the following Perl operators would let you get the intended effect:

    qq
    qw
    qx
    q
    qr

(Hint: perldoc perlop)