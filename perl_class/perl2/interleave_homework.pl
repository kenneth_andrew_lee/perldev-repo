#!/usr/bin/perl
use strict;
use warnings;

my @first = qw(Can unlock secret);
my @second = qw(you the code?);

my @mixed = interleave_words(@first, @second);
print "Result: @mixed\n";

sub interleave_words {
	my $counter = @_;
	foreach my $index (0..$counter/2-1) {  
		splice(@_,$index*2+1, 0,splice(@_,$counter/2+$index,1));
	}
	return @_;
}


# @LIST = splice(@ARRAY, OFFSET, LENGTH, @REPLACE_WITH); 
# counter = number of items in array, must be even
# Number of moves = counter/2 -1  (Half the counter - 1)
# index value starts at 0 and goes to number of moves - 1
# Position that gets moved = Starts at Half the counter, then increments by 1
# Position that is replaced = odd numbers 1,3,5,... = Index * 2 + 1