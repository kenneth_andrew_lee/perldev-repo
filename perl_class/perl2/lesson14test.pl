#!/usr/bin/perl
use strict;
use warnings;

my %test = (one => 1, 
			two => 2, 
			three => 3, 
			four => 4, 
			five => 5,
			six => 6 
);

print %test,"\n\n";

show_results(%test);

sub show_results {
	my $counter = 0;
	print @_, "\n\n";
	while (@_) {
		$counter++;
		#if ($counter == 6) {return;}
		my $a = shift;
		my $b = shift;
		print "key = ", $a, " value = ", $b, " \@_ = ", @_, " size of \@_ = ",scalar (@_), "\n";
	}
	print "Exited while loop!\n";
}
