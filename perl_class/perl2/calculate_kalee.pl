#!/usr/bin/perl
use strict;
use warnings;

my $INVALID = "Invalid statement\n";
my $Accumulator;

while (print(" ") && defined(my $line = <>)) {
	chomp $line;
	my $ok;
	($ok, $Accumulator) = execute($line,$Accumulator);
	if ($ok){
		print "OK\n";
	} else {
		warn $INVALID;
	}
}

sub execute {
	my ($line, $current) = @_;
	return if length $line == 0;

	my $operator_length = index $line, ' ';

	return monodic($line, $current) if $operator_length < 0;
}

sub monadic {
	my ($operator, $current) = @_;
}