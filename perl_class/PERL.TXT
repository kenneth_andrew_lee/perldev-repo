perl2_lesson2-Files
################################################################################
Filehandles

Think about what it takes to read from a file. Every time you ask for more input, Perl has to know where it left off. Perl also has to remember other, more esoteric, information to do with things like buffers and such. You don't want to bother with all of that; you just want to get input. The way Perl remembers everything that it has to know, without bothering us with it, is to hide it all behind something called a filehandle. A filehandle looks like a scalar, but you can't do the usual scalar-ish kinds of things with it (well, you can try, but the results won't be helpful); you can only use it in file reading operations.

boolean = open filehandle, mode, filename

We can do one more thing to make this program more concise. The close() function is the converse of open() (read the perldoc -f documentation on it if you like). As its name suggests, close() causes the file to close; you can no longer use the filehandle without getting an error message. In particular, all pending output to files opened for writing is flushed. (If you look at an output file as it's being written by another program running at the same time, you probably won't see the output show up until some time after the program has printed it. It's buffered by Perl or the operating system and written in chunks to improve efficiency.)

If the lexical variable holding the filehandle is destroyed, Perl calls close() right then and we don't need to call it explicitly. (After all, if the filehandle is no longer available to your program, you can't do anything more with it.) We can cause a lexical variable to be destroyed by limiting its scope. This is a good use for a naked block. Modify make_datafile.pl as shown:

#!/usr/bin/perl
use strict;
use warnings;

my $DATA_FILE = 'songs.data';

{
  open my $fh, '>', $DATA_FILE or die "Couldn't open $DATA_FILE: $!\n";
  print {$fh} <<'EOF';
2'02" Sgt. Pepper's Lonely Hearts Club Band
2'44" With A Little Help From My Friends
3'29" Lucy In The Sky With Diamonds
2'48" Getting Better
2'37 Fixing A Hole
3'35" She's Leaving Home
EOF
}
# close $fh;  ## No longer used, because it is out of scope and already closed!

open() modes

You've seen that to open a filehandle to read from, you specify a mode of '<'; and that to open a filehandle to write to, you specify a mode of '>'. There are many other possible modes, and they are detailed in the perldoc -f open documentation, but be forewarned—most of them are hard to understand and harder still to use correctly. One of the more accessible modes is append, which is denoted by '>>'. If the file does not already exist, it will be created; if it already exists, the output will be added to the end of its current contents.

#!/usr/bin/perl
use strict;
use warnings;

my $DATA_FILE = 'songs.data';

{
  open my $fh, '>>', $DATA_FILE or die "Couldn't open $DATA_FILE: $!\n";
  print {$fh} <<'EOF';
2'02" Sgt. Pepper's Lonely Hearts Club Band
2'44" With A Little Help From My Friends
3'29" Lucy In The Sky With Diamonds
2'48" Getting Better
2'37" Fixing A Hole
3'35" She's Leaving Home
EOF
}


################################################################################
regex metacharacters
* ( ) + [ ] { } | . ? ^

The i modifier tells the regex engine to ignore the case of letters in the regex

Character Classes
/...[abc].../
Match 'a' or 'b' or 'c'

Instead of You can type
[0-9] 			\d
[^0-9] 			\D
[a-zA-Z0-9_] 	\w
[^a-zA-Z0-9_] 	\W
[ \n\t\f\r\v] 	\s
[^ \n\t\f\r\v] 	\S
[^\n] 			.

\d matches any digit.
\D matches any character that is not a digit.
\w stands for "word character"; it matches any upper- or lower-case letter, digit, or underscore.
\W matches any character that is not a "word" character.
\s matches any character that is "white space." The escape sequences after the space character in the character class [ \n\t\f\r] stand for newline, tab (you know those already), form feed, and carriage return. Those are some odd characters, but they all have the distinction of not putting any actual ink on a page or pixels on a screen. You will use this shortcut a lot.
\S matches any character that is not white space. (That's backslash capital S.) . is a special shortcut that matches any character except for a newline. That one is used so often in regular expressions that it was worthy of being only one character long. (Remember in the last lesson that the "period" was one of the special characters in a regex? Now you know why. If you want to match a literal period, backslash it.)

Substitution
s/regex/replacement/

Optional presence: You are matching a word that may or may not be present in the plural form by the addition of the letter 's' (example inputs: 'apple' and 'apples'). Or: You are matching a number that may or may not have a decimal point in the middle (examples: 3.14159 and 42).
Zero or more: You are matching a number again, and after the decimal point there may be any number of digits, including zero
(examples: 17. and 101.7). Or: You are picking apart text containing mathematical expressions and there may or may not be white space characters surrounding binary operators like = and +; in fact, it could be any amount of white space, or none at all (examples: E = m * c**2 and s=d*t**2/2).
One or more: You are matching a number again. This one has at least one digit before the decimal point, possibly more (for example, 2.71828 and 98.4). Or: You are matching a word; it contains one or more letters, but you see no need to go to the Guinness Book of Records to find out the length of the longest word in existence just so you can limit the range of the match; you know that the input is well-behaved enough that "infinite" will do as a maximum length specification.
Exact number: You're matching an airline ticket locator code like we did in the last lesson. Rather than repeat the specification for each character 6 times, you want to say, "Match this (character specification) 6 times." Or: You have a report containing FedEx tracking numbers that are exactly 22 digits long.
Length range: You're creating a crossword puzzle and you want to select words from the dictionary that are between 3 and 10 letters long. Or: You are matching an IP address (version 4) and each octet is a decimal number one to three digits long (examples: 63.171.219.89 and 208.201.239.101).

Quantifier 	Interpretation 								Example 							Meaning
? 			Optional (zero or one)						/apples? and bananas?/				Match 'apple and banana' or 'apples and banana'or 'apple and bananas' or 'apples and bananas'.
* 			Zero or more 								/\d\.\d*/ 							Match a single digit, a period, and then zero or more digits.
+ 			One or more 								/\w+/ 								Match one or more consecutive "word" characters.
{n} 		Exact match (n times) 						/[A-HJ-NP-Z2-9]{6}/ 				Same as the airline locator code regex in the previous lesson.
{m,n}		Range match (from m to n times inclusive)	/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/Match an IPV4 address.
{,n}		Range match (from 0 to number times)		/,\d{,6},/							A comma, optionally followed by a number up to 999999, followed by another comma.
{m,}		Range match (m or more times)				/\n{2,}/ 							Require a gap of at least two blank lines.

Anchors
word boundary, expressed with the sequence \b
Match beginning of string: \A. This sequence matches the place that is the beginning of the string. Whatever comes next in the regex will only match successfully if it is at the beginning of the string. \A should always be at the beginning of a regex (or at the beginning of an alternation group, which we'll get into shortly) to be meaningful.
Match ending of string: \z. Yes, that's a little z. This sequence matches the place that is the ending of the string. Whatever comes before it in the regex will only match successfully if it is at the ending of the string. \z should always be at the end of a regex (or at the end of an alternation group) to be meaningful.

Captures
Place parentheses around the part of the regex that has the match you want to identify. If the match succeeds, the part of the input that matched the part of the regex in parentheses will be placed in the special variable $1.

**** It's a common mistake to perform a regex match and then use the numbered variables without checking to see if the match was successful. If you do that and the regex match was not successful, but you use $1 anyway, its value will be whatever it had from the last successful match with capturing parentheses. This will cause chaos in your programs. Always check to see whether a match succeeded (using any of the three forms of conditional statements you have learned) before using $1 or other numbered variables.

Noncapturing Parentheses
(?:regex)


my $text = "I HAVE THREE APPLES AND FOUR ORANGES"; 
$text =~ /24\/5\/1819/i and print "Found Queen Victoria's birthday\n";
Do you see how using the default delimiters of // requires escaping slashes inside of the regular expression, which makes it harder to read? We call that leaning toothpick syndrome

./reg_literal3.pl
################################################################################
#!/usr/bin/perl
use strict;
use warnings;

foreach my $text ("Stoplight", "Red Light", "Grean means go") {
	print qq{"$text" };
	if ($text =~ /sto/i && $text =~ /top/) {
		print "matches AND clause\n";
	} elsif ($text =~ /light/ || $text =~ /light/i) {
		print "matches OR clause\n";
	} else {
		print "matches neither clause\n";
	}
}
################################################################################

./reg_grep.pl
################################################################################
#!/usr/bin/perl
use strict;
use warnings;

my $regex = shift;

while ( $_ = <> ) {
	print if $_ =~ /$regex/;
}
################################################################################

./reg_grep.pl light ./reg_literal3.pl
cold1:~/perl2$ ./reg_grep.pl light ./reg_literal3.pl
foreach my $text ("Stoplight", "Red Light", "Grean means go") {
} elsif ($text =~ /light/ || $text =~ /light/i) {
cold1:~/perl2$ 

Reading lines: You know about the <> ("readline") operator for getting lines from standard input, and how to use it in scalar and list contexts:
while ( defined( my $line = <> ) ) {
	chomp $line;
	if ( substr $line, 0 eq '<' ) {
		push @old_file_lines, $line;
	} elsif ( substr $line, 0 eq '>' ) {
		push @new_file_lines, $line;
	}  
}

################################################################################
#!/usr/bin/perl
use strict;
use warnings;

# Shift off any arguments in @ARGV that begin with minus signs, and any values they take
while ( defined( my $line = <> ) ) {
	# Process a line of input, which is now in $line
}
################################################################################


The array @ARGV is special to Perl, so you don't declare it with my—it doesn't belong to you, it belongs to Perl.

One more thing—remember how shift() inside a subroutine defaults to shifting @_, the array of subroutine arguments? Well, outside of subroutines, array shift() defaults to @ARGV! (By the way, if you're a C programmer and you're wondering what happened to the name of the program, it's in $0.)

if ( @ARGV > 2 && shift eq "-s" ) {
	
}


################################################################################

Perl One Liners
The -e Flag
Now let's see how we can execute code from the command line. This is done with the -e (execute) option. Type the code below in the Unix window as shown:
cold:~$ perl -e 'print "Hello, World!\n"'
Hello, world!cold:~$ 

See? No problem at all! And we added the newline (\n) just as we would in a program. But Perl supplies a handy option to save us from typing \n at the end of a print statement. It's the -l (that's a lowercase l for "line feed") option. Type the code below in the Unix window as shown:


cold:~$ perl -le 'print "Hello, world!"'                                                                                                                                          
Hello, world!
cold:~$ 

cold:~$ perl -le 'print 1/sqrt(1 - 0.9**2)' # Relativistic time dilation factor at nine-tenths the speed of light                                                                 
2.29415733870562
cold:~$ 

cold:~$ # Relativistic time dilation factor at 100,000 km/h:                                                                                                                      
cold:~$ perl -le '$c = 3E8; $v = 100_000*1E3/3600; print 1/sqrt(1 - ($v/$c) **2)'
1.00000000428669
cold:~$ 

*** Note: You can leave out use strict and use warnings and my from one-liners, but not from programs. You'll encounter programs written by other folks who have left them out, which causes all kinds of problems. Don't be that programmer.

The -n Flag

I think you're ready to experience the real power of one-liners. The perl program can take some options that cause it to behave as if code is present, when it actually isn't. For instance, while (<>) is "magic" code. When it is inside Perl, while (<>) expands to about a dozen lines that shift filenames off @ARGV, open them, read a line into $_, and then test it to see if the line is defined.

The -n flag is "magic" also. It causes Perl to assume that the code you provide is surrounded by a loop reading from inputs. If you type perl -n -e 'code', it is roughly equivalent to this program:
while ( <> )
{
  code
}

Now wait just a minute! Why did that produce the same output? If the -l flag causes print() to add a newline to the end of what it prints, shouldn't we see a blank line between every line of output?

Let's back up a minute and think about why the output looked right when we had just the -n and -e flags. Perl read each line into $_ and—if it matched the regex—printed it. The readline operator left the trailing \n in $_ and print() printed it.

So why isn't there an extra newline with -l? Because in addition to attaching the newline to the end of print() statements, the -l flag performs an implicit chomp on lines read in through the magic <> readline operator.

That feature of -l may not have made a difference in this example, but it will come in handy often in the future, so when we use the -n flag, we'll also use the -l flag. You'll use the -nle flag combination often.



stock_report:
Item    Quantity        Unit Price
Direhorse Reins         21   199
Ikran Saddle    12      341
Thanator Shield 41      99
Fan Lizard Reflectors   350     15
Sturmbeest Corral       1       32000


cold:~/perl2$ perl -nle '/(.*)\t(\d+)\t(\d+)/ and print "$1: ", $2*$3' ./stock_report
Direhorse Reins: 4179
Ikran Saddle: 4092
Thanator Shield: 4059
Fan Lizard Reflectors: 5250
Sturmbeest Corral: 32000
cold:~/perl2$ 

BEGIN and END blocks

Suppose instead of printing the total value of each item in inventory, we want to print the total value of all items in inventory. We would still use -n to loop over all lines of input, adding the value to a running total; we'd also want to print the total. But how can we do that when all the code we put in the -e argument is executed inside the implicit while block generated by -n?

The answer lies in a special capability of Perl called the END block. The END block declares a special subroutine (but without the word sub) that Perl will execute on your behalf when all other code has finished and the program is about to exit. Run the one-liner below in the Unix window as shown:

cold:~/perl2$ perl -nle '/\t(\d+)\t(\d+)/ and $total += $1*$2; END{ print "Total: $total" }' ./stock_report                                                                          
Total: 45401
cold:~/perl2$ 

END{ print "$_: $h{$_}" foreach sort keys %h }
END{ print "$_: $h{$_}" foreach sort { $h{$a} cmp $h{$b} } keys %h }
END{ print "$_: $h{$_}" foreach sort { $a <=> $b } keys %h }
END{ print "$_: $h{$_}" foreach sort { $h{$b} cmp $h{$a} } keys %h }
END{ print "$_: $h{$_}" foreach sort { $h{$a} <=> $h{$b} } keys %h }
END{ print "$_: $h{$_}" foreach sort { $h{$b} <=> $h{$a} } keys %h }
END{ print "$_: $h{$_}" foreach keys %h }


cold:~/perl2$ perl -nle '/ (\d.*)/ and $h{$1}++; END{ print "$_: $h{$_}" foreach sort { $h{$b} <=> $h{$a} } keys %h }' ./connections | head -5
cold:~/perl2$ 

while ( <> ) {
  / (\d.*)/ and $h{$1}++;
}

foreach ( sort { $h{$b} <=> $h{$a} } keys %h ) {
  print "$_: $h{$_}";
}

Let's look at the regex first. Remember that spaces match literally inside regexes, so that leading space (between the first "/" and the "(") forces the parentheses to capture after the end of the first field (the timestamp); the \d at the start of the capture group means the heading line ("Time Address") won't match. We're matching a space followed by a digit and then as many more characters as exist in the line (.*).

The parentheses around \d.* mean that the input that matches that part (the IP address) will be saved in $1. We use that as a key in a hash %h—we have not declared that hash; again, you don't need to declare variables in one-liners (there's no use strict statement).

We postincrement the hash value $h{$1} with ++. Perl will create each hash entry the first time we reference it, so the first time we capture any particular address in $1, this statement will attempt to increment the corresponding hash entry, find that it's not in the hash, automatically create it with a value of undef, and then increment it, turning the value into 1. This all occurs inside the while loop (which is explicit in the equivalent program but implicit in the one-liner).

Then, at the end of the program, we loop ov
er all of the keys of the hash, sorted by descending numerical order of the corresponding value (the number of times each IP address was seen), printing out the IP address and the number of times we saw it.


Just so you understand that there's nothing sacred about the name %h, try it again with a hash %d:

cold:~/perl2$ perl -nle '/ (\d.*)/ and $d{$1}++; END{ print "$_: $d{$_}" foreach sort { $d{$b} <=> $d{$a} } keys %d }' ./connections | head -5
cold:~/perl2$ 


ls | perl -nle '/(Lesson)(\d)/ and rename $_, "${1}0$2"'


The ls command produces a list of every file in the current directory (I could do this from within Perl, but this is a simpler approach), and because stdout is a pipe, it will have one file per line. Perl's -n flag processes that list one file at a time, and the -l flag autochomps each line. The match operator looks for files of the form Lesson followed by a digit (there might be other files in the directory), and when it matches, renames the file to the same name with a zero inserted before the digit. Be aware that this would not have worked if I had executed it after creating the Lesson10 directory. (Why? And how would you fix it?)

Oh, and I snuck something new in there (more is more!). I needed to construct a string that contained the variable $1 followed by the digit 0 and then the variable $2. But if I just typed "$10$2", Perl would think I was referring to the variable $10. Of course, I could use the concatenation operator to get around this: $1 . '0' . $2. But instead I used an alternative syntax for Perl variable names that tells Perl exactly where the variable name begins and ends—with the curly brackets. So, you can write strings like "I have $count ${fruit}s" to get a result like "I have 4 apples".

################################################################################
Implicit -p code
while (<>) {
	#code
	print;
}

################################################################################
The -w Flag
If you get ambitious with the -e flag, you'll appreciate the benefits of turning on warnings. Instead of having to type use warnings at the beginning of the -e argument, you can include -w in the list of perl flags.

cold:~$ perl -MSocket -wle 'print inet_ntoa scalar gethostbyname "www.perl.org"'
cold:~$ perl -MSocket -wle 'print inet_ntoa scalar gethostbyname "www.perl.orc"'
Use of uninitialized value in subroutine entry at -e line 1.
Bad arg length for Socket::inet_ntoa, length is 0, should be 4 at -e line 1.
cold:~$
This tells us that we should check the return from gethostbyname before deciding whether to pass it to inet_ntoa. This is in agreement with the documentation. So we come up with this version:

The -Mstrict Flag
So you've seen that -w invokes use warnings in a one-liner; now what about use strict? There isn't a single letter flag that gives you the same capability. But you can use the -M flag, which is equivalent to use, and pass strict as an argument, giving you this:
perl -Mstrict ...

The -c Flag
Sometimes you want to find out whether a program compiles correctly without actually running it. We can do that. This example illustrates how fast one error can cascade into many. Start by tackling the first error from compiling or running a program, fix it, then compile or run again. Later errors may be caused by the first one. In this case, Perl identifies the problem on the first line.

Rerun the perl -c command. Now the syntax is okay, but perl doesn't run the program. There might still be errors in the code that are only found at runtime. If the program includes a use strict directive, -c will cause the program to be compiled with strictness checking enabled; there is no need to add -Mstrict.

The Trinary Operator
condition ? true_result : false_result

#!/usr/bin/perl
use strict;
use warnings;
foreach my $text ( qw(Matt bats the ball at Atticus) ) {
	print qq{"$text" }, $text =~ /at/ ? "matches" : "does not match", " /at/\n";
}

cold:~/perl2$ perl -MSocket -wle '$packed_addr = gethostbyname shift; print defined $packed_addr ? inet_ntoa $packed
unknown host

cold:~/perl2$ perl -MSocket -wle '$packed_addr = gethostbyname shift; print defined $packed_addr ? inet_ntoa $packed
207.171.7.63

The Trinary Operator as Lvalue
#!/usr/bin/perl
use strict;
use warnings;
my ($schnauzer, $mackerel) = (7, 12);
my $species = 'fish';
my %current_count = ( mammal => 1001, fish => 7269 );
if ( $species eq 'mammal' )
{
$schnauzer += $current_count{$species};
}
else
{
$mackerel += $current_count{$species};
}
print "$schnauzer, $mackerel\n";

#!/usr/bin/perl
use strict;
use warnings;
my ($schnauzer, $mackerel) = (7, 12);
my $species = 'fish';
my %current_count = ( mammal => 1001, fish => 7269 );
( $species eq 'mammal' ? $schnauzer : $mackerel ) += $current_count{$species};
print "$schnauzer, $mackerel\n";

The Scalar Range Operator
@small_integers = 1 .. 99; # Numbers 1 thru 99 inclusive

#!/usr/bin/perl
use strict;
use warnings;
while ( <DATA> )
{
if ( /\A\Z/ .. /\A\.\Z/ )
{
print "Body line: $_";
}
else
{
print "Other line: $_";
}
}
__END__
From: Peter Scott <peter@psdt.com>
To: Scott Gray <scott@oreilly.com>
Subject: Perl 2, Lesson 11
Hi Scott. I just uploaded Lesson 11 of the
Intermediate Perl series. The students are
really going to love the new one-liner capabilities!
.
Junk that's not part of the message...

##numbering.pl
#!/usr/bin/perl
use strict;
use warnings;
while ( <DATA> )
{
print if ( 3 .. 5 ) || ( 10 .. 11 );
}
__END__
Dear Sir,
I received your request for a reference for Mr. Ronald Smith,
who is interviewing with your company and hoping to obtain
employment. I am quite happy to report that
he performed his duties with exemplary conduct during his
entire tenure at our firm and we were eminently satisfied
with his product.
Whenever we observed him creating computer programs,
it was a pleasure to see him go; I cannot find the
words to describe how disgusted we were by his output
of a letter of resignation.

***** Perl2-Lesson 11
String Escapes.  Complete list found in perldoc perlop
\r Carriage return (more on this in the next lesson)
\f Form feed (hardly useful any more)
\a Alarm (ring the bell)
\e The escape character (ESC)
\b Backspace
\nnn Octal character nnn (each n is a digit from 0 through 7)
\xnn Hexadecimal character nn (each n is a digit from 0 through F, uppercase or lowercase)
\cc Control character c (e.g., \cG is Control-G)

\l: Lowercase next character.
\L: Lowercase all characters until end of string or the sequence \E.
\u: Uppercase next character.
\U: Uppercase all characters until end of string or the sequence \E.
\Q: Quote all regex metacharacters so they match only literally until end of string or the sequence \E.
Escape sequences that don't have another meaning inside a regular expression can be used in a regex. So, while \b can't be used to mean backspace in a regex (it's an anchor), \0nn and others can. Let's see how that works. Type the code below in the Unix shell as shown:
perl -le 'print "Ding" if "\a" =~ /\cG/'

perl2_lesson14 - Directories
################################################################################
opendir, readdir, closedir

In the Unix filesystem, a directory is just another file, albeit one that is treated differently and is it is hard for you to read it directly like a file. The functions opendir, readdir, and closedir are used for reading directory files; they have a lot in common with the normal file access functions of open, readline, and close. Perl provides the directory functions even on non-Unix systems, so you don't need to worry about cross-platform compatibility. Here's how they work:

    opendir creates a directory handle, like an input filehandle, except that you can only use it for calling the next two functions. If the directory cannot be read for any reason, opendir returns false and sets $! (similar to open).
    readdir takes a directory handle as argument and, in a scalar context, acts as an iterator, returning the next filename from the directory until there are none left, at which point it returns undef (just like readline). In a list context, it returns all the remaining filenames in the directory.
    closedir tells Perl that you are done using the directory handle that you pass to this function. If you follow the good practice of creating the directory handle in a scope that ends when you are done with the directory anyway, you can omit this call because Perl will call it for you implicitly when it destroys the storage associated with the directory handle as it goes out of scope.








################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
Q: For each row that has PRC693 in it, print the first 16 characters which is 
   the PAN

perl -nle '/PRC693/ and print substr $_, 0,16;' ld_trn_20130912


