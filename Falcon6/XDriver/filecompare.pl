#!/usr/bin/perl
use strict;
use warnings;
#use Data::Dumper;

# Note: "0" and an empty string("") are false. Anything else is basically true.


my $filename;
my $equals;

$filename = 'scr.200k';
my @scr = load_data();
$filename = 'XDriver.response.log';
my @resp = load_data();

# Debug
print scalar(@scr), " elements in the first file, and ",scalar(@resp), " elements in the second file\n";

# Compare the arrays
if (@scr != @resp) {
    $equals = 1;
} else {
  $equals = 1;
  foreach (my $i = 0; $i < @scr; $i++) {  # Test each element in the array
    if ($scr[$i] != $resp[$i]) { # use "ne" if elements are strings, not numbers, Or you can use generic sub comparing 2 values
    	print $scr[$i], ",", $resp[$i],"\n";  # If there is a pair that doesn't match, display the unmatched pair.
      $equals = 0;
      last;
    }
  }
}

if ($equals) {
	print "The two files match.\n";
} else {
	print "The two files DO NOT match.\n";
}

#print Dumper(@scr);
#print Dumper(@resp);


sub load_data {
	open my $fh, '<', $filename or die "Can't open $filename: $!\n";
	my @tmp;
	while ( <$fh> ) {
	  chomp;
	  my @cols = split;
	  if ($#cols > 7) {
		my @data = @cols[5 .. 8]; # array slice
	  	push @tmp,@data;
	  }

	  # Load only the first 3 iterations
	  #if ($. == 3) {
	  #	last;
	  #}
	}
	return @tmp;
}

