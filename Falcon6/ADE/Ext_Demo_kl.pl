#!/usr/bin/perl

#use strict;
#use warnings;

use Data::Dumper;
use lib "$ENV{HOME}/perl5/lib/perl5";

my %field_to_change_type = (
        'customerIdFromHeader'  => 'cusProfile',
        'customerAcctNumber'    => 'accProfile',
        'pan'                   => 'panProfile',
        'paymentInstrumentId'   => 'payInsProfile',
        'oldEntityName'         => 'UDProfile',
        'birthDate'             => 'cusPerInfo',
        'birthCountry'          => 'cusPerInfo',
        'gender'                => 'cusPerInfo',
        'numberOfDependents'    => 'cusPerInfo',
        'maritalStatus'         => 'cusPerInfo',
        'educationalStatus'     => 'cusPerInfo',
        'preferredGreeting'     => 'cusPerInfo',
        'preferredLanguage'     => 'cusPerInfo',
        'givenName'             => 'cusName',
        'middleName'            => 'cusName',
        'surname'               => 'cusName',
        'title'                 => 'cusName',
        'suffix'                => 'cusName',
        'M1'                    => 'militaryStatus',
        'M0'                    => 'militaryStatus',
        'nationalId'            => 'nationalInfo',
        'nationalIdCountry'     => 'nationalInfo',
        'citizenshipCountry'    => 'nationalInfo',
        'driversLicenseNumber'  => 'driversLicense',
        'driversLicenseCountry' => 'driversLicense',
        'taxId'                 => 'taxIdentification',
        'taxIdCountry'          => 'taxIdentification',
        'passportNumber'        => 'passportInfo',
        'passportCountry'       => 'passportInfo',
        'passportExpirationDate'=> 'passportInfo',
        'streetLine1'           => 'addressInfo',
        'streetLine2'           => 'addressInfo',
        'streetLine3'           => 'addressInfo',
        'streetLine4'           => 'addressInfo',
        'city'                  => 'addressInfo',
        'stateProvince'         => 'addressInfo',
        'postalCode'            => 'addressInfo',
        'countryCode'           => 'addressInfo',
        'dateAtAddress'         => 'addressInfo',
        'residenceStatus'       => 'addressInfo',
        'homePhone'             => 'telephoneInfo',
        'preferredPhone'        => 'telephoneInfo',
        'secondaryPhone'        => 'secTelephoneNumber',
        'workPhone'             => 'workTelephoneNumber',
        'mobilePhone'           => 'mobileTelephoneNumber',
        'emailAddress'          => 'cusEmailInfo',
        'pefc'                  => 'cusBankStatus',
        'ofac'                  => 'cusBankStatus',
        'numberOfAccounts'      => 'cusBankStatus',
        'relationshipStartDate' => 'cusBankStatus',
        'vipType'               => 'cusBankStatus',
        'mothersMaidenName'     => 'cusAuthCredentials',
        'openDate'              => 'otherAcctInfo',
        'applicationReferenceNumber'=> 'otherAcctInfo',
        'openChannel'           => 'otherAcctInfo',
        'campaignDate'          => 'otherAcctInfo',
        'portfolio'             => 'acctPortfolio',
        'accountServiceType'    => 'acctPortfolio',
        'status'                => 'acctStatus',
        'statusDate'            => 'acctStatus',
        'overLimitFlag'         => 'acctStatus',
        'numberOfCyclesInactive'=> 'acctStatus',
        'creditLimit'           => 'acctCreditLimit',
        'currencyCode'          => 'acctCreditLimit',
        'currencyConversionRate'=> 'acctCreditLimit',
        'dailyCashLimit'        => 'acctDailyCashLimit',
        'currencyCode'          => 'acctDailyCashLimit',
        'currencyConversionRate'=> 'acctDailyCashLimit',
        'dailyPosLimit'         => 'acctDailyPosLimit',
        'currencyCode'          => 'acctDailyPosLimit',
        'currencyConversionRate'=> 'acctDailyPosLimit',
        'dailyTotalLimit'       => 'acctDailyTotalLimit',
        'currencyCode'          => 'acctDailyTotalLimit',
        'currencyConversionRate'=> 'acctDailyTotalLimit',
        'pan'                   => 'issueCard',
        'paymentInstrumentId'   => 'issueCard',
        'lastIssueDate'         => 'issueCard',
        'plasticIssueType'      => 'issueCard',
        'expirationDate'        => 'issueCard',
        'nameOnInstrument'      => 'issueCard',
        'mediaType'             => 'issueCard',
        'issuingCountry'        => 'issueCard',
        'numberOfPaymentIds'    => 'panInfo',
        'panOpenDate'           => 'panInfo',
        'memberSinceDate'       => 'panInfo',
        'type'                  => 'panInfo',
        'subType'               => 'panInfo',
        'association'           => 'panInfo',
        'incentive'             => 'panInfo',
        'category'              => 'panInfo',
        'cardholderCity'        => 'cardholderLocation',
        'cardholderStateProvince'=> 'cardholderLocation',
        'cardholderPostalCode'  => 'cardholderLocation',
        'cardholderCountryCode' => 'cardholderLocation',
        'status'                => 'PANStatus',
        'statusDate'            => 'PANStatus',
        'activeIndicator'       => 'cardActivation',
        'creditLimit'           => 'panCreditLimit',
        'currencyCode'          => 'panCreditLimit',
        'currencyConversionRate'=> 'panCreditLimit',
        'dailyCashLimit'        => 'panDailyCashLimit',
        'currencyCode'          => 'panDailyCashLimit',
        'currencyConversionRate'=> 'panDailyCashLimit',
        'dailyPosLimit'         => 'panDailyPosLimit',
        'currencyCode'          => 'panDailyPosLimit',
        'currencyConversionRate'=> 'panDailyPosLimit',
);

my %change_type_to_code = (
        'cusProfile'           => '0001',
        'accProfile'           => '0002',
        'panProfile'           => '0003',
        'payInsProfile'        => '0004',
        'UDProfile'            => '0005',
        'cusPerInfo'           => '1000',
        'cusName'              => '1001',
        'militaryStatus'       => '1005',
        'nationalInfo'         => '1100',
        'driversLicense'       => '1104',
        'taxIdentification'    => '1105',
        'passportInfo'         => '1106',
        'addressInfo'          => '1150',
        'telephoneInfo'        => '1207',
        'secTelephoneNumber'   => '1208',
        'workTelephoneNumber'  => '1209',
        'mobileTelephoneNumber'=> '1210',
        'cusEmailInfo'         => '1250',
        'cusBankStatus'        => '1300',
        'cusAuthCredentials'   => '1350',
        'otherAcctInfo'        => '2011',
        'acctPortfolio'        => '2020',
        'acctStatus'           => '2030',
        'acctCreditLimit'      => '2201',
        'acctDailyCashLimit'   => '2203',
        'acctDailyPosLimit'    => '2204',
        'acctDailyTotalLimit'  => '2210',
        'issueCard'            => '3000',
        'panInfo'              => '3010',
        'cardholderLocation'   => '3100',
        'PANStatus'            => '3102',
        'cardActivation'       => '3104',
        'panCreditLimit'       => '3201',
        'panDailyCashLimit'    => '3203',
        'panDailyPosLimit'     => '3204',
);

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;
use Switch;

#my $username = getpwuid $UID;
my $username = $ENV{USER} || getpwuid($<);

use feature qw(switch);
given(uc substr $username,0, 1){
    when("Q") { $uSID=FHDBQ; }
    when("C") { $uSID=FHDBC; }
    when("D") { $uSID=FHDBD; }
    when("P") { $uSID=FHDBP; }
#    else { print "Invalid username"; }
}

my $PRC = $ARGV[0];
print "PRC - $PRC";
print "\n";

open(FILE,'/ffsc/npglr/qa/q1dps/JOB_SCRIPTS/etc/ORACLE_REPORTER_USER.FHDBQ') or die "can't read file 'ORACLE_REPORTER_USER.FHDBQ'";
$SqlAccess= <FILE>;
close (FILE);

my $DB_USRID = substr($SqlAccess, 0, index($SqlAccess, '/'));
my $tmp_var = substr($SqlAccess, index($SqlAccess, '/')+1, length($SqlAccess));
my $DB_PWD = substr($tmp_var,0,index($tmp_var, '@'));
my $DB_Conn_STR = substr($tmp_var,index($tmp_var, '@')+1,50);

use DBI;
my $db = DBI->connect("dbi:Oracle:$uSID",$DB_USRID,$DB_PWD);

my $Ext_Dir="/ffsc/npglr/qa/q1dps/LZ";
print $Ext_Dir;
print "\n";


my $tmp1 ="/home/falcon/q1dps/base/pmax/bin/LANDINGZONE/NMON$$.txt";
print $tmp1;
print "\n";

my $tmp1 ="/home/falcon/q1dps/base/pmax/bin/LANDINGZONE/NMON$$.txt";
print $tmp1;
print "\n";


$current_time = strftime "%H%M%S", localtime;
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2);
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
 $gmtOffset = $gmt_offset_diff - 24;
 } else { }

my $pis_rec_cnt  = 0;
open (PIS,'+>',"/ffsc/npglr/qa/q1dps/JOB_SCRIPTS/etc/PIS.txt") || die "file not found :$!";
# Print Header Line for PIS
printf  PIS "B000000000%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;


my $pis=$db->prepare("Select client_xid,RCD_CRT_DTTM ,RCD_CRT_DTTM ,RCD_CRT_DTTM ,GMT_OFFSET ,CUSTOMER_XID ,ACCOUNT_REFERENCE_XID ,EXTERNAL_TRANSACTION_XID ,SCORE_CUSTOMER_ACCOUNT_XID ,
CARD_TYPE_XCD ,CARD_SUB_TYPE_XCD ,CARD_CATEGORY_XCD ,CARD_ISSUER_XCD ,CARD_OPEN_DT ,MEMBER_SINCE_DT ,ISSUING_COUNTRY_XCD ,CITY_NAME ,COUNTRY_REGION_XCD ,
POSTAL_XCD ,COUNTRY_XCD ,PAYMENT_ID_CNT ,PAYMENT_INSTRUMENT_XID ,CURRENT_CARD_STATUS_XCD ,CURRENT_STATUS_EFFECTIVE_DT ,PIN_LENGTH ,PIN_SET_DT ,PIN_TYPE_XCD ,
PAYMENT_ACTIVE_INDICATOR_XFLG ,PAYMENT_INSTRUMENT_NAME ,PAYMENT_INSTRUMENT_EXPERTN_DT ,LAST_ISSUE_DT ,CARD_ISSUE_TYPE_XCD ,INCENTIVE_XCD ,CURRENCY_XCD ,
TRAN_ISO_CURRENCY_CNVRSN_RATIO ,CREDIT_LIMIT_AMT ,OVERDRAFT_LIMIT_AMT ,POS_DAILY_LIMIT_AMT ,CASH_DAILY_LIMIT_AMT ,DAILY_LIMIT_TYPE_XCD ,MEDIA_TYPE_XCD ,AIP_STATIC_XFLG ,
AIP_DYNAMIC_XFLG ,AIP_VERIFY_XFLG ,AIP_RISK_XFLG ,AIP_ISSUER_AUTHENTICATION_XFLG ,AIP_COMBINED_XFLG ,CHIP_SPEC_XCD ,CHIP_SPEC_VERSION_XCD ,OFFLINE_LOWER_LIMIT ,OFFLINE_UPPER_LIMIT ,
USER_INDICATOR_1_XCD ,USER_INDICATOR_2_XCD ,USER1_XCD ,USER2_XCD ,USER_DATA_1_STRG ,USER_DATA_2_STRG ,USER_DATA_3_STRG ,USER_DATA_4_STRG ,USER_DATA_5_STRG ,
USER_DATA_6_NUM ,USER_DATA_7_STRG from FRAUD_PAYMENT_INSTRUMENT where rownum <= 10");
$pis->execute();

while (my @row = $pis->fetchrow_array() ) {
      foreach (@row) {
       $_ = "\t" if !defined($_);
       print "$_\t";
       my $client_xid = $rows[0];
      }
      print "\n";
}



# print the footer line.
printf PIS "E999999999%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;
close (PIS);


my $SEL = "Select sysdate from dual";
my $sth = $db->prepare($SEL);
$sth->execute();

while ( my @row = $sth->fetchrow_array() ) {
    foreach (@row) {
      $_ = "\t" if !defined($_);
      print "$_\t";
    }
    print "\n";
}

END {
   $db->disconnect if defined($db);
