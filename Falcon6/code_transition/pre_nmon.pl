#!/usr/bin/perl

use lib "$ENV{HOME}/perl5/lib/perl5"; 

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

use DBI;
my $var;
my $db = DBI->connect("dbi:Oracle:fal1dbd","fal1admin","fal1admin"); 

$filename = 'vrol160.txt';
#my $filename = $ARGV[0];
$nmon_rec_cnt = 0;

open (IFH, '<', $filename) || die "Could not find the file: $!";
# /$home_dir/base/pmax/bin/LANDINGZONE/
open (OFH,'+>',"NMON.txt") || die "file not found :$!";	

# Print Header Line for NMON
printf  OFH "B000000000%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;

while (my $rec = <IFH>) {

 $customerAcctNumber        = substr($rec,0,19);  #Account-number
 $pan                       = substr($rec,0,19);  #Pan

 $transactionType           = substr($rec,153,1);  #Trans-Type
 
 $clientIdFromHeader        = substr($rec,248,14);#client-id
  
$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

#connecting to databse and collecting $customerIdFromHeader
$var = $customerAcctNumber;
#$var = "ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4";
#my $var = $db->quote("0000000000000000");

my $dbtran=$db->prepare("select customer_xid
from fraud_payment_instrument
where account_reference_xid = ?
");

$dbtran->execute($var);

while(my @rows=$dbtran->fetchrow_array())
{
foreach (my $rows)
{
$customerIdFromHeader    = $rows[0];
}
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;
 
$nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."NMO".sprintf ("%09d\n", $nmon_rec_cnt_1);

if ( $transactionType =~ 'B' ) {
    $nonmonCode = '2140'; 
	$actionCode = 'B1';
	format_print();
	}
elsif ( $transactionType =~ 'K' ) {
    $nonmonCode = '2131'; 
	$actionCode = 'C1';
	format_print();
	$nonmonCode = '3030';
	format_print();
	}
elsif ( $transactionType =~ 'J' ) {
    $nonmonCode = '2131'; 
	$actionCode = 'C1';
	format_print();
	$nonmonCode = '3030';
	format_print();
	}	
}

sub format_print {
#Formatting the output print.
 printf OFH "DUD17DF26        NMON20  2.0  %-16.16s%-8.8s%-6.6s%02s0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%-04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%-02.02s%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
$nmon_rec_cnt++;
}

# print the footer line.
printf OFH "E999999999%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;

close (OFH);
close (IFH);

#$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt.NMON20; NMON$$ touch status 2>&1";
#system (`$command`);

#unlink $file;

exit (0);