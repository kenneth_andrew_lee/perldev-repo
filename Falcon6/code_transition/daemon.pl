#!/usr/bin/perl

#use strict;
#use warnings;

use lib "$ENV{HOME}/perl5/lib/perl5";
use POSIX;

my %children = ();

my $inst_user = $ENV{'USER'};
$inst_user = $ARGV[0] if (defined $ARGV[0]);
my ($home_dir) = (getpwnam ($inst_user))[7];
exit(200) if (! defined $home_dir);

my $continue = 1;
$SIG{TERM} = sub { $continue = 0 };

$LZ = "/$home_dir/base/pmax/bin/LANDINGZONE";
# 'c:\users/manekuma/perl'

while ( $continue == 1 ) {
     opendir ( IDH, $LZ);
     my @all_files = readdir ( IDH );
     closedir (IDH);
#print "All processed files : @all_files\n";
     print "Getting new file list.\n";
     my @files_to_process = grep (/(\.car|\.frd15|\.c108|\.nmon)$/, @all_files );
	 	 
     for $file (@files_to_process) {
#print "All files to process : $file"."\n";
	 if ( -f $file ) {
	    if ( my $extension = $file =~ /\.car$/i ) {
		our $script = 'cardholder.pl';
		 }
		elsif ( my $extension = $file =~ /\.frd15$/i ) {
		 my $script = 'frd15.pl';
		 }
	    elsif ( my $extension = $file =~ /\.c108$/i ) {
		 my $script = 'dbtran25.pl';
		 }
		elsif ( my $extension = $file =~ /\.nmon$/i ) {
		 my $script = 'pre_nmon.pl';
	    } 
	   }
	   if ( -f $file.".sem" ) {
	   push ( @proc_list, [$script, $file] );
           @current_entry = @{$proc_list[0]};
      #print "The process list is :@current_entry\n";
	   unlink $file.".sem";
	   }
	  } 
	 
while ( scalar @proc_list ) {
if ( scalar (keys %children) <= 10 ) {
@to_execute = @{pop @proc_list};
$pid = fork();

if ( $pid == 0 ) {
print "I am the kid ($$): @to_execute\n";

#umask 0;
#chdir '/' or die "Can't change directory to /: $!\n";

exec ( $^X, @to_execute );
}
}
#print "I am the parent. I have: " . scalar (keys %children) . " children\n"; 
$children{$pid} = $to_execute[1];

for $kid_pid ( keys %children ) {
$child_died = waitpid ( $kid_pid, WNOHANG );
if ( $child_died == $kid_pid ) {
$rc = $?<<255;
print STDERR $to_execute[0]."(pid.$kid_pid)"."excited w/rc =".$?."\n";
delete $children{$pid};
}
}

}
sleep 5;
}	
