#!/usr/bin/perl

#use strict;
#use warnings;

use Data::Dumper;
use lib "$ENV{HOME}/perl5/lib/perl5"; 

my %field_to_change_type = (
                        'customerIdFromHeader'  => 'cusProfile',
						'customerAcctNumber'    => 'accProfile',
						'pan'                   => 'panProfile',
					    'paymentInstrumentId'   => 'payInsProfile',
                        'oldEntityName'         => 'UDProfile',
                        'birthDate'             => 'cusPerInfo',
                        'birthCountry'          => 'cusPerInfo',
						'gender'                => 'cusPerInfo',
						'numberOfDependents'    => 'cusPerInfo',
						'maritalStatus'         => 'cusPerInfo',
						'educationalStatus'     => 'cusPerInfo',
						'preferredGreeting'     => 'cusPerInfo',
						'preferredLanguage'     => 'cusPerInfo',
						'givenName'             => 'cusName',
						'middleName'            => 'cusName',
						'surname'               => 'cusName',
						'title'                 => 'cusName',
						'suffix'                => 'cusName',
						'M1'                    => 'militaryStatus',
						'M0'                    => 'militaryStatus',
						'nationalId'            => 'nationalInfo',
						'nationalIdCountry'     => 'nationalInfo',
						'citizenshipCountry'    => 'nationalInfo',
						'driversLicenseNumber'  => 'driversLicense',
						'driversLicenseCountry' => 'driversLicense',
						'taxId'                 => 'taxIdentification',
						'taxIdCountry'          => 'taxIdentification',
						'passportNumber'        => 'passportInfo',
						'passportCountry'       => 'passportInfo',
						'passportExpirationDate'=> 'passportInfo',
						'streetLine1'           => 'addressInfo',
						'streetLine2'           => 'addressInfo',  
						'streetLine3'           => 'addressInfo',
						'streetLine4'           => 'addressInfo',
						'city'                  => 'addressInfo',
						'stateProvince'         => 'addressInfo',
						'postalCode'            => 'addressInfo',
						'countryCode'           => 'addressInfo',
						'dateAtAddress'         => 'addressInfo',
						'residenceStatus'       => 'addressInfo',
						'homePhone'             => 'telephoneInfo',
						'preferredPhone'        => 'telephoneInfo',
						'secondaryPhone'        => 'secTelephoneNumber',
						'workPhone'             => 'workTelephoneNumber',
						'mobilePhone'           => 'mobileTelephoneNumber',
						'emailAddress'          => 'cusEmailInfo',
						'pefc'                  => 'cusBankStatus',
						'ofac'                  => 'cusBankStatus',
						'numberOfAccounts'      => 'cusBankStatus',
						'relationshipStartDate' => 'cusBankStatus',
						'vipType'               => 'cusBankStatus',
						'memberSinceDate'       => 'cusBankStatus',
						'mothersMaidenName'     => 'cusAuthCredentials',
						'openDate'              => 'otherAcctInfo',
						'applicationReferenceNumber'=> 'otherAcctInfo',
						'openChannel'           => 'otherAcctInfo',
						'campaignDate'          => 'otherAcctInfo', 
						'portfolio'             => 'acctPortfolio',
						'accountServiceType'    => 'acctPortfolio',
						'status_ais'            => 'acctStatus',
						'statusDate'            => 'acctStatus',
						'overLimitFlag'         => 'acctStatus',
						'numberOfCyclesInactive'=> 'acctStatus',
						'creditLimit'           => 'acctCreditLimit',
						'currencyCode'          => 'acctCreditLimit',
						'currencyConversionRate'=> 'acctCreditLimit',
						'dailyCashLimit'        => 'acctDailyCashLimit',
						'currencyCode'          => 'acctDailyCashLimit',
						'currencyConversionRate'=> 'acctDailyCashLimit',
						'dailyPosLimit'         => 'acctDailyPosLimit',
						'currencyCode'          => 'acctDailyPosLimit',
						'currencyConversionRate'=> 'acctDailyPosLimit',
						'dailyTotalLimit'       => 'acctDailyTotalLimit',
						'currencyCode'          => 'acctDailyTotalLimit',
						'currencyConversionRate'=> 'acctDailyTotalLimit',
						'pan'                   => 'issueCard',
						'paymentInstrumentId'   => 'issueCard',
						'lastIssueDate'         => 'issueCard',
						'plasticIssueType'      => 'issueCard',
						'expirationDate'        => 'issueCard',
						'nameOnInstrument'      => 'issueCard',
						'mediaType'             => 'issueCard',
						'issuingCountry'        => 'issueCard',
						'numberOfPaymentIds'    => 'panInfo',
						'panOpenDate'           => 'panInfo',
						'memberSinceDate'       => 'panInfo',
						'type'                  => 'panInfo',
						'subType'               => 'panInfo',
						'association'           => 'panInfo',
						'incentive'             => 'panInfo',
						'category'              => 'panInfo',
						'cardholderCity'        => 'cardholderLocation',
						'cardholderStateProvince'=> 'cardholderLocation',
						'cardholderPostalCode'  => 'cardholderLocation',
						'cardholderCountryCode' => 'cardholderLocation',
						'status_pis'            => 'PANStatus',
						'statusDate'            => 'PANStatus',
						'activeIndicator'       => 'cardActivation',
						'creditLimit'           => 'panCreditLimit',
						'currencyCode'          => 'panCreditLimit',
						'currencyConversionRate'=> 'panCreditLimit',
						'dailyCashLimit'        => 'panDailyCashLimit',
						'currencyCode'          => 'panDailyCashLimit',
						'currencyConversionRate'=> 'panDailyCashLimit',
						'dailyPosLimit'         => 'panDailyPosLimit',
						'currencyCode'          => 'panDailyPosLimit',
						'currencyConversionRate'=> 'panDailyPosLimit',
												
																
         );

my %change_type_to_code = (
                        'cusProfile'           => '0001',
                        'accProfile'           => '0002',
						'panProfile'           => '0003',
						'payInsProfile'        => '0004',
						'UDProfile'            => '0005',
                        'cusPerInfo'           => '1000',
						'cusName'              => '1001',
						'militaryStatus'       => '1005',
						'nationalInfo'         => '1100',
						'driversLicense'       => '1104',
						'taxIdentification'    => '1105',
						'passportInfo'         => '1106',
						'addressInfo'          => '1150',
						'telephoneInfo'        => '1207',
						'secTelephoneNumber'   => '1208',
						'workTelephoneNumber'  => '1209',
						'mobileTelephoneNumber'=> '1210',
						'cusEmailInfo'         => '1250',
						'cusBankStatus'        => '1300',
						'cusAuthCredentials'   => '1350',
						'otherAcctInfo'        => '2011',
						'acctPortfolio'        => '2020',
                        'acctStatus'           => '2030',
						'acctCreditLimit'      => '2201',
						'acctDailyCashLimit'   => '2203',
						'acctDailyPosLimit'    => '2204',
						'acctDailyTotalLimit'  => '2210',
						'issueCard'            => '3000',
						'panInfo'              => '3010',
						'cardholderLocation'   => '3100',
						'PANStatus'            => '3102',
						'cardActivation'       => '3104',
						'panCreditLimit'       => '3201',
						'panDailyCashLimit'    => '3203',
						'panDailyPosLimit'     => '3204',
         );

		 
use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;
use Switch;
 
use DBI;
my $var1;
my $db = DBI->connect("dbi:Oracle:fal1dbq","fal1admin","fal1admin") || die "Could not connect to DB: $!";

#my $filename = "dbtran.car";
my $filename = $ARGV[0];

open (IFH, '<', $filename) || die "Could not find the file: $!";

my $nmon_rec_cnt = 0;
my $cis_rec_cnt  = 0;
my $ais_rec_cnt  = 0;
my $pis_rec_cnt  = 0;

open (NMON,'+>',"NMON.txt") || die "file not found :$!";
# Print Header Line for NMON
printf  NMON "B000000000%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;
 
open (CIS,'+>',"CIS.txt") || die "file not found :$!";
# Print Header Line for CIS
printf CIS "B000000000%-1866.1866s2.0CIS20   PMAX  %-08.8d\n", '' x 1866, $cis_rec_cnt; 

open (AIS,'+>',"AIS.txt") || die "file not found :$!";
# Print the header Line for AIS
printf AIS "B000000000%-1136.1136s2.0AIS20   PMAX  %-08.8d\n", '' x 1136, $ais_rec_cnt;

open (PIS,'+>',"PIS.txt") || die "file not found :$!";
# Print Header Line for PIS
printf  PIS "B000000000%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;


while (my $rec = <IFH>) {

if ($rec =~ /^A/ or $rec =~ /^U/) {

 my %newh = ();
 chomp($rec);
 
 $newh{function_code}           = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}      = substr($rec,1,19);	  # $acct_nbr
 $customerAcctNumber            = substr($rec,1,19);	  # $acct_nbr
 $newh{pan}                     = substr($rec,1,19);      # acct_nbr
 $pan                           = substr($rec,1,19);      # acct_nbr
 $newh{givenName}               = substr($rec,19,30);     # $cus_nm 
 $newh{nameOnInstrument}        = substr($rec,19,30);     # $cus_nm
 $newh{surname}                 = substr($rec,50,25);     # $cus_lst_nm
 $newh{userData12_cis}          = substr($rec,75,30);     # $cus_co_nm
 $newh{mothersMaidenName}       = substr($rec,105,30);    # $cus_md_nm
 $newh{streetLine1}             = substr($rec,135,30);    # $cus_adr_1
 $newh{streetLine2}             = substr($rec,165,30);    # $cus_adr_2
 $newh{city}                    = substr($rec,195,30);    # $cus_city
 $newh{cardholderCity}          = substr($rec,195,30);    # $cus_city  
 $newh{stateProvince}           = substr($rec,225,2);     # $cus_st
 $newh{cardholderStateProvince} = substr($rec,225,2);     # $cus_st
 $newh{postalCode}              = substr($rec,227,9);     #$cus_zip
 $newh{cardholderPostalCode}    = substr($rec,227,9);     #$cus_zip 
 $newh{homePhone}               = substr($rec,236,16);    #$cus_hm_phon1
 $newh{secondaryPhone}          = substr($rec,252,16);    #$cus_hm_phon2
 $newh{workPhone}               = substr($rec,268,16);    #$cus_wrk_phon1
 $newh{mobilePhone}             = substr($rec,284,16);    #$cus_wrk_phon2
 $newh{birthDate}               = substr($rec,300,8);     #$cus_birth_dt1
 $newh{userData07_cis}          = substr($rec,308,8);     #$cus_birth_dt2
 $newh{nationalId}              = substr( $rec,316,11);    #$cus_ssn_1
 $newh{taxId}                   = substr($rec,316,11);    #$cus_ssn_1
 $newh{userData08_cis}          = substr($rec,327,11);    #$cus_ssn_2
 $newh{openDate}                = substr($rec,338,8);     #$crd_opn_dt
 $newh{panOpenDate}             = substr($rec,338,8);     #$crd_opn_dt
 $newh{expirationDate}          = substr($rec,346,8);     #$crd_exp_dt
 $newh{memberSinceDate}         = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{relationshipStartDate}   = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{lastIssueDate}           = substr($rec,362,8);     #$lst_iss_dt
 $newh{dailyTotalLimit}         = substr($rec,371,13);    #$avail_cr
 $newh{dailyCashLimit}          = substr($rec,371,13);    #$avail_cr
 $newh{dailyPosLimit}           = substr($rec,371,13);    #$avail_cr
 $newh{creditLimit}             = substr($rec,383,10);    #$cr_lmt
 $newh{activeIndicator}         = substr($rec,393,1);     #$crd_actv_ind
 $newh{status_ais}              = substr($rec,394,6);     #$crd_blk_ind
 $newh{status_pis}              = substr($rec,394,6);     #$crd_blk_ind
 $newh{plasticIssueType}        = substr($rec,400,2);     #$plastic_issue_type
 $newh{portfolio}               = substr($rec,402,14);    #$portfolio
 $newh{clientIdFromHeader}      = substr($rec,416,14);    #$client_id
 $clientIdFromHeader            = substr($rec,416,14);    #$client_id
 $newh{userData01_cis}          = substr($rec,430,6);     #user_data_01
 $newh{userData02_cis}          = substr($rec,436,6);     #user_data_02
 $newh{userData03_cis}          = substr($rec,442,10);    #user_data_03
 $newh{userData04_cis}          = substr($rec,452,10);    #user_data_04
 $newh{userData05_cis}          = substr($rec,462,15);    #user_data_05
 $newh{userData06_cis}          = substr($rec,477,20);    #user_data_06
 $newh{emailAddress}            = substr($rec,497,40);    #user_data_07

=start
while ((my $key, my $value)= each %newh) {
printf CIS "$key => $newh{$key}\n";
#delete $newh{$key};
}
=cut

$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));
 
if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;}
else {
$gmtOffset = "-$gmt_offset_diff\n";}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;

my %oldh = ();

$var1 = $newh{customerAcctNumber};
#print "The length of AcctNo-1:".length $newh{customerAcctNumber};
$var2 = $newh{clientIdFromHeader};
$var3 = $newh{clientIdFromHeader};

$var1 =~ s/\s+$//;
$var2 =~ s/\s+$//;
$var3 =~ s/\s+$//;
#print "The length of Var-1:".length $var1;
my $cis_code = 0;
my $ais_code = 0;
my $pis_code = 0;

my $ud06 = $newh{userData06_cis};
if ( $ud06 =~ m/^CU/ ) {
$newh{customerIdFromHeader} = substr($ud06,2,19);} 
else {
$newh{customerIdFromHeader} = "P".substr($rec,1,18);}
#print "New-customerIdFromHeader :".$newh{customerIdFromHeader}."\n";
$customerIdFromHeader = $newh{customerIdFromHeader};
#print "the CIFH1: $customerIdFromHeader\n";
=start
if ( $ud06 =~ m/^TN/ ) {
$newh{taxId} = substr($ud06,2,18);} 
if ( $ud06 =~ m/^SS/ ) {
$newh{nationalId} = substr($ud06,2,18);}
if ( $ud06 =~ m/^DL/ ) {
$newh{driversLicenseNumber} = substr($ud06,2,18);}
if ( $ud06 =~ m/^MN/ ) {
$newh{mothersMaidenName} = substr($ud06,2,18);}
if ( $ud06 =~ m/^SQ/ ) {
$newh{userData14_ais} = substr($ud06,2,18);}
if ( $ud06 =~ m/^CM/ ) {
$newh{userData15_cis} = substr($ud06,2,18);}
if ( $ud06 =~ m/^BD/ ) {
$newh{birthDate} = substr($ud06,2,18);}      
if ( $ud06 =~ m/^RD/ ) {
$newh{userData_05} = substr($ud06,2,18);}
if ( $ud06 =~ m/^HP/ ) {
$newh{homePhone} = substr($ud06,2,18);}
if ( $ud06 =~ m/^BP/ ) {
$newh{businessPhone} = substr($ud06,2,18);}
if ( $ud06 =~ m/^CP/ ) {
$newh{secondaryPhone} = substr($ud06,2,18);}
if ( $ud06 =~ m/^FA/ ) {
$newh{mobilePhone} = substr($ud06,2,18);}  
=cut

#print "the count value is :$count\n";
my $cis=$db->prepare("select account_reference_xid, first_name, last_name, user_data_12_strg, mothers_maiden_name, 
address_line_1_strg, address_line_2_strg, city_name, country_region_xcd, postal_xcd, home_phone_num, phone2_num,
work_phone_num, cell_phone_num, to_char(customer_birth_dt,'YYYYMMDD'), user_data_7_strg, national_xid, user_data_8_strg, client_xid,
user_data_1_strg, user_data_2_strg, user_data_3_strg, user_data_4_strg, user_data_5_strg, user_data_6_num,
customer_xid, email_address, to_char(relation_start_dt,'YYYYMMDD'), tax_xid 
from fraud_customer_information
where customer_xid in (
  select customer_xid from fraud_account_summary
  where account_reference_xid = ? and client_xid = ?
) and client_xid = ?
");

$cis->execute($var1, $var2, $var3); 

$found = 0;

while(my @rows=$cis->fetchrow_array())
{
foreach ($rows)
{
$found = 1;
 
$oldh{customerAcctNumber}  = $rows[0];
$oldh{givenName}           = $rows[1];
$oldh{surname}             = $rows[2];
$oldh{userData12}          = $rows[3];
$oldh{mothersMaidenName}   = $rows[4]; 
$oldh{streetLine1}         = $rows[5];
$oldh{streetLine2}         = $rows[6];
$oldh{city}                = $rows[7];
$oldh{stateProvince}       = $rows[8];
$oldh{postalCode}          = $rows[9];
$oldh{homePhone}           = $rows[10];
$oldh{secondaryPhone}      = $rows[11];
$oldh{workPhone}           = $rows[12];
$oldh{mobilePhone}         = $rows[13];
$oldh{birthDate}           = $rows[14];
$oldh{userdata07}          = $rows[15];
$oldh{nationalId}          = $rows[16];
$oldh{userdata08}          = $rows[17];
$oldh{clientIdFromHeader}  = $rows[18]; 
$oldh{userData01_cis}      = $rows[19];
$oldh{userData02_cis}      = $rows[20];
$oldh{userData03_cis}      = $rows[21];
$oldh{userData04_cis}      = $rows[22];
$oldh{userData05_cis}      = $rows[23];
$oldh{userData06_cis}      = $rows[24];
$oldh{customerIdFromHeader}= $rows[25];
$oldh{emailAddress}        = $rows[26];
$oldh{relationshipStartDate}= $rows[27];
$oldh{taxId}                = $rows[28];
}
}

=start
while ((my $key, my $value)= each %oldh) {
printf AIS "$key => $oldh{$key}\n";
#delete $newh{$key};
}
=cut

#print "The length of AcctNo-2:".length $rows[0];
if ($found == 1) {
 
#for AIS information
my $ais=$db->prepare("select to_char(account_open_dt,'YYYYMMDD'), total_daily_limit_amt, account_state_xcd, portfolio_name, customer_xid,
pos_daily_limit_amt, cash_daily_limit_amt, account_reference_xid
from fraud_account_summary
where account_reference_xid = ?
");

$ais->execute($var1);

while(my @rows=$ais->fetchrow_array())
{
foreach (my $rows)
{
$oldh{openDate}            = $rows[0];
$oldh{dailtTotalLimit}     = $rows[1];
$oldh{status_ais}          = $rows[2];
$oldh{portfolio}           = $rows[3];
$oldh{customerIdFromHeader}= $rows[4];
$oldh{dailyPosLimit}       = $rows[5];
$oldh{dailyCashLimit}      = $rows[6];
$oldh{customerAcctNumber}   = $rows[7];
}
}

=start
while ((my $key, my $value)= each %oldh) {
printf AIS "$key => $oldh{$key}\n";
#delete $newh{$key};
}
=cut

#for PIS information
my $pis=$db->prepare("select to_char(payment_instrument_expertn_dt,'YYYYMMDD'), to_char(member_since_dt,'YYYYMMDD'), to_char(last_issue_dt,'YYYYMMDD'), credit_limit_amt,
payment_active_indicator_xflg, card_issue_type_xcd, payment_instrument_name, country_region_xcd, postal_xcd,
to_char(card_open_dt,'YYYYMMDD'), pos_daily_limit_amt, cash_daily_limit_amt, current_card_status_xcd, customer_xid, score_customer_account_xid, city_name
from fraud_payment_instrument
where account_reference_xid = ?
");

$pis->execute($var1);

while(my @rows=$pis->fetchrow_array())
{
foreach (my $rows)
{
$oldh{expirationDate}      = $rows[0];
$oldh{memberSinceDate}     = $rows[1];
$oldh{lastIssueDate}       = $rows[2];
$oldh{creditLimit}         = $rows[3];
$oldh{activeIndicator}     = $rows[4];
$oldh{plasticIssueType}    = $rows[5];
$oldh{nameOnInstrument}    = $rows[6];
$oldh{cardholderStateProvince} = $rows[7];
$oldh{cardholderPostalCode}= $rows[8];
$oldh{panOpenDate}         = $rows[9]; 
$oldh{dailyPosLimit}       = $rows[10];
$oldh{dailyCashLimit}      = $rows[11];
$oldh{status_pis}          = $rows[12];
$oldh{customerIdFromHeader}= $rows[13];
$oldh{pan}                 = $rows[14];
$oldh{cardholderCity}      = $rows[15];
}
}

=start
while ((my $key, my $value)= each %oldh) {
printf AIS "$key => $oldh{$key}\n";
#delete $newh{$key};
}
=cut

my %tochangefields = ();
my $newkey;
my $newvalue;
#my $oldkey;

foreach $newkey (keys(%newh)) {
  $newh{$newkey} =~ s/\s+$//;
  $newh{$newkey} =~ s/^\s+//;
}
#$oldh{$newkey} =~ s/\s+$//;

while (($newkey, $newvalue)= each %newh) {

if($newkey !~ /^(userData|function_code|clientIdFromHeader)/)  {
if ($oldh{$newkey} =~ /^\d*\.{0,1}\d+$/ || $newvalue =~ /^\d*\.{0,1}\d+$/) {
  if ($oldh{$newkey} != $newvalue) {
    my $codekey = $field_to_change_type{$newkey};
    our $code = $change_type_to_code{$codekey};
    push @{$tochangefields{$code}{$newkey}},$oldh{$newkey},$newvalue;
	#print STDERR "Looking up \"$codekey\" and \"$newkey\" in numeric match, found " . $change_type_to_code{$codekey} . "\n";
  }
} else {
  if ($oldh{$newkey} ne $newvalue) {
    my $codekey = $field_to_change_type{$newkey};
    our $code = $change_type_to_code{$codekey};
    push @{$tochangefields{$code}{$newkey}},$oldh{$newkey},$newvalue;
	#print STDERR "Looking up \"$codekey\" and \"$newkey\" in alpha match, found " . $change_type_to_code{$codekey} . "\n";
	}
}
}
}


my $nonmonCode;

my %type_pos = ( 'old' => 0, 'new' => 1 );

@keylist =  ( "pan", "customerIdFromHeader", "characterValue", "city", "code1", "code2", "code3", "countryCode", "date1", "date2", "emailAddress", "entityName", "givenName", "id1", "id2",
"indicator1", "indicator2",	"indicator3", "indicator4", "middleName", "monetaryValue", "numericValue1",	"numericValue2", "phone1", "phone2",
"postalCode", "stateProvince", "streetLine1", "streetLine2", "streetLine3", "streetLine4", "suffix", "surname", "text"
);

foreach my $key (@keylist) {
my $ucKey = ucfirst $key; 
my $evalstr = "\$new$ucKey =". qq(qq($oldh{$key}));
if (!defined eval $evalstr) {
print "The error evaling : $evalstr\n";
}
#print CIS eval "\$new$ucKey = '$oldh{$key}'","\n";
}
$newCustomerId = $newCustomerIdFromHeader;
#print "the CIFH1: $newCustomerId\n";
if ( $code =~ '1000' ) {
$newDate1 = $oldh{birthDate};}
if ( $code =~ '1100' ) {
$newId1 = $oldh{nationalId};}
if ( $code =~ '1105' ) {
$newId1 = $oldh{taxId};}
if ( $code =~ '1207' ) {
$newPhone1 = $oldh{homePhone};}
if ( $code =~ '1208' ) {
$newPhone1 = $oldh{secondaryPhone};}
if ( $code =~ '1209' ) {
$newPhone1 = $oldh{workPhone};}
if ( $code =~ '1210' ) {
$newPhone1 = $oldh{mobilePhone};}
if ( $code =~ '1300' ) {
$newDate1 = $oldh{relationStartDate};
$newDate1 = $oldh{memberSinceDate};}
if ( $code =~ '1350' ) {
$newEntityName = $oldh{mothersMaidenName};}
if ( $code =~ '2011' ) {
$newDate1 = $oldh{openDate};}
if ( $code =~ '2020' ) {
$newText = $oldh{portfolio};}
if ( $code =~ '2030' ) {
$newCode1 = $oldh{status_ais};}
if ( $code =~ '2201' ) {
$newMonetartValue = $oldh{creditLimit};}
if ( $code =~ '2203' ) {
$newMonetaryValue = $oldh{dailyCashLimit};}
if ( $code =~ '2204' ) {
$newMonetaryValue = $oldh{dailyPosLimit};}
if ( $code =~ '2210' ) {
$newMonetaryValue = $oldh{dailyTotalLimit};}
if ( $code =~ '3000' ) {
$newEntityName = $oldh{nameOnInstrument};
$newIndicator1 = $oldh{plasticIssueType};
$newDate2      = $oldh{expirationDate};
$newDate1      = $oldh{lastIssueDate};}
if ( $code =~ '3010' ) {
$newDate2      = $oldh{panOpenDate};
$newDate1      = $oldh{memberSinceDate};}
if ( $code =~ '3100' ) {
$newCity = $oldh{cardholderCity};}
if ( $code =~ '3102' ) {
$newCode1 = $oldh{status_pis};}
if ( $code =~ '3104' ) {
$newIndicator1 = $oldh{activeIndicator};}
if ( $code =~ '3201' ) {
$newMonetaryValue = $oldh{creditLimit};}

while (my($ohk, $ohref) = each(%tochangefields)) {
$nonmonCode = $ohk;
if ($nonmonCode =~ /^1|0001/) {
#if ($nonmonCode =~ /^1/) {
 $cis_code++;}
if ($nonmonCode =~ /^2|0001/) {
#if ($nonmonCode =~ /^2/) {
 $ais_code++;}
if ($nonmonCode =~ /^3|0001/) {
#if ($nonmonCode =~ /^3/) {
 $pis_code++;}


foreach my $key (@keylist) {
my $key = ucfirst $key; 
eval "\$old$key = \$new$key","\n";
}
 $customerIdFromHeader = $oldCustomerIdFromHeader;
 $pan                  = $oldPan;
#print "the CIFH2: $customerIdFromHeader\n";

if ( $nonmonCode =~ '1000' ) {
$oldDate1 = $oldh{birthDate};}
if ( $nonmonCode =~ '1100' ) {
$oldId1 = $oldh{nationalId};}
if ( $nonmonCode =~ '1105' ) {
$oldId1 = $oldh{taxId};}
if ( $nonmonCode =~ '1207' ) {
$oldPhone1 = $oldh{homePhone};}
if ( $nonmonCode =~ '1208' ) {
$oldPhone1 = $oldh{secondaryPhone};}
if ( $nonmonCode =~ '1209' ) {
$oldPhone1 = $oldh{workPhone};}
if ( $nonmonCode =~ '1210' ) {
$oldPhone1 = $oldh{mobilePhone};}
if ( $nonmonCode =~ '1300' ) {
$oldDate1 = $oldh{relationStartDate};
$oldDate1 = $oldh{memberSinceDate};}
if ( $nonmonCode =~ '1350' ) {
$oldEntityName = $oldh{mothersMaidenName};}
if ( $nonmonCode =~ '2011' ) {
$oldDate1 = $oldh{openDate};}
if ( $nonmonCode =~ '2020' ) {
$oldText = $oldh{portfolio};}
if ( $nonmonCode =~ '2030' ) {
$oldCode1 = $oldh{status_ais};}
if ( $nonmonCode =~ '2201' ) {
$oldMonetartValue = $oldh{creditLimit};}
if ( $nonmonCode =~ '2203' ) {
$oldMonetaryValue = $oldh{dailyCashLimit};}
if ( $nonmonCode =~ '2204' ) {
$oldMonetaryValue = $oldh{dailyPosLimit};}
if ( $nonmonCode =~ '2210' ) {
$oldMonetaryValue = $oldh{dailyTotalLimit};}
if ( $nonmonCode =~ '3000' ) {
$oldEntityName = $oldh{nameOnInstrument};
$oldIndicator1 = $oldh{plasticIssueType};
$oldDate2      = $oldh{expirationDate};
$oldDate1      = $oldh{lastIssueDate};}
if ( $nonmonCode =~ '3010' ) {
$oldDate2      = $oldh{panOpenDate};
$oldDate1      = $oldh{memberSinceDate};}
if ( $nonmonCode =~ '3100' ) {
$oldCity = $oldh{cardholderCity};}
if ( $nonmonCode =~ '3102' ) {
$oldCode1 = $oldh{status_pis};}
if ( $nonmonCode =~ '3104' ) {
$oldIndicator1 = $oldh{activeIndicator};}
if ( $nonmonCode =~ '3201' ) {
$oldMonetaryValue = $oldh{creditLimit};}


while (my($ihk, $ihref) = each(%$ohref)) {
my @inner_array = @$ihref;
my $ihk_uc = ucfirst $ihk; 
eval "\$new$ihk_uc = \$inner_array[1]","\n";
#print NMON "\$$type$ihk_uc = \"$inner_array[$pos]\"","\n";
}
$newCustomerId = $newCustomerIdFromHeader;
if ( $nonmonCode =~ '1000' ) {
$newDate1 = $newBirthDate;}
if ( $nonmonCode =~ '1100' ) {
$newId1 = $newNationalId;}
if ( $nonmonCode =~ '1105' ) {
$newId1 = $newTaxId;} 
if ( $nonmonCode =~ '1207' ) {
$newPhone1 = $newHomePhone;}
if ( $nonmonCode =~ '1208' ) {
$newPhone1 = $newSecondaryPhone;}
if ( $nonmonCode =~ '1209' ) {
$newPhone1 = $newWorkPhone;}
if ( $nonmonCode =~ '1210' ) {
$newPhone1 = $newMobilePhone;}
if ( $nonmonCode =~ '1300' ) {
$newDate1 = $newRelationStartDate;
$newDate1 = $newMemberSinceDate;}
if ( $nonmonCode =~ '1350' ) {
$newEntityName = $newMothersMaidenName;}
if ( $nonmonCode =~ '2011' ) {
$newDate1 = $newOpenDate;}
if ( $nonmonCode =~ '2020' ) {
$newText = $newPortfolio;}
if ( $nonmonCode =~ '2030' ) {
$newCode1 = $newStatus_ais;}
if ( $nonmonCode =~ '2201' ) {
$newMonetartValue = $newCreditLimit;}
if ( $nonmonCode =~ '2203' ) {
$newMonetaryValue = $newDailyCashLimit;}
if ( $nonmonCode =~ '2204' ) {
$newMonetaryValue = $newDailyPosLimit;}
if ( $nonmonCode =~ '2210' ) {
$newMonetaryValue = $newDailyTotalLimit;}
if ( $nonmonCode =~ '3000' ) {
$newEntityName = $newNameOnInstrument;
$newIndicator1 = $newPlasticIssueType;
$newDate2      = $newExpirationDate;
$newDate1      = $newLastIssueDate;}
if ( $nonmonCode =~ '3010' ) {
$newDate2      = $newPanOpenDate;
$newDate1      = $newMemberSinceDate;}
if ( $nonmonCode =~ '3100' ) {
$newCity = $newCardholderCity;}
if ( $nonmonCode =~ '3102' ) {
$newCode1 = $newStatus_pis;}
if ( $nonmonCode =~ '3104' ) {
$newIndicator1 = $newActiveIndicator;}
if ( $nonmonCode =~ '3201' ) {
$newMonetaryValue = $newCreditLimit;}

if ($nonmonCode =~ '0001') {
$actionCode = 'T';
}

$nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."NMO".sprintf ("%09d\n", $nmon_rec_cnt_1);

printf NMON "DUSDEBIT         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02s0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%-04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%-2.2s%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
$nmon_rec_cnt++;  

if ($nonmonCode =~ '0001') {
$actionCode = undef;
}
 
}

for my $field (keys(%$ohref)) {
for my $type ( keys %type_pos ) {
my $field_uc = ucfirst $field;
eval "\$$type$field_uc = undef";
}
}
 $newCustomerId = undef;
 $customerIdFromHeader = undef;
 #$actionCode = undef;
=start 
foreach my $key (@keylist) {
my $ucKey = ucfirst $key; 
eval "\$new$ucKey = undef";
}
=cut
while ((my $key, my $value)= each %oldh) {
$oldh{$key} = undef;
}

}  

if ($cis_code > 0 || $found == 0) {

$cis_rec_cnt_1 =  $cis_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."CIS".sprintf("%09d\n", $cis_rec_cnt_1);

printf CIS "DUSDEBIT         CIS20   2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-01.1s%-1.1s%-8.8s%5d%-030.30s%-030.30s%-60.60s%-10.10s%-10.10s%-60.60s%-03.3s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-03.3s%1.1s%-8.8s%1.1s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-3.3s%-3.3s%-8.8s%-4.4s%-4.4s%16.16d%-03.3s%013.6f%-024.24s%-024.24s%-024.24s%-024.24s%-1.1s%-40.40s%-1.1s%-8.8s%-3.3s%-3.3s%-016.16s%-3.3s%-16.16s%-3.3s%-8.8s%-16.16s%-3.3s%-016.16s%-3.3s%-1.1s%-1.1s%2d%4d%-8.8s%-20.20s%-1.1s%-4.4s%-1.1s%-1.1s%4d%4d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},
'' x 40,$externalTransactionId,$customerType,$vipType,$newh{relationshipStartDate},$newh{numberOfAccounts},$newh{givenName},$newh{middleName},
$newh{surname},$title,$suffix,$newh{preferredGreeting},$newh{preferredLanguage},$newh{mothersMaidenName},$newh{householdName},$newh{streetLine1},$newh{streetLine2},
$newh{streetLine3},$newh{streetLine4},$newh{city},$newh{stateProvince},$newh{postalCode},$newh{countryCode},$residenceStatus,$newh{dateAtAddress},$secondaryAddrType,$newh{secondaryAddrStreetLine1},
$newh{secondaryAddrStreetLine2},$newh{secondaryAddrStreetLine3},$newh{secondaryAddrStreetLine4},$newh{secondaryAddrCity},$newh{secondaryAddrStateProvince},
$newh{secondaryAddrPostalCode},$newh{secondaryAddrCountryCode},$newh{employer},$newh{workAddrStreetLine1},$newh{workAddrStreetLine2},$newh{workAddrStreetLine3},
$newh{workAddrStreetLine4},$newh{workAddrCity},$newh{workAddrStateProvince},$newh{workAddrPostalCode},$newh{workAddrCountryCode},$employmentStatus,$newh{emplymentStartDate},
$newh{employerMcc},$newh{occupationCode},$newh{income},$newh{currencyCode},$newh{currencyConversionRate},$newh{homePhone},$newh{secondaryPhone},$newh{workPhone},$newh{mobilePhone},$preferredPhone,$newh{emailAddress},
$educationalStatus,$newh{birthDate},$newh{birthCountry},$newh{citizenshipCountry},$newh{nationalId},$newh{nationalIdCountry},$newh{passportNumber},$newh{passportCountry},$newh{passportExpirationDate},
$newh{driversLicenseNumber},$newh{driversLicenseCountry},$newh{taxId},$newh{taxIdCountry},$gender,$maritalStatus,$newh{numberOfDependents},$newh{creditScore},$newh{creditScoreDate},$newh{creditScoreSource},
$creditScoreRequestReason,$newh{creditRating},$pefp,$ofac,$newh{behaviorScore1},$newh{behaviorScore2},$newh{segmentId1},$newh{segmentId2},$newh{segmentId3},$newh{segmentId4},$newh{userIndicator01},$newh{userIndicator02},
$newh{userIndicator03},$newh{userIndicator04},$newh{userIndicator05},$newh{userCode1},$newh{userCode2},$newh{userCode3},$newh{userCode4},$newh{userCode5},$newh{userData01_cis},$newh{userData02_cis},
$newh{userData03_cis},$newh{userData04_cis},$newh{userData05_cis},$newh{userData06},$newh{userData07_cis},$newh{userData08_cis},$nehw{userData09},$nehw{userData10},$nehw{userData11},
$newh{userData12_cis},$newh{userData13},$newh{userData14},$newh{userData15_cis}, $RESERVED_01; 
$cis_rec_cnt++;
}

if ($ais_code > 0 || $found == 0) {

$ais_rec_cnt_1 =  $ais_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."AIS".sprintf ("%09d\n", $ais_rec_cnt_1);

printf AIS "DUSDEBIT         AIS20   2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-2.2s%-2.2s%-2.2s%-20.20s%-1.1s%-020.20s%-020.20s%-020.20s%-03.3s%-3.3s%-40.40s%-010.10s%-32.32s%5d%5d%-8.8s%-2.2s%-8.8s%02d%-8.8s%-1.1s%-03.3s%013.6f%016d%016d%016d%016d%016d%-1.1s%-1.1s%-1.1s%-1.1s%-14.14s%-4.4s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-3.3s%3d%2d%8d%-10.10s%3d%2d%19.2f%-1.1s%4d%4d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%1.1s%1.1s%1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},$newh{customerAcctNumber},
$externalTransactionId,$type,$ownership,$usage,$newh{jointCustomerId},$vipType,$newh{routingNumber},$newh{bankId},$newh{branchId},$newh{branchCountry},$nehw{branchStateProvince},$newh{branchCity},$newh{branchPostalCode},
$newh{applicationReferenceNumber},$newh{numberOfPaymentIds},$newh{numberOfAuthorizedUsers},$newh{openDate},$newh{status_ais},$newh{statusDate},$newh{authenticationCodeLength},$newh{authenticationCodeSetDate},
$authenticationCodeType,$newh{currencyCode},$newh{currencyConversionRate},$newh{creditLimit},$newh{overdraftLimit},$newh{dailyPosLimit},$newh{dailyCashLimit},$newh{dailyTotalLimit},$cashBackLimitMode,$hasDirectDeposit,$hasOnlinePay,$hasMobilePay,$newh{portfolio},$newh{accountServiceType},
$newh{statementAddress},$newh{statementStreetLine1},$newh{statementStreetLine2},$newh{statementStreetLine3},$newh{statementStreetLine4},$newh{statementCity},$newh{statementStateProvince},
$newh{statementPostalCode},$newh{statementCountryCode},$newh{statementCyclePeriod},$newh{statementDayOfMonth},$newh{interestRate},$newh{interestRateCategory},$newh{numberOfCyclesInactive},
$newh{numberOfCyclesDelinquent},$newh{delinquentAmount},$overLimitFlag,$newh{behaviorScore1},$newh{behaviorScore2},$newh{segmentId1},$newh{segmentId2},$newh{segmentId3},$newh{segmentId4},$newh{userIndicator01},
$newh{userIndicator02},$newh{userIndicator03},$newh{userIndicator04},$newh{userIndicator05},$newh{userCode1},$newh{userCode2},$newh{userCode3},$newh{userCode4},$newh{userCode5},$newh{userData01},
$newh{userData02},$newh{userData03},$newh{userData04},$newh{userData05},$newh{userData06},$newh{userData07},$newh{userData08},$newh{userData09},$newh{userData10},$newh{userData11},$newh{userData12},
$newh{userData13},$newh{userData14_ais},$newh{userData15}, $RESERVED_01;
$ais_rec_cnt++;
}  

if ($pis_code > 0 || $found == 0) {

$pis_rec_cnt_1 =  $pis_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."PIS".sprintf ("%09d\n", $pis_rec_cnt_1);

printf PIS "DUSDEBIT         PIS12   1.2  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-1.1s%-2.2s%-1.1s%-1.1s%-08.8s%-8.8s%-03.3s%-040.40s%-05.5s%-010.10s%-03.3s%3d%-30.30s%-02.2s%-8.8s%2d%-8.8s%-1.1s%-1.1s%-40.40s%-8.8s%-8.8s%-1.1s%-1.1s%-03.3s%013.6f%10d%10d%10d%10d%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%2d%2d%-1.1s%-1.1s%-3.3s%-3.3s%-6.6s%-6.6s%-10.10s%-10.10s%-15.15s%-20.20s%-40.40s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},
$newh{customerAcctNumber},$externalTransactionId,$newh{pan},$type,$subType,$category,$association,$newh{panOpenDate},$newh{memberSinceDate},$nehw{issuingCountry},$newh{cardholderCity},
$newh{cardholderStateProvince},$newh{cardholderPostalCode},$newh{cardholderCountryCode},$newh{numberOfPayments},$newh{paymentInstrumentId},$newh{status_pis},$newh{statusDate},
$newh{pinLength},$newh{pinSetDate},$pinType,$newh{activeIndicator},$newh{nameOnInstrument},$newh{expirationDate},$newh{lastIssueDate},$newh{plasticIssueType},$incentive,$newh{currencyCode},$newh{currencyConversionRate},$newh{creditLimit},
$newh{overdraftLimit},$newh{dailyPosLimit},$newh{dailyCashLimit},$cashBackLimitMode,$mediaType,$aipStatic,$aipDynamic,$aipVerify,$aipRisk,$aipIssuerAuthentication,$aipCombined,
$chipSpecification,$chipSpecVersion,$newh{offlineLowerLimit},$newh{offlineUpperLimit},$newh{userIndicator01},$newh{userIndicator02},$newh{userCode1},
$newh{userCode2},$newh{userData01},$newh{userData02},$newh{userData03},$newh{userData04},$newh{userData05},$newh{userData06},$newh{userData07};
$pis_rec_cnt++;
}
  
while ((my $key, my $value)= each %newh) {
$newh{$key} = undef;
}
  
}

else {

if ($rec =~ /^D/) {

 my %newh = ();
 chomp($rec);
 
 $newh{function_code}           = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}      = substr($rec,1,19);	  # $acct_nbr
 $newh{pan}                     = substr($rec,1,19);      #acct_nbr
 $newh{givenName}               = substr($rec,19,30);     # $cus_nm 
 $newh{nameOnInstrument}        = substr($rec,19,30);     # $cus_nm
 $newh{surname}                 = substr($rec,50,25);     # $cus_lst_nm
 $newh{userData12_cis}          = substr($rec,75,30);     # $cus_co_nm
 $newh{mothersMaidenName}       = substr($rec,105,30);    # $cus_md_nm
 $newh{streetLine1}             = substr($rec,135,30);    # $cus_adr_1
 $newh{streetLine2}             = substr($rec,165,30);    # $cus_adr_2
 $newh{city}                    = substr($rec,195,30);    # $cus_city 
 $newh{stateProvince}           = substr($rec,225,2);     # $cus_st
 $newh{cardholderStateProvince} = substr($rec,225,2);     # $cus_st
 $newh{postalCode}              = substr($rec,227,9);     #$cus_zip
 $newh{cardholderPostalCode}    = substr($rec,227,9);     #$cus_zip 
 $newh{homePhone}               = substr($rec,236,16);    #$cus_hm_phon1
 $newh{secondaryPhone}          = substr($rec,252,16);    #$cus_hm_phon2
 $newh{workPhone}               = substr($rec,268,16);    #$cus_wrk_phon1
 $newh{mobilePhone}             = substr($rec,284,16);    #$cus_wrk_phon2
 $newh{birthDate}               = substr($rec,300,8);     #$cus_birth_dt1
 $newh{userData07_cis}          = substr($rec,308,8);     #$cus_birth_dt2
 $newh{nationalId}              = substr($rec,316,11);    #$cus_ssn_1
 $newh{taxId}                   = substr($rec,316,11);    #$cus_ssn_1
 $newh{userData08_cis}          = substr($rec,327,11);    #$cus_ssn_2
 $newh{openDate}                = substr($rec,338,8);     #$crd_opn_dt
 $newh{panOpenDate}             = substr($rec,338,8);     #$crd_opn_dt
 $newh{expirationDate}          = substr($rec,346,8);     #$crd_exp_dt
 $newh{memberSinceDate}         = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{relationshipStartDate}   = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{lastIssueDate}           = substr($rec,362,8);     #$lst_iss_dt
 $newh{dailyTotalLimit}         = substr($rec,370,13);    #$avail_cr
 $newh{dailyCashLimit}          = substr($rec,370,13);    #$avail_cr
 $newh{dailyPosLimit}           = substr($rec,370,13);    #$avail_cr
 $newh{creditLimit}             = substr($rec,383,10);    #$cr_lmt
 $newh{activeIndicator}         = substr($rec,393,1);     #$crd_actv_ind
 $newh{status}                  = substr($rec,394,6);     #$crd_blk_ind
 $newh{plasticIssueType}        = substr($rec,400,2);     #$plastic_issue_type
 $newh{portfolio}               = substr($rec,402,14);    #$portfolio
 $newh{clientIdFromHeader}      = substr($rec,416,14);    #$client_id
 $clientIdFromHeader            = substr($rec,416,14);
 $newh{userData01_cis}          = substr($rec,430,6);     #user_data_01
 $newh{userData02_cis}          = substr($rec,436,6);     #user_data_02
 $newh{userData03_cis}          = substr($rec,442,10);    #user_data_03
 $newh{userData04_cis}          = substr($rec,452,10);    #user_data_04
 $newh{userData05_cis}          = substr($rec,462,15);    #user_data_05
 
 $newh{userData06_cis}          = substr($rec,477,20);    #user_data_06
  
 $newh{emailAddress}            = substr($rec,497,40);    #user_data_07

 
$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;
 
$actionCode = $newh{function_code};
 
#connecting to DB and pulling customerIdFromHeader value.
$var = $newh{customerAcctNumber};
$var =~ s/\s+$//;

my $cis=$db->prepare("select customer_xid 
from fraud_customer_information
where account_reference_xid = ?
");

$cis->execute($var); 

while(my $rows=$cis->fetchrow_array())
{
$customerIdFromHeader = $rows;
}

if ( $customerIdFromHeader =~ /^P/ ){
$newh{customerIdFromHeader} = substr($customerIdFromHeader,1,18);
} else {
$newh{customerIdFromHeader} = $customerIdFromHeader;
}

for ( my $cd=1; $cd<=3; $cd++ ) {
  my ( $customerIdFromHeader, $newCustomerId, $customerAcctNumber, $newCustomerAcctNumber, $pan , $newPan, $paymentInstrumentId, $newPaymentInstrumentId, $OldEntityName, $newEntityName )  =  '';

  switch($cd) {
    case 1 { $customerIdFromHeader = $newh{customerIdFromHeader}; }
    case 2 { $customerAcctNumber   = $newh{customerAcctNumber}; }
    case 3 { $pan                  = $newh{customerAcctNumber}; }
    }
 
 my $nonmonCode = $cd;
 
 $nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."NMO".sprintf ("%09d\n", $nmon_rec_cnt_1);
 
 printf NMON "DUSDEBIT         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%-2.2s%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
    $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
	$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
	$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
	$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
	$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
	$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
	$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
	$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
	$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
	$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
	$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
	$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
 $nmon_rec_cnt++;   
 }
}

else {
if ($rec =~ /^T/ or $rec =~ /^C/) {

 %newh = ();
 chomp($rec);
 
 $newh{function_code}             = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}        = substr($rec,1,19);	    # $acct_nbr
 $newh{newCustomerAcctNumber}     = substr($rec,20,19);     # $newCustomerAcctNumber 
 #$newh{newCustomerId}      = substr($rec,1,19);      # $customerIdFromHeader
 #$newh{customerIdFromHeader}             = substr($rec,20,19);     # $newCustomerId
 $newh{pan}                       = substr($rec,1,19);      # $pan
 $newh{newPan}                    = substr($rec,20,19);     # $newPan
 

$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
} 

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;
 
$actionCode = $newh{function_code};
#$clientIdFromHeader = substr($filename,7,6);
$clientIdFromHeader = 'PRC533';
 
#connecting to DB and pulling customerIdFromHeader value.
$var = $newh{customerAcctNumber};
$var =~ s/\s+$//;

$cis=$db->prepare("select customer_xid 
from fraud_payment_instrument
where account_reference_xid = ?
");

$cis->execute($var);


while(@rows=$cis->fetchrow_array())
{
$customerIdFromHeader = $rows[0]; 
 }
 print "CIFH : $customerIdFromHeader\n";
if ( $customerIdFromHeader =~ /^P/ ){
$newh{customerIdFromHeader} = $customerIdFromHeader;
$newh{newCustomerId}        = $customerIdFromHeader;
$customerIdFromHeader       = substr($customerIdFromHeader,1,18);
} 
 
if ( $newh{pan} == $customerIdFromHeader ) {
    $x = 1; }
 else { $x = 2; } 
 
for ( $cd=$x; $cd<=3; $cd++ ) {
 my ( $customerIdFromHeader, $newCustomerId, $customerAcctNumber, $newCustomerAcctNumber, $pan , $newPan, $paymentInstrumentId, $newPaymentInstrumentId, $OldEntityName, $newEntityName )  =  '';
  
  switch($cd) {
    case 1 { $customerIdFromHeader = $newh{customerIdFromHeader};
             $newCustomerId        = $newh{newCustomerId}; 	 } 
    case 2 { $customerAcctNumber   = $newh{customerAcctNumber};
             $newCustomerAcctNumber= $newh{newCustomerAcctNumber};	}
    case 3 { $pan                  = $newh{pan};
             $newPan               = $newh{newPan};	}
    }
	
 my $nonmonCode = $cd;
 
$nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."NMO".sprintf ("%09d\n", $nmon_rec_cnt_1);
 
 printf NMON "DUSDEBIT         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%-2.2s%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
    $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
	$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
	$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
	$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
	$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
	$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
	$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
	$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
	$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
	$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
	$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
	$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
 $nmon_rec_cnt++;   
 }

}

while ((my $key, my $value)= each %newh) {
$newh{$key} = undef;
}

}

}
	
}	

{
  $db->disconnect if defined($db);
}

# print the footer line.
printf NMON "E999999999%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;
close (NMON);

# Print the footer.
printf CIS "E999999999%-1866.1866s2.0CIS20   PMAX  %-08.8d\n", '' x 1866, $cis_rec_cnt;
close (CIS);

# print the footer line.
printf AIS "E999999999%-1136.1136s2.0AIS20   PMAX  %-08.8d\n", '' x 1136, $ais_rec_cnt;
close (AIS);

# print the footer line.
printf PIS "E999999999%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;
close (PIS);

close (IFH);

=start
if ($nmon_rec_cnt == 0) {
unlink "/$home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt";
} 
else {	
#/$ENV/PIS.txt.PIS12;
$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt.NMON20; NMON20$$ touch status 2>&1";
system (`$command`);
} 
if ($pis_rec_cnt == 0) {
unlink "/$home_dir/base/pmax/bin/LANDINGZONE/PIS$$.txt";
} 
else { 
#/$ENV/PIS.txt.PIS12;
$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/PIS$$.txt.PIS12; PIS$$ touch status 2>&1";
system (`$command`);
}
if ($ais_rec_cnt == 0) {
unlink "/$home_dir/base/pmax/bin/LANDINGZONE/AIS$$.txt";
}
else {
#/$ENV/AIS.txt.AIS20;
$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/AIS$$.txt.AIS20; AIS$$ touch status 2>&1";
system (`$command`);
}
if ($cis_rec_cnt == 0) {
unlink "/$home_dir/base/pmax/bin/LANDINGZONE/CIS$$.txt";
}
else {
#/$ENV/CIS.txt.CIS20;
$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/CIS$$.txt.CIS20; CIS$$ touch status 2>&1";
system (`$command`);
}

unlink $filename;

exit (0);
=cut
