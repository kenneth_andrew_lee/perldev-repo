#!/usr/bin/perl

# use lib "$ENV{HOME}/perl5/lib/perl5"; 

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

use DBI;
my $var;
my $db = DBI->connect("dbi:Oracle:fhdbq","reporter","reporter");

my $ext_rec_cnt = 0;

open (IFH, '<', "EXTMSGDATE.txt") || die "Could not find the file: $!";
while ($rec = <IFH> ) {
$start_date = substr($rec, 0, 12);
}
close (IFH); 

print "start :$start_date\n";
 
open (EXT,'+>',"EXTMSG.txt") || die "file not found :$!";	

# Print Header Line
printf EXT "B000000000%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $ext_rec_cnt;

$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;


my $extmsg=$db->prepare("select * from (
  select
    maint_pan as pan,
    processor as prc,
    nvl(rf_data_after_rf_base,'000') as base_risk_factor,
    rf_data_after_rf_ovrd as override_risk_factor,
    rf_data_after_rf_ovrd_from_dt as override_from_dt,
    rf_data_after_rf_ovrd_to_dt as override_to_dt,
    rf_note_after_rf_base_note as base_note,
    rf_note_after_rf_ovrd_note as override_note,
	record_tstamp as event_dttm,
    maint_funct as change_type,
    row_number() over (partition by maint_pan order by record_tstamp desc) as row_number
 from 
    cnx1310_risk_factor_changes rf
  where 1=1 
    and to_char(record_tstamp, 'YYYYMMDDHHMi') > ? 
    and maint_funct in ('ADD','UPDATE')
    and processor != 'PRC365'
)
where row_number = 1
");

$extmsg->execute($start_date);

while(my @rows=$extmsg->fetchrow_array())
{
foreach (my $rows)
{
$customerAcctNumber      = $rows[0];
$customerIdFromHeader    = $rows[0];
$serviceId               = $rows[0];
$clientIdFromHeader      = $rows[1];
$userData01              = $rows[2];
$userData02              = $rows[3];
$userData06              = $rows[4];
$userData07              = $rows[5];
$userData33              = $rows[6];
$userData34              = $rows[7];      
}

$ext_rec_cnt_1 =  $ext_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."EXT".sprintf ("%09d\n", $ext_rec_cnt_1);

# Formatting the output print.
printf EXT "DUSDEBIT         EXT10   1.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s\n",
  $clientIdFromHeader, $recordCreationDate, $recordCreationTime, $recordCreationMilliseconds,
  $gmtOffset, $customerIdFromHeader, $customerAcctNumber, $externalTransactionId, $serviceId, $transactionDate, $transactionTime, $validity,
  $entityType, $extSource, $notificationName, $notificationStatus, $score1, $score2, $score3, $userData01, $userData02, $userData03, $userData04,
  $userData05, $userData06, $userData07, $userData08, $userData09, $userData10, $userData11, $userData12, $userData13, $userData14, $userData15,
  $userData16, $userData17, $userData18, $userData19, $userData20, $userData21, $userData22, $userData23, $userData24, $userData25, $userData26, 
  $userData27, $userData28, $userData29, $userData30, $userData31, $userData32, $userData33, $userData34;
  $ext_rec_cnt++;
}

open (OFH,'+>',"EXTMSGDATE.txt") || die "file not found :$!";	
print OFH $transactionDate.$transactionTime."\n";
close (OFH);

# Print the footer.
printf EXT "E999999999%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $ext_rec_cnt; 
close (EXT);

=start
if ($ext_rec_cnt == 0) {
unlink "/$home_dir/base/pmax/bin/LANDINGZONE/EXTMSG$$.txt";
} 
else {	
#/$ENV/PIS.txt.PIS12;
$command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/EXTMSG$$.txt.EXT10; EXT10$$ touch status 2>&1";
system (`$command`);
}
=cut 