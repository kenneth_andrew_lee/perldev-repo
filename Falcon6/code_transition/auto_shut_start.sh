#!/bin/ksh

# . `dirname $0`/pmax-ctl

cd /home/falcon/t1dps/base/pmax/util

rm -f $PMAX_BIN/logs/pmax.log

. ./pmax_env

function Shutdown
{
./pmax-ctl shutdown
if [[ $? -ne 0 ]]
then
echo "Error shutting down scoring server:"
echo "Check $PMAX_BIN/logs/pmax.log fileD"
else
echo "The scoring server is now shut down:"
fi
}

function Restart
{
Shutdown
#sleep 35
nohup ./pmax-ctl startup &
echo "Waiting for DBFEs"
sleep 120
if [[ ! -f $PMAX_HOME/bin/logs/pmax.log ]]
then
echo "Error starting scoring server:"
echo "No PMAX_BIN/logs/pmax.log file is found"
else
while [[ $dbfe -ne 47 && $time_cnt -le 10 ]]
do
sleep 60
./dbfe_cnt
dbfe=$?
time_cnt=$time_cnt+1
done
fi

if [[ -f $PMAX_HOME/bin/logs/pmax.log && $dbfe -ne 47 ]]
then
# echo "The scoring server is restarting:" >/dev/null 2<&1
echo "Error starting scoring server:"
echo "Check PMAX_BIN/logs/pmax.log"
elif [[ -f $PMAX_HOME/bin/logs/pmax.log && $dbfe -eq 47 ]]
then
echo "The scoring server has restarted:"
# echo "The DBFE count has reached to the maximum"
fi
}

case "$1" in
shutdown)
            Shutdown
                           ;;
restart)    Restart
                           ;;
      *)    echo "Invalid option\n"
            echo "Type Shutdown or Restart\n"
                        ;;
esac
exit 0
