#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the total number of data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{
open (FH,'+>',"NMON$y.txt") || die "file not found :$!";	

# Print Header Line
printf  FH "B000000000%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $rec_cnt; 
&datagen();
# print the footer line.
printf FH "E999999999%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $rec_cnt; 
close (FH);

$command = "touch NMON$y.txt.NMON20; NMON$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;
}

sub datagen() {

for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);
my $new_digest = sha1_hex($i+1);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;
my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..16)];
my $first_name = ucfirst substr($string,0,6);
my $middle_name =  ucfirst substr ($string,7,1);
my $last_name = ucfirst substr ($string,8,15);
my $full_name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
	$customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
	$clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $newGivenName = $full_name;
    $newSurname = $last_name;
	$newMiddleName = $middle_name;
    $pan = int(rand(10**15-1))+(10**15-1);
    $newPostalCode = int(rand(10**9-1))+(10**9-1);
	$newCode2 = int(rand(10**2-1))+(10**5-1);
    $userData09 = int(rand(10**14-1))+(10**14-1);
    $nonmonCode = int(rand(10**3-1))+(10**3-1);
	$customerAcctNumber = $digest;
	$newCustomerAcctNumber = $new_digest;
	$transactionDate = $current_date;
	$transactionTime = $timestring;
	$oldCountryCode = '191';
	
# Formatting the output print.	
  printf FH "DUD17DF26        NMON20  2.0  %-16.16s%-8.8s%-6.6s%03d%06d%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%-4.4sBAA%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s0C%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%019d%019d%-3.3s%013d%010d%010d%010d%010d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
  $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,$userCode4,$userCode5,$userData01,$userData02,$userData03,$userData04,$userData05,$userData06,$userData07,$userData08,$userData09,$userData10,$userData11,$userData12,$userData13,$userData14,$userData15,$RESERVED_01;                                                                                                                                                  
  $rec_cnt++;
  }
   }
