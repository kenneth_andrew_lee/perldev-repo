#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

# Generating unique random hexadecimal number using sha1_hex.
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# Getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the total number of data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{

# my $rec_cnt = 0;
open (FH,'+>',"CIS$y.txt") || die "file not found :$!";	

# Print Header Line
printf FH "B000000000%-1836.1836s2.0CIS20   PMAX  %-08.8d\n", '' x 1836, $rec_cnt; 
&datagen();
# Print the footer.
printf FH "E999999999%-1836.1836s2.0CIS20   PMAX  %-08.8d\n", '' x 1836, $rec_cnt; 
close (FH);

$command = "touch CIS$y.txt.CIS20; CIS$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;

}

sub datagen {

# $rec_cnt = 0;
for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;
my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..16)];
my $first_name = ucfirst substr($string,0,6);
my $middle_name =  ucfirst substr ($string,7,1);
my $last_name = ucfirst substr ($string,8,15);
my $full_name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function.
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
    $givenName = $full_name;
    $surname = $last_name;
    $homePhone = int(rand(10**9-1))+(10**9-1);
    $postalCode = int(rand(10**5-1))+(10**5-1);
    $userData01 = int(rand(10**15-1))+(10**15-1);
    $customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
	$middleName = $middle_name;
	$customerAcctNumber = $digest;
	$countryCode = '191';
	$dateAtAddress = $current_date;
	$employmentStartDate = $current_date;
	$birthDate = $current_date;
	$creditScoreDate = $current_date;

# Formatting the output print.
  printf FH "DUD17DF26        CIS20   2.0  %-16.16s%-8.8s%-6.6s%03d%06d%-20.20s%-40.40s%-32.32sIV%-8.8s%05d%-30.30s%-30.30s%-60.60sMr.       Sr.       %-60.60s%-3.3s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3sO%-8.8sM%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3s400%-8.8s%-4.4s%-4.4s%016.16d%-3.3s%13.13d%-24.24s%-24.24s%-24.24s%-24.24sH%-40.40sM%-8.8s%-3.3s%-3.3s%-16.16s%-3.3s%-16.16s%-3.3s%-8.8s%-16.16s%-3.3s%-16.16s%-3.3sMS%2d%4d%-8.8s%-20.20sN%-4.4sYY%4d%4d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s\n",
  $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$relationshipStartDate,$numberOfAccounts,$givenName,$middleName,$surname,$preferredGreeting,$preferredLanguage,$mothersMaidenName,$householdName,$streetline1,$streetLine2,$streetline3,$streetLine4,$city,$stateProvince,$postalCode,$countryCode,$dateAtAddress,$secondaryAddrStreetLine1,$secondaryAddrStreetLine2,$secondaryAddrStreetLine3,$secondaryAddrStreetLine4,$secondaryAddrCity,$secondaryAddrStateProvince,$secondaryAddrPostalCode,$secondaryAddrCountryCode,$employer,$workAddrStreetLine1,$workAddrStreetLine2,$workAddrStreetLine3,$workAddrStreetLine4,$workAddrCity,$workAddrStateProvince,$workAddrPostalCode,
  $workAddrCountryCode,$emplymentStartDate,$employerMcc,$occupationCode,$income,$currencyCode,$currencyConversionRate,$homePhone,$secondaryPhone,$workPhone,$mobilePhone,$emailAddress,$birthDate,$birthCountry,$citizenshipCountry,$nationalId,$nationalIdCountry,$passportNumber,$passportCountry,$passportExpirationDate,$driversLicenseNumber,$driversLicenseCountry,$taxId,$taxIdCountry,$numberOfDependents,$creditScore,$creditScoreDate,$creditScoreSource,$creditRating,$behaviorScore1,$behaviorScore2,$segmentId1,$segmentId2,$segmentId3,$segmentId4,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,$userCode4,$userCode5,$userData01,$userData02,$userData03,$userData04,$userData05,$userData06,$userData07,$userData08,
  $userData08,$userData09,$userData10,$userData11,$userData12,$userData13,$userData14,$userData15;
  $rec_cnt++;
}
 }
 