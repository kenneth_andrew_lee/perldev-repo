#!/usr/bin/perl

#use strict;
use warnings;

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

#use Switch;
use Data::Dumper;
use DBI;

my $var1;
my $var2;
my $var3;
my $db    = DBI->connect( "dbi:Oracle:fal1dbq", "fal1admin", "fal1admin" );
my $found = 0;
my %newh  = ();
my %oldh = ();
my $key1;
my $key2;
my $filename = "APCH.PRC595.G0021.CAR";
my $clientIdFromHeader;


my %field_to_change_type = (
                        'customerIdFromHeader'  => 'cusProfile',
                        'customerAcctNumber'    => 'accProfile',
                        'pan'                   => 'panProfile',
                        'paymentInstrumentId'   => 'payInsProfile',
                        'oldEntityName'         => 'UDProfile',
                        'birthDate'             => 'cusPerInfo',
                        'birthCountry'          => 'cusPerInfo',                        
                        'gender'                => 'cusPerInfo',
                        'numberOfDependents'    => 'cusPerInfo',
                        'maritalStatus'         => 'cusPerInfo',
                        'educationalStatus'     => 'cusPerInfo',
                        'preferredGreeting'     => 'cusPerInfo',
                        'preferredLanguage'     => 'cusPerInfo',
                        'givenName'             => 'cusName',
                        'middleName'            => 'cusName',
                        'surname'               => 'cusName',
                        'title'                 => 'cusName',
                        'suffix'                => 'cusName',
                        'M1'                    => 'militaryStatus',
                        'M0'                    => 'militaryStatus',
                        'nationalId'            => 'nationalInfo',
                        'nationalIdCountry'     => 'nationalInfo',
                        'citizenshipCountry'    => 'nationalInfo',
                        'driversLicenseNumber'  => 'driversLicense',
                        'driversLicenseCountry' => 'driversLicense',
                        'taxId'                 => 'taxIdentification',
                        'taxIdCountry'          => 'taxIdentification',
                        'passportNumber'        => 'passportInfo',
                        'passportCountry'       => 'passportInfo',
                        'passportExpirationDate'=> 'passportInfo',
                        'streetLine1'           => 'addressInfo',
                        'streetLine2'           => 'addressInfo',  
                        'streetLine3'           => 'addressInfo',
                        'streetLine4'           => 'addressInfo',
                        'city'                  => 'addressInfo',
                        'stateProvince'         => 'addressInfo',
                        'postalCode'            => 'addressInfo',
                        'countryCode'           => 'addressInfo',
                        'dateAtAddress'         => 'addressInfo',
                        'residenceStatus'       => 'addressInfo',
                        'homePhone'             => 'telephoneInfo',
                        'preferredPhone'        => 'telephoneInfo',
                        'secondaryPhone'        => 'secTelephoneNumber',
                        'workPhone'             => 'workTelephoneNumber',
                        'mobilePhone'           => 'mobileTelephoneNumber',
                        'emailAddress'          => 'cusEmailInfo',
                        'pefc'                  => 'cusBankStatus',
                        'ofac'                  => 'cusBankStatus',
                        'numberOfAccounts'      => 'cusBankStatus',
                        'relationshipStartDate' => 'cusBankStatus',
                        'vipType'               => 'cusBankStatus',
                        'mothersMaidenName'     => 'cusAuthCredentials',
                        'openDate'              => 'otherAcctInfo',
                        'applicationReferenceNumber'=> 'otherAcctInfo',
                        'openChannel'           => 'otherAcctInfo',
                        'campaignDate'          => 'otherAcctInfo', 
                        'portfolio'             => 'acctPortfolio',
                        'accountServiceType'    => 'acctPortfolio',
                        'status'                => 'acctStatus',
                        'statusDate'            => 'acctStatus',
                        'overLimitFlag'         => 'acctStatus',
                        'numberOfCyclesInactive'=> 'acctStatus',
                        'creditLimit'           => 'acctCreditLimit',
                        'currencyCode'          => 'acctCreditLimit',
                        'currencyConversionRate'=> 'acctCreditLimit',
                        'dailyCashLimit'        => 'acctDailyCashLimit',
                        'currencyCode'          => 'acctDailyCashLimit',                        
                        'currencyConversionRate'=> 'acctDailyCashLimit',
                        'dailyPosLimit'         => 'acctDailyPosLimit',
                        'currencyCode'          => 'acctDailyPosLimit',
                        'currencyConversionRate'=> 'acctDailyPosLimit',
                        'dailyTotalLimit'       => 'acctDailyTotalLimit',
                        'currencyCode'          => 'acctDailyTotalLimit',
                        'currencyConversionRate'=> 'acctDailyTotalLimit',
                        'pan'                   => 'issueCard',
                        'paymentInstrumentId'   => 'issueCard',
                        'lastIssueDate'         => 'issueCard',
                        'plasticIssueType'      => 'issueCard',
                        'expirationDate'        => 'issueCard',
                        'nameOnInstrument'      => 'issueCard',
                        'mediaType'             => 'issueCard',
                        'issuingCountry'        => 'issueCard',
                        'numberOfPaymentIds'    => 'panInfo',
                        'panOpenDate'           => 'panInfo',
                        'memberSinceDate'       => 'panInfo',
                        'type'                  => 'panInfo',
                        'subType'               => 'panInfo',
                        'association'           => 'panInfo',
                        'incentive'             => 'panInfo',
                        'category'              => 'panInfo',
                        'cardholderCity'        => 'cardholderLocation',
                        'cardholderStateProvince'=> 'cardholderLocation',
                        'cardholderPostalCode'  => 'cardholderLocation',
                        'cardholderCountryCode' => 'cardholderLocation',
                        'status'                => 'PANStatus',
                        'statusDate'            => 'PANStatus',
                        'activeIndicator'       => 'cardActivation',
                        'creditLimit'           => 'panCreditLimit',
                        'currencyCode'          => 'panCreditLimit',
                        'currencyConversionRate'=> 'panCreditLimit',
                        'dailyCashLimit'        => 'panDailyCashLimit',
                        'currencyCode'          => 'panDailyCashLimit',                     
                        'currencyConversionRate'=> 'panDailyCashLimit',
                        'dailyPosLimit'         => 'panDailyPosLimit',
                        'currencyCode'          => 'panDailyPosLimit',
                        'currencyConversionRate'=> 'panDailyPosLimit',
                                                
                                                                
         );

my %change_type_to_code = (
                        'cusProfile'           => '0001',
                        'accProfile'           => '0002',
                        'panProfile'           => '0003',
                        'payInsProfile'        => '0004',
                        'UDProfile'            => '0005',
                        'cusPerInfo'           => '1000', 
                        'cusName'              => '1001',
                        'militaryStatus'       => '1005',
                        'nationalInfo'         => '1100',
                        'driversLicense'       => '1104',
                        'taxIdentification'    => '1105',
                        'passportInfo'         => '1106',  
                        'addressInfo'          => '1150',
                        'telephoneInfo'        => '1207',
                        'secTelephoneNumber'   => '1208',
                        'workTelephoneNumber'  => '1209',
                        'mobileTelephoneNumber'=> '1210',
                        'cusEmailInfo'         => '1250',
                        'cusBankStatus'        => '1300',
                        'cusAuthCredentials'   => '1350',
                        'otherAcctInfo'        => '2011',
                        'acctPortfolio'        => '2020',
                        'acctStatus'           => '2030',
                        'acctCreditLimit'      => '2201',
                        'acctDailyCashLimit'   => '2203',
                        'acctDailyPosLimit'    => '2204',
                        'acctDailyTotalLimit'  => '2210',
                        'issueCard'            => '3000',
                        'panInfo'              => '3010',
                        'cardholderLocation'   => '3100',
                        'PANStatus'            => '3102',
                        'cardActivation'       => '3104',
                        'panCreditLimit'       => '3201',
                        'panDailyCashLimit'    => '3203',
                        'panDailyPosLimit'     => '3204',
         );


open( IFH, '<', $filename ) || die "Could not find the file: $!";
while ( my $rec = <IFH> ) {
    if ( $rec =~ /^A/ or $rec =~ /^U/ ) {
        chomp($rec);

        $newh{function_code}      = substr( $rec, 0, 1 );     # $function_code
        $newh{customerAcctNumber} = substr( $rec, 1, 19 );    # $acct_nbr

        #$newh{customerIdFromHeader}   = substr($rec,1,19);      #user_data_06
        $newh{pan} = substr( $rec, 1, 19 );                   #acct_nbr

        $newh{givenName}         = substr( $rec, 19,  30 );   # $cus_nm
        $newh{nameOnInstrument}  = substr( $rec, 19,  30 );   # $cus_nm
        $newh{surname}           = substr( $rec, 50,  25 );   # $cus_lst_nm
        $newh{userData12_cis}    = substr( $rec, 75,  30 );   # $cus_co_nm
        $newh{mothersMaidenName} = substr( $rec, 105, 30 );   # $cus_md_nm
        $newh{streetLine1}       = substr( $rec, 135, 30 );   # $cus_adr_1
        $newh{streetLine2}       = substr( $rec, 165, 30 );   # $cus_adr_2
        $newh{city}              = substr( $rec, 195, 30 );   # $cus_city
        $newh{stateProvince}     = substr( $rec, 225, 2 );    # $cus_st
        $newh{cardholderStateProvince} = substr( $rec, 225, 2 );    # $cus_st
        $newh{postalCode}              = substr( $rec, 227, 9 );    #$cus_zip
        $newh{cardholderPostalCode}    = substr( $rec, 227, 9 );    #$cus_zip
        $newh{homePhone}       = substr( $rec, 236, 16 );    #$cus_hm_phon1
        $newh{secondaryPhone}  = substr( $rec, 252, 16 );    #$cus_hm_phon2
        $newh{workPhone}       = substr( $rec, 268, 16 );    #$cus_wrk_phon1
        $newh{mobilePhone}     = substr( $rec, 284, 16 );    #$cus_wrk_phon2
        $newh{birthDate}       = substr( $rec, 300, 8 );     #$cus_birth_dt1
        $newh{userData07_cis}  = substr( $rec, 308, 8 );     #$cus_birth_dt2
        $newh{nationalId}      = substr( $rec, 316, 11 );    #$cus_ssn_1
        $newh{taxId}           = substr( $rec, 316, 11 );    #$cus_ssn_1
        $newh{userData08_cis}  = substr( $rec, 327, 11 );    #$cus_ssn_2
        $newh{openDate}        = substr( $rec, 338, 8 );     #$crd_opn_dt
        $newh{panOpenDate}     = substr( $rec, 338, 8 );     #$crd_opn_dt
        $newh{expirationDate}  = substr( $rec, 346, 8 );     #$crd_exp_dt
        $newh{memberSinceDate} = substr( $rec, 354, 8 );     #$crd_mbr_snc_dt
        $newh{relationshipStartDate}= substr( $rec, 354, 8 ); #$crd_mbr_snc_dt
        $newh{lastIssueDate}   = substr( $rec, 362, 8 );     #$lst_iss_dt
        $newh{dailyTotalLimit} = substr( $rec, 370, 13 );    #$avail_cr
        $newh{dailyCashLimit}  = substr( $rec, 370, 13 );    #$avail_cr
        $newh{dailyPosLimit}   = substr( $rec, 370, 13 );    #$avail_cr
        $newh{creditLimit}     = substr( $rec, 383, 10 );    #$cr_lmt
        $newh{activeIndicator} = substr( $rec, 393, 1 );     #$crd_actv_ind
        $newh{status}          = substr( $rec, 394, 6 );     #$crd_blk_ind
        $newh{plasticIssueType} = substr( $rec, 400, 2 ); #$plastic_issue_type
        $newh{portfolio}          = substr( $rec, 402, 14 );    #$portfolio
        $newh{clientIdFromHeader} = substr( $rec, 416, 14 );    #$client_id
        $clientIdFromHeader       = substr( $rec, 416, 14 );
        $newh{userData01_cis}     = substr( $rec, 430, 6 );     #user_data_01
        $newh{userData02_cis}     = substr( $rec, 436, 6 );     #user_data_02
        $newh{userData03_cis}     = substr( $rec, 442, 10 );    #user_data_03
        $newh{userData04_cis}     = substr( $rec, 452, 10 );    #user_data_04
        $newh{userData05_cis}     = substr( $rec, 462, 15 );    #user_data_05

        $newh{userData06_cis} = substr( $rec, 477, 20 );        #user_data_06

        $newh{emailAddress} = substr( $rec, 497, 40 );          #user_data_07
    }
}


$var1 = $newh{customerAcctNumber};
$var2 = $newh{clientIdFromHeader};
$var3 = $newh{clientIdFromHeader};

$var1 =~ s/\s+$//;
$var2 =~ s/\s+$//;
$var3 =~ s/\s+$//;

my $cis_code = 0;
my $ais_code = 0;
my $pis_code = 0;

my $ud06 = $newh{userData06_cis};
if ( $ud06 =~ m/CU/ ) {
    $newh{customerIdFromHeader} = substr($ud06,2,19);
} else {
    $newh{customerIdFromHeader} = "P".substr($rec,1,18);
} 

print $var1, $var2, $var3, "\n";


my $cis = $db->prepare(
    "select 
        account_reference_xid, 
        first_name, 
        last_name, 
        user_data_12_strg, 
        mothers_maiden_name, 
        address_line_1_strg, 
        address_line_2_strg, 
        city_name, 
        country_region_xcd, 
        postal_xcd, 
        home_phone_num, 
        phone2_num,
        work_phone_num, 
        cell_phone_num, 
        customer_birth_dt, 
        user_data_7_strg, 
        national_xid, 
        user_data_8_strg, 
        client_xid,
        user_data_1_strg, 
        user_data_2_strg, 
        user_data_3_strg, 
        user_data_4_strg, 
        user_data_5_strg, 
        user_data_6_num,
        customer_xid, 
        email_address, 
        relation_start_dt, tax_xid 
    from 
        fraud_customer_information
            where customer_xid in (
                select customer_xid from fraud_account_summary
                where account_reference_xid = ? and client_xid = ?
            ) and client_xid = ?"
);

$cis->execute( $var1, $var2, $var3 );
 
while ( my @rows = $cis->fetchrow_array() ) {
    #foreach ( my $rows ) {
        #print "Found a record, loading oldh hash\n\n";
        #print Dumper(@rows),"\n";
        $oldh{customerAcctNumber}    = $rows[0];
        $oldh{givenName}             = $rows[1];
        $oldh{surname}               = $rows[2];
        $oldh{userData12}            = $rows[3];
        $oldh{mothersMaidenName}     = $rows[4];
        $oldh{streetLine1}           = $rows[5];
        $oldh{streetLine2}           = $rows[6];
        $oldh{city}                  = $rows[7];
        $oldh{stateProvince}         = $rows[8];
        $oldh{postalCode}            = $rows[9];
        $oldh{homePhone}             = $rows[10];
        $oldh{secondaryPhone}        = $rows[11];
        $oldh{workPhone}             = $rows[12];
        $oldh{mobilePhone}           = $rows[13];
        $oldh{birthDate}             = $rows[14];
        $oldh{userdata07}            = $rows[15];
        $oldh{nationalId}            = $rows[16];
        $oldh{userdata08}            = $rows[17];
        $oldh{clientIdFromHeader}    = $rows[18];
        $oldh{userData01_cis}        = $rows[19];
        $oldh{userData02_cis}        = $rows[20];
        $oldh{userData03_cis}        = $rows[21];
        $oldh{userData04_cis}        = $rows[22];
        $oldh{userData05_cis}        = $rows[23];
        $oldh{userData06_cis}        = $rows[24];
        $oldh{customerIdFromHeader}  = $rows[25];
        $oldh{emailAddress}          = $rows[26];
        $oldh{relationshipStartDate} = $rows[27];
        $oldh{taxId}                 = $rows[28];
    #}
}

# Close Connection
$db->disconnect();


my %tochangefields = ();
my $newkey;
my $newvalue;


#Cleanup
foreach $newkey ( keys(%newh) ) {
    $newh{$newkey} =~ s/\s+$//;
    $newh{$newkey} =~ s/^\s+//;
}

while ( ( $newkey, $newvalue ) = each %newh ) {
    if ( $newkey !~ /^(userData|function_code|clientIdFromHeader)/ ) {
        if ( defined($oldh{$newkey}) && ($oldh{$newkey} =~ /^\d*\.{0,1}\d+$/ || $newvalue =~ /^\d*\.{0,1}\d+$/ )) {
            if ( $oldh{$newkey} != $newvalue ) {
                my $codekey = $field_to_change_type{$newkey};
                my $code    = $change_type_to_code{$codekey};
                push @{ $tochangefields{$code}{$newkey} }, $oldh{$newkey},
                    $newvalue;
            }
        } else {
            if ( defined($oldh{$newkey}) && ($oldh{$newkey} ne $newvalue )) {
                my $codekey = $field_to_change_type{$newkey};
                my $code    = $change_type_to_code{$codekey};
                push @{ $tochangefields{$code}{$newkey} }, $oldh{$newkey},
                    $newvalue;

#print STDERR "Looking up \"$codekey\" and \"$newkey\" in alpha match, found " . $change_type_to_code{$codekey} . "\n";
            }
        }
    }
}

my $nonmonCode;

my %type_pos = ( 'old' => 0, 'new' => 1 );

my @keylist = (
    "customerIdFromHeader", "characterValue",
    "city",                 "code1",
    "code2",                "code3",
    "countryCode",          "date1",
    "date2",                "emailAddress",
    "entityName",           "givenName",
    "id1",                  "id2",
    "indicator1",           "indicator2",
    "indicator3",           "indicator4",
    "middleName",           "monetaryValue",
    "numericValue1",        "numericValue2",
    "phone1",               "phone2",
    "postalCode",           "stateProvince",
    "streetLine1",          "streetLine2",
    "streetLine3",          "streetLine4",
    "suffix",               "surname",
    "text"
);


print "keys and newh{key} and oldh{key} values:\n";
for $key (@keylist) {
    if ($key ne '') {
        $uckey = ucfirst $key;

        #if ($key eq "streetLine1") {
        #    print "error, error, error\n";
        #}
        
        # concatinate '$new' with 'StreetLine1'
        $uckey = '$new' . $uckey;

        #print "\$uckey = $uckey\n";
        #if ($key eq "streetLine1") {
        #    print "error, error, error\n";
        #}

        #print "\$key = $key, \$uckey = $uckey\n";


        #if ($key eq "streetLine1") {
        #    print "error, error, error\n";
        #}

        #if ($key eq "streetLine1") {
        #    $newStreetLine1 = $oldh{$key} if defined ($oldh{$key});
        #    print "\$newStreetLine1  = $newStreetLine1\n"; 
        #}
        
        #if ($key eq "streetLine1") {
        #    my $junk = "LANE 34";
        #    # $newStreetLine1 = 'LANE LANE';  <== what we want.
        #    print eval "$uckey = $junk", "\n";
        #}

        print "eval happens here - ", eval "$uckey = '$oldh{$key}'", "\n" if defined ($oldh{$key});
        #print "error message - " . $@, "\n";
        #if ($key eq "streetLine1") {
        #    my $junk = $oldh{$key};
        #    print eval "$uckey = '$junk'", "\n";
        #}

        # safe up to this point
        #my $junk = $oldh{$key}
        #print eval "$uckey = $junk", "\n";
        #print eval "$uckey = $oldh{$key}" if defined ($oldh{$key});
        
        #print "error message - " . $@, "\n";
        #if ($key eq "streetLine1") {
        #    print "error, error, error\n";
        #}

        #print $uckey, "\n";
        #print $newh{$key} if defined($newh{$key});
        #print "\n";
    }
}




__END__ 

for $key (@keylist) {
    my $ucKey = ucfirst $key; 
    eval "\$new$ucKey = $oldh{$key}";
}


print scalar keys %newh;
print " keys for \%newh:\n";
for $key (keys %newh) {
    print $key, "\n";    
}

print scalar keys %oldh;
print " keys for \%oldh:\n";
for $key (keys %oldh) {
    print $key, "\n";    
}


        print "\$oldh{customerAcctNumber} = $oldh{customerAcctNumber}", "\n"; 
        print "\$rows[0] = $rows[0]\n\n";



 while (@data = $sth->fetchrow_array()) {
            my $firstname = $data[1];




$oldh{openDate}            = ' ';
$oldh{dailtTotalLimit}     = ' ';
$oldh{status}              = ' ';
$oldh{portfolio}           = ' ';
$oldh{customerIdFromHeader}= ' ';
$oldh{dailyPosLimit}       = ' ';
$oldh{dailyCashLimit}      = ' ';
$oldh{customerAcctNumber}  = ' ';

$oldh{expirationDate}      = ' ';
$oldh{memberSinceDate}     = ' ';
$oldh{lastIssueDate}       = ' ';
$oldh{creditLimit}         = ' ';
$oldh{activeIndicator}     = ' ';
$oldh{plasticIssueType}    = ' ';
$oldh{nameOnInstrument}    = ' ';
$oldh{cardholderStateProvince} = ' ';
$oldh{cardholderPostalCode}= ' ';
$oldh{panOpenDate}         = ' '; 
$oldh{dailyPosLimit}       = ' ';
$oldh{dailyCashLimit}      = ' ';
$oldh{status}              = ' ';
$oldh{customerIdFromHeader}= ' ';






#$print $tgs{'ebooks'}{'linux 101'};
for $key1 (keys %oldh) {
   print "key1 = $key1\n";
   for $key2 (keys %{$oldh{$key1}}) {
       print "key2 = $key2\n";
   }
}



my $nonmonCode;

my %type_pos = ( 'old' => 0, 'new' => 1 );

my @keylist = (
    "customerIdFromHeader", "characterValue",
    "city",                 "code1",
    "code2",                "code3",
    "countryCode",          "date1",
    "date2",                "emailAddress",
    "entityName",           "givenName",
    "id1",                  "id2",
    "indicator1",           "indicator2",
    "indicator3",           "indicator4",
    "middleName",           "monetaryValue",
    "numericValue1",        "numericValue2",
    "phone1",               "phone2",
    "postalCode",           "stateProvince",
    "streetLine1",          "streetLine2",
    "streetLine3",          "streetLine4",
    "suffix",               "surname",
    "text"
);

foreach my $key (@keylist) {
    my $ucKey = ucfirst $key;
    eval "\$new$ucKey = $oldh{$key}";
}
my $newCustomerId = $newCustomerIdFromHeader;
