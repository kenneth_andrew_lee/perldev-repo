#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the number of total data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{
open (FH,'+>',"FRD$y.txt") || die "file not found :$!";	

# Print the header Line
printf FH "B000000000%-779.779s1.1FRD15   PMAX  %-08.8d\n", '' x 779, $rec_cnt; 
&datagen();
# print the footer line.
printf FH "E999999999%-779.779s1.1FRD15   PMAX  %-08.8d\n", '' x 779, $rec_cnt; 
close (FH);
  
$command = "touch FRD$y.txt.FRD15; FRD$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;
}

sub datagen() {
  
for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;

my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..40)];
my $first_name = ucfirst substr($string,0,15);
my $middle_name =  ucfirst substr ($string,16,8);
my $last_name = ucfirst substr ($string,24,15);
my $name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
	$clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $customerAcctNumber = $digest;
	$activeDate = $current_date;
	$activeTime = $timestring;
    $endDate = $current_date;
	$endTime = $timestring;
	$key = '#' x 60;
	$value = '$' x 255;
	
	
# Formatting the output print.	
 printf FH "DUD17DF26FRD15   1.5  %-16.16s%-8.8s%-6.6s%03.3d%06.6d%-20.20s%-40.40s%-32.32s%-19.19s%-30.30sTRAN1 %-8.8s%-6.6s%-8.8s%-6.6s%-8.8s%-6.6s%-8.8s%-6.6s1 11 FPINCN%-32.32s%-32.32s%-8.8s%-8.8s%-6.6s%03dA%-8.8sAD%-20.20s%-40.40s%-20.20s%-20.20s%-40.40s%-20.20s%-3.3s%-19.19s%-3.3s%-13.13s%-3.3s%-10.10s%-20.20s%-4.4s%-40.40sEP%-32.32sI%-4.4s%-1.1s%-3.3s%-3.3s%-10.10s%-100.100s\n", 
 $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$pan,$paymentInstrumentId,$caseCreationDate,$caseCreationTime,$dateOfFirstIncident,$timeOfFirstIncident,$dateOfLastIncident,
 $timeOfLastIncident,$blockDate,$fiTransactionIdReference,$externalTransactionIdReference,$recordTypeReference,$transactionDate,$transactionTime,$transactionTimeMilliseconds,$postDate,$debitCustomerId,$debitAcctNumber,$debitAcctBranchId,$creditCustomerId,$creditAcctNumber,$creditBranchId,$RESERVED_01,$transactionAmount,$transactionCurrencyCode,$transactionCurrencyConversionRate,$transactionCountryCode,$transactionPostalCode,$merchantId,$mcc,$deviceId,$transactionReferenceNumber,$nonmonCode,$userIndicator01,$userCode1,$userCode2,$userData01,$RESERVED_02;
 $rec_cnt++;
}
} 