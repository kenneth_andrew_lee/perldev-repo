#!/usr/bin/perl

use lib "$ENV{HOME}/perl5/lib/perl5";

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

use DBI;
my $var;
my $db = DBI->connect( "dbi:Oracle:fhdbd", "reporter", "reporter" );

my $ext_rec_cnt = 0;

open( EXT, '+>', "EXTMSG$$.txt" ) || die "file not found :$!";

# Print Header Line
printf EXT "B000000000%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $ext_rec_cnt;

$current_time = strftime "%H%M%S", localtime;
$gmt_date     = strftime "%Y%m%d", gmtime;
$gmt_time     = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr( gettimeofday, 11, 2 );
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = ( substr( $gmt_time, 0, 2 ) - substr( $current_time, 0, 2 ) );

if ( $gmt_offset_diff > 12 ) {
    $gmtOffset = $gmt_offset_diff - 24;
} else {
    $gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;

$date_1 = '20151022093000';
$date_2 = '20151022100000';
$date_3 = '20150503';

#connecting to databse and collecting $customerIdFromHeader
#$var = $customerAcctNumber;
#$var = "ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4";
#my $var = $db->quote("0000000000000000");

my $extmsg = $db->prepare(
    "select  
  MAINT_PAN, 
  PROCESSOR,
  RF_DATA_AFTER_RF_BASE,
  RF_DATA_AFTER_RF_OVRD,
  RF_DATA_AFTER_RF_OVRD_FROM_DT,
  RF_DATA_AFTER_RF_OVRD_TO_DT,    
  RF_NOTE_AFTER_RF_BASE_NOTE,
  RF_NOTE_AFTER_RF_OVRD_NOTE ,
  RECORD_TSTAMP,
  to_char(sysdate,'yyyymmddhh24miss') as run_date,  
  MAINT_FUNCT
  FROM CNX1310_RISK_FACTOR_CHANGES rf 
INNER JOIN ( 
  --find PANs having open cases 
  SELECT 
    PRC,
    PAN
  FROM (
    --find case activity records for the changed risk factor accounts
    SELECT 
      pr.PRC 
      ,ca.CASE_NUMBER 
      ,ca.CASE_STATUS 
      ,ca.FALCON_LAST_UPDATED_DTTM 
      ,pi.PAN 
      ,ROW_NUMBER() OVER (PARTITION BY pr.PRC, ca.CASE_NUMBER ORDER BY ca.FALCON_LAST_UPDATED_DTTM DESC) AS ROW_NUMBER 
    FROM CASE_ACTIVITIES ca 
    INNER JOIN PAYMENT_INSTRUMENTS pi 
      ON pi.ID = ca.PAYMENT_INSTRUMENT_ID 
    INNER JOIN ACCOUNTS ac 
      ON ac.ID = pi.ACCOUNT_ID 
    INNER JOIN CUSTOMERS cs 
      ON cs.ID = ac.CUSTOMER_ID 
    INNER JOIN PROCESSORS pr 
      ON pr.ID = cs.PROCESSOR_ID 
    INNER JOIN (
      --select risk factor records changed since the last cycle
      SELECT DISTINCT 
        rf.PROCESSOR 
        ,rf.MAINT_PAN 
      FROM CNX1310_RISK_FACTOR_CHANGES rf 
      WHERE rf.RECORD_TSTAMP BETWEEN TO_DATE(?,'yyyymmddhh24miss') AND TO_DATE(?,'yyyymmddhh24miss') --replace hard coded dates with variables, start date to end date 
      ) RISK_FACTOR_CHANGES 
    ON RISK_FACTOR_CHANGES.MAINT_PAN = pi.PAN 
    AND RISK_FACTOR_CHANGES.PROCESSOR = pr.PRC
    --WHERE ca.FALCON_LAST_UPDATED_DTTM <= TO_DATE(?,'yyyymmddhh24miss') --replace hard coded date with END date variable 
    --AND ca.CASE_NUMBER = 1 --remove this line, added just to make debugging easier
  ) 
  WHERE ROW_NUMBER = 1 AND CASE_STATUS <> 'Closed' --most recent case activity record, but only select open cases
) PANS_WITH_CASES 
  ON PANS_WITH_CASES.PRC = rf.PROCESSOR
  AND PANS_WITH_CASES.PAN = rf.MAINT_PAN              
"
);

$extmsg->execute( $date_1, $date_2 );

while ( my @rows = $extmsg->fetchrow_array() ) {
    foreach ( my $rows ) {
        $customerAcctNumber   = $rows[0];
        $customerIdFromHeader = $rows[0];
        $serviceId            = $rows[0];
        $clientIdFromHeader   = $rows[1];
        $userData01           = $rows[2];
        $userData02           = $rows[3];
        $userData06           = $rows[4];
        $userData07           = $rows[5];
        $userData33           = $rows[6];
        $userData34           = $rows[7];
    }

    $ext_rec_cnt_1 = $ext_rec_cnt + 1;
    my $externalTransactionId
        = substr( $clientIdFromHeader, 0, 6 )
        . $transactionDate
        . $transactionTime . "EXT"
        . sprintf( "%09d\n", $ext_rec_cnt_1 );

    # Formatting the output print.
    printf EXT
        "DUD17DF26        EXT10   1.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s\n",
        $clientIdFromHeader, $recordCreationDate, $recordCreationTime,
        $recordCreationMilliseconds,
        $gmtOffset,             $customerIdFromHeader, $customerAcctNumber,
        $externalTransactionId, $serviceId,            $transactionDate,
        $transactionTime,       $validity,
        $entityType, $extSource, $notificationName, $notificationStatus,
        $score1, $score2, $score3, $userData01, $userData02, $userData03,
        $userData04,
        $userData05, $userData06, $userData07, $userData08, $userData09,
        $userData10, $userData11, $userData12, $userData13, $userData14,
        $userData15,
        $userData16, $userData17, $userData18, $userData19, $userData20,
        $userData21, $userData22, $userData23, $userData24, $userData25,
        $userData26,
        $userData27, $userData28, $userData29, $userData30, $userData31,
        $userData32, $userData33, $userData34;
    $ext_rec_cnt++;
}

# Print the footer.
printf EXT "E999999999%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $ext_rec_cnt;
close(EXT);

if ( $ext_rec_cnt == 0 ) {
    unlink "/$home_dir/base/pmax/bin/LANDINGZONE/EXTMSG$$.txt";
} else {
    #/$ENV/PIS.txt.PIS12;
    $command = "touch /$home_dir/base/pmax/bin/LANDINGZONE/EXTMSG$$.txt.EXT10; EXT10$$ touch status 2>&1";
    system(`$command`);
}
