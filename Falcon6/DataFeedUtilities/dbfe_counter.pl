#!/usr/bin/perl

use strict;
use warnings;

my %items;

while (<>) {
        if (/client=DBFE(\d{1,2})/) {
                $items{$1}++;
        }
}
print "Number of DBFEs: ", scalar keys %items,"\n";

# open $fh..
# while (1) {
#     while (my $line = <$fh>) {
#         # $line ...
#     }
#     # eof reached on $fh
#     sleep 1;
#     # clear eof flag on $fh
#     seek($fh, 0, 1);      
# }
