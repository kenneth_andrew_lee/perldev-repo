#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the total number of data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{
open (FH,'+>',"DBTRAN$y.txt") || die "file not found :$!";	

# Print the header Line.
printf  FH "B000000000%-944.944s2.5DBTRAN25PMAX  %-08.8d\n", '' x 944, $rec_cnt; 
&datagen();
# print the footer line.
printf FH "E999999999%-944.944s2.5DBTRAN25PMAX  %-08.8d\n", '' x 944, $rec_cnt; 
close (FH);

$command = "touch DBTRAN$y.txt.DBTRAN25; DBTRAN$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;
}

sub datagen() {

for ( my $i = $x; $i < $c; $i++ ) {


# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;
my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..16)];
my $first_name = ucfirst substr($string,0,6);
my $middle_name =  ucfirst substr ($string,7,1);
my $last_name = ucfirst substr ($string,8,15);
my $full_name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
	$customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
	$clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $merchantName = $full_name;
    $merchantCity = $last_name;
	$merchantState = $middle_name;
    $pan = int(rand(10**15-1))+(10**15-1);
    $merchantPostalCode = int(rand(10**8-1))+(10**8-1);
	$atmProcessingCode = int(rand(10**5-1))+(10**5-1);
    $userData03 = int(rand(10**14-1))+(10**14-1);
    $mcc = int(rand(10**3-1))+(10**3-1);
	$customerAcctNumber = $digest;
	$openDate = $current_date;
    $acctExpireDate = $current_date;
	$cardExpireDate = $current_date;
	$plasticIssueDate = $current_date;
	$customerDateOfBirth = $current_date;
	$recurringAuthExpireDate = $current_date;
	$transactionDate = $current_date;
	$postDate = $current_date;
	$transactionTime = $timestring;
	$merchantCountryCode = '191';
	
# Formatting the output print.
   printf  FH "DUD17DF26        DBTRAN252.5  %-16.16s%-8.8s%-6.6s%03d%06d%-20.20s%-40.40s%-32.32s%-19.19sA%-9.9s%-3.3s%-8.8s%-8.8sF%-8.8s%-8.8s%-10.10s%-10.10sM%-8.8s%3d%010dAB%-8.8s%-6.6s%013d%-3.3s%013dAA%-4.4s%-9.9s%-3.3sIIC%-8.8sNBB%-1.1s%-1.1s%-10.10s%-10.10s%-10.10sY %04d%04d%04dYB%-2.2s%-14.14s%-14.14s%-6.6s%-40.40s%-30.30s%-3.3sB%-5.5s%-5.5s%-15.15s%-20.20s%-40.40sBA%-8.8s%-19.19sYY%-1.1sYNYN0%013d%013d%013d%-4.4s%-6.6sNIC0MAA%-12.12s%-3.3s%-16.16sAC00B%-1.1sIICA0B%-10.10s%-10.10sV%05d%05d%-2.2s%-2.2s%-2.2s%-2.2s%-8.8sBA%02d%-8.8s%-5.5sA%-16.16s%-1.1s%013d%-13.13s%-40.40s%-30.30s21010101%-6.6s%-10.10s%-10.10s%-1.1s%-1.1s%-5.5s%-5.5s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-6.6s%-6.6s%-6.6s%06.6s\n",
   $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$pan,$cardPostalCode,$cardCountryCode,$openDate,$plasticIssueDate,$acctExpireDate,$cardExpireDate,$dailyMerchandiseLimit,$dailyCashLimit,$customerDateOfBirth,$numberofCards,$incomeOrCashBack,$transactionDate,$transactionTime,$transactionAmount,$transactionCurrencyCode,$transactionCurrencyConversionRate,$mcc,$merchantPostalCode,$merchantCountryCode,$postDate,$userIndicator01,$userIndicator02,$userData01,$userData02,$onUsMercahantId,$externalScore1,$externalScore2,$externalScore3,$$randomDigits,$portfolio,$clientId,$acquirerBin,$merchantName,$merchantCity,$merchantState,$userIndicator03,$userIndicator04,$userData03,$userData04,$userData05,$padActionExpireDate,$cardMasterAcctNumber,$RESERVED_01,$availableBalance,$availableDailycashLimit,$availableDailyMerchandiseLimit,$atmHostMcc,$atmProcessingCode,$acquirerId,$acquirerCountry,$terminalId,$RESERVED_02,$terminalVerificationResults,$cardVerificationResults,$atcCard,$atcHost,$RESERVED_03,$offlineLowerLimit,$offlineUpperLimit,$recurringAuthFrequency,$recurringAuthExpireDate,$cardPinLength,$cardPinSetDate,$processorAuthReasonCode,$merchantId,$cardOrder,$cashbackAmount,$userData06,$userData07,$paymentInstrumentId,$authId,$userData08,$userData09,$userIndicator05,$userIndicator06,$userIndicator07,$userIndicator08,$modelControl1,$modelControl2,$modelControl3,$modelControl4,$RESERVED_04,$segmentId1,$segmentId2,$segmentId3,$segmentId4;
   $rec_cnt++;
   }
}