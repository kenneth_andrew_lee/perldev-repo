#!/usr/bin/perl

#use strict;
#use warnings;

use Data::Dumper;
use lib "$ENV{HOME}/perl5/lib/perl5"; 
my $home_dir = (getpwuid ($<))[7];

my %field_to_change_type = (
                        'customerIdFromHeader'  => 'cusProfile',
						'customerAcctNumber'    => 'accProfile',
						'pan'                   => 'panProfile',
					    'paymentInstrumentId'   => 'payInsProfile',
                        'oldEntityName'         => 'UDProfile',
                        'birthDate'             => 'cusPerInfo',
                        'birthCountry'          => 'cusPerInfo',						
						'gender'                => 'cusPerInfo',
						'numberOfDependents'    => 'cusPerInfo',
						'maritalStatus'         => 'cusPerInfo',
						'educationalStatus'     => 'cusPerInfo',
						'preferredGreeting'     => 'cusPerInfo',
						'preferredLanguage'     => 'cusPerInfo',
						'givenName'             => 'cusName',
						'middleName'            => 'cusName',
						'surname'               => 'cusName',
						'title'                 => 'cusName',
						'suffix'                => 'cusName',
						'M1'                    => 'militaryStatus',
						'M0'                    => 'militaryStatus',
						'nationalId'            => 'nationalInfo',
						'nationalIdCountry'     => 'nationalInfo',
						'citizenshipCountry'    => 'nationalInfo',
						'driversLicenseNumber'  => 'driversLicense',
						'driversLicenseCountry' => 'driversLicense',
						'taxId'                 => 'taxIdentification',
						'taxIdCountry'          => 'taxIdentification',
						'passportNumber'        => 'passportInfo',
						'passportCountry'       => 'passportInfo',
						'passportExpirationDate'=> 'passportInfo',
						'streetLine1'           => 'addressInfo',
						'streetLine2'           => 'addressInfo',  
						'streetLine3'           => 'addressInfo',
						'streetLine4'           => 'addressInfo',
						'city'                  => 'addressInfo',
						'stateProvince'         => 'addressInfo',
						'postalCode'            => 'addressInfo',
						'countryCode'           => 'addressInfo',
						'dateAtAddress'         => 'addressInfo',
						'residenceStatus'       => 'addressInfo',
						'homePhone'             => 'telephoneInfo',
						'preferredPhone'        => 'telephoneInfo',
						'secondaryPhone'        => 'secTelephoneNumber',
						'workPhone'             => 'workTelephoneNumber',
						'mobilePhone'           => 'mobileTelephoneNumber',
						'emailAddress'          => 'cusEmailInfo',
						'pefc'                  => 'cusBankStatus',
						'ofac'                  => 'cusBankStatus',
						'numberOfAccounts'      => 'cusBankStatus',
						'relationshipStartDate' => 'cusBankStatus',
						'vipType'               => 'cusBankStatus',
						'mothersMaidenName'     => 'cusAuthCredentials',
						'openDate'              => 'otherAcctInfo',
						'applicationReferenceNumber'=> 'otherAcctInfo',
						'openChannel'           => 'otherAcctInfo',
						'campaignDate'          => 'otherAcctInfo', 
						'portfolio'             => 'acctPortfolio',
						'accountServiceType'    => 'acctPortfolio',
						'status'                => 'acctStatus',
						'statusDate'            => 'acctStatus',
						'overLimitFlag'         => 'acctStatus',
						'numberOfCyclesInactive'=> 'acctStatus',
						'creditLimit'           => 'acctCreditLimit',
						'currencyCode'          => 'acctCreditLimit',
						'currencyConversionRate'=> 'acctCreditLimit',
						'dailyCashLimit'        => 'acctDailyCashLimit',
						'currencyCode'          => 'acctDailyCashLimit',						
						'currencyConversionRate'=> 'acctDailyCashLimit',
						'dailyPosLimit'         => 'acctDailyPosLimit',
						'currencyCode'          => 'acctDailyPosLimit',
						'currencyConversionRate'=> 'acctDailyPosLimit',
						'dailyTotalLimit'       => 'acctDailyTotalLimit',
						'currencyCode'          => 'acctDailyTotalLimit',
						'currencyConversionRate'=> 'acctDailyTotalLimit',
						'pan'                   => 'issueCard',
						'paymentInstrumentId'   => 'issueCard',
						'lastIssueDate'         => 'issueCard',
						'plasticIssueType'      => 'issueCard',
						'expirationDate'        => 'issueCard',
						'nameOnInstrument'      => 'issueCard',
						'mediaType'             => 'issueCard',
						'issuingCountry'        => 'issueCard',
						'numberOfPaymentIds'    => 'panInfo',
						'panOpenDate'           => 'panInfo',
						'memberSinceDate'       => 'panInfo',
						'type'                  => 'panInfo',
						'subType'               => 'panInfo',
						'association'           => 'panInfo',
						'incentive'             => 'panInfo',
						'category'              => 'panInfo',
						'cardholderCity'        => 'cardholderLocation',
						'cardholderStateProvince'=> 'cardholderLocation',
						'cardholderPostalCode'  => 'cardholderLocation',
						'cardholderCountryCode' => 'cardholderLocation',
						'status'                => 'PANStatus',
						'statusDate'            => 'PANStatus',
						'activeIndicator'       => 'cardActivation',
						'creditLimit'           => 'panCreditLimit',
						'currencyCode'          => 'panCreditLimit',
						'currencyConversionRate'=> 'panCreditLimit',
						'dailyCashLimit'        => 'panDailyCashLimit',
						'currencyCode'          => 'panDailyCashLimit',						
						'currencyConversionRate'=> 'panDailyCashLimit',
						'dailyPosLimit'         => 'panDailyPosLimit',
						'currencyCode'          => 'panDailyPosLimit',
						'currencyConversionRate'=> 'panDailyPosLimit',
												
																
         );

my %change_type_to_code = (
                        'cusProfile'           => '0001',
                        'accProfile'           => '0002',
						'panProfile'           => '0003',
						'payInsProfile'        => '0004',
						'UDProfile'            => '0005',
                        'cusPerInfo'           => '1000', 
						'cusName'              => '1001',
						'militaryStatus'       => '1005',
						'nationalInfo'         => '1100',
						'driversLicense'       => '1104',
						'taxIdentification'    => '1105',
						'passportInfo'         => '1106',  
						'addressInfo'          => '1150',
						'telephoneInfo'        => '1207',
						'secTelephoneNumber'   => '1208',
						'workTelephoneNumber'  => '1209',
						'mobileTelephoneNumber'=> '1210',
						'cusEmailInfo'         => '1250',
						'cusBankStatus'        => '1300',
						'cusAuthCredentials'   => '1350',
						'otherAcctInfo'        => '2011',
						'acctPortfolio'        => '2020',
                        'acctStatus'           => '2030',
						'acctCreditLimit'      => '2201',
						'acctDailyCashLimit'   => '2203',
						'acctDailyPosLimit'    => '2204',
						'acctDailyTotalLimit'  => '2210',
						'issueCard'            => '3000',
						'panInfo'              => '3010',
						'cardholderLocation'   => '3100',
						'PANStatus'            => '3102',
						'cardActivation'       => '3104',
						'panCreditLimit'       => '3201',
						'panDailyCashLimit'    => '3203',
						'panDailyPosLimit'     => '3204',
         );

		 
use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;
use Switch;
 
use DBI;
my $var;
my $db = DBI->connect("dbi:Oracle:fal1dbd","fal1admin","fal1admin");

#$filename = "card.car";
my $filename = $ARGV[0];
open (IFH, '<', $filename) || die "Could not find the file: $!";

my $nmon_rec_cnt = 0;
my $cis_rec_cnt  = 0;
my $ais_rec_cnt  = 0;
my $pis_rec_cnt  = 0;

open (NMON,'+>',"$home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt") || die "file not found :$!";
# Print Header Line for NMON
printf  NMON "B000000000%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;
 
open (CIS,'+>',"$home_dir/base/pmax/bin/LANDINGZONE/CIS$$.txt") || die "file not found :$!";
# Print Header Line for CIS
printf CIS "B000000000%-1836.1836s2.0CIS20   PMAX  %-08.8d\n", '' x 1836, $cis_rec_cnt; 

open (AIS,'+>',"$home_dir/base/pmax/bin/LANDINGZONE/AIS$$.txt") || die "file not found :$!";
# Print the header Line for AIS
printf AIS "B000000000%-1106.1106s2.0AIS20   PMAX  %-08.8d\n", '' x 1106, $ais_rec_cnt;

open (PIS,'+>',"$home_dir/base/pmax/bin/LANDINGZONE/PIS$$.txt") || die "file not found :$!";
# Print Header Line for PIS
printf  PIS "B000000000%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;


while (my $rec = <IFH>) {

if ($rec =~ /^A/ or $rec =~ /^U/) {

 my %newh = ();
 chomp($rec);
 
 $newh{function_code}           = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}      = substr($rec,1,19);	  # $acct_nbr
 
 $newh{customerIdFromHeader}   = substr($rec,1,19);      #user_data_06
 $newh{pan}                     = substr($rec,1,19);      #acct_nbr
 
 $newh{givenName}               = substr($rec,19,30);     # $cus_nm 
 $newh{nameOnInstrument}        = substr($rec,19,30);     # $cus_nm
 $newh{surname}                 = substr($rec,50,25);     # $cus_lst_nm
 $newh{userData12_cis}          = substr($rec,75,30);     # $cus_co_nm
 $newh{mothersMaidenName}       = substr($rec,105,30);    # $cus_md_nm
 $newh{streetLine1}             = substr($rec,135,30);    # $cus_adr_1
 $newh{streetLine2}             = substr($rec,165,30);    # $cus_adr_2
 $newh{city}                    = substr($rec,195,30);    # $cus_city 
 $newh{stateProvince}           = substr($rec,225,2);     # $cus_st
 $newh{cardholderStateProvince} = substr($rec,225,2);     # $cus_st
 $newh{postalCode}              = substr($rec,227,9);     #$cus_zip
 $newh{cardholderPostalCode}    = substr($rec,227,9);     #$cus_zip 
 $newh{homePhone}               = substr($rec,236,16);    #$cus_hm_phon1
 $newh{secondaryPhone}          = substr($rec,252,16);    #$cus_hm_phon2
 $newh{workPhone}               = substr($rec,268,16);    #$cus_wrk_phon1
 $newh{mobilePhone}             = substr($rec,284,16);    #$cus_wrk_phon2
 $newh{birthDate}               = substr($rec,300,8);     #$cus_birth_dt1
 $newh{userData07_cis}          = substr($rec,308,8);     #$cus_birth_dt2
 $newh{nationalId}              = substr($rec,316,11);    #$cus_ssn_1
 $newh{taxId}                   = substr($rec,316,11);    #$cus_ssn_1
 $newh{userData08_cis}          = substr($rec,327,11);    #$cus_ssn_2
 $newh{openDate}                = substr($rec,338,8);     #$crd_opn_dt
 $newh{panOpenDate}             = substr($rec,338,8);     #$crd_opn_dt
 $newh{expirationDate}          = substr($rec,346,8);     #$crd_exp_dt
 $newh{memberSinceDate}         = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{relationshipStartDate}   = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{lastIssueDate}           = substr($rec,362,8);     #$lst_iss_dt
 $newh{dailyTotalLimit}         = substr($rec,370,13);    #$avail_cr
 $newh{dailyCashLimit}          = substr($rec,370,13);    #$avail_cr
 $newh{dailyPosLimit}           = substr($rec,370,13);    #$avail_cr
 $newh{creditLimit}             = substr($rec,383,10);    #$cr_lmt
 $newh{activeIndicator}         = substr($rec,393,1);     #$crd_actv_ind
 $newh{status}                  = substr($rec,394,6);     #$crd_blk_ind
 $newh{plasticIssueType}        = substr($rec,400,2);     #$plastic_issue_type
 $newh{portfolio}               = substr($rec,402,14);    #$portfolio
 $newh{clientIdFromHeader}      = substr($rec,416,14);    #$client_id
 $clientIdFromHeader            = substr($rec,416,14);
 $newh{userData01_cis}          = substr($rec,430,6);     #user_data_01
 $newh{userData02_cis}          = substr($rec,436,6);     #user_data_02
 $newh{userData03_cis}          = substr($rec,442,10);    #user_data_03
 $newh{userData04_cis}          = substr($rec,452,10);    #user_data_04
 $newh{userData05_cis}          = substr($rec,462,15);    #user_data_05
 
 $newh{userData06_cis}          = substr($rec,477,20);    #user_data_06
  
 $newh{emailAddress}            = substr($rec,497,40);    #user_data_07
 
 
$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
  }

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;


my %oldh = ();

#for CIS information
$var = $newh{customerIdFromHeader};

my $cis_code = 0;
my $ais_code = 0;
my $pis_code = 0;

my $ud06 = $newh{userData06_cis};
if ( $ud06 =~ m/CU/ ) {
$newh{customerIdFromHeader} = substr($ud06,2,17);
} 
else {
$newh{customerIdFromHeader} = "P".substr($rec,1,18);
} 

#print "the count value is :$count\n";
my $cis=$db->prepare("select account_reference_xid, first_name, last_name, user_data_12_strg, mothers_maiden_name, 
address_line_1_strg, address_line_2_strg, city_name, country_region_xcd, postal_xcd, home_phone_num, phone2_num,
work_phone_num, cell_phone_num, customer_birth_dt, user_data_7_strg, national_xid, user_data_8_strg, client_xid,
user_data_1_strg, user_data_2_strg, user_data_3_strg, user_data_4_strg, user_data_5_strg, user_data_6_num,
customer_xid, email_address, relation_start_dt, tax_xid 
from fraud_customer_information
where account_reference_xid = ?
");

$cis->execute($var); 

$found = 0;

while(my @rows=$cis->fetchrow_array())
{
foreach (my $rows)
{
$found = 1;

$oldh{customerAcctNumer}   = $rows[0];
$oldh{givenName}           = $rows[1];
$oldh{surname}             = $rows[2];
$oldh{userData12}          = $rows[3];
$oldh{mothersMaidenName}   = $rows[4]; 
$oldh{streetLine1}         = $rows[5];
$oldh{streetLine2}         = $rows[6];
$oldh{city}                = $rows[7];
$oldh{stateProvince}       = $rows[8];
$oldh{postalCode}          = $rows[9];
$oldh{homePhone}           = $rows[10];
$oldh{secondaryPhone}      = $rows[11];
$oldh{workPhone}           = $rows[12];
$oldh{mobilePhone}         = $rows[13];
$oldh{birthDate}           = $rows[14];
$oldh{userdata07}          = $rows[15];
$oldh{nationalId}          = $rows[16];
$oldh{userdata08}          = $rows[17];
$oldh{clientIdFromHeader}  = $rows[18]; 
$oldh{userData01_cis}      = $rows[19];
$oldh{userData02_cis}      = $rows[20];
$oldh{userData03_cis}      = $rows[21];
$oldh{userData04_cis}      = $rows[22];
$oldh{userData05_cis}      = $rows[23];
$oldh{userData06_cis}      = $rows[24];
$oldh{customerIdFromHeader}= $rows[25];
$oldh{emailAddress}        = $rows[26];
$oldh{relationshipStartDate}= $rows[27];
$oldh{taxId}                = $rows[28];
}
}

if ($found == 1) {
 
#for AIS information
my $ais=$db->prepare("select account_open_dt, total_daily_limit_amt, account_state_xcd, portfolio_name, customer_xid,
pos_daily_limit_amt, cash_daily_limit_amt
from fraud_account_summary
where account_reference_xid = ?
");

$ais->execute($var);

while(my @rows=$ais->fetchrow_array())
{
foreach (my $rows)
{
$oldh{openDate}            = $rows[0];
$oldh{dailtTotalLimit}     = $rows[1];
$oldh{status}              = $rows[2];
$oldh{portfolio}           = $rows[3];
$oldh{customerIdFromHeader}= $rows[4];
$oldh{dailyPosLimit}       = $rows[5];
$oldh{dailyCashLimit}      = $rows[6];
}
}

#for PIS information
my $pis=$db->prepare("select payment_instrument_expertn_dt, member_since_dt, last_issue_dt, credit_limit_amt,
payment_active_indicator_xflg, card_issue_type_xcd, payment_instrument_name, country_region_xcd, postal_xcd,
card_open_dt, pos_daily_limit_amt, cash_daily_limit_amt, current_card_status_xcd, customer_xid
from fraud_payment_instrument
where account_reference_xid = ?
");

$pis->execute($var);

while(my @rows=$pis->fetchrow_array())
{
foreach (my $rows)
{
$oldh{expirationDate}      = $rows[0];
$oldh{memberSinceDate}     = $rows[1];
$oldh{lastIssueDate}       = $rows[2];
$oldh{creditLimit}         = $rows[3];
$oldh{activeIndicator}     = $rows[4];
$oldh{plasticIssueType}    = $rows[5];
$oldh{nameOnInstrument}    = $rows[6];
$oldh{cardholderStateProvince} = $rows[7];
$oldh{cardholderPostalCode}= $rows[8];
$oldh{panOpenDate}         = $rows[9]; 
$oldh{dailyPosLimit}       = $rows[10];
$oldh{dailyCashLimit}      = $rows[11];
$oldh{status}              = $rows[12];
$oldh{customerIdFromHeader}= $rows[13];
}
}

my %tochangefields = ();
my $newkey;
my $newvalue;
#my $oldkey;

foreach $newkey (keys(%newh)) {
  $newh{$newkey} =~ s/\s+$//;
  $newh{$newkey} =~ s/^\s+//;
}

while (($newkey, $newvalue)= each %newh) {

if($newkey !~ /^(userData|function_code|clientIdFromHeader)/)  {
if ($oldh{$newkey} =~ /^\d*\.{0,1}\d+$/ || $newvalue =~ /^\d*\.{0,1}\d+$/) {
  if ($oldh{$newkey} != $newvalue) {
    my $codekey = $field_to_change_type{$newkey};
    my $code = $change_type_to_code{$codekey};
    push @{$tochangefields{$code}{$newkey}},$oldh{$newkey},$newvalue;
	#print STDERR "Looking up \"$codekey\" and \"$newkey\" in numeric match, found " . $change_type_to_code{$codekey} . "\n";
  }
} else {
  if ($oldh{$newkey} ne $newvalue) {
    my $codekey = $field_to_change_type{$newkey};
    my $code = $change_type_to_code{$codekey};
    push @{$tochangefields{$code}{$newkey}},$oldh{$newkey},$newvalue;
	#print STDERR "Looking up \"$codekey\" and \"$newkey\" in alpha match, found " . $change_type_to_code{$codekey} . "\n";
	}
}
}
}

my $nonmonCode;

my %type_pos = ( 'old' => 0, 'new' => 1 );

@keylist =  ( "customerIdFromHeader", "characterValue", "city", "code1", "code2", "code3", "countryCode", "date1", "date2", "emailAddress", "entityName", "givenName", "id1", "id2",
"indicator1", "indicator2",	"indicator3", "indicator4", "middleName", "monetaryValue", "numericValue1",	"numericValue2", "phone1", "phone2",
"postalCode", "stateProvince", "streetLine1", "streetLine2", "streetLine3", "streetLine4", "suffix", "surname", "text"
);

foreach my $key (@keylist) {
my $ucKey = ucfirst $key; 
eval "\$new$ucKey = $oldh{$key}";
}
my $newCustomerId = $newCustomerIdFromHeader;


while (my($ohk, $ohref) = each(%tochangefields)) {

$nonmonCode = $ohk;
if ($nonmonCode =~ /^1/) {
 $cis_code++;
}
if ($nonmonCode =~ /^2/) {
 $ais_code++;
}
if ($nonmonCode =~ /^3/) {
 $pis_code++;
}

foreach my $key (@keylist) {
my $key = ucfirst $key; 
eval "\$old$key = \$new$key";
}
my $customerIdFromHeader = $oldCustomerIdFromHeader;

while (my($ihk, $ihref) = each(%$ohref)) {
my @inner_array = @$ihref;
my $ihk_uc = ucfirst $ihk; 
eval "\$new$ihk_uc = \$inner_array[1]";
#print NMON "\$$type$ihk_uc = \"$inner_array[$pos]\"","\n";
}
my $newCustomerId = $newCustomerIdFromHeader;


$nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."NMO".sprintf ("%09d\n", $nmon_rec_cnt_1);

printf NMON "DUD19FZ6         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02s0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%-04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%01s0%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
$nmon_rec_cnt++;   

}

for my $field (keys(%$ohref)) {
for my $type ( keys %type_pos ) {
my $field_uc = ucfirst $field;
eval "\$$type$field_uc = undef";
}
}
my $newCustomerId = undef;
my $customerIdFromHeader = undef;

while ((my $key, my $value)= each %oldh) {
$oldh{$key} = undef;
}

}

#print $newh{$customerAcctNumber},"\n";
if ($cis_code > 0 || $found == 0) {

$cis_rec_cnt_1 =  $cis_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."CIS".sprintf("%09d\n", $cis_rec_cnt_1);

printf CIS "DUD19FZ6         CIS20   2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-01.1s%-1.1s%-8.8s%5d%-030.30s%-030.30s%-60.60s%-10.10s%-10.10s%-60.60s%-03.3s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-03.3s%1.1s%-8.8s%1.1s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-3.3s%-3.3s%-8.8s%-4.4s%-4.4s%16.16d%-03.3s%013.6f%-024.24s%-024.24s%-024.24s%-024.24s%-1.1s%-40.40s%-1.1s%-8.8s%-3.3s%-3.3s%-016.16s%-3.3s%-16.16s%-3.3s%-8.8s%-16.16s%-3.3s%-016.16s%-3.3s%-1.1s%-1.1s%2d%4d%-8.8s%-20.20s%-1.1s%-4.4s%-1.1s%-1.1s%4d%4d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},
'' x 40,$externalTransactionId,$customerType,$vipType,$newh{relationshipStartDate},$newh{numberOfAccounts},$newh{givenName},$newh{middleName},
$newh{surname},$title,$suffix,$newh{preferredGreeting},$newh{preferredLanguage},$newh{mothersMaidenName},$newh{householdName},$newh{streetline1},$newh{streetLine2},
$newh{streetline3},$newh{streetLine4},$newh{city},$newh{stateProvince},$newh{postalCode},$newh{countryCode},$residenceStatus,$newh{dateAtAddress},$secondaryAddrType,$newh{secondaryAddrStreetLine1},
$newh{secondaryAddrStreetLine2},$newh{secondaryAddrStreetLine3},$newh{secondaryAddrStreetLine4},$newh{secondaryAddrCity},$newh{secondaryAddrStateProvince},
$newh{secondaryAddrPostalCode},$newh{secondaryAddrCountryCode},$newh{employer},$newh{workAddrStreetLine1},$newh{workAddrStreetLine2},$newh{workAddrStreetLine3},
$newh{workAddrStreetLine4},$newh{workAddrCity},$newh{workAddrStateProvince},$newh{workAddrPostalCode},$newh{workAddrCountryCode},$employmentStatus,$newh{emplymentStartDate},
$newh{employerMcc},$newh{occupationCode},$newh{income},$newh{currencyCode},$newh{currencyConversionRate},$newh{homePhone},$newh{secondaryPhone},$newh{workPhone},$newh{mobilePhone},$preferredPhone,$newh{emailAddress},
$educationalStatus,$newh{birthDate},$newh{birthCountry},$newh{citizenshipCountry},$newh{nationalId},$newh{nationalIdCountry},$newh{passportNumber},$newh{passportCountry},$newh{passportExpirationDate},
$newh{driversLicenseNumber},$newh{driversLicenseCountry},$newh{taxId},$newh{taxIdCountry},$gender,$maritalStatus,$newh{numberOfDependents},$newh{creditScore},$newh{creditScoreDate},$newh{creditScoreSource},
$creditScoreRequestReason,$newh{creditRating},$pefp,$ofac,$newh{behaviorScore1},$newh{behaviorScore2},$newh{segmentId1},$newh{segmentId2},$newh{segmentId3},$newh{segmentId4},$newh{userIndicator01},$newh{userIndicator02},
$newh{userIndicator03},$newh{userIndicator04},$newh{userIndicator05},$newh{userCode1},$newh{userCode2},$newh{userCode3},$newh{userCode4},$newh{userCode5},$newh{userData01_cis},$newh{userData02_cis},
$newh{userData03_cis},$newh{userData04_cis},$newh{userData05_cis},$newh{userData06_cis},$newh{userData07_cis},$newh{userData08_cis},$nehw{userData09},$nehw{userData10},$nehw{userData11},
$newh{userData12_cis},$newh{userData13},$newh{userData14},$newh{userData15}; 
$cis_rec_cnt++;
}

if ($ais_code > 0 || $found == 0) {

$ais_rec_cnt_1 =  $ais_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."AIS".sprintf ("%09d\n", $ais_rec_cnt_1);

printf AIS "DUD19FZ6         AIS20   2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-2.2s%-2.2s%-2.2s%-20.20s%-1.1s%-020.20s%-020.20s%-020.20s%-03.3s%-3.3s%-40.40s%-010.10s%-32.32s%5d%5d%-8.8s%-2.2s%-8.8s%02d%-8.8s%-1.1s%-03.3s%013.6f%016d%016d%016d%016d%016d%-1.1s%-1.1s%-1.1s%-1.1s%-14.14s%-4.4s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-010.10s%-3.3s%3d%2d%8d%-10.10s%3d%2d%19.2f%-1.1s%4d%4d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%1.1s%1.1s%1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s\n", 
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},$newh{customerAcctNumber},
$externalTransactionId,$type,$ownership,$usage,$newh{jointCustomerId},$vipType,$newh{routingNumber},$newh{bankId},$newh{branchId},$newh{branchCountry},$nehw{branchStateProvince},$newh{branchCity},$newh{branchPostalCode},
$newh{applicationReferenceNumber},$newh{numberOfPaymentIds},$newh{numberOfAuthorizedUsers},$newh{openDate},$newh{status},$newh{statusDate},$newh{authenticationCodeLength},$newh{authenticationCodeSetDate},
$authenticationCodeType,$newh{currencyCode},$newh{currencyConversionRate},$newh{creditLimit},$newh{overdraftLimit},$newh{dailyPosLimit},$newh{dailyCashLimit},$newh{dailyTotalLimit},$cashBackLimitMode,$hasDirectDeposit,$hasOnlinePay,$hasMobilePay,$newh{portfolio},$newh{accountServiceType},
$newh{statementAddress},$newh{statementStreetLine1},$newh{statementStreetLine2},$newh{statementStreetLine3},$newh{statementStreetLine4},$newh{statementCity},$newh{statementStateProvince},
$newh{statementPostalCode},$newh{statementCountryCode},$newh{statementCyclePeriod},$newh{statementDayOfMonth},$newh{interestRate},$newh{interestRateCategory},$newh{numberOfCyclesInactive},
$newh{numberOfCyclesDelinquent},$newh{delinquentAmount},$overLimitFlag,$newh{behaviorScore1},$newh{behaviorScore2},$newh{segmentId1},$newh{segmentId2},$newh{segmentId3},$newh{segmentId4},$newh{userIndicator01},
$newh{userIndicator02},$newh{userIndicator03},$newh{userIndicator04},$newh{userIndicator05},$newh{userCode1},$newh{userCode2},$newh{userCode3},$newh{userCode4},$newh{userCode5},$newh{userData01},
$newh{userData02},$newh{userData03},$newh{userData04},$newh{userData05},$newh{userData06},$newh{userData07},$newh{userData08},$newh{userData09},$newh{userData10},$newh{userData11},$newh{userData12},
$newh{userData13},$newh{userData14},$newh{userData15};
$ais_rec_cnt++;
}  

if ($pis_code > 0 || $found == 0) {

$pis_rec_cnt_1 =  $pis_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."PIS".sprintf ("%09d\n", $pis_rec_cnt_1);

printf PIS "DUD19FZ6         PIS12   1.2  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-1.1s%-2.2s%-1.1s%-1.1s%-08.8s%-8.8s%-03.3s%-040.40s%-05.5s%-010.10s%-03.3s%3d%-30.30s%-02.2s%-8.8s%2d%-8.8s%-1.1s%-1.1s%-40.40s%-8.8s%-8.8s%-1.1s%-1.1s%-03.3s%013.6f%10d%10d%10d%10d%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%2d%2d%-1.1s%-1.1s%-3.3s%-3.3s%-6.6s%-6.6s%-10.10s%-10.10s%-15.15s%-20.20s%-40.40s\n",
$newh{clientIdFromHeader},$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$newh{customerIdFromHeader},
$newh{customerAcctNumber},$externalTransactionId,$newh{pan},$type,$subType,$category,$association,$newh{panOpenDate},$newh{memberSinceDate},$nehw{issuingCountry},$newh{cardholderCity},
$newh{cardholderStateProvince},$newh{cardholderPostalCode},$newh{cardholderCountryCode},$newh{numberOfPayments},$newh{paymentInstrumentId},$newh{status},$newh{statusDate},
$newh{pinLength},$newh{pinSetDate},$pinType,$activeIndicator,$newh{nameOnInstrument},$newh{expirationDate},$newh{lastIssueDate},$plasticIssueType,$incentive,$newh{currencyCode},$newh{currencyConversionRate},$newh{creditLimit},
$newh{overdraftLimit},$newh{dailyPosLimit},$newh{dailyCashLimit},$cashBackLimitMode,$mediaType,$aipStatic,$aipDynamic,$aipVerify,$aipRisk,$aipIssuerAuthentication,$aipCombined,$chipSpecification,$chipSpecVersion,$newh{offlineLowerLimit},$newh{offlineUpperLimit},$newh{userIndicator01},$newh{userIndicator02},$newh{userCode1},
$newh{userCode2},$newh{userData01},$newh{userData02},$newh{userData03},$newh{userData04},$newh{userData05},$newh{userData06},$newh{userData07};
$pis_rec_cnt++;
}
  
while ((my $key, my $value)= each %newh) {
$newh{$key} = undef;
}
  
}


else {
if ($rec =~ /^D/) {

 my %newh = ();
 chomp($rec);
 
 $newh{function_code}           = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}      = substr($rec,1,19);	  # $acct_nbr
 
 #$newh{customerIdFromHeader}    = substr($rec,1,19);      #user_data_06
 $newh{pan}                     = substr($rec,1,19);      #acct_nbr
 
 $newh{givenName}               = substr($rec,19,30);     # $cus_nm 
 $newh{nameOnInstrument}        = substr($rec,19,30);     # $cus_nm
 $newh{surname}                 = substr($rec,50,25);     # $cus_lst_nm
 $newh{userData12_cis}          = substr($rec,75,30);     # $cus_co_nm
 $newh{mothersMaidenName}       = substr($rec,105,30);    # $cus_md_nm
 $newh{streetLine1}             = substr($rec,135,30);    # $cus_adr_1
 $newh{streetLine2}             = substr($rec,165,30);    # $cus_adr_2
 $newh{city}                    = substr($rec,195,30);    # $cus_city 
 $newh{stateProvince}           = substr($rec,225,2);     # $cus_st
 $newh{cardholderStateProvince} = substr($rec,225,2);     # $cus_st
 $newh{postalCode}              = substr($rec,227,9);     #$cus_zip
 $newh{cardholderPostalCode}    = substr($rec,227,9);     #$cus_zip 
 $newh{homePhone}               = substr($rec,236,16);    #$cus_hm_phon1
 $newh{secondaryPhone}          = substr($rec,252,16);    #$cus_hm_phon2
 $newh{workPhone}               = substr($rec,268,16);    #$cus_wrk_phon1
 $newh{mobilePhone}             = substr($rec,284,16);    #$cus_wrk_phon2
 $newh{birthDate}               = substr($rec,300,8);     #$cus_birth_dt1
 $newh{userData07_cis}          = substr($rec,308,8);     #$cus_birth_dt2
 $newh{nationalId}              = substr($rec,316,11);    #$cus_ssn_1
 $newh{taxId}                   = substr($rec,316,11);    #$cus_ssn_1
 $newh{userData08_cis}          = substr($rec,327,11);    #$cus_ssn_2
 $newh{openDate}                = substr($rec,338,8);     #$crd_opn_dt
 $newh{panOpenDate}             = substr($rec,338,8);     #$crd_opn_dt
 $newh{expirationDate}          = substr($rec,346,8);     #$crd_exp_dt
 $newh{memberSinceDate}         = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{relationshipStartDate}   = substr($rec,354,8);     #$crd_mbr_snc_dt
 $newh{lastIssueDate}           = substr($rec,362,8);     #$lst_iss_dt
 $newh{dailyTotalLimit}         = substr($rec,370,13);    #$avail_cr
 $newh{dailyCashLimit}          = substr($rec,370,13);    #$avail_cr
 $newh{dailyPosLimit}           = substr($rec,370,13);    #$avail_cr
 $newh{creditLimit}             = substr($rec,383,10);    #$cr_lmt
 $newh{activeIndicator}         = substr($rec,393,1);     #$crd_actv_ind
 $newh{status}                  = substr($rec,394,6);     #$crd_blk_ind
 $newh{plasticIssueType}        = substr($rec,400,2);     #$plastic_issue_type
 $newh{portfolio}               = substr($rec,402,14);    #$portfolio
 $newh{clientIdFromHeader}      = substr($rec,416,14);    #$client_id
 $newh{userData01_cis}          = substr($rec,430,6);     #user_data_01
 $newh{userData02_cis}          = substr($rec,436,6);     #user_data_02
 $newh{userData03_cis}          = substr($rec,442,10);    #user_data_03
 $newh{userData04_cis}          = substr($rec,452,10);    #user_data_04
 $newh{userData05_cis}          = substr($rec,462,15);    #user_data_05
 
 $newh{userData06_cis}          = substr($rec,477,20);    #user_data_06
  
 $newh{emailAddress}            = substr($rec,497,40);    #user_data_07

 
$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;
 
$actionCode = $newh{function_code};
 
#connecting to DB and pulling customerIdFromHeader value.
$var = $newh{customerAcctNumber};

my $cis=$db->prepare("select customer_xid 
from fraud_customer_information
where account_reference_xid = ?
");

$cis->execute($var); 

while(my @rows=$cis->fetchrow_array())
{
foreach (my $rows)
{
$customerIdFromHeader  = $rows[0]; 
}
 }

if ( $customerIdFromHeader =~ /^P/ ){
$newh{customerIdFromHeader} = substr($customerIdFromHeader,1,18);
} 
 
for ( my $cd=1; $cd<=3; $cd++ ) {
  my ( $customerIdFromHeader, $newCustomerId, $customerAcctNumber, $newCustomerAcctNumber, $pan , $newPan, $paymentInstrumentId, $newPaymentInstrumentId, $OldEntityName, $newEntityName )  =  '';

  switch($cd) {
    case 1 { $customerIdFromHeader = $newh{customerIdFromHeader}; }
    case 2 { $customerAcctNumber   = $newh{customerAcctNumber}; }
    case 3 { $pan                  = $newh{customerAcctNumber}; }
    }
 
 my $nonmonCode = $cd;
 
 $nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."MNO".sprintf ("%09d\n", $nmon_rec_cnt_1);
 
 printf NMON "DUD19FZ6         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%01s0%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
    $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
	$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
	$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
	$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
	$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
	$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
	$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
	$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
	$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
	$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
	$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
	$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
 $nmon_rec_cnt++;   
 }
}

else {
if ($rec =~ /^T/) {

 my %newh = ();
 chomp($rec);
 
 $newh{function_code}             = substr($rec,0,1);       # $function_code
 $newh{customerAcctNumber}        = substr($rec,1,19);	    # $acct_nbr
 $newh{newCustomerAcctNumber}     = substr($rec,20,19);     # $newCustomerAcctNumber 
 $newh{customerIdFromHeader}      = substr($rec,1,19);      # $customerIdFromHeader
 $newh{newCustomerId}             = substr($rec,20,19);     # $newCustomerId
 $newh{pan}                       = substr($rec,1,19);      # $pan
 $newh{newPan}                    = substr($rec,20,19);     # $newPan
 
$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
} 

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;
 
$actionCode = $newh{function_code};
$clientIdFromHeader = 'PRC240';
 
#connecting to DB and pulling customerIdFromHeader value.
$var = $newh{customerAcctNumber};

my $cis=$db->prepare("select customer_xid 
from fraud_customer_information
where account_reference_xid = ?
");

$cis->execute($var); 

while(my @rows=$cis->fetchrow_array())
{
foreach (my $rows)
{
$customerIdFromHeader   = $rows[0]; 
}
 }
 
if ( $customerIdFromHeader =~ /^P/ ){
$customerIdFromHeader = substr($customerIdFromHeader,1,18);
} 
 
if ( $newh{pan} == $customerIdFromHeader ) {
    $x = 1; }
 else { $x = 2; } 
 
for ( $cd=$x; $cd<=3; $cd++ ) {
 my ( $customerIdFromHeader, $newCustomerId, $customerAcctNumber, $newCustomerAcctNumber, $pan , $newPan, $paymentInstrumentId, $newPaymentInstrumentId, $OldEntityName, $newEntityName )  =  '';
  
  switch($cd) {
    case 1 { $customerIdFromHeader = $newh{customerIdFromHeader};
             $newCustomerId        = $newh{newCustomerId}; 	 } 
    case 2 { $customerAcctNumber   = $newh{customerAcctNumber};
             $newCustomerAcctNumber= $newh{newCustomerAcctNumber};	}
    case 3 { $pan                  = $newh{pan};
             $newPan               = $newh{newPan};	}
    }
	
 my $nonmonCode = $cd;
 
$nmon_rec_cnt_1 =  $nmon_rec_cnt + 1;
my $externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."MNO".sprintf ("%09d\n", $nmon_rec_cnt_1);
 
 printf NMON "DUD19FZ6         NMON20  2.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-8.8s%-6.6s%04.4sBA%-1.1s%-40.40s%-20.20s%-19.19s%-30.30s%-20.20s%-40.40s%-19.19s%-30.30s%01s0%-30.30s%-30.30s%-30.30s%-30.30s%-60.60s%-60.60s%-10.10s%-10.10s%-60.60s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-3.3s%-10.10s%-10.10s%-3.3s%-3.3s%-24.24s%-24.24s%-24.24s%-24.24s%-40.40s%-40.40s%-8.8s%-8.8s%-8.8s%-8.8s%-20.20s%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%19d%19d%-3.3s%13d%10d%10d%10d%10d%-10.10s%-10.10s%-60.60s%-60.60s%-50.50s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s%-30.30s\n", 
    $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,
	$externalTransactionId,$transactionDate,$transactionTime,$nonmonCode,$contactMethod,$contactMethodId,$serviceRepresentativeId,$pan,$paymentInstrumentId,
	$newCustomerId,$newCustomerAcctNumber,$newPan,$newPaymentInstrumentId,$actionCode,$newGivenName,$oldGivenName,$newMiddleName,$oldMiddleName,$newSurname,
	$oldSurname,$newSuffix,$oldSuffix,$newEntityName,$oldEntityName,$newStreetLine1,$oldStreetLine1,$newStreetLine2,$oldStreetLine2,$newStreetLine3,
	$oldStreetLine3,$newStreetLine4,$oldStreetLine4,$newCity,$oldCity,$newStateProvince,$oldStateProvince,$newPostalCode,$oldPostalCode,
	$newCountryCode,$oldCountryCode,$newPhone1,$oldPhone1,$newPhone2,$oldPhone2,$newEmailAddress,$oldEmailAddress,$newDate1,$oldDate1,$newDate2,
	$oldDate2,$newId1,$oldId1,$newId2,$oldId2,$newCode1,$oldCode1,$newCode2,$oldCode2,$newCode3,$oldCode3,$newIndicator1,$oldIndicator1,
	$newIndicator2,$oldIndicator2,$newIndicator3,$oldIndicator3,$newIndicator4,$oldIndicator4,$newMonetaryValue,$oldMonetaryValue,$currencyCode,
	$currencyConversionRate,$newNumericValue1,$oldNumericValue1,$newNumericValue2,$oldNumericValue2,$newCharacterValue,$oldCharacterValue,$newText,
	$oldText,$comment,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,
	$userCode4,$userCode5,$userData01_cis,$userData02_cis,$userData03_cis,$userData04_cis,$userData05_cis,$userData06_cis,$userData07_cis,$userData08_cis,$userData09,$userData10,
	$userData11,$userData12_cis,$userData13,$userData14,$userData15,$RESERVED_01; 
 $nmon_rec_cnt++;   
 }

}

while ((my $key, my $value)= each %newh) {
$newh{$key} = undef;
}

}

}
	
}	

{
  $db->disconnect if defined($db);
}

# print the footer line.
printf NMON "E999999999%-2099.2099s2.0NMON20  PMAX  %-08.8d\n", '' x 2099, $nmon_rec_cnt;
close (NMON);

# Print the footer.
printf CIS "E999999999%-1836.1836s2.0CIS20   PMAX  %-08.8d\n", '' x 1836, $cis_rec_cnt;
close (CIS);

# print the footer line.
printf AIS "E999999999%-1106.1106s2.0AIS20   PMAX  %-08.8d\n", '' x 1106, $ais_rec_cnt;
close (AIS);

# print the footer line.
printf PIS "E999999999%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $pis_rec_cnt;
close (PIS);

close (IFH);


if ($nmon_rec_cnt == 0) {
unlink "$home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt";
} 
else {	
#/$ENV/PIS.txt.PIS12;
$command = "touch $home_dir/base/pmax/bin/LANDINGZONE/NMON$$.txt.NMON20; NMON20$$ touch status 2>&1";
system (`$command`);
} 
if ($pis_rec_cnt == 0) {
unlink "$home_dir/base/pmax/bin/LANDINGZONE/PIS$$.txt";
} 
else { 
#/$ENV/PIS.txt.PIS12;
$command = "touch $home_dir/base/pmax/bin/LANDINGZONE/PIS$$.txt.PIS12; PIS$$ touch status 2>&1";
system (`$command`);
}
if ($ais_rec_cnt == 0) {
unlink "$home_dir/base/pmax/bin/LANDINGZONE/AIS$$.txt";
}
else {
#/$ENV/AIS.txt.AIS20;
$command = "touch $home_dir/base/pmax/bin/LANDINGZONE/AIS$$.txt.AIS20; AIS$$ touch status 2>&1";
system (`$command`);
}
if ($cis_rec_cnt == 0) {
unlink "$home_dir/base/pmax/bin/LANDINGZONE/CIS$$.txt";
}
else {
#/$ENV/CIS.txt.CIS20;
$command = "touch $home_dir/base/pmax/bin/LANDINGZONE/CIS$$.txt.CIS20; CIS$$ touch status 2>&1";
system (`$command`);
}

unlink "$home_dir/base/pmax/bin/LZ/$filename";

exit (0);
