#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# getting input from the user

print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the total number of data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{
open (FH,'+>',"PIS$y.txt") || die "file not found :$!";	

# Print Header Line
printf  FH "B000000000%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $rec_cnt; 
&datagen();
# print the footer line.
printf FH "E999999999%-527.527s1.2PIS12   PMAX  %-08.8d\n", '' x 527, $rec_cnt; 
close (FH);

$command = "touch PIS$y.txt.PIS12; PIS$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;
}


sub datagen() {

for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;
my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..16)];
my $first_name = ucfirst substr($string,0,6);
my $middle_name =  ucfirst substr ($string,7,1);
my $last_name = ucfirst substr ($string,8,15);
my $full_name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
	$customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
	$clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $merchantName = $full_name;
    $merchantCity = $last_name;
	$merchantState = $middle_name;
    $pan = int(rand(10**15-1))+(10**15-1);
    $merchantPostalCode = int(rand(10**8-1))+(10**8-1);
	$atmProcessingCode = int(rand(10**5-1))+(10**5-1);
    $userData03 = int(rand(10**14-1))+(10**14-1);
    $mcc = int(rand(10**3-1))+(10**3-1);
	$customerAcctNumber = $digest;
	$openDate = $current_date;
    $acctExpireDate = $current_date;
	$cardExpireDate = $current_date;
	$plasticIssueDate = $current_date;
	$customerDateOfBirth = $current_date;
	$recurringAuthExpireDate = $current_date;
	$transactionDate = $current_date;
	$postDate = $current_date;
	$transactionTime = $timestring;
	$merchantCountryCode = '191';
	
# Formatting the output print.
   printf FH "DUD17DF26        PIS12   1.2  %-16.16s%-8.8s%-6.6s%03d%06d%-20.20s%-40.40s%-32.32s%-19.19sDDAPV%-8.8s%-8.8s%-3.3s%-40.40s%-5.5s%-10.10s%-3.3s%03d%-30.30s00%-8.8s%02d%-8.8sCY%-40.40s%-8.8s%-8.8sIC%-3.3s%013d%010d%010d%010d%010d4CYYYYYYV140%02d%02d%-1.1s%-1.1s%-3.3s%-3.3s%-6.6s%-6.6s%-10.10s%-10.10s%-15.15s%-20.20s%-40.40s\n",
   $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$pan,$panOpenDate,$memberSinceDate,$issuingCountry,$cardholderCity,$cardholderStateProvince,$cardholderPostalCode,$cardholderCountryCode,$numberOfPayments,$paymentInstrumentId,$statusDate,$pinLength,$pinSetDate,$nameOnInstrument,$expirationDate,$lastIssueDate,$currencyCode,$currencyConversionRate,$creditLimit,$overdraftLimit,$dailyPosLimit,$dailyCashLimit,$offlineLowerLimit,$offlineUpperLimit,$userIndicator01,$userIndicator02,$userCode1,$userCode2,$userData01,$userData02,$userData03,$userData04,$userData05,$userData06,$userData07;                                                                                                                      
   $rec_cnt++;
   }
}