#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);

use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the number of total data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{
open (FH,'+>',"AIS$y.txt") || die "file not found :$!";	

# Print the header Line
printf FH "B000000000%-1106.1106s2.0AIS20   PMAX  %-08.8d\n", '' x 1106, $rec_cnt; 
&datagen();
# print the footer line.
printf FH "E999999999%-1106.1106s2.0AIS20   PMAX  %-08.8d\n", '' x 1106, $rec_cnt; 
close (FH);
  
$command = "touch AIS$y.txt.AIS20; AIS$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;
}

sub datagen() {
  
for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.
my $current_date = `date +"%Y%m%d"`;
   $current_date = substr($current_date,0,-1);
my $timestring = strftime "%H%M%S", localtime;
my @chars = ("a".."z");
my $string = join '', @chars[map {int rand @chars} (1..16)];
my $first_name = ucfirst substr($string,0,6);
my $middle_name =  ucfirst substr ($string,7,1);
my $last_name = ucfirst substr ($string,8,15);
my $full_name = $first_name." ".$middle_name.".".$last_name;

# Initialising values to the variable function
    $recordCreationDate = $current_date;
    $recordCreationTime = $timestring;
    $givenName = $full_name;
    $surname = $last_name;
    $jointCustomerId = int(rand(10**15-1))+(10**15-1);
    $branchPostalCode = int(rand(10**9-1))+(10**9-1);
	$statementPostalCode = int(rand(10**9-1))+(10**9-1);
    $userCode1 = int(rand(10**15-1))+(10**15-1);
    $customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
	$clientIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $accountServiceType = int(rand(10**3-1))+(10**3-1);
	$middleName = $middle_name;
	$customerAcctNumber = $digest;
	$openDate = $current_date;
    $statusDate = $current_date;
	$authenticationCodeSetDate = $current_date;
	$branchCountry = '191';
	
	
# Formatting the output print.	
  printf FH "DUD17DF26        AIS20   2.0  %-16.16s%-8.8s%-6.6s%3d%6d%-20.20s%-40.40s%-32.32sD B G %-20.20sV%-20.20s%-20.20s%-20.20s%-3.3s%-3.3s%-40.40s%-10.10s%-32.32s%05d%05d%-8.8s00%-8.8s%02d%-8.8sI%-3.3s%013d%16d%016d%16d%016d%-16.16sCYYY%-14.14s%-4.4s%-60.60s%-40.40s%-40.40s%-40.40s%-40.40s%-40.40s%-3.3s%-10.10s%-3.3s%03d%02d%08d%-10.10s%03d%02d%019d*%04d%04d%-6.6s%-6.6s%-6.6s%-6.6s%-1.1s%-1.1s%1.1s%1.1s%1.1s%-3.3s%-3.3s%-3.3s%-3.3s%-3.3s%-6.6s%-6.6s%-6.6s%-8.8s%-8.8s%-8.8s%-10.10s%-10.10s%-15.15s%-15.15s%-20.20s%-20.20s%-40.40s%-40.40s%-60.60s\n", 
  $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$jointCustomerId,$routingNumber,$bankId,$branchId,$branchCountry,$branchStateProvince,$branchCity,$branchPostalCode,$applicationReferenceNumber,$numberOfPaymentIds,$numberOfAuthorizedUsers,$openDate,$statusDate,$authenticationCodeLength,$authenticationCodeSetDate,$currencyCode,$currencyConversionRate,$creditLimit,$overdraftLimit,$dailyPosLimit,$dailyCashLimit,$dailyTotalLimit,$portfolio,$accountServiceType,$statementAddressee,$statementStreetLine1,$statementStreetLine2,$statementStreetLine3,$statementStreetLine4,$statementCity,$statementStateProvince,$statementPostalCode,$statementCountryCode,$statementCyclePeriod,$statementDayOfMonth,$interestRate,$interestRateCategory,$numberOfCyclesInactive,$numberOfCyclesDelinquent,$delinquentAmount,$behaviorScore1,$behaviorScore2,$segmentId1,$segmentId2,$segmentId3,$segmentId4,$userIndicator01,$userIndicator02,$userIndicator03,$userIndicator04,$userIndicator05,$userCode1,$userCode2,$userCode3,$userCode4,$userCode5,$userData01,$userData02,$userData03,$userData04,$userData05,$userData06,$userData07,$userData08,$userData09,$userData10,$userData11,$userData12,$userData13,$userData14,$userData15;    
  $rec_cnt++;
}
} 