#!/usr/bin/perl
# use lib "$ENV{HOME}/perl5/lib/perl5"; 
use strict;
use warnings;
use DBI;

#my $customerAcctNumber = shift or die "Usage: $0 customerAcctNumber\n";
my $customerAcctNumber = '4202580000002831';
my $dbh = DBI->connect("dbi:Oracle:fhdb03q","reporter","reporter");

my $counter = $dbh->selectrow_array('SELECT count(*) FROM cnx1310_risk_factor_changes where maint_pan = ?', undef, $customerAcctNumber);
print "\$counter = $counter\n";

my $sth = $dbh->prepare("select maint_pan, processor, rf_data_after_rf_base, rf_note_after_rf_base_note, rf_data_after_rf_ovrd, rf_note_after_rf_ovrd_note, rf_data_after_rf_ovrd_from_dt, rf_data_after_rf_ovrd_to_dt from cnx1310_risk_factor_changes where maint_pan = ?");
$sth->execute($customerAcctNumber);

my @row;
while(@row = map { defined($_) ? $_ : ''} $sth->fetchrow_array()) {
	print "@row\n";
}

$dbh->disconnect or warn "Error disconnecting: $DBI::errstr\n";