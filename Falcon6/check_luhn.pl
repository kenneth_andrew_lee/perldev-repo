#!/usr/bin/perl
use strict;
use warnings;
use Algorithm::LUHN qw(check_digit is_valid);


my $pan = shift or die "Usage: $0 PAN\n";

my $return = is_valid($pan);

if ($return) {
    print "$pan is valid\n";
} else {
    print "$pan is invalid\n";
    my $newpan = substr($pan,0,15) . check_digit(substr($pan,0,15));
    printf "%-19s\n", $newpan;

}

