#!/usr/bin/perl
use strict;
use warnings;

my $inst_user = $ENV{'USER'};
$inst_user = $ARGV[0] if (defined $ARGV[0]);

my ($home_dir) = (getpwnam ($inst_user))[7];
exit(200) if (! defined $home_dir);

my $file = "$home_dir/base/pmax/bin/logs/pmax.log";
# my $file = '/export/home/falcon/base/pmax/bin/logs/pmax.log';

my $fh;
my %items;
open ($fh, '<', $file) || die "could not open the file /pmax.log: $!";

while ( <$fh> ) {
    if (/client=DBFE(\d{1,2})/) {
        $items{$1}++;
    }
}
close ($fh);
my $c = scalar keys %items;
print "Number of DBFEs: ",$c,"\n";
exit $c;
