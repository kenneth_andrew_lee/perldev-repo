#!/usr/bin/perl
use warnings;
use strict;


eval {
	local $SIG{ALRM} = sub{die;};
	my ($oalarm) = alarm(1);

	open STATUS, "-|", "cd ~/base/pmax/util; raw-stats;" or die "Can't run $!";
	while (<STATUS>) {
	chomp;
	my ($processName,$pid,$startTime,$statStart,$collectTime,$description,$name,$instance,$value) = split(/:/,$_);
		print "$processName,$pid,$startTime,$statStart,$collectTime,$description,$name,$instance,$value\n" if ($processName eq 'OutSpool') or ($processName eq 'TranSpool');
	}
	close(STATUS);
}
alarm($oalarm);

__END__

if ($? ne 0) {
        writeLog("Error: Command execution failed ($!) - running - $command");
        $error_msg.="Error: Command execution failed ($!) - running - $command\n\n";
        $returnValue=2;
} else {
        writeLog("Success: $command");
}


## Result is not defined after timeout duration
## Exit with 0;
if ( ! defined($result) )
{
        writeLog("Error: $command timed-out and failed");
        return 0;
} else {
        ## Success
        return 1;
}

open STATUS, "-|", "cd ~/base/pmax/util; raw-stats;" or die "Can't run $!";
while (<STATUS>) {
  chomp;
  my ($processName,$pid,$startTime,$statStart,$collectTime,$description,$name,$instance,$value) = split(/:/,$_);
  print "$processName,$pid,$startTime,$statStart,$collectTime,$description,$name,$instance,$value\n" if ($processName eq 'OutSpool') or ($processName eq 'TranSpool');
}
close(STATUS);




my $command_to_check = "$SSH_CON $host $SSH_CMD";
if (! execute_command($command_to_check)) {
                     next;
}
 
 
sub execute_command($)
{
        ### Executes a command with timeout
        ### Returns 0 if fails
        ### Returns 1 on success
 
        my $command=shift;
        my $timeout=30;
 
        eval {
                local $SIG{ALRM} = sub{die;};
                my($oalarm) = alarm($timeout);
                $result=`$command 2>&1`;
                if ($? ne 0) {
                        writeLog("Error: Command execution failed ($!) - running - $command");
                        $error_msg.="Error: Command execution failed ($!) - running - $command\n\n";
                        $returnValue=2;
                } else {
                        writeLog("Success: $command");
                }
                alarm($oalarm);
        };
 
        ## Result is not defined after timeout duration
        ## Exit with 0;
        if ( ! defined($result) )
        {
                writeLog("Error: $command timed-out and failed");
                return 0;
        } else {
                ## Success
                return 1;
        }
}