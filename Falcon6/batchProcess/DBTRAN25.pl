#!/usr/bin/perl

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

use DBI;
my $var;
#my $db = DBI->connect("dbi:Oracle:faldbs","falconadmin","falconadmin");

#my $filename = 'visanet.txt';
my $filename = '52c.txt';
open (IFH, '<', $filename) || die "Could not find the file: $!";

open (OFH,'+>',"DBTRAN.txt") || die "file not found :$!";	

# Print the header Line.
printf  OFH "B000000000%-944.944s2.5DBTRAN25PMAX  %-08.8d\n", '' x 944, $rec_cnt; 

while (my $rec = <IFH>) {

 $customerAcctNumber        = substr($rec,0,19);  #Account-number
 $pan                       = substr($rec,0,19);  #Pan
 $authPostFlag              = substr($rec,19,1); #A  #Auth-Post-Flag
 $cardPostalCode            = substr($rec,20,9);  #Card-Post-Code 
 $cardCountryCode           = substr($rec,29,3);  #Card-Country-Code
 $openDate                  = substr($rec,32,8);  #Open-Date
 $plasticIssueDate          = substr($rec,40,8);  #Plastic-Issue-Date
 $plasticIssueType          = substr($rec,48,1);  #Plastic-Issue-Type
 $acctExpireDate            = substr($rec,49,8);  #Acct-Exp-Date
 $cardExpireDate            = substr($rec,57,8);  #Card-Exp-Date
 $dailyMerchandiseLimit     = substr($rec,65,10); #Avail-Cr
 $dailyCashLimit            = substr($rec,75,10); #Cr-Line
 $customerGender            = substr($rec,85,1);  #Gender
 $customerDateOfBirth       = substr($rec,86,2);  #Birth-Date
 $numberOfCards             = substr($rec,94,3);  #Num-card-num
 $incomeOrCashBack          = substr($rec,97,10); #Income
 $cardType                  = substr($rec,107,1); #Card-Type 
 $cardUse                   = substr($rec,108,1); #card-Use
 $transactionDate           = substr($rec,109,10);#Trans-Date
 $transactionTime           = substr($rec,117,9); #Trans-Time
 $transactionAmount         = substr($rec,123,113);#amount-dols
 $transactionCurrencyCode   = substr($rec,136,3);  #Trans-curr-code
 $transactionCurrencyConversionRate  = substr($rec,139,13);  #Trans-curr-conv
 $authDecisionCode          = substr($rec,152,1);  #Auth-Decision-Cd
 $transactionType           = substr($rec,153,1);  #Trans-Type
 $mcc                       = substr($rec,154,4);  #Merch-Cat
 $merchantPostalCode        = substr($rec,158,9);  #Merch-Post-Code
 $merchantCountryCode       = substr($rec,167,3);  #Merch-Ciuntry-Cd
 $pinVerifyCode             = substr($rec,170,1);  #pin-verify-code
 $cvvVerifyCode             = substr($rec,171,1);  #cvv-flag
 $posEntryMode              = substr($rec,172,1);  #pos-entry-mode
 $postDate                  = substr($rec,173,8);  #Post-date
 $authPostMiscIndicator     = substr($rec,181,1);  #Post-Authed-Cd
 $mismatchIndicator         = substr($rec,182,1);  #Mismatch-Ind
 $caseCreationIndicator     = substr($rec,183,1);  #Case-Cre-Ind
 $userIndicator01           = substr($rec,184,1);  #user-ind-1
 $userIndicator02           = substr($rec,185,1);  #user-ind-2
 $userData01                = substr($rec,186,10); #user-data-1 
 $userData02                = substr($rec,196,10); #user-data-2
 $onUsMercahantId           = substr($rec,206,10); #merch-id
 $merchantDataProvided      = substr($rec,216,1);  #merch-data-avail 
 $cardholderDataProvided    = substr($rec,217,1);  #card-data-avail
 $externalScore1            = substr($rec,218,4);  #ext-score1
 $externalScore2            = substr($rec,222,4);  #ext-score2
 $externalScore3            = substr($rec,226,4);  #ext-score3
 $customerPresent           = substr($rec,230,1);  #cust-present-ind 
 $atmOwner                  = substr($rec,231,1);  #cat-type
 $randomDigits              = substr($rec,232,2);  #random-digits
 $portfolio                 = substr($rec,234,14); #portfolio
 $clientId                  = substr($rec,248,14); #client-id
 $acquirerBin               = substr($rec,262,6);  #ica-number
 $merchantName              = substr($rec,268,40); #merch-nm
 $merchantCity              = substr($rec,308,30); #merch-city
 $merchantState             = substr($rec,338,3);  #merch-st
 $caseSuppressionIndicator  = substr($rec,341,1);  #cas-supr-ind
 $userIndicator03           = substr($rec,342,5);  #user-ind-3
 $userIndicator04           = substr($rec,347,5);  #user-ind-4
 $userData03                = substr($rec,352,15); #user-data-3
 $userData04                = substr($rec,367,20); #user-data-4
 $userData05                = substr($rec,387,40); #user-data-5
 $realtimeRequest           = substr($rec,427,1);  #rltm-req
 $padResponse               = substr($rec,428,1);  #pad-resp
 $padActionExpireDate       = substr($rec,429,8);  #pad-action-until-date
 $cardMasterAcctNumber      = substr($rec,437,19); #card-mstr-acct-nbr 
 $cardAipStatic             = substr($rec,456,1);  #card-aip-static
 $cardAipDynamic            = substr($rec,457,2);  #crad-aip-dynam
 $cardAipVerify             = substr($rec,459,1);  #card-aip-verify
 $cardAipRisk               = substr($rec,460,1);  #card-aip-risk
 $cardAipIssuerAuthentication = substr($rec,461,1);#card-aip-iss-auth
 $cardAipCombined           = substr($rec,462,1);  #card-aip-app-crypt
 $cardDailyLimitCode        = substr($rec,463,1);  #card-delinq-cycles
 $availableDailyCashLimit   = substr($rec,477,13); #card-cash-bal
 $availableDailyMerchandiseLimit = substr($rec,491,13);#card-merch-bal
 $atmHostMcc                = substr($rec,503,4);  #auth-host-mcc
 $atmProcessingCode         = substr($rec,507,6);  #auth-atm-proc-cd
 $atmCameraPresent          = substr($rec,513,1);  #auth-atm-camera
 $cardPinType               = substr($rec,514,1);  #card-pin-type
 $cardMediaType             = substr($rec,515,1);  #card-media-type 
 $cvv2Present               = substr($rec,516,1);  #cvv2-present
 $cvv2Response              = substr($rec,517,1);  #cvv2-response
 $avsResponse               = substr($rec,518,1);  #avs-response
 $transactionCategory       = substr($rec,519,1);  #tran-category
 $acquirerId                = substr($rec,520,1);  #acq-id
 $acquirerCountry           = substr($rec,532,3);  #acq-cntry 
 $terminalId                = substr($rec,535,16); #term-id
 $terminalType              = substr($rec,551,1);  #term-type
 $terminalEntryCapability   = substr($rec,552,1);  #term-entry-cap
 $posConditionCode          = substr($rec,553,2);  #pos-cond-cd
 $networkId                 = substr($rec,555,2);  #atm-ntwk-id
 $authExpireDateVerify      = substr($rec,558,1);  #auth-exp-dte-ver
 $authSecondaryVerify       = substr($rec,559,1);  #auth-iss-verify
 $authBeneficiary           = substr($rec,560,1);  #auth-bene
 $authResponseCode          = substr($rec,561,1);  #auth-resp-cd 
 $authReversalReason        = substr($rec,562,1);  #auth-rev-reas
 $authCardIssuer            = substr($rec,563,1);  #auth-card-iss
 $terminalVerificationResults = substr($rec,564,10);#auth-tvr
 $cardVerificationResults   = substr($rec,574,10); #auth-cvr 
 $cryptogramValid           = substr($rec,584,1);  #crypt-valid
 $atcCard                   = substr($rec,585,5);  #atc-card
 $atcHost                   = substr($rec,590,5);  #atc-host
 
 $offlineLowerLimit         = substr($rec,597,2);  #offl-limit-inc
 $offlineUpperLimit         = substr($rec,599,2);  #offl-limit-crd
 $recurringAuthFrequency    = substr($rec,601,2);  #recurr-freq
 $recurringAuthExpireDate   = substr($rec,603,8);  #recurr-exp-dte
 $linkedAcctType            = substr($rec,611,1);  #card-assoc
 $cradIncentive             = substr($rec,612,1);  #card-incent
 $cardPinLength             = substr($rec,613,2);  #card-pin-len
 $cardPinSetDate            = substr($rec,615,8);  #card-pin-dte
 $processorAuthReasonCode   = substr($rec,623,8);  #proc-reason-cd
 $standinAdvice             = substr($rec,631,1);  #auth-advice 
 $merchantId                = substr($rec,632,16); #acq-merch-id
 $cardOrder                 = substr($rec,648,1);  #card-order
 $cashbackAmount            = substr($rec,649,13); #cashbk-amt 
 $userData06                = substr($rec,662,13); #user-data-6
 $userData07                = substr($rec,675,40); #user-data-7
 
#$clientIdFromHeader = 'PRC860';

$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

=start
#connecting to databse and collecting $customerIdFromHeader
$var = $customerAcctNumber;
#$var = "ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4";
#my $var = $db->quote("0000000000000000");

my $dbtran=$db->prepare("select customer_xid
from fraud_payment_instrument
where account_reference_xid = ?
");

$dbtran->execute($var);

while(my @rows=$dbtran->fetchrow_array())
{
foreach (my $rows)
{
$customerIdFromHeader    = $rows[0];
#$givenName              = $rows[1];
}
}
=cut

$customerIdFromHeader  = $customerAcctNumber;
$pan                   = $customerAcctNumber;

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;

#Formatting the output print.
 printf  OFH "DUD17DF26        DBTRAN252.5  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-1.1s%-9.9s%-3.3s%-8.8s%-8.8s%-1.1s%-8.8s%-08.8s%010d%010d%-1.1s%-8.8s%3d%10d%-1.1s%-1.1s%-8.8s%-6.6s%013.2f%-3.3s%013.6f%-1.1s%-1.1s%-4.4s%-9.9s%-3.3s%-1.1s%-1.1s%-1.1s%-8.8s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-10.10s%-10.10s%-10.10s%-1.1s%-1.1s%04d%04d%04d%-1.1s%-1.1s%-2.2s%-14.14s%-14.14s%-6.6s%-40.40s%-30.30s%-3.3s%-1.1s%-5.5s%-5.5s%-15.15s%-20.20s%-40.40s%-1.1s%-1.1s%-8.8s%-19.19s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%013.2f%013.2f%013.2f%-4.4s%-6.6s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-12.12s%-3.3s%-16.16s%-1.1s%-1.1s%-2.2s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-10.10s%-10.10s%-1.1s%05d%05d%-2.2s%-2.2s%-2.2s%-2.2s%-8.8s%-1.1s%-1.1s%02d%-8.8s%-5.5s%-1.1s%-16.16s%-1.1s%013.2f%-13.13s%-40.40s%-30.30s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-1.1s%-6.6s%-10.10s%-10.10s%-1.1s%-1.1s%-5.5s%-5.5s%-1.1s%-1.1s%-1.1s%-1.1s%-3.3s%-6.6s%-6.6s%-6.6s%06.6s\n",
  $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$pan,$authPostFlag,$cardPostalCode,$cardCountryCode,$openDate,$plasticIssueDate,$plasticIssueType,$acctExpireDate,$cardExpireDate,$dailyMerchandiseLimit,$dailyCashLimit,$customerGender,$customerDateOfBirth,$numberOfCards,$incomeOrCashBack,$cardType,$cardUse,$transactionDate,$transactionTime,$transactionAmount,$transactionCurrencyCode,$transactionCurrencyConversionRate,$authDecisionCode,$transactionType,$mcc,$merchantPostalCode,$merchantCountryCode,$pinVerifyCode,$cvvVerifyCode,$posEntryMode,$postDate,$authPostMiscIndicator,$mismatchIndicator,$caseCreationIndicator,$userIndicator01,$userIndicator02,$userData01,$userData02,$onUsMercahantId,$merchantDataProvided,$cardholderDataProvided,$externalScore1,$externalScore2,$externalScore3,$customerPresent,$atmOwner,$randomDigits,$portfolio,$clientId,$acquirerBin,$merchantName,$merchantCity,$merchantState,$caseSuppressionIndicator,$userIndicator03,$userIndicator04,$userData03,$userData04,$userData05,$realtimeRequest,$padResponse,$padActionExpireDate,$cardMasterAcctNumber,$cardAipStatic,$cardAipDynamic,$RESERVED_01,$cardAipVerify,$cardAipRisk,$cardAipIssuerAuthentication,$cardAipCombined,$cardDailyLimitCode,$availableBalance,$availableDailycashLimit,$availableDailyMerchandiseLimit,$atmHostMcc,$atmProcessingCode,$atmCameraPresent,$cardPinType,$cardMediaType,$cvv2Present,$cvv2Response,$avsResponse,$transactionCategory,$acquirerId,$acquirerCountry,$terminalId,$terminalType,$terminalEntryCapability,$posConditionCode,$networkId,$RESERVED_02,$authExpireDateVerify,$authSecondaryVerify,$authBeneficiary,$authResponseCode,$authReversalReason,$authCardIssuer,$terminalVerificationResults,$cardVerificationResults,$cryptogramValid,$atcCard,$atcHost,$RESERVED_03,$offlineLowerLimit,$offlineUpperLimit,$recurringAuthFrequency,$recurringAuthExpireDate,$linkedAcctType,$cardIncentive,$cardPinLength,$cardPinSetDate,$processorAuthReasonCode,$standinAdvice,$merchantId,$cardOrder,$cashbackAmount,$userData06,$userData07,$paymentInstrumentId,$avsRequest,$cvrOfflinePinVerificationPerformed,$cvrOfflinePinVerificationFailed,$cvrPinTryLimitExceeded,$posUnattended,$posOffPremises,$posCardCapture,$posSecurity,$authId,$userData08,$userData09,$userIndicator05,$userIndicator06,$userIndicator07,$userIndicator08,$modelControl1,$modelControl2,$modelControl3,$modelControl4,$RESERVED_04,$segmentId1,$segmentId2,$segmentId3,$segmentId4;
  $rec_cnt++;
}

# print the footer line.
printf OFH "E999999999%-944.944s2.5DBTRAN25PMAX  %-08.8d\n", '' x 944, $rec_cnt; 

close (OFH);
close (IFH);
#$command = "touch DBTRAN.txt.DBTRAN25; DBTRAN touch status 2>&1";
#system (`$command`);