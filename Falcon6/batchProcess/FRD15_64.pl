#!/usr/bin/perl

use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

use DBI;
my $var;
my $db = DBI->connect("dbi:Oracle:faldbs","falconadmin","falconadmin");

#my $filename = 'VROL160.txt';
my $filename = '52c.txt';
open (IFH, '<', $filename) || die "Could not find the file: $!";

open (OFH,'+>',"FRD15.txt") || die "file not found :$!";	

# Print the header Line.
printf  OFH "B000000000%-779.779s1.5FRD15   PMAX  %-08.8d\n", '' x 779, $rec_cnt; 

while (my $rec = <IFH>) {

    $clientIdFromHeader              = substr($rec,1,6);  #PROC-ID
    
	$recordCreationDate              = substr($rec,79,26);#FRAUD-RPT-DATE
	
    $customerIdFromHeader            = substr($rec,135,19); #ACCT-NBR
    $customerAcctNumber              = substr($rec,135,19); #ACCT-NBR
    $externalTransactionId           = substr($rec,129,32);#clientId(6char).date/time stamp(14).file typr(3char:FRD).record/row#(9char)
	
	$pan                             = substr($rec,135,19);#ACCT-NBR
    $paymentInstrumentId             = substr($rec,135,19);#ACCT-NBR
    $messageType                     = 'TRAN';
    $fraudFlag                       = '1'; #-Confirmed Fraud--
	$caseCreation                    = '';
    $caseCreationDate                = '';
	$caseCreationTime                = '';
    $dateOfFirstIncident             = '';
    $timeOfFirstIncident             = '';
    $dateOfLastIncident              = '';
    $timeOfLastIncident              = '';
    $blockDate                       = '';
    $blockTime                       = '';
    $caseTag                         = '1'; #-Confirmed Fraud--
    $fraudType                       = substr($rec,158,1); #FRAUD-TYPE
    $recordSource                    = 'S';
    $fraudFindMethod                 = 'O';
	$blockLevel                      = 'P'; 
    $liability                       = '';
    $fiTransactionIdReference        = '';
    $externalTransactionIdReference  = substr($rec,315,32);#transactionId from DBTRAN25*
    $recordTypeReference             = 'DBTRAN25';
    $transactionDate                 = substr($rec,381,10); #TRAN_DATE
    #$transactionTime                 = substr($rec,363,6);  #use DBTRAN25*
    $transactionTimeMilliseconds     = '';
    $authPostFlag                    = 'A';
    $postDate                        = '';
	$decisionCode                    = 'A'; 
	$depositWithdrawalFlag           = '';
	$debitCustomerId                 = '';
	$debitAcctNumber                 = '';
	$debitAcctBranchId               = '';
	$creditCustomerId                = '';
	$creditAcctNumber                = '';
	$creditBranchId                  = '';
    $RESERVED_01                     = '';
    $transactionAmount               = substr($rec,360,18);#TRAN-AMT
	$transactionCurrencyCode         = substr($rec,378,3); #CURR-CODE 
	$transactionCurrencyConversionRate= '';
	#$transactionCountryCode           = substr($rec,581,3); #use DBTRAN25(38)*
	#$transactionPostalCode            = substr($rec,584,10);#use DBTRAN25(37)*
	#$merchantId                       = substr($rec,594,20);#use DBTRAN25(125)*
	$mcc                              = substr($rec,547,4); #MCC
	$deviceId                         = '';
	$onUsFlag                         = '';
	$paymentOrderFlag                 = '';
	$transactionReferenceNumber       = '';
	#$pinVerifyCode                    = substr($rec,692,1); #use DBTRAN25(39)*
	$nonmonCode                       = '';
	$userIndicator01                  = '';
	$userCode1                        = '';
	$userCode2                        = '';
	$userData01                       = '';
	$RESERVED_02                      = '';


$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
#$recordCreationDate = $gmt_date;       *
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

#connecting to database and collecting few transaction references.
$var = "ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4";
#my $var = $db->quote("0000000000000000");
#$var = $customerAcctNumber;

my $frd=$db->prepare("select transaction_dttm, merchant_country_xcd, merchant_postal_xcd, merchant_xid, transaction_pin_verify_xcd 
from import_debit_auth_post
where account_reference_xid = ?
");

$frd->execute($var);

while(my @rows=$frd->fetchrow_array())
{
foreach (my $rows)
{
$transactionTime         = $rows[0];
$merchantCountryCode     = $rows[1];
$merchantPostalCode      = $rows[2];
$merchantId              = $rows[3];
$pinVerifyCode           = $rows[4];
}
}

#$transactionDate = strftime "%Y%m%d", localtime;
#$transactionTime = strftime "%H%M%S", localtime;

#Formatting the output print.
 printf OFH "DUD17DF26        FRD15   1.5  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-30.30s%-4.4s%-2.2s%-8.8s%-6.6s%-8.8s%-6.6s%-8.8s%-6.6s%-8.8s%-6.6s%-2.2s%-3.3s%-1.1s%-3.3s%-1.1s%-1.1s%-32.32s%-32.32s%-8.8s%-8.8s%-6.6s%03d%-1.1s%-8.8s%-1.1s%-1.1s%-20.20s%-40.40s%-20.20s%-20.20s%-40.40s%-20.20s%-3.3s%019.2f%-3.3s%-13.13s%-3.3s%-10.10s%-20.20s%-4.4s%-40.40s%-1.1s%-1.1s%-32.32s%-1.1s%-4.4s%-1.1s%-3.3s%-3.3s%-10.10s%-100.100s\n", 
            $clientIdFromHeader,$recordCreationDate,$recordCreationTime,$recordCreationMilliseconds,$gmtOffset,$customerIdFromHeader,$customerAcctNumber,$externalTransactionId,$pan,$paymentInstrumentId,$messageType,$fraudFlag,$caseCreationDate,$caseCreationTime,$dateOfFirstIncident,$timeOfFirstIncident,$dateOfLastIncident,$timeOfLastIncident,$blockDate,$blockTime,$caseTag,$fraudType,$recordSource,$fraudFindMethod,$blockLevel,$liability,$fiTransactionIdReference,$externalTransactionIdReference,$recordTypeReference,$transactionDate,$transactionTime,$transactionTimeMilliseconds,$authPostFlag,$postDate,$decisionCode,$depositWithdrawalFlag,$debitCustomerId,$debitAcctNumber,$debitAcctBranchId,$creditCustomerId,$creditAcctNumber,$creditBranchId,$RESERVED_01,$transactionAmount,$transactionCurrencyCode,$transactionCurrencyConversionRate,$transactionCountryCode,$transactionPostalCode,$merchantId,$mcc,$deviceId,$onUsFlag,$paymentOrderFlag,$transactionReferenceNumber,$pinVerifyCode,$nonmonCode,$userIndicator01,$userCode1,$userCode2,$userData01,$RESERVED_02;
    $rec_cnt++;
}
# print the footer line.
printf OFH "E999999999%-779.779s1.5FRD15   PMAX  %-08.8d\n", '' x 779, $rec_cnt; 
close (IFH);
close (OFH);

#$command = "touch DBTRAN.txt.DBTRAN25; DBTRAN touch status 2>&1";
#system (`$command`);