#!/usr/bin/perl

use strict;
use warnings;

for (1..10000) {
	my $pan = "4" . "0" x 8 . sprintf("%07.7d", $_) . "   ";
	print "$pan\n";
}