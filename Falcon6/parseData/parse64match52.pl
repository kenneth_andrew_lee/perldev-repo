#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my $sixfour_filename = "U:/data/Chase/export10.csv";
my $fivetwo_filename = "U:/data/Chase/export0.csv";

# Put data into a hash
my $sixfour_hash_ref = &load_64_data;
my $fivetwo_hash_ref = &load_52_data;

my @pans = keys (%$sixfour_hash_ref);
my $pan = $pans[0];
print Dumper($pan), "\n";

my @dates = keys %$sixfour_hash_ref{$pan};
print Dumper(@dates), "\n";

my @keys = keys %$sixfour_hash_ref{$pan}->{$dates[0]};
print Dumper(@keys), "\n";

print %$sixfour_hash_ref{$pan}->{$dates[0]}->{$keys[0]}, "\n";

#print Dumper($sixfour_hash_ref), "\n";

#print Dumper(%$sixfour_hash_ref{$pan}{@dates[0]}), "\n";
#print Dumper(%$sixfour_hash_ref{$pan}), "\n";
#my @dates = keys (%$sixfour_hash_ref->$pan);
#my $datevalue = $dates[0];
#print Dumper($sixfour_hash_ref{$pan}{$datevalue}), "\n";

#my ($key,$value) = each %sixfour_hash_ref;
#my ($key1,$value1) = each $sixfour_hash_ref{$key};
#print $key, $value1, $sixfour_hash_ref{$key}{$value1}{TRAN_AMT}, "\n";
#print $fivetwo_hash_ref->{1}->{SCORE_CUSTOMER_ACCOUNT_XID}, $fivetwo_hash_ref->{1}->{TRANSACTION_DTTM}, "\n";
#print Dumper($sixfour_hash_ref);
exit(0);

# Return a hash_ref
sub load_64_data {
  my $filename = $sixfour_filename;
  open my $fh, '<', $filename or die "Couldn't open file: $!\n";
  my %hash=();
  while ( <$fh> ) {
    chomp;

    my @array = map {
      s/^\s+//; # strip leading spaces
      s/\s+$//; # strinp trailing spaces
      s/"//g;    # Remove Double Quotes
      $_        # return the modified string
      } split(',');
    @array = grep(s/\s*$//g, @array);
    $hash{$array[0]}{$array[1]}{TRN_AMT} = $array[2];
    $hash{$array[0]}{$array[1]}{CRD_CLNT_ID} = $array[3];
    $hash{$array[0]}{$array[1]}{SIC_CD} = $array[4];
    $hash{$array[0]}{$array[1]}{LST_UPD_DT} = $array[5];
    $hash{$array[0]}{$array[1]}{USER_DATA_7} = $array[6];
  }
  return \%hash;
}

# Return a hash_ref
sub load_52_data {
  my $filename = $fivetwo_filename;
  open my $fh, '<', $filename or die "Couldn't open file: $!\n";
  my $counter = 0;
  my %hash=();
  while ( <$fh> ) {
    chomp;
    my @array = map {
      s/^\s+//; # strip leading spaces
      s/\s+$//; # strinp trailing spaces
      s/"//g;    # Remove Double Quotes
      $_        # return the modified string
      } split(',');
    $hash{$counter}{SCORE_CUSTOMER_ACCOUNT_XID} = $array[0];
    $hash{$counter}{CREATED_DTTM} = $array[1];
    $hash{$counter}{RCD_CRT_DTTM} = $array[2];
    $hash{$counter}{TRANSACTION_DTTM} = $array[3];
    $hash{$counter}{MERCHANT_CATEGORY_XCD} = $array[4];
    $hash{$counter}{TRANSACTION_AMT} = $array[5];
    $hash{$counter}{CLIENT_XID} = $array[6];
    $counter++;
  }
  return \%hash;
}



__DATA__
$VAR1 = {
          '483313xx29969599' => {
                                  '2017-05-03 22:21:02' => {
                                                             'LST_UPD_DT' => '2017-05-03 20:21:02',
                                                             'CRD_CLNT_ID' => 'PRC0F0',
                                                             'USER_DATA_7' => '367124x8462x298',
                                                             'SIC_CD' => '4121',
                                                             'TRN_AMT' => '0'
                                                           },



    ## Does record exist for ACCT_NBR ?
    #if (exists $hash{$array[0]}) {
    #  if (exists $hash{$array[0]}{$array[1]}) {
    #    warn("Element already exists!");
    #  } else {
    #    #$hash{$array[0]}{$array[1]} = $array[1];
    #    $hash{$array[0]}{$array[1]}{TRN_AMT} = $array[2];
    #    $hash{$array[0]}{$array[1]}{CRD_CLNT_ID} = $array[3];
    #    $hash{$array[0]}{$array[1]}{SIC_CD} = $array[4];
    #    $hash{$array[0]}{$array[1]}{LST_UPD_DT} = $array[5];
    #    $hash{$array[0]}{$array[1]}{USER_DATA_7} = $array[6];
    #  }
    #} else {
    #  #$hash{$array[0]} = $array[0];
    #  #$hash{$array[0]}{$array[1]} = $array[1];
    #  $hash{$array[0]}{$array[1]}{TRN_AMT} = $array[2];
    #  $hash{$array[0]}{$array[1]}{CRD_CLNT_ID} = $array[3];
    #  $hash{$array[0]}{$array[1]}{SIC_CD} = $array[4];
    #  $hash{$array[0]}{$array[1]}{LST_UPD_DT} = $array[5];
    #  $hash{$array[0]}{$array[1]}{USER_DATA_7} = $array[6];
    #}
    #print Dumper %hash,"\n";
    #return \%hash;


C:\Users\kenlee\Documents\srccode\perldev-repo\Falcon6>perl parse64match52.pl
keys on reference is experimental at parse64match52.pl line 17.
keys on reference is experimental at parse64match52.pl line 20.
Scalar value @dates[0] better written as $dates[0] at parse64match52.pl line 20.
Scalar value @dates[0] better written as $dates[0] at parse64match52.pl line 23.
Scalar value @keys[0] better written as $keys[0] at parse64match52.pl line 23.
$VAR1 = '42x767x19343x546';

$VAR1 = '2017-05-04 21:33:48';
$VAR2 = '2017-05-04 21:21:21';
$VAR3 = '2017-05-04 12:19:13';
$VAR4 = '2017-04-29 20:11:52';
$VAR5 = '2017-05-03 10:07:23';
$VAR6 = '2017-05-04 12:17:54';
$VAR7 = '2017-05-02 13:36:34';
$VAR8 = '2017-05-04 20:24:35';
$VAR9 = '2017-05-04 12:45:04';
$VAR10 = '2017-05-02 19:24:56';
$VAR11 = '2017-05-03 09:20:36';
$VAR12 = '2017-05-04 21:31:07';
$VAR13 = '2017-05-01 18:55:01';
$VAR14 = '2017-04-30 15:42:01';
$VAR15 = '2017-05-01 18:53:40';
$VAR16 = '2017-05-01 18:51:57';
$VAR17 = '2017-05-03 10:05:00';
$VAR18 = '2017-05-04 12:20:02';
$VAR19 = '2017-05-01 18:54:58';
$VAR20 = '2017-04-29 13:19:25';
$VAR21 = '2017-04-30 16:35:12';

$VAR1 = 'SIC_CD';
$VAR2 = 'CRD_CLNT_ID';
$VAR3 = 'LST_UPD_DT';
$VAR4 = 'USER_DATA_7';
$VAR5 = 'TRN_AMT';

4121

C:\Users\kenlee\Documents\srccode\perldev-repo\Falcon6>


