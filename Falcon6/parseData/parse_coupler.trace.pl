#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
use parse_dbtran25;

my %bad = ();
my %dbtran_req = ();
my %dbtran_rsp = ();
my %counts = ();
my $counter=0;
#my $filename = "/home/falcon/q1dps/base/pmax/bin/coupler.trace";
my $filename = $ARGV[0];
open( IFH,  '<',  $filename ) || die "Could not find the file: $!";
while (<IFH>) {
  chomp;
  if ($_ =~ /^00000995.*$/ || $_ =~ /^00001069.*$/) {
    if ($_ =~ /^00000995.*$/) { 
      my $headerlength=substr($_,8,4) + 17 + 52;  # 138(ext header) + 17(reserved) + 52(base header) = 207
      my $dbtran = get_request_ref($_,$headerlength);
      $dbtran_req{$counter}{LENGTH} = length($_);
      $dbtran_req{$counter}{VALUE} = $_;
      $dbtran_req{$counter}{DBTRAN} = $dbtran;
    } elsif ($_ =~ /^00001069.*$/) { 
      my $headerlength=substr($_,8,4) + 52;  # 138(ext header) + 52(base header) = 190
      my $dbtran = get_response_ref($_,$headerlength);
      $dbtran_rsp{$counter}{LENGTH} = length($_);
      $dbtran_rsp{$counter}{VALUE} = $_;
      $dbtran_rsp{$counter}{DBTRAN} = $dbtran;
    } else {
       $bad{$counter}{LENGTH} = length($_);
       $bad{$counter}{VALUE} = $_;
    }
    $counter++;
  }
}
close IFH;

print "Requests:\n";
$counter=0;
foreach my $key (sort { $a <=> $b } keys %dbtran_req) {
  my $length = $dbtran_req{$key}{LENGTH};
  my $d_ref = $dbtran_req{$key}{DBTRAN};
  if ($counter==0) {print_header($dbtran_req{$key}{VALUE})};
  foreach my $k (keys %{$d_ref}) {
    print "$key:$length:$k:${$d_ref}{$k}", "\n";
  }
  $counter++;
  $counts{REQUEST} = $counter;
}
print "\n\n";



print "Responses:\n";
$counter=0;
foreach my $key (sort { $a <=> $b } keys %dbtran_rsp) {
  my $length = $dbtran_rsp{$key}{LENGTH};
  my $d_ref = $dbtran_rsp{$key}{DBTRAN};
  #my $value = $dbtran_rsp{$key}{VALUE};
  #print "$length:$value", "\n";
  #print Dumper(%$d_ref), "\n";
  if ($counter==0) {print_header($dbtran_rsp{$key}{VALUE})};
  foreach my $k (keys %{$d_ref}) {
    print "$key:$length:$k:${$d_ref}{$k}", "\n";
  }
  $counts{RESPONSE} = $counter;
  $counter++;
}
print "\n\n";

print "Other:\n";
$counter=0;
foreach my $key (sort { $a <=> $b } keys %bad) {
  my $length = $bad{$key}{LENGTH};
  my $value = $bad{$key}{VALUE};
  print "$length:$value", "\n";
  $counts{OTHER} = $counter;
  $counter++;

}
print "\n\n";

while (my ($key, $value) = each %counts) {
  print "$key:$counts{$key}", "\n";
}

exit(0);

