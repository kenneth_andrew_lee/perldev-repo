#!/usr/bin/perl
use strict;
use warnings;

use IO::Socket;

my $remote_host = '10.211.228.80';
my $remote_port = '49001';

my $socket = IO::Socket::INET->new(PeerAddr => $remote_host,
                                   PeerPort => $remote_port,
                                   Proto    => "tcp",
                                   Type     => SOCK_STREAM)
    or die "Couldn't connect to $remote_host:$remote_port : $@\n";

# ... do something with the socket
#print $socket "Why don't you call me anymore?\n";

$answer = <$socket>;

print "\$answer = $answer", "\n";
# and terminate the connection when we're done
close($socket);




__END__
#!/usr/bin/perl
#tcpserver.pl

use IO::Socket::INET;

# flush after every write
$| = 1;

my ($socket,$client_socket);
my ($peeraddress,$peerport);

# creating object interface of IO::Socket::INET modules which internally does 
# socket creation, binding and listening at the specified port address.
$socket = new IO::Socket::INET (
LocalHost => '127.0.0.1',
LocalPort => '5000',
Proto => 'tcp',
Listen => 5,
Reuse => 1
) or die "ERROR in Socket Creation : $!\n”;

print "SERVER Waiting for client connection on port 5000";

while(1)
{
# waiting for new client connection.
$client_socket = $socket->accept();

# get the host and port number of newly connected client.
$peer_address = $client_socket->peerhost();
$peer_port = $client_socket->peerport();

print “Accepted New Client Connection From : $peeraddress, $peerport\n ”;

# write operation on the newly accepted client.
$data = “DATA from Server”;
print $client_socket “$data\n”;
# we can also send the data through IO::Socket::INET module,
# $client_socket->send($data);

# read operation on the newly accepted client
$data = <$client_socket>;
# we can also read from socket through recv()  in IO::Socket::INET
# $client_socket->recv($data,1024);
print “Received from Client : $data\n”;
}

$socket->close();












use strict;
use warnings;
use IO::Socket::INET;
 
# auto-flush on socket
$| = 1;
 
# create a connecting socket
my $socket = new IO::Socket::INET(PeerHost => '10.211.228.80',
    							  PeerPort => '49001',
    							  Proto => 'tcp',
);

die "cannot connect to the server $!\n" unless $socket;
print "connected to the server\n";
 
# data to send to a server
#my $req = 'hello world';
#my $size = $socket->send($req);
#print "sent data of length $size\n";
 
# notify server that request has been sent
#shutdown($socket, 1);
 
# receive a response of up to 1024 characters from server
my $response = "";
$socket->recv($response, 1024);
print "received response: $response\n";
 
$socket->close();
