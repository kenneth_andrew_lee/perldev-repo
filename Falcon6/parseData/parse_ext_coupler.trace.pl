#!/usr/bin/perl

use strict;
use warnings;
use parse_cam216;
#use Data::Dumper;

my $filename = "/home/falcon/q1dps/base/pmax/bin/ext_coupler.trace";
open( IFH,  '<',  $filename ) || die "Could not find the file: $!";
while (<IFH>) {
  chomp;
  if ($_ =~ /^00003006.*$/ || $_ =~ /^000000000010200000216.*$/ || $_ =~ /^000000000010100000216.*$/) {
    if ($_ =~ /^00003006.*$/ and length($_)==3068) {  # 
      #print "length:",length(substr($_,62)),"\n";  # 3006 + 62 = 3068
      # print "length:",length(substr($_,207)),"\n";  # 978 + 207 = 1185
      my $dbtran = get_cam216req_ref(substr($_,62));
      # print "Message: $_", "\n";
      print_message($dbtran);
      print "\n";
    } elsif (length($_)==62) { 
      # print "length:",length(substr($_,207)),"\n"; # 1069 + 207 = 1276
      my $header = get_header_ref($_);
      # print "Message: $_", "\n";
      #print_message($header);
      #print "\n";
    } else {
      print "length of string is: ", length($_),": $_", "\n";
    }
  }
}

close IFH;
exit(0);
