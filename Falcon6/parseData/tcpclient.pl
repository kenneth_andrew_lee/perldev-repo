#!/usr/bin/perl
#tcpclient.pl
use warnings;
use strict;

use IO::Socket::INET;

#use sigtrap 'handler' => \&myhand, 'INT';
# equivalent to $SIG{INT}=\&myhand;

# flush after every write
$| = 1;

my ( $socket, $client_socket, $data, $return_message );
my %message = ();
my $filename = "";
my $counter = 0;

# creating object interface of IO::Socket::INET modules which internally creates 
# socket, binds and connects to the TCP server running on the specific port.
#QA 10.211.228.80
#DEV 10.211.228.79
$socket = new IO::Socket::INET (
  PeerHost => 'localhost',
  PeerPort => '49001',
  Proto => 'tcp',
) or die "ERROR in Socket Creation : $!\n";

print "TCP Connection Success.\n";

while ($socket->recv($data,52)) {
  $counter++;
  print "$counter ";
  store_data($data); # put received message into %message
  show_message();    # display message to screen
  check_data();      # validate and send response.
}

$socket->close();
exit(0);


sub store_data {
  my $data = shift;
  print "Received from Server : $data\n";
  if (length $data != 52) {exit(1);}
  $message{eh_app_data_len} = substr($data,0,8);
  $message{eh_ext_hdr_len} = substr($data,8,4);
  $message{eh_tran_code} = substr($data,12,9);
  $message{eh_source} = substr($data,21,10);
  $message{eh_dest} = substr($data,31,10);
  $message{eh_error} = substr($data,41,10);
  $message{eh_filler} = substr($data,51,1);
}


sub check_data {
  my $tran_type = substr($message{eh_tran_code},0,1);
  if ($tran_type == 0) {return;}
  my $tran_code = substr($message{eh_tran_code},2,8);
  if ($tran_code == 50) {
    $return_message = build_returnheader();
    send_message($return_message);
  }
  if ($tran_code == 51) {
    $return_message = build_returnheader();
    send_message($return_message);
  }
  return;
}


sub build_returnheader {
  my $tran_type = substr($message{eh_tran_code},0,1);
  if ($tran_type == 1) {
    return $message{eh_app_data_len} .
      $message{eh_ext_hdr_len} .
      "2" . substr($message{eh_tran_code},1,8) .
      $message{eh_source} .
      $message{eh_dest} .
      $message{eh_error} .
      $message{eh_filler}; 
  } else {
    return "";
  }
}


sub send_message {
  my $data = shift;
  if (length($data) == 52) {
    print "Send to Server : $data - ", length $data, "\n";
    $socket->send($data);
    #shutdown($socket,1);  # Notify server that the request has been sent
  } else {
    print "Message ($data) - incorrect length to send - ", length $data, "\n";
  }
  return;
}


sub show_message {
  print "\$message{eh_app_data_len} = $message{eh_app_data_len}\n";
  print "\$message{eh_ext_hdr_len} = $message{eh_ext_hdr_len}\n";
  print "\$message{eh_tran_code} = $message{eh_tran_code}\n";
  print "\$message{eh_source} = $message{eh_source}\n";
  print "\$message{eh_dest} = $message{eh_dest}\n";
  print "\$message{eh_error} = $message{eh_error}\n";
  print "\$message{eh_filler} = $message{eh_filler}\n";
  return;
}

