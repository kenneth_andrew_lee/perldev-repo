sub get_header_ref{
  my $header = shift;
  my %header = ();
  if (length($header) != 62) {
    return \%header;
  }
  $header{EH_APP_DATA_LEN}                   = substr $header,0,8;
  $header{EH_EXT_HDR_LEN}                    = substr $header,8,4;
  $header{EH_TRAN_CODE}                      = substr $header,12,9;
  $header{EH_SOURCE}                         = substr $header,21,10;
  $header{EH_DEST}                           = substr $header,31,10;
  $header{EH_ERROR}                          = substr $header,41,10;
  $header{EH_FILLER}                         = substr $header,51,1;
  $header{EH_EXT_HDR}                        = substr $header,52,10;
  return \%header;
}

sub get_cam216req_ref{
  my $cam = shift;
  my %cam = ();
  #if (length($cam) != 3042) {
  #  return \%cam;
  #}
  $cam{ENTITY_ID}            = substr $cam,0,40;
  $cam{ENTITY_TYPE}          = substr $cam,40,40;
  $cam{CLIENT_ID}            = substr $cam,80,40;
  $cam{CASE_STATUS_CODE}     = substr $cam,120,40;
  $cam{CASE_SUB_STATUS_CODE} = substr $cam,160,40;
  $cam{FRAUD_TYPE}           = substr $cam,200,40;
  $cam{COMMENTS}             = substr $cam,240,2000;
  $cam{NUM_CODES}            = substr $cam,2240,2;
  $cam{CASE_ACTION_CODE1}    = substr $cam,2242,40;
  $cam{CASE_ACTION_CODE2}    = substr $cam,2282,40;
  $cam{CASE_ACTION_CODE3}    = substr $cam,2322,40;
  $cam{CASE_ACTION_CODE4}    = substr $cam,2362,40;
  $cam{CASE_ACTION_CODE5}    = substr $cam,2402,40;
  $cam{CASE_ACTION_CODE6}    = substr $cam,2442,40;
  $cam{CASE_ACTION_CODE7}    = substr $cam,2482,40;
  $cam{CASE_ACTION_CODE8}    = substr $cam,2522,40;
  $cam{CASE_ACTION_CODE9}    = substr $cam,2562,40;
  $cam{CASE_ACTION_CODE10}   = substr $cam,2602,40;
  $cam{CASE_ACTION_CODE11}   = substr $cam,2642,40;
  $cam{CASE_ACTION_CODE12}   = substr $cam,2682,40;
  $cam{CASE_ACTION_CODE13}   = substr $cam,2722,40;
  $cam{CASE_ACTION_CODE14}   = substr $cam,2762,40;
  $cam{CASE_ACTION_CODE15}   = substr $cam,2802,40;
  $cam{CASE_ACTION_CODE16}   = substr $cam,2842,40;
  $cam{CASE_ACTION_CODE17}   = substr $cam,2882,40;
  $cam{CASE_ACTION_CODE18}   = substr $cam,2922,40;
  $cam{CASE_ACTION_CODE19}   = substr $cam,2962,40;
  $cam{CASE_ACTION_CODE20}   = substr $cam,3002,40;

  return \%cam;
}


sub print_message_header{
  my $tran_ref = shift;

  my @keys = sort keys %{ $tran_ref };
  # Print header line
  foreach my $key (@keys) {
    print "$key,";
  }
  print "\n";
  return;
}


sub print_message{
  my $tran_ref = shift;

  my @keys = sort keys %{ $tran_ref };
  # print body
  foreach my $key (@keys) {
    print "${$tran_ref}{$key},";
  }
  return;
}

1;
