#!/usr/bin/perl
use strict;
use warnings;

use DBI;

# Pull parameters off of command line
my $database = 'DBI:Oracle:FHDBQ';
my $username = 'reporter';
my $password = 'reporter';
my $workflow;
#0160
my $pan = '49194813xxxx3597';

# connection to the database
my $dbh = DBI->connect($database, $username, $password) or die "Can't make database connect: $DBI::errstr\n";    
my $sth = $dbh -> prepare(qq(
	begin :workflow := risk_utils.get_workflow_from_pan(:pan); end;
));
$sth->bind_param(":pan", $pan);
$sth->bind_param_inout(":workflow", \$workflow, 20);

$sth->execute();

print $workflow, "\n";

# Close Connection
$dbh->disconnect();
