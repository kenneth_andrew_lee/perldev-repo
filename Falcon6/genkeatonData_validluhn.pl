#!/usr/bin/perl
use strict;
use warnings;
use Algorithm::LUHN qw(check_digit is_valid);

for (1..100) {
    my $pan = "433722" . "0" x 3 . sprintf("%06.6d", rand(100000));
    $pan .= check_digit($pan);
    printf "%-19s\n", $pan
}
