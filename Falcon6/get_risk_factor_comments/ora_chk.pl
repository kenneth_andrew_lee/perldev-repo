#!/usr/bin/perl
#
#
# Sample Call
# perl ora_chk.pl 2015100100 201511010000
#
use strict;
use warnings;

use DBI;
use DBD::Oracle qw(:ora_types);

# Pull parameters off of command line
my ($start_date, $end_date) = @ARGV;
die "Need Start Date\n" if (not defined $start_date);
die "Need End Date\n" if (not defined $end_date);

# connection to the database
my $dbh = DBI->connect('DBI:Oracle:fhdbd','reporter','reporter') or die "Can't make database connect: $DBI::errstr\n";    
my $curref;
my $sth_cursor;
my $sth = $dbh -> prepare("begin falcon_esp_jobs.get_risk_factor_comments (:start_date_in, :end_date_in, :curref); end;");
$sth->bind_param(":start_date_in", $start_date);
$sth->bind_param(":end_date_in", $end_date);
$sth->bind_param_inout(":curref", \$sth_cursor, 0, {ora_type => ORA_RSET });
$sth->execute();

### Retrieve the returned rows of data
my @row;
while(@row = map { defined($_) ? $_ : ' ***N/A*** '} $sth_cursor->fetchrow_array()) {
	print "Row: @row\n";
}
warn "Data fetching terminated early by error: $DBI::errstr\n" if $DBI::err;


# Close Connection
$dbh->disconnect();


__END__
Notes:
The stored proc falcon_esp_jobs.get_risk_factor_comments returns a cursor with 
the following columns:
    RECORD_TSTAMP as EVENT_DTTM,
    MAINT_FUNCT as CHANGE_TYPE,
    MAINT_PAN as PAN,
    PROCESSOR as PRC,
    RF_DATA_AFTER_RF_BASE AS BASE_RISK_FACTOR,
    RF_DATA_AFTER_RF_OVRD AS OVERRIDE_RISK_FACTOR,
    RF_DATA_AFTER_RF_OVRD_FROM_DT AS OVERRIDE_FROM_DATE,
    RF_DATA_AFTER_RF_OVRD_TO_DT AS OVERRIDE_TO_DATE,
    RF_NOTE_AFTER_RF_BASE_NOTE AS BASE_NOTE,
    RF_NOTE_AFTER_RF_OVRD_NOTE AS OVERRIDE_NOTE
