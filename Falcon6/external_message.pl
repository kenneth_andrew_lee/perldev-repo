#!/usr/bin/perl
# use lib "$ENV{HOME}/perl5/lib/perl5"; 
use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;
use DBI;
use strict;
#use warnings;

#my $customerAcctNumber = shift or die "Usage: $0 customerAcctNumber\n";
my $customerAcctNumber = '43230500xxxx8809'; # 1122

open (EXT,'+>',"EXTMSG.txt") || die "file not found :$!";              

my $dbh = DBI->connect("dbi:Oracle:fhdb03q","reporter","reporter");
my $current_time = strftime "%H%M%S", localtime; 
my $gmt_date = strftime "%Y%m%d", gmtime;
my $gmt_time = strftime "%H%M%S", gmtime;
my $gmtOffset = 0;
my $recordCreationDate = $gmt_date;
my $recordCreationTime = $gmt_time;
my $ext_rec_cnt = 0;
my $ext_rec_cnt_1;
#my $customerAcctNumber = "";
my $customerIdFromHeader = "";
my $serviceId = "";
my $clientIdFromHeader = "";
my $userData01 = "";
my $userData33 = "";
my $userData02 = "";
my $userData34 = "";
my $userData06 = "";
my $userData07 = "";
my $counter = 0;
my $cnt = 0;
my $validity = undef;
my $entityType = undef;
my $extSource = undef;
my $notificationName = undef;
my $notificationStatus = undef;
my $score1 = undef;
my $score2 = undef;
my $score3 = undef;
#my $userData01 = undef;
#my $userData02 = undef;
my $userData03 = undef;
my $userData04 = undef;
my $userData05 = undef;
#my $userData06 = undef;
#my $userData07 = undef;
my $userData08 = undef;
my $userData09 = undef;
my $userData10 = undef;
my $userData11 = undef;
my $userData12 = undef;
my $userData13 = undef;
my $userData14 = undef;
my $userData15 = undef;
my $userData16 = undef;
my $userData17 = undef;
my $userData18 = undef;
my $userData19 = undef;
my $userData20 = undef;
my $userData21 = undef;
my $userData22 = undef;
my $userData23 = undef;
my $userData24 = undef;
my $userData25 = undef;
my $userData26 = undef;
my $userData27 = undef;
my $userData28 = undef;
my $userData29 = undef;
my $userData30 = undef;
my $userData31 = undef;
my $userData32 = undef;
#my $userData33 = undef;
#my $userData34 = undef;
my @row = undef;
my $recordCreationMilliseconds = undef;
my $gmt_offset_diff = undef;
my $transactionDate = undef;
my $transactionTime = undef;
my $sth = undef;
my $externalTransactionId = undef;

# calculate millisecond upto two decimal places(centiSeconds)
$recordCreationMilliseconds = substr(gettimeofday, 11,2);

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
	$gmtOffset = $gmt_offset_diff - 24;
} else {
	$gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;

#connecting to databse and collecting $customerIdFromHeader
$counter = $dbh->selectrow_array('SELECT count(*) FROM cnx1310_risk_factor_changes where maint_pan = ?', undef, $customerAcctNumber);

# Print Header Line
printf EXT "B000000000%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $counter;

$sth=$dbh->prepare("select maint_pan, processor, rf_data_after_rf_base, rf_note_after_rf_base_note, rf_data_after_rf_ovrd, rf_note_after_rf_ovrd_note,rf_data_after_rf_ovrd_from_dt, rf_data_after_rf_ovrd_to_dt from cnx1310_risk_factor_changes where maint_pan = ?");
$sth->execute($customerAcctNumber);

while(@row = map { defined($_) ? $_ : ''} $sth->fetchrow_array()) {
	$customerAcctNumber      = $row[0];
	$customerIdFromHeader    = $row[0];
	$serviceId               = $row[0];
	$clientIdFromHeader      = $row[1];
	$userData01              = $row[2];
	$userData33              = $row[3];
	$userData02              = $row[4];
	$userData34              = $row[5];
	$userData06              = $row[6];
	$userData07              = $row[7];      
	$cnt++;
	$externalTransactionId = substr($clientIdFromHeader,0,6).$transactionDate.$transactionTime."EXT".sprintf ("%09d\n", $cnt);
	# Formatting the output print.
	printf EXT "DUD17DF26        EXT10   1.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s\n", $clientIdFromHeader, $recordCreationDate, $recordCreationTime, $recordCreationMilliseconds, $gmtOffset, $customerIdFromHeader, $customerAcctNumber, $externalTransactionId, $serviceId, $transactionDate, $transactionTime, $validity, $entityType, $extSource, $notificationName, $notificationStatus, $score1, $score2, $score3, $userData01, $userData02, $userData03, $userData04, $userData05, $userData06, $userData07, $userData08, $userData09, $userData10, $userData11, $userData12, $userData13, $userData14, $userData15, $userData16, $userData17, $userData18, $userData19, $userData20, $userData21, $userData22, $userData23, $userData24, $userData25, $userData26, $userData27, $userData28, $userData29, $userData30, $userData31, $userData32, $userData33, $userData34;
	#print "@row\n";
}


# Print the footer.
printf EXT "E999999999%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $counter; 
close (EXT);
