#!/usr/bin/perl
use strict;
use warnings;
 
use Net::Ping;

my @servers = ('sl73ffboapq001','sl73ffboapq002'
	, 'sl73ffbodbq003', 'sl73ffbodbq004'
	, 'sl73ffcbdbq005', 'sl73ffcbdbq006', 'sl73ffcbdbq007', 'sl73ffcbdbq008'
	, 'sl73ffcmapq003', 'sl73ffcmapq004'
	, 'sl73ffexapq003', 'sl73ffexapq004'
	, 'sl73ffhdbq001', 'sl73ffhdbq002', 'sl73ffhdbq003'
	, 'sl73ffldapq003', 'sl73ffldapq004'
	, 'sl73ffscapq005', 'sl73ffscapq006', 'sl73ffscapq007', 'sl73ffscapq008'
	, 'sl73fftdbq001', 'sl73fftdbq002', 'sl73fftdbq003'
	, 'sl73ffwsapq003', 'sl73ffwsapq004');

my %results = ();
#my @servers = ('sl73ffscapq007');

my $p = Net::Ping->new("tcp");
$p->port_number('22');

for (@servers) {
	my ($ret, $duration, $ip) = $p->ping($_, 1);
	$results{$_}{'RESULT'} = $ret;
	$results{$_}{'DURATION'} = $duration;
	$results{$_}{'IP'} = $ip;
	#print "$_ returned the value $ret at ip $ip with duration $duration", "\n";
	#if ($ret) {
	#	$results{$_}{'RESULT'} = true;
	#	#print "$_ Success! at ip $ip with duration $duration", "\n";
	#} else {
	#	$results{$_}{'RESULT'} = false;
	#	#print "$_ Failure! at ip $ip with duration $duration", "\n";
	#}
}

$p->close();

while (my ($k, $v) = each %results) {
	while (my ($key, $value) = each %$v) {
    	#print "$k: $key -> $value\n";
    	if ($key eq 'RESULT' and !$value) {
    		print "$k failed ping test","\n";
    	}
    }	
}

#for 
#print Failed Servers:


__END__
#%tgs = (
#    'top 5' =>  [ 'Best linux OS', 'Best System Monitoring', 'Best Linux Text editors' ],
#    '15 example' => [ 'rpm command', 'crontab command', 'Yum command', 'grep command' ],
#);
#
#To access all elements one by one, do the following.
#
#foreach my $key ( keys %tgs )  {
#    print "Articles in group $key are: ";
#    foreach ( @{$tgs{$key}} )  {
#        print $_;
#    }
#}
