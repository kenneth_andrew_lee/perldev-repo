#!/usr/bin/perl
use strict;
use warnings;

use Switch;

my $line = shift or die "takes one arg";
#my $line = "41036300: hncMaster Jul 23 01:02:22 : Shutdown complete";
# perl get_log_message.pl 41030400: hncCoupler         Mar  9 16:43:20 : Just sent the initial Start Msg to external device
my $tmp;
my ($code, $message, $processName, $timestamp);
my ($severityCode, $processNumber, $errorNumber);
my ($severityCodeName, $processNumberName, $errorNumberName);

# The following describes the format of messages in pmax.log. Each message is 
# terminated by a line-feed character. 
# code: processName timestamp : message
# 41036300: hncMaster Jul 23 01:02:22 : Shutdown complete


#my $LOGFILE = "pmax.log";
#open(LOGFILE) or die("Could not open log file.");
#foreach my $line (logfile) {
    chomp($line);  # remove the newline from $line.
	($code, $tmp, $message) = split /: /, $line;
	($processName, $timestamp) = split / /, $tmp, 2;

#}
#$close(LOGFILE);

print "$code\n", "$processName\n", "$timestamp\n", "$message", "\n";

#Break out $code
if ( $code =~ /(\d{1})103(\d{2})(\d{2})/ ) {
    #print " severity code: $1", "\n";
    $severityCode = $1;
    #print "process number: $2", "\n";
    $processNumber = $2;
    #print "  error number: $3", "\n";
    $errorNumber = $3;
}

print " severity code: $1", "\n", "process number: $2", "\n", "  error number: $3", "\n";

switch ($severityCode) {
	case "0"	{ $severityCodeName = "NO-ERROR" }
	case "1"	{ $severityCodeName = "FATAL" }
	case "2"	{ $severityCodeName = "ERROR" }
	case "3"	{ $severityCodeName = "WARNING" }
	case "4"	{ $severityCodeName = "INFORMATION" }
	case "5"	{ $severityCodeName = "DEBUG INFO" }
}

switch ($processNumber) {
	case "0" { $processNumberName = "Unknown Process" }
	case "4" { $processNumberName = "Coupler" }
	case "1E"	{ $processNumberName = "Account Aging" }
	case "4C"	{ $processNumberName = "List Loader" }
	case "4D"	{ $processNumberName = "Rules Reloader" }
	case "4E"	{ $processNumberName = "Rule Controller" }
	case "50"	{ $processNumberName = "RB Unload" }
	case "52"	{ $processNumberName = "HNC Exec Process" }
	case "54"	{ $processNumberName = "HNC Batch Gateway Process" }
	case "55"	{ $processNumberName = "HNC RuleBatch Process" }
	case "59"	{ $processNumberName = "Batchout Process" }
	case "5A"	{ $processNumberName = "Coupler Test" }
	case "5B"	{ $processNumberName = "Test tool" }
	case "5C"	{ $processNumberName = "ISAM Manager" }
	case "5D"	{ $processNumberName = "Batch Process" }
	case "5E"	{ $processNumberName = "Database" }
	case "5F"	{ $processNumberName = "Rules Engine" }
	case "60"	{ $processNumberName = "Spooler" }
	case "61"	{ $processNumberName = "Statistics Display" }
	case "62"	{ $processNumberName = "Shell Tool" }
	case "63"	{ $processNumberName = "Master Process" }
	case "65"	{ $processNumberName = "AAR_RRM" }
	case "66"	{ $processNumberName = "DBFE" }
	case "6B"	{ $processNumberName = "GUTS MQI" }
	case "6C"	{ $processNumberName = "GUTS Scheduler" }
	case "6D"	{ $processNumberName = "Hotlist refresh" }
}

switch ($errorNumber) {
	case "00"	{ $errorNumberName = "UNKNOWN" }
	case "01"	{ $errorNumberName = "FATAL_ERROR" }
	case "02"	{ $errorNumberName = "ERROR" }
	case "03"	{ $errorNumberName = "WARNING_ERROR" }
	case "04"	{ $errorNumberName = "INFORMATIONAL" }
	case "f3"	{ $errorNumberName = "CUSTOMER_ERROR" }
	case "f4"	{ $errorNumberName = "PRODUCT_ERROR" }
	case "f5"	{ $errorNumberName = "ISAM_ERROR" }
	case "f6"	{ $errorNumberName = "FILE_ERROR" }
	case "f7"	{ $errorNumberName = "MESSAGE_ERROR" }
	case "f8"	{ $errorNumberName = "MEMORY_ERROR" }
	case "f9"	{ $errorNumberName = "DATABASE_ERROR" }
	case "fa"	{ $errorNumberName = "PROCESS_ERROR" }
	case "fb"	{ $errorNumberName = "PARAMETER_ERROR" }
	case "fc"	{ $errorNumberName = "ILLEGAL_MESSAGE" }
	case "fd"	{ $errorNumberName = "SOCKET_ERROR" }
	case "fe"	{ $errorNumberName = "CONFIG_ERROR" }
	case "ff"	{ $errorNumberName = "TIMEOUT_ERROR" }
	case "af"	{ $errorNumberName = "DBINSERT_ERROR" }
	case "ae"	{ $errorNumberName = "DBSTARTUP_ERROR" }
	case "ad"	{ $errorNumberName = "DBUPDATE_ERROR" }
	case "ba"	{ $errorNumberName = "USER_RULE_ERROR" }
}

print "severity:$severityCodeName,", "process:$processNumberName,", "error:$errorNumberName,", "message:", "$message", "\n";

return;
