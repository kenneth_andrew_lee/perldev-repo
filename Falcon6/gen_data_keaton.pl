#!/usr/bin/perl
use strict;
use warnings;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
my $date = $year+1900 . sprintf "%02d", $mon+1 . sprintf "%02d", $mday;
my $time =  sprintf "%-2.2d", $hour . sprintf "%-2.2d", $min . sprintf "%-2.2d", $sec;

# Create 1,000,000
for (1..100000) {
	my $pan = "4" . "0" x 8 . sprintf("%07.7d", rand(10000)) . "   ";
	#my $customerAccountNumber = sha1_hex($pan);
	$pan = sprintf "%-16.16d", $pan;
	for ( 1..10) {
		my $milli = sprintf "%03d", $_;
		my $amt = (rand(997) + 2);	
		printf "%040s,%-19s,%06.2f,%08s,%06s,%03s",$pan, $pan,$amt, $date,$time,$milli;
		print "\n";
	}
}

__END__
# use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);  # don't need this any longer
# customerAccountNumber,pan,amt,date,time,milli
