#!/usr/bin/perl -w
use Tk;
$mw = MainWindow->new;
$mw->title("Documents");
# Create a frame for our menubar across the top of the window
$f = $mw->Frame(-relief => 'ridge', -borderwidth => 2)
->pack(-side => 'top', -anchor => 'n', -expand => 1, -fill => 'x');
# Create the menubutton, with two items: New Doc and a separator
$filem = $f->Menubutton(-text => "File",
-tearoff => 0,
-menuitems => [ ["command" => "New Document",
-command => \&new_document],
"-"
]) ->pack(-side => 'left');
# We will open document 1 to begin with, and we want to limit the number
# of documents in our list to 0-9 (leaves 10 docs max in menu)
$doc_num = 1;
$doc_list_limit = 9;
# Create button that will do the same thing as the New Document menu item
$mw->Button(-text => "New Document",
-command => \&new_document)->pack(-side => 'bottom',
-anchor => 'e');
# The entry will display the current doc we are "editing".
$entry = $mw->Entry(-width => 80) ->pack(-expand => 1, -fill => 'both');
MainLoop;
# Creates the next doc in line, incs the doc counter
# Adds the new doc to the menu, and removes any docs from the
# menu that are over the limit (oldest out first)
sub new_document {
my $name = "Document $doc_num";
$doc_num++;
push (@current, $name);
$filem->command(-label => "$name",
-command => [ \&select_document, $name ]);
&select_document ($name);
if ($#current > $doc_list_limit) {
$filem->menu->delete(2);
shift (@current);
}
}sub select_document {
my ($selected) = @_;
$entry->delete(0, 'end');
$entry->insert('end', "SELECTED DOCUMENT: $selected");
}
