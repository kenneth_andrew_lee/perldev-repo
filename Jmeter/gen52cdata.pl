#!/usr/bin/perl
################################################################################
# Creates data for jmeter to run testing in 52c
# 
# 
# 
# 
# 
# 
################################################################################

use strict;
use warnings;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# Static Variables
# 9999
#my $acct_nbr             = '49599939xxxx9955';   
my $card_post_code       = '226030000';              
my $birth_date           = '19630101';               
#my $trans_date           = '20130628';           
#my $trans_time           = '080000';             
#my $tran_amt             = '+000000010.01';      
my $merch_post_code      = '801110000';          
my $merch_id             = '    331234';         
my $ext_score1           = '0025';               
my $portfolio            = 'FALCON TEST TR';     
my $client_id            = 'PRCC95';             
my $merch_nm             = 'MERCHANT #1234';     
my $merch_city           = 'DENVER';             
my $merch_st             = 'CO';                 
#3333
my $card_master_acct_nbr = '11112222xxxx4444';
my $recur_auth_exp_date  = '20161231';           
my $card_status_date     = '20161231';           


my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
my $trans_date = $year+1900 . (sprintf "%02d", $mon+1) . sprintf "%02d", $mday;

# Create xxx cards and 100,000 amounts: 10000
for (1..100000) {
	my $acct_nbr = "4" . "0" x 9 . sprintf("%06.6d", rand(100000)) . "   ";

	for ( 1..5) {
		($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);	
		my $trans_time =  sprintf "%-02.2d", $hour . sprintf "%-02.2d", $min . sprintf "%-02.2d", $sec;
		my $tran_amt = (rand(99999.00)/100);	
		$tran_amt = sprintf "+%012.2f", $tran_amt;
		printf "\"%-19s\",\"%-9s\",\"%-8s\",\"%-8s\",\"%-6s\",\"%-13s\",\"%-9s\",\"%-14s\"", $acct_nbr, $card_post_code, $birth_date, $trans_date, $trans_time, $tran_amt, $merch_post_code, $client_id;
		print "\n";
	}
}

sub usage_example {
	print 'C:\Program Files\Java\apache-jmeter-2.11\bin>perl gen52cdata.pl > abc.txt', "\n";
}

__END__
# Fields being created:
acct_nbr,card_post_code,birth_date,trans_date,trans_time,tran_amt,merch_post_code,client_id
