#!/usr/bin/perl
use strict;
use warnings;
my $amt;
for (1..100000) {
	my $pan = "4" . "0" x 9 . sprintf("%06.6d", rand(100000));
	for ( 1..10) {
		if ($_ == 6) {
			$amt = 6.00;	
		} else {
			$amt = (rand(997) + 2);	
		}
		printf "%040s,%-19s,%06.2f\n",$pan, $pan, $amt;
		#print "\n***$hour****\n";
	}
}

sub usage_example {
	print 'C:\Program Files\Java\apache-jmeter-2.11\bin>perl gen64data.pl > score.dat', "\n";
}

