#!/usr/bin/perl
#
# Problem 3a
use strict;
use warnings;
#use Data::Dumper;

my $filename = '3.dat';
my %hash = ();

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";


# Example data
##1 @ 387,801: 11x22
while (my $row = <$fh>) {
#while (my $row = <DATA>) {	
	chomp $row;
	my @values = split / /, $row;

	my @coordinates = split /,/, $values[2];
	substr($coordinates[1], length($coordinates[1])-1 , 1) = "";
	my @size = split /x/, $values[3];

	#print "\$coordinates[0] = " . $coordinates[0] . "\n";
	#print "\$coordinates[1] = " . $coordinates[1] . "\n";
	#print "       \$size[0] = " . $size[0] . "\n";
	#print "       \$size[1] = " . $size[1] . "\n\n";

	for my $i ($coordinates[0] .. $coordinates[0]+$size[0]-1) {
		for my $j ($coordinates[1] .. $coordinates[1]+ $size[1]-1) {
			#print "\$hash{$i}{$j}=$hash{$i}{$j}\n";
			if ($hash{$i}{$j}) {
				$hash{$i}{$j} += 1;
			} else {
				$hash{$i}{$j} = 1;
			}
		}
	}
}
close ($fh);

# Count how many hash elements have a value > 1
my $counter = 0;
foreach my $outter (keys %hash) {
	foreach my $inner (keys %{$hash{$outter}}) {
		if ($hash{$outter}{$inner} > 1) {
			$counter += 1;
			#print "\$hash{$outter}{$inner}=$hash{$outter}{$inner}\n"
		}
	}
}

print "$counter\n";


# 115304 is the right answer

#print Dumper(@values) . "\n";
#print Dumper(@coordinates) . "\n";
#print Dumper(@size) . "\n";
#1 @ 387,801: 11x22
#2 @ 101,301: 19x14
#3 @ 472,755: 11x16
#4 @ 518,720: 23x17
#5 @ 481,939: 29x20
#6 @ 665,299: 23x18
#7 @ 866,652: 28x19
#8 @ 77,274: 26x21
#9 @ 621,302: 20x22
#10 @ 936,278: 13x27

__DATA__
#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
