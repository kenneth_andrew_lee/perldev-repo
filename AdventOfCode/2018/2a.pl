#!/usr/bin/perl
# Keep count of frequency of letters.
# 
# 
use strict;
use warnings;
use Data::Dumper;

my $filename = '2.dat';
my $cnt2=0;
my $cnt3=0;
my %letters=();

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";
	 
while (my $row = <$fh>) {
	%letters=();
	chomp $row;
	for my $c (split //, $row) {
		if (exists $letters{$c}) {
			$letters{$c} += 1
		} else {
			$letters{$c} = 1
		}
	}
	#print Dumper(%letters);
	my $tmp2 = 0;
	my $tmp3 = 0;
	foreach my $key ( keys %letters ) {
		if ($letters{$key} == 2) {
			$tmp2 = 1;
		}
		if ($letters{$key} == 3) {
			$tmp3 = 1;
		}
	}
	$cnt2 += $tmp2;
	$cnt3 += $tmp3;
}
close ($fh);

print $cnt2*$cnt3 . "\n";

# 13692 is not the answer