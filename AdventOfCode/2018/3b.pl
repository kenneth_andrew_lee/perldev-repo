#!/usr/bin/perl
#
# Problem 3b
# Amidst the chaos, you notice that exactly one claim doesn't overlap by even a 
# single square inch of fabric with any other claim. If you can somehow draw 
# attention to it, maybe the Elves will be able to make Santa's suit after all!
# For example, in the claims above, only claim 3 is intact after all claims are made.
# What is the ID of the only claim that doesn't overlap?
#
# Example data
##1 @ 387,801: 11x22
use strict;
use warnings;
#use Data::Dumper;

my $filename = '3.dat';
my %hash = ();

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";


#my $data_start = tell DATA; 	
while (my $row = <$fh>) {
#while (my $row = <DATA>) {	
	chomp $row;
	my @values = split / /, $row;

	my @coordinates = split /,/, $values[2];
	substr($coordinates[1], length($coordinates[1])-1 , 1) = "";
	my @size = split /x/, $values[3];

	#print "\$values[0] = " . $values[0] . "\n";
	#print "\$coordinates[0] = " . $coordinates[0] . "\n";
	#print "\$coordinates[1] = " . $coordinates[1] . "\n";
	#print "       \$size[0] = " . $size[0] . "\n";
	#print "       \$size[1] = " . $size[1] . "\n\n";

	for my $i ($coordinates[0] .. $coordinates[0]+$size[0]-1) {
		for my $j ($coordinates[1] .. $coordinates[1]+ $size[1]-1) {
			#print "\$hash{$i}{$j}=$hash{$i}{$j}\n";
			if ($hash{$i}{$j}) {
				$hash{$i}{$j} = "X";
			} else {
				$hash{$i}{$j} = $values[0];
			}
		}
	}
}
close ($fh);

#seek DATA, $data_start, 0;

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";
while (my $row = <$fh>) {
#while (my $row = <DATA>) {	
	chomp $row;
	my @values = split / /, $row;

	my @coordinates = split /,/, $values[2];
	substr($coordinates[1], length($coordinates[1])-1 , 1) = "";
	my @size = split /x/, $values[3];

	my $isxflag = 1;
	my $testvalue = $values[0];
	for my $i ($coordinates[0] .. $coordinates[0]+$size[0]-1) {
		for my $j ($coordinates[1] .. $coordinates[1]+ $size[1]-1) {
			#print "($i,$j) $hash{$i}{$j}\n";
			if ($hash{$i}{$j} ne $testvalue) {
				if ($hash{$i}{$j} eq 'X') {
					$isxflag = 0;
				} else {
					if ($isxflag) {
						print "Answer: $testvalue\n";
					}
					$testvalue = $hash{$i}{$j};  ## new value
					$isxflag = 1;
				}
			}	
		}
	}
	if ($isxflag) {
		print "Answer: $testvalue\n";
	}
}
close ($fh);



## Count how many hash elements have a value > 1
#foreach my $outter (sort keys %hash) {
#	foreach my $inner (sort keys %{$hash{$outter}}) {
#		my $c = $hash{$inner}{$outter};
#		if (scalar @{ $c } == 1) {
#			print "($inner,$outter)@$c ";
#		}
#	}
#	print "\n";
#}
#print "\n\n";


#print Dumper(@values) . "\n";
#print Dumper(@coordinates) . "\n";
#print Dumper(@size) . "\n";
#1 @ 387,801: 11x22
#2 @ 101,301: 19x14
#3 @ 472,755: 11x16
#4 @ 518,720: 23x17
#5 @ 481,939: 29x20
#6 @ 665,299: 23x18
#7 @ 866,652: 28x19
#8 @ 77,274: 26x21
#9 @ 621,302: 20x22
#10 @ 936,278: 13x27

__DATA__
#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
