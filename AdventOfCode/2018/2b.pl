#!/usr/bin/perl
#
# Confident that your list of box IDs is complete, you're ready to find the 
# boxes full of prototype fabric.

# The boxes will have IDs which differ by exactly one character at the same 
# position in both strings. For example, given the following box IDs:

# abcde
# fghij
# klmno
# pqrst
# fguij
# axcye
# wvxyz
# The IDs abcde and axcye are close, but they differ by two characters (the 
# second and fourth). However, the IDs fghij and fguij differ by exactly one 
# character, the third (h and u). Those must be the correct boxes.

# What letters are common between the two correct box IDs? (In the example 
# above, this is found by removing the differing character from either ID, 
# producing fgij.)
#
use strict;
use warnings;
use Data::Dumper;

my $filename = '2.dat';
my @list = ();
my %letters = ();
my $match = "";
my $length = 0;

open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";
	 
while (my $row = <$fh>) {
	chomp $row;
	$length = length($row);
	push(@list, $row);
}
close ($fh);
print "length of row:" . $length . "\n";

foreach  my $number (0..$length) {
	#print "$number\n";
	my %hash = ();
	for my $element (@list) {
		#print "\$element:$element\n";
		my $value = $element;
		substr($value, $number, 1) = "";
  		#print "\$value:$value\n";
		if (exists $hash{$value}) {
			# Found our match
			$match = $value;
			print "Match found($number):" . $match . "\n";
			last;
		} else {
			$hash{$value} = 1;
		}
	}
	if ($match) {
		last;
	}
}

print "$match\n";

# cypueihajytordkgzxfqplbwn

