#!/usr/bin/perl
# What is the first frequency your device reaches twice?
# Store in a hash.  If found, counter will be a two, and we have our solution.
# Terminalogy:  The result is the frequency.
use strict;
use warnings;
 
my $frequency = 0;
my %freq = ();
my $filename = '1.dat';
my $result = "";


for (my $i = 1; $i < 1000; $i++) {
	print "$i\n";

	open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";
	 
	while (my $row = <$fh>) {
		chomp $row;
		$frequency += $row;
		# print "$frequency ";
		#Is frequency in hash?
		if (exists $freq{$frequency}) {
			# winner winner chicken dinner
			print "\n$frequency\n";
			$result = $frequency;
			last;
		} else {
			$freq{$frequency} = 1;
		}
	}
	close ($fh);

	if ($result) {
		print "Result not empty";
		last;
	}
}
#print "\n\nFrequency: $frequency\n";
print "\n\nAnswer: $result\n";
