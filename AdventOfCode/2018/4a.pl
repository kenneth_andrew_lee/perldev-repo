#!/usr/bin/perl
#
# Problem 4a
use strict;
use warnings;
#use Data::Dumper;

my $filename = '4.dat';
my %hash = ();

#open(my $fh, '<:encoding(UTF-8)', $filename) or die "Could not open file '$filename' $!";

# Get data into segements that have:
# Date (mm-dd)
# ID of the Guard
# Minutes asleep

#while (my $row = <$fh>) {
while (my $row = <DATA>) {	
	chomp $row;
	#my @values = split / /, $row;
	#substr EXPR,OFFSET,LENGTH
	my $fulldate = substr $row, 1, 16;
	my $date = substr $row, 6,5;
	my ($ID) = $row =~ /Guard (#\d{1,3})/;
	my $data = substr $row, 19;
	my $hours = substr $row, 12, 2;
	my $minutes = substr $row, 15, 2;

	if (defined $ID) {
		print "$date\t$ID\t$data\t\t\t$hours\t$minutes\n";
	} else {
		print "$date\t\t$data\t\t\t$hours\t$minutes\n";
	}

	#my @coordinates = split /,/, $values[2];
	#substr($coordinates[1], length($coordinates[1])-1 , 1) = "";
	#my @size = split /x/, $values[3];


}
#close ($fh);


__DATA__
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up