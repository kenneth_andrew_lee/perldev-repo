#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

#my $value1 = $ARGV[0];
my $value1 = '897494889999';

# write your solution here
my %hash = ();
foreach my $char (split //, $value1) {
    if (defined $hash{$char}) {
        $hash{$char} += 1; 
    } else {
        $hash{$char} = 1;
    }
}
my @hashes = sort { $hash{$a} <=> $hash{$b} } keys %hash;
my $max = $hashes[-1];
print "$max";
