use Data::Dumper;
#my $value1 = $ARGV[0];
#my $value1 = 1919;
my $value1 = 9988777754;
TOP:
my $sum=0;
for($value1=~/./sg) {
  $sum+=$_;
}
if ($sum>9) {
  $value1=$sum;
  goto TOP;
}
print $sum;

#print Dumper(%hash), "\n";

exit(0);
__END__

=for comment
Given n, take the sum of the digits of n. If that value has more 
than one digit, continue reducing in this way until a 
single-digit number is produced.

Inputs
value1

A whole number.
Sample Test Cases
Sample Input 1

value1: 1919
output: 2
=cut