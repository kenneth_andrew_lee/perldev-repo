#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
# 1.27614,-0.60947
# ax^2+bx+c=0'
# quadratic equation, which is: (-b+-√b^2-4ac) / 2a.
my $value1 = "9x^2+6x-7=0";
#my $value1 = "x^2-6x+9=0";
#print $value1,"\n";
my ($a, $b, $c) = $value1 =~ /^(.*)x\^2(.+)x(.+)=0$/; 
if (!$a) {
    $a = 1;
}
my ($r1,$r2) = 0;
my $descr = $b * $b - 4 * $a * $c;
if ($descr < 0) {
    print "imaginary,imaginary";
} else {
    $r1 = (-$b + sqrt($descr)) / (2 * $a);
    $r2 = (-$b - sqrt($descr)) / (2 * $a);
    ($r1 > 0) ? printf("%.7s", $r1) : printf("%.8s", $r1);
    print ",";
    ($r2 > 0) ? printf("%.7s", $r2) : printf("%.8s", $r2);
}