#!/usr/bin/env python3

# Special array merge
# Merge two arrays according to special rules.
#
# Solved
# Instructions
# You'll get two inputs, each will be a comma separated list of numbers. Each input will contain the same amount of numbers. You'll need to merge the two lists by inserting numbers from value2 into value1. Each number from value2 should be inserted such that it is placed after the next larg(est/er ?) number from value1
# Inputs
# value1
# First comma separated list of numbers
#
# value2
# Second comma separated list of numbers
#
# Sample Test Cases
# Short List
# value1 9,5
# value2 7,10
# output 9,10,5,7
# Large List
# value1 34,18,4,102
# value2 15,19,120,64
# output 34,64,18,19,4,15,102,120
#####
#Testing long list failed
#value1 1,120,37,17,64,120,24,55,500,255
#value2 5,40,21,135,32,110,121,68,300,1000
#expected output 1,5,120,135,37,40,17,21,64,110,120,121,24,32,55,68,500,1000,255,300
#actual output 1,5,120,121,37,40,17,21,64,68,120,135,24,32,55,110,500,1000,255,300
#####
import sys
 
#value1 = sys.argv[1]
#value2 = sys.argv[2]
 
#value1 = "1,120,37,17,64,120,24,55,500,255"
value1 = "1,119,37,17,64,120,24,55,500,255"
value2 = "5,40,21,135,32,110,121,68,300,1000"
#expected output 1,5,120,135,37,40,17,21,64,110,120,121,24,32,55,68,500,1000,255,300
#actual output 1,5,120,121,37,40,17,21,64,68,120,135,24,32,55,110,500,1000,255,300
# actual output 1,5,119,121,37,40,17,21,64,68,120,135,24,32,55,110,500,1000,255,300
# write your solution here
items = list()
items1 = [int(e) for e in value1.split(',')]  # List of integers
items2 = [int(e) for e in value2.split(',')]  # List of integers
items2.sort()
# print (items2)  # debug printing
for item1 in items1:
    items.append(item1)
    for item2 in items2:
        if item2 > item1:
            items.append(item2)
            items2.remove(item2)
            break
 
str1 = ','.join(str(e) for e in items)
print (str1)

