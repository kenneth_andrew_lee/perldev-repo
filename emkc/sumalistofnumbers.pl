#!/usr/bin/perl
use strict;
use warnings;

# write your solution here
#my $value1 = $ARGV[0];
my $value1 = "-3,9,-10,19,-5,-21,9,-4";

# write your solution here
my $sum = 0;
foreach my $value (split /,/, $value1) {
    $sum += $value;
}
print $sum;