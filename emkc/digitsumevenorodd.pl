#!/usr/bin/perl
use strict;
use warnings;

#my $value1 = $ARGV[0];
my $value1 = 662638;

# write your solution here
my @value1 = split //, $value1;
my $sum = 0;

foreach (@value1) {
    $sum += $_;
}

if ($sum%2==0) {
    print "even";
} else {
    print "odd";
}