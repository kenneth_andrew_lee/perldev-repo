#!/usr/bin/perl
# perl bugtracking.pl 'normal 1 normal 2 normal 3'
# 1 2 3
#
# 
# perl bugtracking.pl 'high 1 high 2 high 3'
# 3 2 1
#
# 
# perl bugtracking.pl 'high 802 normal 810 normal 561 normal 537 normal 842 high 707 normal 921 high 775 high 810 high 221 high 844 normal 599 high 525 high 282 high 693 high 848 high 124 high 711 normal 900 high 355'
# 355 711 124 848 693 282 525 844 221 810 775 707 802 810 561 537 842 921 599 900
# 
#
# perl bugtracking.pl normal e normal e normal r high n high i high g normal m normal a high n high e normal n
# e n g i n e e r m a n
# 
#
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

@a=split/ /,pop;for($i=0;$i<~~@a;$i+=2){if($a[$i]eq"high"){unshift(@r,$a[$i+1])}else{push(@r,$a[$i+1])}}print join" ",@r;

exit();


__END__

    print "$a[$i+1]", "\n";


=for winners 
# by hydrazer
pop=~s/(.)( \S+)/$\=l eq$1?$\.$2:($2.$\)/ger;print
=cut


=for problem
Bug-tracking system
The EngineerMan team need your help in creating a bug-tracking system. They want to keep a 
list of all bugs in their applications. They classify bugs according to whether they are 
"high" priority or "normal". Each bug has an identifier which could be a number or a letter.

When a high-priority bug is inserted into the list, it is immediately moved to the 
beginning. When a normal bug is inserted, it is moved to the end.

Your input is going to be space-separated pairs of priorities/identifiers (each pair 
is also separated by a space). Your program will output a space-separated list of the 
bug identifiers in the correct order.

Test Cases
Test Case 1
Argument 1
normal 1 normal 2 normal 3

Expected Output
1 2 3
Test Case 2
Argument 1
high 1 high 2 high 3

Expected Output
3 2 1
Test Case 3
Argument 1
high 802 normal 810 normal 561 normal 537 normal 842 high 707 normal 921 high 775 high 810 high 221 high 844 normal 599 high 525 high 282 high 693 high 848 high 124 high 711 normal 900 high 355

Expected Output
355 711 124 848 693 282 525 844 221 810 775 707 802 810 561 537 842 921 599 900
Test Case 4
Argument 1
normal e normal e normal r high n high i high g normal m normal a high n high e normal n

Expected Output
e n g i n e e r m a n
=cut


=for testcase
Test Case 1
Argument 1
'normal 1 normal 2 normal 3'
Expected Output
1 2 3

Test Case 2
Argument 1
'high 1 high 2 high 3'
Expected Output
3 2 1

Test Case 3
Argument 1
'high 802 normal 810 normal 561 normal 537 normal 842 high 707 normal 921 high 775 high 810 high 221 high 844 normal 599 high 525 high 282 high 693 high 848 high 124 high 711 normal 900 high 355'
Expected Output
355 711 124 848 693 282 525 844 221 810 775 707 802 810 561 537 842 921 599 900

Test Case 4
Argument 1 
'normal e normal e normal r high n high i high g normal m normal a high n high e normal n'
Expected Output
e n g i n e e r m a n
=cut

=for checkingitout

     1  2  3  4         1  2  3  4
A =  5  6  7  8   B =   5  6  7  8
     9 10 11 12         9 10 11 12
    13 14 15 16        13 14 15 16

=cut