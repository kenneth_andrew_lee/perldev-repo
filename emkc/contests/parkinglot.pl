#!/usr/bin/perl

# perl parkinglot.pl '15 10 5 25 20'
# 15 10 5 25 20
#
#

# perl parkinglot.pl '20 25 49 32 77 70 24'
# 49 77 70 24 25 32 20
#
# 

# perl parkinglot.pl '16210 17184 13069 16254 9856 7089 8817 20489 17750 2479 13095 9404'
# 17184 13069 17750 13095 9856 20489 16254 2479 9404 8817 7090 16211
#
# 

# perl parkinglot.pl 'race car!'
# 17184 13069 17750 13095 9856 20489 16254 2479 9404 7089 16210 8817
# 
# 

# perl parkinglot.pl '24214 11994 6910 21924 39278 30603 15855 15101 35407 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387'
# 21924 24214 6910 11994 15855 15101 30603 35407 39278 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387
# 
# 

#use POSIX qw/ceil/;
#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


@d=split/ /,pop;
$m=scalar@d;
for(@d){
  $s=$_%$m;
  while(1){
    if($a[$s]){
      $s++
    }else{
      $a[$s]=$_." ";
      last;
    }
  }
}
print@a;

exit();


__END__


#'15 10 5 25 20'
my @d=split/ /,pop;
#print join",",@d, "\n";
my $m=scalar @d;
my @a=();
for (@d) {
  my $s=$_%$m;
  #print "\$_:$_", " ", "\$m:$m", " ", "\$s:$s","\n";
  while(1) {
    if($a[$s]){
      #print "\$a[$s]" . " is true\n";
      $s++;
    } else {
      #print "\$a[$s]" . " is false\n";
      $a[$s]=$_." ";
      last;
    }
  }
  #print "     \$_:$_", " ", "\$m:$m", " ", "\$s:$s", " ", "\$a[$s]:$a[$s]", "\n";  
}



=for winners 
# Hydrazer
map{$@=$_%@@;$@++while$$[$@];$$[$@]=$_}@@=<@ARGV>;print"@$"

# Natte
map{$i=$_%@a;$i++while@r[$i];@r[$i]=$_}@a=glob pop;print"@r"

=cut


=for problem
Parking lot
The EngineerMan team have decided to make a parking lot in which the cars are parked in a certain slot 
according to their plate number. The slot number is given by the following equation:
slot_number = plate_number mod number_of_slots

If a car wants to park in a slot that is already occupied, it should park in the 
next slot. If the next slot is already occupied, it should park in the one after, 
and so on. You can safely assume that no overflows are going to happen.

The given input is a series of space-separated numbers representing the plate numbers 
You can safely assume that the number of slots is always equal to the number of 
cars (plates). The expected output is the plate numbers separated by a space and 
sorted by their slot numbers.

Test Cases

Test Case 1
Argument 1
15 10 5 25 20
Expected Output
15 10 5 25 20

Test Case 2
Argument 1
20 25 49 32 77 70 24
Expected Output
49 77 70 24 25 32 20

Test Case 3
Argument 1
17184 7090 13069 16254 9856 8817 20489 17750 2479 13095 9404 16211
Expected Output
17184 13069 17750 13095 9856 20489 16254 2479 9404 8817 7090 16211

Test Case 4
Argument 1
16210 17184 13069 16254 9856 7089 8817 20489 17750 2479 13095 9404
Expected Output
17184 13069 17750 13095 9856 20489 16254 2479 9404 7089 16210 8817

Test Case 5
Argument 1
24214 11994 6910 21924 39278 30603 15855 15101 35407 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387
Expected Output
21924 24214 6910 11994 15855 15101 30603 35407 39278 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387
=cut


=for testcase
Test Case 1
'15 10 5 25 20'
Expected Output
15 10 5 25 20

Test Case 2
'20 25 49 32 77 70 24'
Expected Output
49 77 70 24 25 32 20

Test Case 3
'17184 7090 13069 16254 9856 8817 20489 17750 2479 13095 9404 16211'
Expected Output
17184 13069 17750 13095 9856 20489 16254 2479 9404 8817 7090 16211

Test Case 4
'16210 17184 13069 16254 9856 7089 8817 20489 17750 2479 13095 9404'
Expected Output
17184 13069 17750 13095 9856 20489 16254 2479 9404 7089 16210 8817

Test Case 5
'24214 11994 6910 21924 39278 30603 15855 15101 35407 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387'
Expected Output
21924 24214 6910 11994 15855 15101 30603 35407 39278 15818 15355 29550 37973 38392 27430 32911 40304 33502 25496 20863 22387
=cut


=for misc

=cut