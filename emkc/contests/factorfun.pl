#!/usr/bin/perl
# perl factorfun.pl 12
# 28
# 
# 
# perl factorfun.pl 4
# 7
# 
# 
# perl factorfun.pl 48
# 124
# 
# 
# perl factorfun.pl 192
# 508
# 
# 
# perl factorfun.pl 11520
# 39858
# 
# 
# perl factorfun.pl 46080
# 159666
# 
# 
# perl factorfun.pl 552960
# 1965840
# 
# 
# perl factorfun.pl 2211840
# 7864080
# 
# 
# perl factorfun.pl 28350
# 90024
# 
# 
# perl factorfun.pl 113400
# 450120
# 
# 
# perl factorfun.pl 1360800
# 5687136
# 
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$x=pop;for(1..$x){$s+=$_ if$x%$_==0}print$s;

exit();

__END__

$x=pop;
for(1..$x){
  $s+=$_ if$x%$_==0;
}
print $s;




=for test
function sumfactors(number) {
    let sum_value = 0;
    for (let counter = 1; counter < number ; counter++) {
        if ((number%counter)==0) {
            sum_value += counter;
        }
    }
    return sum_value;
}

exports.sumfactors = sumfactors;
=cut

=for winners
@n%$_ or$\+=$_ for@n=1..pop;print
hashkitten

$\+=$n%$_?0:$_ for 1..($n=pop);print
Lyndon

=cut


=for comment
$_ The default input and pattern-searching space.
$" When an array or an array slice is interpolated into a double-quoted string or a similar context such as /.../, its elements are separated by this value. Default is a space.
$` The string preceding whatever was matched by the last successful pattern match, not counting any matches hidden within a BLOCK or eval enclosed by the current BLOCK.
=cut



=for comment
Factor Fun
The EngineerMan team have been experimenting with numbers and noticed that there are lots of other numbers that can be used to divide this number into another integer number (factors). They took this one step further and wanted to compute the sum of all the unique factors. They need your help to write such a program.

Your input will be a singular number N.

All numbers N will satisfy N = (2^a)(3^b)(5^c)(7^d) for some values a, b, c, d such that 1<N<2^32. a,b,c & d are all natural numbers.

Your output should be a number which is the sum of all possible positive real integer factors of a N.

Beware that your code only has a limited amount of time to run!

Example:

Input: 12
Factors of 12:
- 1
- 2
- 3
- 4
- 6
- 12
Sum of factors: 1 + 2 + 3 + 4 + 6 + 12 = 28
Output: 28


Input: 4
Factors of 4:
 - 1
 - 2 # 2(2), but we only want unique factors
 - 4
﻿Sum of factors: 1 + 2 + 4 = 7
Output: 7


Test Cases
Test Case 1
Argument 1
12
Expected Output
28
Test Case 2
Argument 1
4
Expected Output
7
Test Case 3
Argument 1
48
Expected Output
124
Test Case 4
Argument 1
192
Expected Output
508
Test Case 5
Argument 1
11520
Expected Output
39858
Test Case 6
Argument 1
46080
Expected Output
159666
Test Case 7
Argument 1
552960
Expected Output
1965840
Test Case 8
Argument 1
2211840
Expected Output
7864080
Test Case 9
Argument 1
28350
Expected Output
90024
Test Case 10
Argument 1
113400
Expected Output
450120
Test Case 11
Argument 1
1360800
Expected Output
5687136
=cut

