#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;

#perl ./primeadvantage.pl 0
# 2
#perl ./primeadvantage.pl 19
# 19
#perl ./primeadvantage.pl 48
# 53
#perl ./primeadvantage.pl 
#

$_=pop;
$_=1x$_;s/^(11+?)\g1+$//;print$_>1;



exit();
__END__

sub P{for($i=2;$i<$n;$i++){if($n%$i==0){return 0}}return 1}
$n=pop;
if($n<3){$n=2}
while(P()==0){$n++}
print$n;


sub P{for($i=2;$i<$n;$i++){if($n%$i==0){return 0}}return 1}$n=pop;if($n<3){$n=2}while(P()==0){$n++}print$n;


sub P{for($i=2;$i<$n;$i++){if($n%$i==0){return 0}}return 1}$n=pop;if($n<3){$n=2}while(P($n)==0){$n++}print$n;


sub P{for($i=2;$i<$n;$i++){if($n%$i==0){return 0}}return 1}
$n=pop;
if($n<3){$n=2}
while(P($n)==0){$n++}
print$n;


=for docs
n = Math.abs(n);
// 2, 3 are prime
if(n<=3) return n>1;
// multiples of 2, 3 not prime
if(n % 2===0 || n % 3===0) return false;
// factor of 6k-1 or 6k+1 => not prime
for(var i=6, I=Math.sqrt(n)+1; i<=I; i+=6)
  if(n % (i-1)===0 || n % (i+1)===0) return false;
return true;
=cut

=for comment
Prime Advantage
You'll be given a number, it might be prime, it might not. If the number is prime, output the number 
supplied. If the number isn't prime, output the next highest prime number.

Test Cases

Test Case 1
Argument 1: 0
Expected Output: 2

Test Case 2
Argument 1: 19
Expected Output: 19

Test Case 3
Argument 1: 48
Expected Output: 53
=cut



