#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl symmetricmerge.pl ainotu eirsty
# aenorsuy
# perl symmetricmerge.pl eginr amn
# aegimr
# perl symmetricmerge.pl inopst bot
# binps

$_=join'',sort split//,pop.pop;s/(.*)\1//g;print;


exit();
__END__


# winner JoKing
print grep{//&grep/$'/,@ARGV}a..z




=for comment
Symmetric Merge
You are given two inputs, each input is a sorted collection of a-z characters.
You have to merge them together following two rules:
  + remove non unique letters.
  + keep it sorted.

Test Cases
Test Case 1
Argument 1: ainotu
Argument 2: eirsty
Expected Output: aenorsuy
Test Case 2
Argument 1: eginr
Argument 2: amn
Expected Output: aegimr
Test Case 3
Argument 1: inopst
Argument 2: bot
Expected Output: binps
Test Case 4
Argument 1: cefhnr
Argument 2: dorw
Expected Output: cdefhnow
Test Case 5
Argument 1: abcdef
Argument 2: acf
Expected Output: bde
Test Case 6
Argument 1: ijp
Argument 2: fhijkpqr
Expected Output: fhkqr
Test Case 7
Argument 1: a
Argument 2: z
Expected Output: az
Test Case 8
Argument 1: aefghijopqxy
Argument 2: bcdklmnrstuvwz
Expected Output: abcdefghijklmnopqrstuvwxyz
Test Case 9
Argument 1: afinqvy
Argument 2: bejmruz
Expected Output: abefijmnqruvyz
Test Case 10
Argument 1: bfgilmqrswyz
Argument 2: bfgmnopqrsx
Expected Output: ilnopwxyz
=cut
