#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
#perl lowhighhighlow.pl LLHHHLLLLLHLLLHHLLHLLHLLLLHLLHLLLLHLLHLHLLLHHHHHHHLLHHLHLLHLLHLHLLHLHHHLLLHLLHLLLLHLLLHHH
# Hello World
#perl lowhighhighlow.pl HHLLLHHHHHLHHHLLHHLHHLHHHHLHHLHHHHLHHLHLHHHLLLLLLLHHLLHLHHLHHLHLHHLHLLLHHHLHHLHHHHLHHHLLL
# Hello World
#perl lowhighhighlow.pl HHLHHHLLHHLHHLHHLLHLLHHLHHLHHHHLHHHLLHLHHHLHHLHLHHLHLLLHHHLHHHLHL
# emkc.org
#perl lowhighhighlow.pl HHLLHHLLLLHLLHHHHHLHHLLLHHLHLLLHLLLHHHHHHHLHLLLHLLHLHHLLLLHLHHHLLLHLLHHHLLHLLHLHHHLHHHLHLLLHHHHHHHLHHLLLHHLHLLLHLLLHHHHHHHLHLLLLLLHLLLLLHHLHLLLHHHLHLLHHHHLHHLLLHHLHHHHLHHLHLLHHLLHLLHLLLLHLLLLLHHLHLLLHHHLHHLHHHHLHLHHHLLLHHHHHHHLHHLHHHHLHHLHLHHLHHLHLLLHLLLHLHHHLLLLLLLHLHHLLLLHLLHLHLLLHHHHHHHLHHLHHLLHLLLLLHHLHHLLHLLHLLLHHLLLHHHHHHHLHHLLLHHLHLLHHHHHLLLLLLLHLLHHHHHLHHHHHLLHLHHHLLLHLLLHHHHHLLLLLLLHLHHLLLLHLLHLHLLLHHHHHHHLHLLHHHHLHHHLLHHLHLLLHLLHLHHLLLLLHHHHHHHLHHHHLHHLHHHHHLLHLHHHLHHLHHHLLHHHLLLLLLLHLLLLLHHLHHHHLLLHLHHLLHHLHLLLHLLHLLLHHLLLHHHHHHHLHHHHLLLHLHLLLHHHLLLLLLLHLHHLLLLHLLLHHLLHLHHHLHHLHLLHHHHLHHLLLHHLHHLHLLLHLLLHLHHHLLLLLLLHLLLLLHHHLLLLLLLHLLLHLLLHLLLHHLLHLHHLHLLLHHHHHHHLHHHHLLLHLLHHHLLHLHHLLLLHLHHHLHHHLLLLLLLHLLHHHLLHLLHLHHHHLLLLLLLHLHHLLLLHLLHHHHHLHHHLLHHHLLLLLLLHLHHHLHHLHLLHHHHLHLLLHHHLHHLLLHHLHHLHLLLHLLLHLHHHLLLLLLLHLHHLLLLHLLHHHHHLHHHLLHHLHHLHLLLLHHHHHHHLHHLHLHHLHLLHHLLHLHHLLLLHLHHHHHHLHLLHHLLHLHHLLLLHLHHLLLLHLLHHHLLHLLHLHHHLHHHLHLLLHHHHHHHLHLLHHHHLHHLLLLLHLLLHHLLLHHHHHHHLHHLHHLLHLLLHHLLHLHHHLHHLHLLLHLLHLLLLLHHLHHHLHLLHLLLHHLLLHHLHLLLLHHHHHHHLLLLLLHHHLLLLLLLHLLHLLLLHLLHLHLLHLLHLHHHLHHHLHLLLHHHHHHHLHHLHHLLHLLLHHLLHLHHHLHHLHLLLHLLHLLLLLHHLHHHLHLLHLLLHHLLLHHHHHHHLHLLHLHHLHHLLLHHLHHLHHHHLHHLHHHHHLLLLLLLHLLHHLHHLHHHLLHHLHHHLLHHLHLLLLLLLHHHHHHHLHLLHHHHLHHLLLLLHLLLLLHHLHLLHHHHHLLLLLLLHLLLLLHHLHLLHLHHLHHHHHLLHLHLLLHHHLLLLLH
# This string is particularly long to make it hard to test case abuse by testing a few bits in the string then outputting the message. A long message will keep that away!
#$_=pop;
#y/LH/01/;

map{/$c/?$s.='0':/[LH]/?$s.='1':'';$c=$_}pop=~/./g;$s=~s/^.//;print@a=pack"B*",$s;

exit();
__END__

# winner
print pack'B*',`dd`=~s/./$'&&0+$'!~"^$&"/ger







=for comment
Low High High High Low High Low
The team have been playing with physical transport layers, and found a very reliable method of 
binary encoding called NRZI. Their Arduino-based logic analyzer outputs either H or L depending 
on if the state at that moment in time is high or low. The team have an encoder working, but 
need help writing a decoder.

Given a string of characters H and L, decode using non-return-to-zero-inverted, and output 
the ASCII message contained within.
NRZI encoding works by outputting a 1 if there is a transition (i.e. the old value does not 
equal the new value), otherwise it is a 0.

Test Cases
Test Case 1
Argument 1: LLHHHLLLLLHLLLHHLLHLLHLLLLHLLHLLLLHLLHLHLLLHHHHHHHLLHHLHLLHLLHLHLLHLHHHLLLHLLHLLLLHLLLHHH
Expected Output: Hello World

Test Case 2
Argument 1: HHLLLHHHHHLHHHLLHHLHHLHHHHLHHLHHHHLHHLHLHHHLLLLLLLHHLLHLHHLHHLHLHHLHLLLHHHLHHLHHHHLHHHLLL
Expected Output: Hello World

Test Case 3
Argument 1: HHLHHHLLHHLHHLHHLLHLLHHLHHLHHHHLHHHLLHLHHHLHHLHLHHLHLLLHHHLHHHLHL
Expected Output: emkc.org

Test Case 4
Argument 1: HHLLHHLLLLHLLHHHHHLHHLLLHHLHLLLHLLLHHHHHHHLHLLLHLLHLHHLLLLHLHHHLLLHLLHHHLLHLLHLHHHLHHHLHLLLHHHHHHHLHHLLLHHLHLLLHLLLHHHHHHHLHLLLLLLHLLLLLHHLHLLLHHHLHLLHHHHLHHLLLHHLHHHHLHHLHLLHHLLHLLHLLLLHLLLLLHHLHLLLHHHLHHLHHHHLHLHHHLLLHHHHHHHLHHLHHHHLHHLHLHHLHHLHLLLHLLLHLHHHLLLLLLLHLHHLLLLHLLHLHLLLHHHHHHHLHHLHHLLHLLLLLHHLHHLLHLLHLLLHHLLLHHHHHHHLHHLLLHHLHLLHHHHHLLLLLLLHLLHHHHHLHHHHHLLHLHHHLLLHLLLHHHHHLLLLLLLHLHHLLLLHLLHLHLLLHHHHHHHLHLLHHHHLHHHLLHHLHLLLHLLHLHHLLLLLHHHHHHHLHHHHLHHLHHHHHLLHLHHHLHHLHHHLLHHHLLLLLLLHLLLLLHHLHHHHLLLHLHHLLHHLHLLLHLLHLLLHHLLLHHHHHHHLHHHHLLLHLHLLLHHHLLLLLLLHLHHLLLLHLLLHHLLHLHHHLHHLHLLHHHHLHHLLLHHLHHLHLLLHLLLHLHHHLLLLLLLHLLLLLHHHLLLLLLLHLLLHLLLHLLLHHLLHLHHLHLLLHHHHHHHLHHHHLLLHLLHHHLLHLHHLLLLHLHHHLHHHLLLLLLLHLLHHHLLHLLHLHHHHLLLLLLLHLHHLLLLHLLHHHHHLHHHLLHHHLLLLLLLHLHHHLHHLHLLHHHHLHLLLHHHLHHLLLHHLHHLHLLLHLLLHLHHHLLLLLLLHLHHLLLLHLLHHHHHLHHHLLHHLHHLHLLLLHHHHHHHLHHLHLHHLHLLHHLLHLHHLLLLHLHHHHHHLHLLHHLLHLHHLLLLHLHHLLLLHLLHHHLLHLLHLHHHLHHHLHLLLHHHHHHHLHLLHHHHLHHLLLLLHLLLHHLLLHHHHHHHLHHLHHLLHLLLHHLLHLHHHLHHLHLLLHLLHLLLLLHHLHHHLHLLHLLLHHLLLHHLHLLLLHHHHHHHLLLLLLHHHLLLLLLLHLLHLLLLHLLHLHLLHLLHLHHHLHHHLHLLLHHHHHHHLHHLHHLLHLLLHHLLHLHHHLHHLHLLLHLLHLLLLLHHLHHHLHLLHLLLHHLLLHHHHHHHLHLLHLHHLHHLLLHHLHHLHHHHLHHLHHHHHLLLLLLLHLLHHLHHLHHHLLHHLHHHLLHHLHLLLLLLLHHHHHHHLHLLHHHHLHHLLLLLHLLLLLHHLHLLHHHHHLLLLLLLHLLLLLHHLHLLHLHHLHHHHHLLHLHLLLHHHLLLLLH
Expected Output: This string is particularly long to make it hard to test case abuse by testing a few bits in the string then outputting the message. A long message will keep that away!
=cut
