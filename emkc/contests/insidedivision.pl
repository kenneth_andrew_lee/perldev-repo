#!/usr/bin/perl
# perl insidedivision.pl '11111125'
# 125 
#
# 

# perl insidedivision.pl '611166'
# 111
#
# 

# perl insidedivision.pl '52668'
# 266
#
# 

# perl insidedivision.pl '24611142'
# 111
#
# 

# perl insidedivision.pl '2454588656'
# 656
#
# 

#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


$b=pop;
unless($b%(substr$b,$a++,3)==0){
    print $_;
}else{
    print $_;
}



exit();

$b=pop;
while($_=substr$b,$a++,3){
    if($b%$_==0){
        print $_;
        last;
    }
}

$b=pop;while($_=substr$b,$a++,3){if($b%$_==0){print $_;last}}
__END__

    print "$a[$i+1]", "\n";


=for winners 
# Hash Kitten
pop=~/...(?{$_%$&||print$&})^/

# hydrazer
$_=pop;s/(?=(...))./$_%$1||print$1/ge

shanethegammer
for($x=$y=pop;;$y/=10){$x%($z=$y%1e3)||(print$z)/0}
=cut


=for problem
Inside Division
Inside Division


You'll be supplied a single number as input. Within this number, some series of of 3 digits will evenly divide into the input. You must output which 3 digits does.



Example:

Given a number of 11224, you should check if 112, 122, and 224 divide evenly. In this case, only 122 does.

11224
112
 122    <- this one
  224


Guidelines:

The number will never contain zeroes
Each number will have exactly one 3 digit sequence that will evenly divide


Test Cases
Test Case 1
Argument 1
11111125

Expected Output
125
Test Case 2
Argument 1
611166

Expected Output
111
Test Case 3
Argument 1
52668

Expected Output
266
Test Case 4
Argument 1
24611142

Expected Output
111
Test Case 5
Argument 1
2454588656

Expected Output
656
=cut


=for testcase
Test Case 1
Argument 1
11111125
Expected Output
125

Test Case 2
Argument 1
611166
Expected Output
111

Test Case 3
Argument 1
52668
Expected Output
266

Test Case 4
Argument 1
24611142
Expected Output
111

Test Case 5
Argument 1
2454588656
Expected Output
656
=cut

