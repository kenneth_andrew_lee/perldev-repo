#!/usr/bin/perl

# perl nthsum.pl '232,872,696,744,456,497,40,776,858,845,637' '9'
# 858
#
#

# perl nthsum.pl '346,544,714,631,735,584,402,283,666,119,925,97,260,723,852' '13'
# 260
#
# 

# perl nthsum.pl '215,101,422,670,836,760,419,815,451,506,257' '10'
# 506
#
# 

# perl nthsum.pl '901,938,325,442,908,230,565,411,210,669,152' '7'
# 565
# 
# 

# perl nthsum.pl '178,832,653,223,255,637,574,478,221,462,865,788,345,210,403,513,364,397,29,490,383' '8'
# 991
# 
# 

# perl nthsum.pl '77,314,591,580,304,913,847,205,444,974,488,220,805,395,428,96,45,572,864,757' '1'
# 9919
# 
# 

# perl nthsum.pl '68,561,340,446,239,431,944,905,288,565,236,36,996,697' '2'
# 3641
#
# 

# perl nthsum.pl 
# '763,827,573,84,722,10,297' '3'
# 583
# 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$c=0;
$a=pop;
@l=split//,pop;
until(join('',@l) eq $a){
    push(@l,shift@l);
    $c++
}
print$c;


exit();


__END__


=for winners 
# Hydrazer
$a=pop;map$\+=++$y%$a?0:$_,eval pop;print


#Natte
$b=pop;pop=~s/(\d*,?){$b}/$\+=$1/reg;print

=cut


=for problem
nth Sum

You'll be given two pieces of input: 
1) a comma separated list of integers, and 
2) a single integer (nth). 
You'll need to sum up every nth integer in the list of integers and output the sum. 
Indexes start at 1. Every 2nd integer in the list of 1,2,3,4,5,6 would be 2,4,6.

Test Cases

Test Case 1
Argument 1
232,872,696,744,456,497,40,776,858,845,637
Argument 2
9
Expected Output
858

Test Case 2
Argument 1
346,544,714,631,735,584,402,283,666,119,925,97,260,723,852
Argument 2
13
Expected Output
260

Test Case 3
Argument 1
215,101,422,670,836,760,419,815,451,506,257
Argument 2
10
Expected Output
506

Test Case 4
Argument 1
901,938,325,442,908,230,565,411,210,669,152
Argument 2
7
Expected Output
565

Test Case 5
Argument 1
178,832,653,223,255,637,574,478,221,462,865,788,345,210,403,513,364,397,29,490,383
Argument 2
8
Expected Output
991

Test Case 6
Argument 1
77,314,591,580,304,913,847,205,444,974,488,220,805,395,428,96,45,572,864,757
Argument 2
1
Expected Output
9919

Test Case 7
Argument 1
68,561,340,446,239,431,944,905,288,565,236,36,996,697
Argument 2
2
Expected Output
3641

Test Case 8
Argument 1
763,827,573,84,722,10,297
Argument 2
3
Expected Output
583
=cut


=for testcase
Test Case 1
'232,872,696,744,456,497,40,776,858,845,637' '9'
858

Test Case 2
'346,544,714,631,735,584,402,283,666,119,925,97,260,723,852' '13'
260

Test Case 3
'215,101,422,670,836,760,419,815,451,506,257' '10'
506

Test Case 4
'901,938,325,442,908,230,565,411,210,669,152' '7'
565

Test Case 5
'178,832,653,223,255,637,574,478,221,462,865,788,345,210,403,513,364,397,29,490,383' '8'
991

Test Case 6
'77,314,591,580,304,913,847,205,444,974,488,220,805,395,428,96,45,572,864,757' '1'
9919

Test Case 7
'68,561,340,446,239,431,944,905,288,565,236,36,996,697' '2'
3641

Test Case 8
'763,827,573,84,722,10,297' '3'
583
=cut


=for misc

=cut