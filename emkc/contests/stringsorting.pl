#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl stringsorting.pl ENGINEERMAN
# AEEEGIMNNNR
# perl stringsorting.pl HEXF
# EFHX
# perl stringsorting.pl L0NGNUMB3R
# 03BGLMNNRU
# perl stringsorting.pl 1234567890
# 0123456789=cut
# 


print sort split//,pop;

exit();
__END__

# winner
for$i(split",",pop){($b,$r,$u,$h)=map{hex substr$i,$_,2}6,10,12,14;$b==80?push@A,substr pack("H*",substr$i,14),0,$u:$b>82?$B[$r]+=$h:0}print$A[(grep$B[$_]==(sort{$a-$b}@B)[0],0..$#B)[0]]




=for comment
String Sorting
The Engineer Man team have thought of a new hashing algorithm, where you sort all the letters in the string, numbers first, then letters.

They seem to have forgotten how to install some languages, but need a working version.



Given a string, sort all the letters in order [0-9][A-Z]

Test Cases
Test Case 1
Argument 1: ENGINEERMAN
Expected Output: AEEEGIMNNNR
Test Case 2
Argument 1: HEXF
Expected Output: EFHX
Test Case 3
Argument 1: L0NGNUMB3R
Expected Output: 03BGLMNNRU
Test Case 4
Argument 1: 1234567890
Expected Output: 0123456789=cut
