#!/usr/bin/perl
# perl alignmentanarchy.pl 'Magnam modi q&uisquam velit.'
# 27
# 
# 
# perl alignmentanarchy.pl 'Porro dolore &dolor neque labore.,Ut numquam qu&iquia quaerat adipisci quiquia aliquam.'
# 52
# 
# 
# perl alignmentanarchy.pl 'Non aliq&uam dolore adipisci non dolor.,Numq&uam non ut velit magnam amet.,Neque aliquam etincidunt con&sectetur ipsum labore.'
# 58
# 
# 
# perl alignmentanarchy.pl 'Ut tempora neque labore dolorem magnam non t&empora.,Magnam voluptatem &sit quiquia sit dolore.,Non tempor&a aliquam etincidunt quiquia labore neque ut.,Velit dolor& aliquam modi sed.'
# 89
# 
# perl alignmentanarchy.pl 'Consectetur amet sit modi eius est no&n.,Numquam velit velit adipisci& tempora consectetur.,Dolorem sit dolor adipisci velit nequ&e.,Ipsum tempora quisquam eius labor&e neque.,T&empora eius amet est.'
# 58
# 
# perl alignmentanarchy.pl 'Ipsum velit ut sit magn&am amet non.,Quaerat quiqu&ia sit voluptatem.,Etincidunt quiquia dolor ips&um voluptatem non non ipsum.,Non& modi quiquia magnam.,Labore labore modi non quaerat tempora& modi dolor.,Dolor tempora voluptatem neque etincidunt velit eius porr&o.'
# 85
# 
#
# perl alignmentanarchy.pl 'Neque q&uiquia dolorem eius dolorem porro aliquam ut.,Porro aliquam quaerat porro& magnam modi.,Voluptatem p&orro sed labore velit neque.,Quaerat numquam modi non porro qua&erat ipsum.,Consectetur eius tem&pora magnam ut.,Aliquam magnam tem&pora quisquam consectetur.,Sed quisquam eius dolore sit &etincidunt quaerat neque.'
# 
# 
# 
# perl alignmentanarchy.pl 'Porro& consectetur sit adipisci labore sit sed.,Ut non magnam tem&pora sit quaerat.,Sit d&olor consectetur sit non est dolore non.,Voluptatem eius adipisci est magn&am quaerat.,Quiquia quiquia sed non &neque labore neque modi.,Est eius quiqu&ia tempora.,Adipisci non sed &consectetur adipisci dolore.,Sed tempora eius velit amet p&orro non.'
# 74
# 
#
# perl alignmentanarchy.pl 'Labore aliquam labore sit tempora non dolo&rem amet.,Tempora quisquam tempora et&incidunt amet dolorem est.,Adipisci non quiquia velit neque qui&quia.,Ipsum quiquia non aliquam& quiquia modi.,Eius etincidunt sed conse&ctetur sit.,Magnam e&ius dolorem aliquam quiquia dolore.,Velit dolor porro quaerat quiqu&ia.,Modi amet non dolore por&ro.,Amet quiquia sed quiq&uia magnam.'
# 77
# 
#
# perl alignmentanarchy.pl 'Neque velit sed lab&ore dolorem non.,Velit quisquam velit q&uisquam porro.,Magnam voluptatem dolor porro labore adipisci qui&squam.,Porro consectetur non adipisci quiquia sit aliqu&am.,Ipsum numquam quisq&uam quaerat.,Dolore& neque tempora etincidunt voluptatem.,Voluptatem sed ipsum labore dolor consect&etur.,Dolor aliquam neque e&ius ipsum.,Velit modi aliq&uam eius porro.,Est modi quisquam po&rro.'
# 86
# 
#


#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

map{}split/,/,pop;
print$l+$r, "\n";


exit();
map{$i=index($_,'&');$m=length($_);if($l<$i){$l=$i};if($r<$m-$i-1){$r=$m-$i-1}}split/,/,pop;print$l+$r, "\n";


__END__

$x=pop;
for(1..$x){
  $s+=$_ if$x%$_==0;
}
print $s;

=for solutions
map${$i++%2?A:B}<y///c?${$i%2?B:A}=y///c:0,pop=~/[^&,]+/g;print$A+$B
=cut


=for hints
Input and output
The field is a grid of 3x3 boxes.
The input is a string of 9 characters :

If the box is subject to a weather phenomenon, the letter used for the phenomena
If the box is not subject to a weather phenomenon, the letter used is `X`

The grid is described row per row (from 1 to 9).

╔═══════╗
║ 1 2 3 ║
║ 4 5 6 ║
║ 7 8 9 ║
╚═══════╝
You have to write to standard output the locations where the house can be built using the grid above in ascendant order.



Helpers
Here are some helping graphics showing you where you can and can't for each device :
(X is where you can't build the house)

╔═══════╗        ╔═══════╗
║ !     ║        ║ X   X ║
║       ║   =>   ║       ║
║       ║        ║ X     ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║     X ║
║       ║   =>   ║       ║
║     ! ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║       ║
║   +   ║   =>   ║   X   ║
║       ║        ║       ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║ X   X ║
║   *   ║   =>   ║   X   ║
║       ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║ *     ║   =>   ║ X     ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║   $   ║   =>   ║ X X X ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝


╔═══════╗        ╔═══════╗
║       ║        ║       ║
║       ║   =>   ║   X   ║
║   $   ║        ║ X X X ║
╚═══════╝        ╚═══════╝
=cut


=for example 
Input :

XWFLXXXHX


The corresponding grid is the following with the following devices :

╔═══════╗        ╔═══════╗
║   W F ║        ║   ! + ║
║ L     ║   =>   ║ $     ║
║   H   ║        ║   *   ║
╚═══════╝        ╚═══════╝
Now, we write a X where we can't build the house.

╔═══════╗        ╔═══════╗
║   ! + ║        ║ X X X ║
║ $     ║   =>   ║ X X X ║
║   *   ║        ║ X X   ║
╚═══════╝        ╚═══════╝
Then, we clearly see that the box 9 is the only place where we can build the house, so we output 9.
=cut

=for winners

=cut


=for comment
$_ The default input and pattern-searching space.
$" When an array or an array slice is interpolated into a double-quoted string or a similar context such as /.../, its elements are separated by this value. Default is a space.
$` The string preceding whatever was matched by the last successful pattern match, not counting any matches hidden within a BLOCK or eval enclosed by the current BLOCK.
=cut



=for comment
Natural damage control
The EngineerMan team wants to build a house in order to create a secret HQ. Unfortunately, the building land is subject to weather damage.



You are going to help design the facilities used to protect the new HQ from natural damages.



The following weather phenomena may happen :



 - Windstorm (W)

 - Flood (F)

 - Heat wave (H)

 - Landslide (L)



Each phenomenon has a letter associated to it. For each one, you can install the following devices :

 

 - For Windstorm, you can use a windbreak (!)

 - For Flood, you can use a pipe system (+)

 - For Heat Wave, you can plant trees (*)

 - For Landslide, you can install a drainage device ($)



However, each device restricts you from building the house where it is planted and sometimes around the device. The following restrictions applies :



 - A windbreak prevents building a house 2 boxes around it (non diagonals) and where the windbreak is.

 - A pipe system prevent building a house where it is.

 - Planting trees prevent building a house where they are and in diagonals boxes.

 - A drainage device prevents building a house around it (non diagonals) and on it.



Input and output


The field is a grid of 3x3 boxes.

The input is a string of 9 characters :



If the box is subject to a weather phenomenon, the letter used for the phenomena
If the box is not subject to a weather phenomenon, the letter used is `X`


The grid is described row per row (from 1 to 9).



╔═══════╗
║ 1 2 3 ║
║ 4 5 6 ║
║ 7 8 9 ║
╚═══════╝
You have to write to standard output the locations where the house can be built using the grid above in ascendant order.



Helpers


Here are some helping graphics showing you where you can and can't for each device :

(X is where you can't build the house)





╔═══════╗        ╔═══════╗
║ !     ║        ║ X   X ║
║       ║   =>   ║       ║
║       ║        ║ X     ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║     X ║
║       ║   =>   ║       ║
║     ! ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║       ║
║   +   ║   =>   ║   X   ║
║       ║        ║       ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║ X   X ║
║   *   ║   =>   ║   X   ║
║       ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║ *     ║   =>   ║ X     ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║   $   ║   =>   ║ X X X ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝


╔═══════╗        ╔═══════╗
║       ║        ║       ║
║       ║   =>   ║   X   ║
║   $   ║        ║ X X X ║
╚═══════╝        ╚═══════╝
Example case


Input :

XWFLXXXHX


The corresponding grid is the following with the following devices :

╔═══════╗        ╔═══════╗
║   W F ║        ║   ! + ║
║ L     ║   =>   ║ $     ║
║   H   ║        ║   *   ║
╚═══════╝        ╚═══════╝
Now, we write a X where we can't build the house.

╔═══════╗        ╔═══════╗
║   ! + ║        ║ X X X ║
║ $     ║   =>   ║ X X X ║
║   *   ║        ║ X X   ║
╚═══════╝        ╚═══════╝
Then, we clearly see that the box 9 is the only place where we can build the house, so we output 9.

Test Cases
Test Case 1
Argument 1
WHXXFLXXX
Expected Output
8
Test Case 2
Argument 1
XXXXHWLFX
Expected Output
2
Test Case 3
Argument 1
HXLXXWFXX
Expected Output
89
Test Case 4
Argument 1
XWXXHFXLX
Expected Output
4
=cut

