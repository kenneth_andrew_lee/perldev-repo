#!/usr/bin/perl
#use Data::Dumper;

#perl distancetonearestvowel.pl 'abcxyz'

# My solution
#$,=",";print map{/aeiou/?0:/lrx/?3:/cgkmqswy/?2:1}pop=~/./g;

$_=pop;y/a-z/0-210-210-3210-3210-321/;s/\B/,/g;print;


exit();
__END__


# Winner
$_=pop;y/a-z/012101210-3210-3210-321/;s/\B/,/g;print

$_=pop;y/a-z/0-210-210-3210-3210-321/;s/\B/,/g;print

$_=pop;y/a-z/01210121012321012321012321/;s/\B/,/g;print



Distance to Nearest Vowel
You will receive a string of characters. Your job is to return a list of numbers that represent the distance of each letter to it's nearest vowel. If the current character is a vowel then the distance is considered 0.



Sample Input 

abcxyz


Sample Output

0,1,2,3,2,1


Guidelines

The alphabet wraps around, this means the letter "z" and "a" are considered adjacent
The input string will be between 1 and 100 chars long 
Vowels are: a, e, i, o, u
The input string will contain lowercase letters only


a x  97   0   1    01100001  
b    98   1   2    01100010  
c    99   2   3    01100011  
d   100   1   4    01100100  
e x 101   0   5    01100101  
f   102   1   6    01100110  
g   103   2   7    01100111  
h   104   1   8    01101000  
i x 105   0   9    01101001  

j   106   1  10    01101010  
k   107   2  11    01101011  
l   108   3  12    01101100  
m   109   2  13    01101101  
n   110   1  14    01101110  
o x 111   0  15    01101111  
p   112   1  16    01110000  
q   113   2  17    01110001  
r   114   3  18    01110010  
s   115   2  19    01110011  
t   116   1  20    01110100  
u x 117   0  21    01110101  
v   118   1  22    01110110  
w   119   2  23    01110111  
x   120   3  24    01111000  
y   121   2  25    01111001  
z   122   1  26    01111010  



if <106





3:'lrx'
2:'cgkmqswy'
1:'bdfhjnptvz'
0:'aeiou'

1,2,5,6,9,10,15,16,17,21,22,23
3,4,7,8,12,13,14,18,19,20,24,25

01210121012321012321012321
--------------------------
00000000001111111111222222
01234567890123456789012345
0++--++--+++---+++---+++-- 
01111111111111111111111111
01210121012321012321012321
abcdefghijklmnopqrstuvwxyz



abc
def
ghi
jkl
mno
pqr
stu
vwx
yz


abcd
efgh
ijkl
mnop
qrst
uvwx
yz


abcde
fghij
klmno
pqrst
uvwxy
z


abcdef
ghijkl
mnopqr
stuvwx
yz



abcdefg
hijklmn
opqrstu
vwxyz



abcdefgh
ijklmnop
qrstuvwx
yz



abcd    97  - 100
efgh    101 - 104
ijklmn  105 - 110
opqrst  111 - 116
uvwxyz  117 - 122

                                           3                       3                       3
       2               2               2       2               2       2               2       2
    1      1       1       1       1               1       1               1       1               1
 0             0               0                       0                       0                    
97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122

0: 97, 101, 105, 111, 117
1: 98, 100, 102, 104, 106, 110, 112, 116, 118, 122
2: 99, 103, 107, 109, 113, 115, 119, 121
3: 108, 114, 120

0: 01100001, 01100101, 01101001, 01101111, 01110101
1: 01100010, 01100100, 01100110, 01101000, 01101010, 01101110, 01110000, 01110100, 01110110, 01111010
2: 01100011, 01100111, 01101011, 01101101, 01110001, 01110011, 01110111, 01111001
3: 01101100, 01110010, 01111000


>>> 108^114
30
>>> 108^120
20
>>> 114^ 108
30
>>> 114^120
10
>>> 120^108
20
>>> 120^114
10


\sin\left(1.5708x\ -\ 1.5708\right)\ +1

1.33sin(1.047x + 2.618) + 1.5