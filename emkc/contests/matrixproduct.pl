#!/usr/bin/perl
# perl matrixproduct.pl '3,6,9,3,10,6,10,9,13,1,1,4,8,5,15,11' '2,9,3,2,7,4,8,8,9,12,5,6,6,15,4,2'
# 2879
# 
# 
# perl matrixproduct.pl '8,18,17,11,18,2,19,16,19,17,5,22,2,18,22,19' '5,4,16,2,1,14,11,22,2,2,15,24,21,1,22,14'
# 10562
# 
# 
# perl matrixproduct.pl '3,12,25,15,12,33,19,18,8,20,27,1,2,18,8,9' '25,14,29,26,11,9,16,33,18,5,33,6,19,17,14,24'
# 16157
#
# 
# perl matrixproduct.pl '24,9,2,34,8,30,2,27,26,40,33,2,15,44,10,33' '44,29,29,39,20,19,7,46,44,23,2,21,6,22,9,19'
# 31215
#
# 
# perl matrixproduct.pl '45,43,3,46,19,7,55,34,33,1,11,25,28,35,2,62' '30,19,4,43,22,36,49,55,53,19,32,62,26,48,55,41'
# 66108
#
# 
# perl matrixproduct.pl '78,68,13,15,42,21,54,25,39,72,57,3,3,30,42,19' '13,29,17,48,19,62,40,1,62,78,26,29,59,3,3,13'
# 77842
#
# 
# perl matrixproduct.pl '36,93,41,39,32,90,3,21,54,45,69,97,50,41,85,46' '93,21,88,95,33,49,15,82,73,83,64,31,91,7,5,71'
# 184255
#
# 
# perl matrixproduct.pl '28,48,118,57,86,100,90,118,83,108,108,75,57,6,84,13' '58,41,114,90,80,50,35,25,97,64,118,104,107,99,25,72'
# 359631
#
# 
# perl matrixproduct.pl '117,104,80,67,135,140,68,12,106,6,89,114,35,23,21,19' '31,22,88,114,76,51,29,101,25,39,30,96,95,97,41,129'
# 296140
#
# 
# perl matrixproduct.pl '23,100,122,126,5,63,110,156,74,86,35,50,12,141,62,92' '158,61,116,50,106,165,65,123,134,80,122,111,65,61,93,165'
# 532779
#
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

@b=split/,/,pop;
@a=split/,/,pop;
for$r(0..3){
  for$c(0..3){
    $s+=$a[$r]*$b[$c];
  }
}
print$s;

exit();


__END__



=for winners 
=cut


=for problem
Matrix Product
The EngineerMan team has been working with computer graphics and has come across lots of matrices which they are currently computing by hand.

They need your help to find the dot-product of 2 same-sized matrices, then sum all the cells together.



Your input will be given as 2 arguments representing the 2 matrices. Each cell will be comma-separated and will always form a 4x4 matrix, dividing every 4 cells as a new line:

ABCDEFGHIJKLMNOP
ABCD
EFGH
IJKL
MNOP
Your output should be the sum of all the matrix elements in the dot product.



Test Cases
Test Case 1
Argument 1
3,6,9,3,10,6,10,9,13,1,1,4,8,5,15,11
Argument 2
2,9,3,2,7,4,8,8,9,12,5,6,6,15,4,2

Expected Output
2879
Test Case 2
Argument 1
8,18,17,11,18,2,19,16,19,17,5,22,2,18,22,19
Argument 2
5,4,16,2,1,14,11,22,2,2,15,24,21,1,22,14

Expected Output
10562
Test Case 3
Argument 1
3,12,25,15,12,33,19,18,8,20,27,1,2,18,8,9
Argument 2
25,14,29,26,11,9,16,33,18,5,33,6,19,17,14,24

Expected Output
16157
Test Case 4
Argument 1
24,9,2,34,8,30,2,27,26,40,33,2,15,44,10,33
Argument 2
44,29,29,39,20,19,7,46,44,23,2,21,6,22,9,19

Expected Output
31215
Test Case 5
Argument 1
45,43,3,46,19,7,55,34,33,1,11,25,28,35,2,62
Argument 2
30,19,4,43,22,36,49,55,53,19,32,62,26,48,55,41

Expected Output
66108
Test Case 6
Argument 1
78,68,13,15,42,21,54,25,39,72,57,3,3,30,42,19
Argument 2
13,29,17,48,19,62,40,1,62,78,26,29,59,3,3,13

Expected Output
77842
Test Case 7
Argument 1
36,93,41,39,32,90,3,21,54,45,69,97,50,41,85,46
Argument 2
93,21,88,95,33,49,15,82,73,83,64,31,91,7,5,71

Expected Output
184255
Test Case 8
Argument 1
28,48,118,57,86,100,90,118,83,108,108,75,57,6,84,13
Argument 2
58,41,114,90,80,50,35,25,97,64,118,104,107,99,25,72

Expected Output
359631
Test Case 9
Argument 1
117,104,80,67,135,140,68,12,106,6,89,114,35,23,21,19
Argument 2
31,22,88,114,76,51,29,101,25,39,30,96,95,97,41,129

Expected Output
296140
Test Case 10
Argument 1
23,100,122,126,5,63,110,156,74,86,35,50,12,141,62,92
Argument 2
158,61,116,50,106,165,65,123,134,80,122,111,65,61,93,165

Expected Output
532779
=cut


=for testcase
Test Case 1
Argument 1
3,6,9,3,10,6,10,9,13,1,1,4,8,5,15,11
Argument 2
2,9,3,2,7,4,8,8,9,12,5,6,6,15,4,2
Expected Output
2879

Test Case 2
Argument 1
8,18,17,11,18,2,19,16,19,17,5,22,2,18,22,19
Argument 2
5,4,16,2,1,14,11,22,2,2,15,24,21,1,22,14
Expected Output
10562

Test Case 3
Argument 1
3,12,25,15,12,33,19,18,8,20,27,1,2,18,8,9
Argument 2
25,14,29,26,11,9,16,33,18,5,33,6,19,17,14,24
Expected Output
16157

Test Case 4
Argument 1
24,9,2,34,8,30,2,27,26,40,33,2,15,44,10,33
Argument 2
44,29,29,39,20,19,7,46,44,23,2,21,6,22,9,19
Expected Output
31215

Test Case 5
Argument 1
45,43,3,46,19,7,55,34,33,1,11,25,28,35,2,62
Argument 2
30,19,4,43,22,36,49,55,53,19,32,62,26,48,55,41
Expected Output
66108

Test Case 6
Argument 1
78,68,13,15,42,21,54,25,39,72,57,3,3,30,42,19
Argument 2
13,29,17,48,19,62,40,1,62,78,26,29,59,3,3,13
Expected Output
77842

Test Case 7
Argument 1
36,93,41,39,32,90,3,21,54,45,69,97,50,41,85,46
Argument 2
93,21,88,95,33,49,15,82,73,83,64,31,91,7,5,71
Expected Output
184255

Test Case 8
Argument 1
28,48,118,57,86,100,90,118,83,108,108,75,57,6,84,13
Argument 2
58,41,114,90,80,50,35,25,97,64,118,104,107,99,25,72
Expected Output
359631

Test Case 9
Argument 1
117,104,80,67,135,140,68,12,106,6,89,114,35,23,21,19
Argument 2
31,22,88,114,76,51,29,101,25,39,30,96,95,97,41,129
Expected Output
296140

Test Case 10
Argument 1
23,100,122,126,5,63,110,156,74,86,35,50,12,141,62,92
Argument 2
158,61,116,50,106,165,65,123,134,80,122,111,65,61,93,165
Expected Output
532779
=cut

=for checkingitout

     1  2  3  4         1  2  3  4
A =  5  6  7  8   B =   5  6  7  8
     9 10 11 12         9 10 11 12
    13 14 15 16        13 14 15 16

=cut