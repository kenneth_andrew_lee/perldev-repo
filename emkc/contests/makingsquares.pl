#!/usr/bin/perl
# perl makingsquares.pl 2 3
# ++
# ||
# ++
#
# perl makingsquares.pl 4 5
# +--+
# |  |
# |  |
# |  |
# +--+
#
# perl makingsquares.pl 10 3
# +--------+
# |        |
# +--------+
#
# perl makingsquares.pl 5 15
# +---+
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# |   |
# +---+
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$h=pop;$w=pop;for$i(1..$h){print $i==1||$i==$h?'+':'|';for$x(1..$w-2){print $i==1||$i==$h?'-':' '}print $i==1||$i==$h?"+\n":"|\n"}

exit();

__END__

$h=pop;
$w=pop;
for$i(1..$h){
  print $i==1||$i==$h?'+':'|';
  for$x(1..$w-2){
    print $i==1||$i==$h?'-':' ';
  }
  print $i==1||$i==$h?"+\n":"|\n";
}




=for test
++
||
++
=cut

=for winners
`dd`=~/
/;print$\="
+"."-"x($e=$`-2)."+",("
|".$"x$e."|")x($'-2)
hydrazer

($w,$h)=@ARGV;$e="+"."-"x($w-=2)."+
";print$e.("|"." "x$w."|
")x($h-2).$e
shanethegamer



$_ The default input and pattern-searching space.
$" When an array or an array slice is interpolated into a double-quoted string or a similar context such as /.../, its elements are separated by this value. Default is a space.
$` The string preceding whatever was matched by the last successful pattern match, not counting any matches hidden within a BLOCK or eval enclosed by the current BLOCK.


=cut



=for comment
Making Squares
The EM team needs your help drawing squares to the screen.

Given the width and height of a square you're job will be to return an ASCII drawing of that square. 
Use the dash -  symbol for horizontal lines, use the pipe | symbol for vertical lines, and lastly use the plus + symbol for corners.
Both the height and width will be greater then 1 and less then 20. 

Example 1: 
Input 1: 2 #Width
Input 2: 3 #Height

Output:
++
||
++
Example 2:

Input 1: 4 #Width
Input 2: 5 #Height

Output:
+--+
|  |
|  |
|  |
+--+
Test Cases
Test Case 1
Argument 1
2
Argument 2
3
Expected Output
++
||
++
Test Case 2
Argument 1
4
Argument 2
5
Expected Output
+--+
|  |
|  |
|  |
+--+
Test Case 3
Argument 1
10
Argument 2
3
Expected Output
+--------+
|        |
+--------+
Test Case 4
Argument 1
5
Argument 2
15
Expected Output
+---+
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
|   |
+---+
=cut

