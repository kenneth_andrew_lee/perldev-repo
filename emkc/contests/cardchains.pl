#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl cardchains.pl '1 13 24 40 48 20 6 45 16 12 21 11 8 37 22 33 14 50 47 43 7 38 10 2 15 19 36 0 34 23 32 31 4 18 28 49 39 30 51 3 42 17 26 41 44 5 46 9 29 27 25 35'
# 10

#perl cardchains.pl '1 3 0 2 4'
#input  : '1 3 0 2 4'
#output : 2

#perl cardchains.pl '0 2 1 4 3'
#input  : '0 2 1 4 3'
#output : 3
#0->0
#2->1->2
#4->3->4


#perl cardchains.pl '0 1 2 3 4'
#input  : '0 1 2 3 4'
#output : 5

#sub p{($v)=@_;return if(exists$h{$v});$t=$a[$v];$h{$v}=$t;p($t);}@a=split/ /,pop;for(0..$#a){unless(exists$h{$_}){p($_);$c++}}print$c
#sub p{($v)=@_;return if(defined $h{$v});$t=$a[$v];$h{$v}=$t;p($t)}$c=0;@a=split/ /,pop;for(0..$#a){unless (defined $h{$_}){p($_);$c++}}print$c
#sub p{($v)=@_;if(exists$h{$v}){$c++;return}$t=$a[$v];$h{$v}=$t;p($t)}@a=split/ /,pop;for(0..$#a){if (not exists($h{$_})){p($_)}}print$c;
#sub p{($v)=@_;return if(exists$h{$v});$t=$a[$v];$h{$v}=$t;p($t)}@a=split/ /,pop;map{unless(exists$h{$_}){p($_);$c++}}(0..$#a);print$c;


#sub p{($v)=@_;return if(exists$h{$v});$t=$a[$v];$h{$v}=$t;p($t)}@a=split/ /,pop;for(0..$#a){unless(exists$h{$_}){p($_);$c++}}print$c

sub p{
  ($v)=@_;
  return if(exists$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}

@a=split/ /,pop;
for(0..$#a){
  unless(exists$h{$_}){
    p($_);
    $c++;
  }
}
print$c;






exit();

__END__

Just a subroutine that calls itself recursively
```sub p{
  ($v)=@_;
  return if(exists$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}

@a=split/ /,pop;
for(0..$#a){
  unless(exists$h{$_}){
    p($_);
    $c++;
  }
}
print$c;```



sub p{
  ($v)=@_;
  return if(exists$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}
@a=split/ /,pop;
map{unless(exists$h{$_}){p($_);$c++}}(0..$#a);
print$c;

exit();




sub p{
  ($v)=@_;
  return if(exists$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}
@a=split/ /,pop;
map{unless(exists$h{$_}){p($_);$c++}}(0..$#a);
print$c;



sub p{
  ($v)=@_;
  if(exists$h{$v}){
    $c++;
    return;
  }
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}
@a=split/ /,pop;
for(0..$#a){if (not exists($h{$_})){p($_)}}
print$c;


sub p{
  ($v)=@_;
  return if(exists$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}
@a=split/ /,pop;

for(0..$#a){
  unless(exists$h{$_}){
    p($_);
    $c++;
  }
}
print$c;




sub p{($v)=@_;return if(defined$h{$v});$t=$a[$v];$h{$v}=$t;p($t)}$c=0;@a=split/ /,pop;for(0..$#a){unless(defined$h{$_}){p($_);$c++}}print$c;


sub p{
  ($v)=@_;
  return if(defined$h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}
$c=0;
@a=split/ /,pop;
for(0..$#a){
  unless (defined $h{$_}){
    p($_);
    $c++;
  }
}
print$c;







sub p{
  ($v)=@_;
  return if(defined $h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t)
}
$c=0;
%h=();
@a=split/ /,pop;
for(0..$#a){
  unless (defined $h{$_}){
    p($_);
    $c++;
  }
}
print$c;




sub p{
  ($v)=@_;
  if(defined $h{$v}){
    $c++;
    return;
  }
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
}

$c=0;
%h=();
@a=split/ /,pop;
for(0..$#a){
  p($_);
}
print $c;




sub p{
  ($v)=@_;
  return if(defined $h{$v});
  $t=$a[$v];
  $h{$v}=$t;
  p($t);
  $c++
}

$c=0;
%h=();
@a=split/ /,pop;
for(0..$#a){
    p($_);
  }
}
print $c;


0 1 2 3 4
1 3 0 2 4

(0->1->3->2)
(4)

=for comment
Card Chains
For this contest we are playing with cards.

To set up the game, we begin by placing 52 cards in a line, face down.

The goal is ultimately to arrange the cards (string of numbers each separated by a single space) in ascending order (0, 1, 2, ..., 51) by performing a rearrangement cycle. Then, output the number of cycles it took.

How to perform a rearrangement cycle:

Step 1: Pick up a random card that is face down.

Step 2: Place that card at it's proper sequential location, so if the card has a 5 on it, place it at the 6th spot.

Step 3a: If a card already exists at that spot, pick it up and go back to Step 2.

Step 3b: If no card already exists at that spot, a cycle has been completed and you score one point.

Step 4: If all cards are now face up and in order, the game is over, otherwise go back to Step 1.



Examples with only 5 cards for readability (the test cases all contain 52 cards):


input  : 1 3 0 2 4
output : 2


input  : 0 2 1 4 3
output : 3


input  : 0 1 2 3 4
output : 5
￼



Example with the 52 cards:
input  : 1 13 24 40 48 20 6 45 16 12 21 11 8 37 22 33 14 50 47 43 7 38 10 2 15 19 36 0 34 23 32 31 4 18 28 49 39 30 51 3 42 17 26 41 44 5 46 9 29 27 25 35
output : 10
=cut

