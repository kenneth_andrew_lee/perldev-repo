#!/usr/bin/perl
# perl hexstrings.pl '68656c6c6f20776f726c64'
# hello world
# 
# 
# perl hexstrings.pl '73656372657420737175697272656c20627573696e657373'
# secret squirrel business
# 
# 
# perl hexstrings.pl '737570657263616c6966726167696c697374696365787069616c69646f63696f7573'
# supercalifragilisticexpialidocious
#
# 
# perl hexstrings.pl '7261696e626f777320f09f8c8820616e6420756e69636f726e7320f09fa68420'
# rainbows 🌈 and unicorns 🦄
#
# 
# perl hexstrings.pl '57684f2077496c4c206245206649725374'
# WhO wIlL bE fIrSt
#
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

print pack"H*",pop;

exit();


__END__



=for winners 
=cut


=for problem
Hexstrings
The EngineerMan team are being sent data in a very un-compact format i.e. 

Text has been converted to hex but stored as a string it might even have some emojis thrown in!

Help them decode it and return the normal string representation of the data.
=cut


=for testcase
Test Cases
Test Case 1
Argument 1
68656c6c6f20776f726c64

Expected Output
hello world
Test Case 2
Argument 1
73656372657420737175697272656c20627573696e657373

Expected Output
secret squirrel business
Test Case 3
Argument 1
737570657263616c6966726167696c697374696365787069616c69646f63696f7573

Expected Output
supercalifragilisticexpialidocious
Test Case 4
Argument 1
7261696e626f777320f09f8c8820616e6420756e69636f726e7320f09fa68420

Expected Output
rainbows 🌈 and unicorns 🦄
Test Case 5
Argument 1
57684f2077496c4c206245206649725374

Expected Output
WhO wIlL bE fIrSt
=cut

