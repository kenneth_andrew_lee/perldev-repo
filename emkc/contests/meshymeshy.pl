#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl meshymeshy.pl AAAABBBBAAAA
# 0A4B8A12
# perl meshymeshy.pl HHHXXXXXXXXXXXX
# 0H3X15
# perl meshymeshy.pl HELLOWORLD
# 0H1E2L4O5W6O7R8L9D10
# perl meshymeshy.pl AAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAA
# perl meshymeshy.pl THISVERYLONGONESHOULDMAKEITDIFFICULTTOTESTCASEABUSE
# 0T1H2I3S4V5E6R7Y8L9O10N11G12O13N14E15S16H17O18U19L20D21M22A23K24E25I26T27D28I29F31I32C33U34L35T37O38T39E40S41T42C43A44S45E46A47B48U49S50E51


#map{$c++;if($_ ne $a){$h{$c}=$_;$a=$_}}pop=~/./g;@b=sort{$a<=>$b}keys%h;for(@b){print$_-1,$h{$_}}print$c;

map{$c++;if($h{$c}){$h{$c}=$_;$a=$_}}pop=~/./g;
@b=sort{$a<=>$b}keys%h;
for(@b){
  print$_-1,$h{$_};
}
print$c;


exit();
__END__

# winner
print pop=~s/(.?)\1*/$-[0]$1/gr

# winner
print pop=~s/(.)?\1*/$-[0]$1/gr

=for comment
Meshy Meshy
The EngineerMan team are working on a new compression format, its similar to run-length 
encoding, but takes its roots from greedy meshing.

Start by taking a list of characters,

AAAABBBBAAAA
Write our the indexes of all edges where a transition occurs, from letter to letter, 
nothing to letter or letter to nothing

< > - <A> 0
<A> - <B> 4
<B> - <A> 8
<A> - < > 12
In between these, add the new letter.

0A4B8A12

Input will always be uppercase letters.

Test Cases
Test Case 1
Argument 1: AAAABBBBAAAA
Expected Output: 0A4B8A12

Test Case 2
Argument 1: HHHXXXXXXXXXXXX
Expected Output: 0H3X15

Test Case 3
Argument 1: HELLOWORLD
Expected Output: 0H1E2L4O5W6O7R8L9D10

Test Case 4
Argument 1: AAAAAAAAAAAAAAA
Expected Output: 0A15

Test Case 5
Argument 1: THISVERYLONGONESHOULDMAKEITDIFFICULTTOTESTCASEABUSE
Expected Output: 0T1H2I3S4V5E6R7Y8L9O10N11G12O13N14E15S16H17O18U19L20D21M22A23K24E25I26T27D28I29F31I32C33U34L35T37O38T39E40S41T42C43A44S45E46A47B48U49S50E51
=cut

