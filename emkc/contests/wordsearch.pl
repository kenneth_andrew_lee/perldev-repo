#!/usr/bin/perl
# perl wordsearch.pl 'HEXF' 'KJASDAJKSDJAHLNBCENVOUEZXQPDOGAYQUYDREMABOGCCVXMNNAMNFJBAIGKCGXHLDIRIMECBONSPEEDDREAMPAFBNASCVBESGYAAIUPRISINGSAYSHHELLOWORLD';
# Word: HEXF

#print "Is INT\n" if ($test =~/-*\d+/);
#print "Is FLOAT\n" if ($test =~/-*\d+\.\d+/);

$s=pop;
$n=pop;

$cube=length($s)**(1/3);
$square= sqrt(length($s));
if ($cube=~/-*\d+/) {
  print "\$cube:$cube", "\n";  
} elsif ($square=~/-*\d+/) {
  print "\$square:$square", "\n";
} else {
  print "one-dimension\n";
}



exit();

__END__


#31 bytes with perl
print join' ',sort split/ /,pop;


Winner HashKitten
print"@{[sort<@ARGV>]}"

2nd place Natte
$,=$";print sort glob pop

3rd place ChiefChippy2
print"@{[sort glob`dd`]}"


=for wordsearch
Word Search
The EngineerMan team have gotten into word-searches lately, but are stuck on a few.
They need some help finding the first letter of each word.


Your task is to write a program which given a string and a n-D matrix, give the 
coordinates of the starting letter of the phrase.

For this contest, each dimension of the matrix will be of equal length, so a 
5x5 matrix is valid, but a 4x5 or 9x8 is not.

The word you are given will only ever traverse one dimension at a time, so 
you could only go across the X axis, or the Y axis, but not both (no diagonals).

The word may also be in reverse within the matrix, but will always exist 
within the matrix.

The matrix will be of either 1, 2 or 3 dimension.



Example
Input: HEXF KJASDAJKSDJAHLNBCENVOUEZXQPDOGAYQUYDREMABOGCCVXMNNAMNFJBAIGKCGXHLDIRIMECBONSPEEDDREAMPAFBNASCVBESGYAAIUPRISINGSAYSHHELLOWORLD
Word: HEXF
Matrix: KJASDAJKSDJAHLNBCENVOUEZXQPDOGAYQUYDREMABOGCCVXMNNAMNFJBAIGKCGXHLDIRIMECBONSPEEDDREAMPAFBNASCVBESGYAAIUPRISINGSAYSHHELLOWORLD

matrix length = 125
matrix dimensions = 5x5x5

Unwrapped Matrix:
=======> Z
==> X
V Y

KJASD  QPDOG  AMNFJ  SPEED  AIUPR
AJKSD  AYQUY  BAIGK  DREAM  ISING
JAHLN  DREMA  CGXHL  PAFBN  SAYSH
BCENV  BOGCC  DIRIM  ASCVB  HELLO
OUEZX  VXMNN  ECBON  ESGYA  WORLD

Word Searched:
.....  .....  .....  .....  .....
.....  .....  .....  .....  .....
..H..  ..E..  ..X..  ..F..  .....
.....  .....  .....  .....  .....
.....  .....  .....  .....  .....

Index of first letter:
Column = 2
Row = 2
Aisle = 0

Output: 2,2,0

Test Cases

Test Case 1
Argument 1: HEXF
Argument 2: KJASDAJKSDJAHLNBCENVOUEZXQPDOGAYQUYDREMABOGCCVXMNNAMNFJBAIGKCGXHLDIRIMECBONSPEEDDREAMPAFBNASCVBESGYAAIUPRISINGSAYSHHELLOWORLD
Expected Output: 2,2,0

Test Case 2
Argument 1: BONES
Argument 2: BONESONESANESGTESFIASNMZZ
Expected Output: 0,0

Test Case 3
Argument 1: ENGINEERMAN
Argument 2: AVIDEOWASJUSTUPLOADEDTOENGINEERMANSCHANNEL
Expected Output: 23
=cut

