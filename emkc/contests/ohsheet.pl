#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
#use JSON;
# perl ohsheet.pl '[{"name":"Sheet 1", "cells":[["A1", "A2"], ["B1", "B2"]]}]' 'Sheet 1!A1'
# A1
#
# perl ohsheet.pl '[{"name": "Team Members", "cells": [["Discord Name", "Discord Tag"], ["BIPit", "5339"], ["brainplot", "2984"], ["Brtwrst", "2937"], ["DataDoge", "3108"], ["Ghostrunner0808", "0514"], ["HexF", "0015"], ["joosthoi", "0460"], ["kevrocks67", "3603"], ["Neptune", "4000"], ["pdog", "5848"], ["Pyro", "8749"], ["RuskyHacker", "3908"]]}]' 'Team Members!A6'
# Ghostrunner0808
#
# perl ohsheet.pl '[{"name": "Team Members", "cells": [["Discord Name", "Discord Tag"], ["BIPit", "5339"], ["brainplot", "2984"], ["Brtwrst", "2937"], ["DataDoge", "3108"], ["Ghostrunner0808", "0514"], ["HexF", "0015"], ["joosthoi", "0460"], ["kevrocks67", "3603"], ["Neptune", "4000"], ["pdog", "5848"], ["Pyro", "8749"], ["RuskyHacker", "3908"]]}, {"name": "Horizontal", "cells": [["Time", "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"], ["Measurement", "0", "8", "32", "21", "12", "60", "60", "119", "72", "81", "70", "22", "204", "234", "196", "105", "272", "306", "90", "76", "160", "168", "22", "0", "264", "75", "364", "108", "84", "203", "90", "465", "480", "462", "306", "315", "612", "555", "342", "351", "480", "574", "42", "774", "616", "900", "414", "376"]]}]' 'Horizontal!AT1'
# 900
#

pop=~/!(\D+)/;@@=A..$1;pop=~/$`(.*?\[+){$'}(.*?, ){$#@}"\K\w+/;print$&

exit();

__END__




=for docs
=cut

=for comment
Oh Sheet
The EngineerMan team are working on a new Spreadsheet application, and need to be able to get values 
from specific cells depending on the formula provided.
You will be given a JSON object, containing a list of named sheets, each containing a 2d-array of 
cells and their values.

Resolve the index given by input 2, and return the value within that cell.
Input 2 will always take the format
[Sheet Name]![column][row]

Columns are indexed by letters,  whilst rows are indexed by numbers.

Example
Input 1: [{"name":"Sheet 1", "cells":[["A1", "A2"], ["B1", "B2"]]}]
Input 2: Sheet 1!A1

Output: A1
Test Cases

Test Case 1
Argument 1: [{"name": "Team Members", "cells": [["Discord Name", "Discord Tag"], ["BIPit", "5339"], ["brainplot", "2984"], ["Brtwrst", "2937"], ["DataDoge", "3108"], ["Ghostrunner0808", "0514"], ["HexF", "0015"], ["joosthoi", "0460"], ["kevrocks67", "3603"], ["Neptune", "4000"], ["pdog", "5848"], ["Pyro", "8749"], ["RuskyHacker", "3908"]]}]
Argument 2: Team Members!A6
Expected Output: Ghostrunner0808

Test Case 2
Argument 1: [{"name": "Team Members", "cells": [["Discord Name", "Discord Tag"], ["BIPit", "5339"], ["brainplot", "2984"], ["Brtwrst", "2937"], ["DataDoge", "3108"], ["Ghostrunner0808", "0514"], ["HexF", "0015"], ["joosthoi", "0460"], ["kevrocks67", "3603"], ["Neptune", "4000"], ["pdog", "5848"], ["Pyro", "8749"], ["RuskyHacker", "3908"]]}, {"name": "Horizontal", "cells": [["Time", "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"], ["Measurement", "0", "8", "32", "21", "12", "60", "60", "119", "72", "81", "70", "22", "204", "234", "196", "105", "272", "306", "90", "76", "160", "168", "22", "0", "264", "75", "364", "108", "84", "203", "90", "465", "480", "462", "306", "315", "612", "555", "342", "351", "480", "574", "42", "774", "616", "900", "414", "376"]]}]
Argument 2: Horizontal!AT1
Expected Output: 900
=cut

