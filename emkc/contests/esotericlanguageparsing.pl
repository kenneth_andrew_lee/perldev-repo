#!/usr/bin/perl 
#perl ./esotericlanguageparsing.pl '.>!>>.>>>!'
#aBcD .>!>>.>>>!
# > goes to the next letter
# . prints lc; go back to a
# ! prints uc; go back to a

# my best
#$a=pop;$b='a';map{if(/>/){$b++}else{print/\./?$b:uc$b;$b='a'}}$a=~/./gs


#map{printf"%c",(/!/?64:96)+length}pop=~/>*./g
#$a=('>>>');
$a=('.>!>>.>>>!');
print "$a\n";
for($a=~/>*./g){
    print "$_ \n";
    #printf"%c\n",(/!/?64:96)+length;
}




exit(0);
__END__
# winners

map{printf"%c",(/!/?64:96)+length}$ARGV[0]=~/(>*.)/g

map$i=ord>61?$i+1:!print(chr($i+ord()%45^96)),pop=~/./g

$_=pop;s/(>*)(.)/print chr length($1)+('!'eq$2?65:97)/ge

# even better after contest
# map{printf"%c",(/!/?64:96)+length}pop=~/>*./g


for($a=~/./gs){
    />/
    ? print chr($b)
}

/run perl ```
$a=('.>!>>.>>>!');
for($a=~/>*./g){
    print "$_ ";
    printf"%c\n",(/!/?64:96)+length;
}```


The for loop regex breaks the string into four strings.  '>' greedy match that ends with any other character.
.
>!
>>.
>>>!
in the ascii table 65 is 'A', and 97 is 'a'.  Since +length adds the length of the string, you start one back.
Ternary operator determines if it is 65 or 97.  65 for !, otherwise 97
. isn't !, so 96 + 1 = 97, which is a
>! is !, so 65 + 2, which is B
>>. isn't !, so 96 + 3 = 99 = c
>>>! is !, so 65 + 4 = 69 = D
Very very clever!


map {printf"%c",(/!/?64:96)+length} $ARGV[0]=~/(>*.)/g




    if (/>/) {
        $b++;
    } else {
        if (/!/) {
            print chr($b);
        } else {
            print chr($b+32);
        }
        $b=65;
    }   










$a=pop;$b='a';map{if(/>/){$b++}else{print/\./?$b:uc$b;$b='a'}}$a=~/./gs

map{printf"%c",(/!/?64:96)+length}$ARGV[0]=~/(>*.)/g

map$i=ord>61?$i+1:!print(chr($i+ord()%45^96)),pop=~/./g






#Incrementing strings: $z = 'z'; print ++$z; will display aa

print $foo ? "True\n" : "False\n";
for($a=~/./gs){
    /[.!]/
    ? (print$b and $b='a')
    : $b++
} 



for($a=~/./gs){
/>/
? $b++
: print/\./,$b='a';
  ? $b
  : uc $b
}




# Sample code to test.
$a='.>!>>.>>>!';
$b='a';
$_=substr($a,0,1);
print "\$_:$_\n";
print /\./ . "\n";
print /\./?$b:uc$b;$b='a';
print "\n$b\n";

$a='.>!>>.>>>!';
$b='b';
$_=substr($a,2,1);
print "\$_:$_\n";      # $_:!
print /\!/ . "\n";     # 1
print /\./?$b:uc$b;    # B
print "\n$b\n";        # b

$a=pop;
$b='a';
map{
    />/
    ? $b++
    : print$b=/\./
        ? $b
        : uc$b;
    } $a=~/./gs;

$a=pop;
$b='a';
map{
    if(/>/){
        $b++
    }else{
        print/\./
        ? $b
        : uc$b;$b='a'
    }}$a=~/./gs

$a=pop;$b='a';map{if(/>/){$b++}else{print/\./?$b:uc$b;$b='a'}}$a=~/./gs  71 characters


$a=pop;
$b='a';
map{
    />/
    ? $b++
    : print/\./
        ? $b
        : uc$b;$b='a'if(/[.!]/)
    } $a=~/./gs;

$a=pop;$b='a';map{/>/?$b++:print/\./?$b:uc$b;$b='a'if(/[.!]/)}$a=~/./gs  71 characters

# Basic solution
for($a=~/./gs){
    if (/>/) {
        $b++;
    } else {
        if (/\./) {
            print $b;
        } else {
            print uc $b;
        }
        $b='a';
    }   
}

for($a=~/./gs){
    if (/>/) {
        $b++;
    } else {
        if (/!/) {
            print chr($b);
        } else {
            print chr($b+32);
        }
        $b=65;
    }   
}


# Break into printable sections - #.>!>>.>>>!
#.
#>!
#>>.
#>>>!
#for(pop=~/>*./g){
#    print "$_ ";
#    printf"%c\n",(/!/?64:96)+length;
#}


# Goal: want $b='a'if/[.!]/ to only be $b='a' in the ternary operator, or somewhere.
# Trying to get a solution that is about 50 characters long.  Playing perl gold, and 
#not being able to reset the value of $b back to 'a' after a print easily is too costly.


### Full Problem
Plotting The Trail
Using positional movements as an input, you must write a program to determine the ending coordinates that a player is at. Each positional movement will either increment or decrement the x or y value by one, depending on the input.



Possible Movements

U = Up
R = Right
D = Down
L = Left


Sample Input Argument 1 (the positional movements)

UURRDD


Sample Output

2,0


Guidelines

The starting coordinates are 0,0
The ending coordinates can be positive or negative x and y values