#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;

#perl ./leapyear.pl 2000
#2004,2008,2012
#perl ./leapyear.pl 2001
#1992,1996,2000
#perl ./leapyear.pl 1900
#1888,1892,1896
#perl ./leapyear.pl 2100
#2088,2092,2096
#perl ./leapyear.pl 1105
#1092,1096,1104


sub L{($_)=@_;$_=$_%($_%25?4:16)?0:1}
$b=pop;if(L($b)){$b++;while($c<3){if(L($b)){push(@a,$b);$c++}$b++}}else{$b--;while($c<3){if(L($b)){push(@a,$b);$c++}$b--}}
print join",",sort@a;


exit();
__END__
$_=$_%($_%25?4:16)?"n":"y"


sub L{($y)=@_;if($y%4!=0){$r=0}elsif($y%100!=0){$r=1}elsif($y%400!=0){$r=0}else{$r=1}}
$b=$_=pop;if(L($_)){$b++;while($c<3){if(L($b)){push(@a,$b);$c++}$b++}}else{$b--;while($c<3){if(L($b)){push(@a,$b);$c++}$b--}}
print join",",sort@a;



$b=pop;
if(L($b)){
  $b++;
  while($c<3){
    if(L($b)){
      push(@a,$b);
      $c++
    }
    $b++
  }
}else{
  $b--;
  while($c<3){
    if(L($b)){
      push(@a,$b);$c++
    }
    $b--
  }
}






=for docs
if (year is not divisible by 4) then (it is a common year)
else if (year is not divisible by 100) then (it is a leap year)
else if (year is not divisible by 400) then (it is a common year)
else (it is a leap year)
=cut

=for comment
Leap Years
A leap year, is a year that has 366 days in it, instead of the normal 365 days. Your job is to determine if the provided year is a leap year, and if it was, return the next 3 leap years as a comma separated list (not including the provided year), If it was not a leap year, provide the 3 previous leap years. 

Example
Sample Input:2000
Expected Output:2004,2008,2012


Guidelines
Your input argument will be between the year 0100 and 3000
Your output should be 3 years separated by a comma
Wikipedia (Leap Year) contains more information on what a leap year is and how to calculate it
I've tried my best to remove all of the golfing languages so as to provide a bit more challenge

Test Case 1
Argument 1: 2000
Expected Output: 2004,2008,2012

Test Case 2
Argument 1: 2001
Expected Output: 1992,1996,2000

Test Case 3
Argument 1: 1900
Expected Output: 1888,1892,1896

Test Case 4
Argument 1: 2100 
Expected Output: 2088,2092,2096

=cut

if (year is not divisible by 4) then (it is a common year)
else if (year is not divisible by 100) then (it is a leap year)
else if (year is not divisible by 400) then (it is a common year)
else (it is a leap year)