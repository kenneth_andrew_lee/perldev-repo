#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;

#perl ./complicatedcurrents.pl '>v<>v^>>v<X<<<<^<v^<^>>^^ ^'
# 9 
#perl ./complicatedcurrents.pl '>v<>v^>>v<><<X<^<v^<^>>^^ v'
# 1
#perl ./complicatedcurrents.pl '>>><<v^<>v>>>>vX^<<<>^<<< ^'
# 0
#

@a=split/,/,pop;
print "\@a:@a", "\n";


exit();

__END__


=for comment
Complicated Currents
The Engineer Man Team recently were testing out a new RC boat; however, 
it appears to have been swept out to sea.

They want to know when it will hit land so they can go fetch it, 
or if it will hit the land at all.

For this contest, the water will be in a 5x5 grid of cells.
Each cell will either be one of - <>^v or X indicating the direction 
of the current, which will take 1 unit of time to pass through. 
X indicates the island the team are testing from.

You will be given a list of 25 cells to map onto a grid, and a 
direction which the boat was released from the island.

Your task is to return how many time units it takes to return back 
to the island after release, and if there is no way, return 0.

Example
Input: >v<>v^>>v<X<<<<^<v^<^>>^^ ^

Grid:
>v<>v
^>>v<
X<<<<
^<v^<
^>>^^

Path:
>v   
^>>v 
X<<< 

Output: 9


Test Cases
Test Case 1
Argument 1: >v<>v^>>v<X<<<<^<v^<^>>^^
Argument 2: ^
Expected Output: 9

Test Case 2
Argument 1: >v<>v^>>v<><<X<^<v^<^>>^^
Argument 2: v
Expected Output: 1

Test Case 3
Argument 1: >>><<v^<>v>>>>vX^<<<>^<<<
Argument 2: ^
Expected Output: 0
=cut


=for code
@a=split/,/,pop;
print "\@a:@a", "\n";
#if(!defined$l){$l='^'}
map{
$m=1;
/(.)(.)/;
if($l=$1){$m++}
if($1 eq"<"){
  $l+=$2
}else{
  $r+=$2
}

print "\$l:$l","|","\$r:$r","|","\$1:$1","|","\$2:$2","|","\$_:$_","\n";
}@a;

print "after map:","\$l:$l","|","\$r:$r","\n";
=cut