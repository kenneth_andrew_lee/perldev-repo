#!/usr/bin/perl
#use Data::Dumper;
#use List::Util 'min';
#perl kl.pl 'acce'
#perl kl.pl 'accebb'
#perl kl.pl 'accebbbbbubbbbi'
#perl kl.pl 'ccccccccccccccccceccccccccccccccccccccccccccc'

$l=pop;
$c=0;
for($l=~/./g){
  if(/[aeiou]/){
    push@v,$c;
  }
  $c++;
}
@a=split//,$l;
for(0..length($l)-1){
  if(@a[$_]=~/[aeiou]/){
    @r[$_]=0;
  }else{
    $k=$_;
    @x=map{abs($_-$k)}@v;
    @y=sort{$a<=>$b}@x;
    $m=$y[0];
    @r[$_]=$m;
  }
}
print join",",@r;


exit();
__END__


# Winner @mem
$t=pop=~tr/aeiou/0/r;$j=index$t,0;map{$x=index((substr$t,$i),0,i);push@r,(substr$t,$i++,1) eq 0?$j=0:$x<=$j++?$x<0?$j:$x:$j;}$t=~/./g;print join',',@r

$t=pop=~tr/aeiou/0/r;
$j=index$t,0;
map{$x=index((substr$t,$i),0,i);push@r,(substr$t,$i++,1) eq 0?$j=0:$x<=$j++?$x<0?$j:$x:$j;}$t=~/./g;
print join',',@r



$d = 1;
for ( pop =~ /./g ) {
  if (/[aeiou]/) {
    $a[$c] = 0;
    $d = 1;
    #print "\@a:@a\n";
    
    # todo: fix backwards array elements.
    $x = $c - 1;
    while ( ($x >= 0) and ($a[$x] > $a[$x-1]) ) {
      # if the value I will increase to = the next value, stop
      if ($a[$x+1] == $a[$x] or $a[$x-1] == $a[$x] or $a[$x-1] == $a[$x+1]) {last}
      #print "\@a:@a" , " " , "\$x:$x" , " " , "\$a[\$x-1]:$a[$x-1]" ," " , "\$a[\$x]:$a[$x]" , " " , "\$a[\$x+1]:$a[$x+1]" ,  "\n";
      $a[$x] = $a[ $x + 1 ] + 1;
      $x--;
    }

  } else {
    $a[$c] = $d;
    $d++;
    #print "\@a:@a\n";
  }
  $c++;
}
#print "\@a:@a\n";
print join",",@a;




#0,1,1,0,1,2
#011011111011110

#@m=map{/[aeiou]/?0:1}pop=~/./g;

#      12321       =       111**2   5:3
#     1234321      =      1111**2   7:4
#    123454321     =     11111**2   9:5
#substr EXPR,OFFSET,LENGTH,REPLACEMENT
sub g{($_)=@_;if(length($_)%2){return(substr($_,0,length($_)-2)**2);}else{$_=(substr($_,0,length($_)-1)**2);$p=(length($_)/2);$r=substr($_,$p,1,'');return$_;}}
$m=join"",map{/[aeiou]/?0:1}pop=~/./g;
$m=~s/(1{3,})/g($&)/eg;
$m=~s/\B/,/g;
print "$m";



for (pop=~/./g) {
  if (/[aeiou]/) {
    print "$_ is a vowel\n";
  } else {
    print "$_ is a consonant\n";
  }
}





print map{y/aeiou//}pop=~/./g; # y/aeiou//;

'accebb'
 100100
 011011

'accebbbbbubbbbi'
 100100000100001
 011011111011110
 011012321012210

var str = prompt("Please enter a string: ");

function test(str){ 
  return /a\w{3}b/.test(str) || /b\w{3}a/.test(str); 
}

console.log(test(str));


const str = 'vatghvf';
const nearest = (arr = [], el) => arr.reduce((acc, val) => Math.min(acc,Math.abs(val - el)), Infinity);
const vowelNearestDistance = (str = '') => {
   const s = str.toLowerCase();
   const vowelIndex = [];
   for(let i = 0; i < s.length; i++){
      if(s[i] === 'a' || s[i] === 'e' || s[i] === 'i' || s[i] === 'o' ||
      s[i] === 'u'){
         vowelIndex.push(i);
      };
   };
   return s.split('').map((el, ind) => nearest(vowelIndex, ind));
};
console.log(vowelNearestDistance(str));






# Winner
$_=pop;y/a-z/012101210-3210-3210-321/;s/\B/,/g;print

$_=pop;y/a-z/0-210-210-3210-3210-321/;s/\B/,/g;print

$_=pop;y/a-z/01210121012321012321012321/;s/\B/,/g;print


exit();
__END__

Distance to Nearest Vowel 2
You will receive a string of characters. This time you need to return a list of numbers that represent 
the distance of each letter to it's nearest vowel in the string. If the current character is a vowel 
then the distance is considered 0.



Sample Input 

accebb


Sample Output

0,1,1,0,1,2


Guidelines

The input string will be between 1 and 100 chars long 
if the letter is directly between 2 vowels return that distance
Vowels are: a, e, i, o, u
Every input string will have at least 1 vowel
The input string will contain lowercase letters only


const str = 'vatghvf';
const nearest = (arr = [], el) => arr.reduce((acc, val) => Math.min(acc,
Math.abs(val - el)), Infinity);
const vowelNearestDistance = (str = '') => {
   const s = str.toLowerCase();
   const vowelIndex = [];
   for(let i = 0; i < s.length; i++){
      if(s[i] === 'a' || s[i] === 'e' || s[i] === 'i' || s[i] === 'o' ||
      s[i] === 'u'){
         vowelIndex.push(i);
      };
   };
   return s.split('').map((el, ind) => nearest(vowelIndex, ind));
};
console.log(vowelNearestDistance(str));
