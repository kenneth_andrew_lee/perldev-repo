#!/usr/bin/perl
# perl romanconversions.pl 'IX'
# 9
#
# 
# perl romanconversions.pl 'L'
# 50
#
# 
# perl romanconversions.pl 'MMCV'
# 2105
# 
#
# perl romanconversions.pl 'MMXXII'
# 2022
# 
#
# perl romanconversions.pl 'CDXCV'
# 495
# 
#
# perl romanconversions.pl 'LX'
# 60
# 
#
# perl romanconversions.pl 'MCCCLXVI'
# 1366
# 
#
# perl romanconversions.pl 'DC'
# 600
# 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$_=pop;
s/IV/F/;s/IX/G/;s/XL/H/;s/XC/N/;s/CD/J/;s/CM/K/;
for$i(/./g){$i eq 'I'?$s++:$i eq 'V'?$s+=5:$i eq 'X'?$s+=10:$i eq 'L'?$s+=50:$i eq 'C'?$s+=100:$i eq 'D'?$s+=500:$i eq 'M'?$s+=1000:$i eq 'F'?$s+=4:$i eq 'G'?$s+=9:$i eq 'H'?$s+=40:$i eq 'N'?$s+=90:$i eq 'J'?$s+=400:$i eq 'K'?$s+=900:0}
print$s;



exit();


__END__

    print "$a[$i+1]", "\n";


=for winners 
# 1
$x+=($%=10**(205558%ord()%7)%1999)-2*$x%$%for split"",pop;print$x

# 2
$_=pop;
s/M/DD/g;
s\CD\CCCC\;
s/D/CCCCC/g;
s\C\LL\g;
s/XL/XXXX/;
s\L\XXXXX\g;
s/X/VV/g;
s\IV\IIII\;
s/V/IIIII/g;
print length;
=cut


=for problem
Convert a given Roman Numeral(s) to its Arabic equivalent.

Test Case 1
Argument 1
IX
Expected Output
9

Test Case 2
Argument 1
L
Expected Output
50

Test Case 3
Argument 1
MMCV
Expected Output
2105

Test Case 4
Argument 1
MMXXII
Expected Output
2022

Test Case 5
Argument 1
CDXCV
Expected Output
495

Test Case 6
Argument 1
LX
Expected Output
60

Test Case 7
Argument 1
MCCCLXVI
Expected Output
1366

Test Case 8
Argument 1
DC
Expected Output
600


=cut


=for testcase
Test Case 1
Argument 1
IX
Expected Output
9

Test Case 2
Argument 1
L
Expected Output
50

Test Case 3
Argument 1
MMCV
Expected Output
2105

Test Case 4
Argument 1
MMXXII
Expected Output
2022

Test Case 5
Argument 1
CDXCV
Expected Output
495

Test Case 6
Argument 1
LX
Expected Output
60

Test Case 7
Argument 1
MCCCLXVI
Expected Output
1366

Test Case 8
Argument 1
DC
Expected Output
600
=cut

