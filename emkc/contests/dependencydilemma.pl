#!/usr/bin/perl
# perl dependencydilemma.pl '3A' '2B+1C=>1A,3D+1B=>3C'
# 7
#
# perl dependencydilemma.pl '3A' '2B+1C=>1A,3D+1A=>3C'
# 0
#
# perl dependencydilemma.pl '8C' '3B+2A=>1C,3B=>2A'
# 48
#

use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


my $c=$ARGV[0];
my ($i,$j)=split//,$c;

my %h=();
for (split/,/,$ARGV[1]) {
  my ($a,$b)=split/=>/;
  $h{$a}=$b;
}

# check to see if 


exit();

__END__


while (my ($key, $value) = each %h) {
  #say $key, ":", $value;
  my ($x,$y)=split//,$value;
  if ($y eq $j) {
    say $i, " ", $j, " ", $x, " ", $y;
  }
}
#say Dumper($c);
#say Dumper(%h);


=for comment
Dependency Dilemma
The EM team have been working on material processing software. They need help counting the total amount of raw materials required for each, and if it is even possible to make the final output.

You will be given 2 inputs - the first, the product which should be made and the second, the different inputs and outputs for each process that can be run.

You will need to return the quantity of the most abundant raw material required, or 0 if it is impossible to make the product.



Each ingredient is denoted by a singular letter, A-Z in capital form. Your first input will contain one of these along with a number, indicating the number of required materials.

Each process consists of a list of ingredients and products each delimited by a "+" symbol, with "=>" separating the ingredients on the left, and the products on the right. Your inputs will contain a list of these, comma separated.





Example 1:

Input 1: 3A
Input 2: 2B+1C=>1A,3D+1B=>3C

﻿# Input 1 is indicating we are making 3A.
processes:
 - 2B+1C=>1A # 1A is made with 2B and 1C
 - 3D+1B=>3C # 3C is made with 3D and 1B

# Break up 3A into its ingredients
current ingredientes: 
  - 6B
  - 3C

# we cannot break down B any futher, but we can break down 3C.
current ingredients:
  - 7B
  - 3D

# B requires 7, whilst D requires 3. B is the most abundent resource.
Output: 7﻿﻿﻿﻿


Example 2:

Input 1: 3A
Input 2: 2B+1C=>1A,3D+1A=>3C

﻿# Input 1 is indicating we are making 3A.
processes:
 - 2B+1C=>1A # 1A is made with 2B and 1C
 - 3D+1A=>3C # 3C is made with 3D and 1A - notice 1A and not 1B like previous example
﻿
# Break up 3A into its ingredients
current ingredientes: 
  - 6B
  - 3C

# we cannot break down B any futher, but we can break down 3C.
current ingredients:
  - 6B
  - 3D
  - 1A

# Making 3A requires 1A, thus there is an circular dependency on A and we cannot produce this, so we output 0
Output: 0﻿
Test Cases
Test Case 1
Argument 1: 3A
Argument 2: 2B+1C=>1A,3D+1B=>3C
Expected Output: 7
Test Case 2
Argument 1: 3A
Argument 2: 2B+1C=>1A,3D+1A=>3C
Expected Output: 0
Test Case 3
Argument 1: 8C
Argument 2: 3B+2A=>1C,3B=>2A
Expected Output: 48
=cut

