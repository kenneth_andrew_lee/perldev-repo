#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl baggingarea.pl 4006381333931
# 3 accept
#
# perl baggingarea.pl 3799413122023
# 3 accept
# 
# perl baggingarea.pl 3232654481191
# 6 reject
# 
# perl baggingarea.pl 1162371709056
# 6 accept
#
# perl baggingarea.pl 8942813582563
# 9 reject
# 
# perl baggingarea.pl 5158143830765
# 5 accept
# 
# perl baggingarea.pl 6130251206804
# 8 reject
# 
# perl baggingarea.pl 9770818545352
# 2 accept
# 
# perl baggingarea.pl 5869001603503
# 5 reject
# 

for(split//,pop){$c++;$v=int($s/10)*10+10-$s;if($c<13){$s+=$_*($c%2==0?3:1)}else{print$v,' ',$v==$_?'accept':'reject'}}


exit();
__END__

# winner hashkitten
print$_=map($_*3.1..399,pop=~/../g)%10,$",/$'/?accep:rejec,t

# next runner up joking
pop=~s/(.)./$a-=$1+$&*3/gre;print$a%=10,$'-$a?' reject':' accept'

for(split//,pop){
$c++;
$v=int($s/10)*10+10-$s;
if($c<13){$s+=$_*($c%2==0?3:1)}else{print$v,' ',$v==$_?'accept':'reject'};
}


int($s/10)*10+10-$s
print int($s/10)*10+10;

=for docs
GS1 prefix
The first three digits of the EAN-13 (GS1 Prefix) usually identify the GS1 Member Organization which the manufacturer has joined (not necessarily where the product is actually made).
Manufacturer code
The manufacturer code is a unique code assigned to each manufacturer by the numbering authority indicated by the GS1 Prefix. All products produced by a given company will use the same manufacturer code. EAN-13 uses what are called "variable-length manufacturer codes". Assigning fixed-length 5-digit manufacturer codes, as the UCC has done until recently, means that each manufacturer can have up to 99,999 product codes. Many manufacturers do not have that many products, which means hundreds or even thousands of potential product codes are being wasted on manufacturers that only have a few products. Thus if a potential manufacturer knows that it is only going to produce a few products, EAN-13 may issue it a longer manufacturer code, leaving less space for the product code. This results in more efficient use of the available manufacturer and product codes.
Product code
The product code is assigned by the manufacturer. The product code immediately follows manufacturer code. The total length of manufacturer code plus product code should be 9 or 10 digits depending on the length of country code (2-3 digits)
Check digit
The check digit is an additional digit, used to verify that a barcode has been scanned correctly. It is computed modulo 10, where the weights in the checksum calculation alternate 3 and 1. In particular, since the weights are relatively prime to 10, the EAN-13 system will detect all single digit errors. It also recognizes 90% of transposition errors (all cases, where the difference between adjacent digits is not 5).

Calculation of checksum digit
The checksum is calculated as sum of products - taking an alternating weight value (3 or 1) times the value of each data digit. The checksum digit is the digit, which must be added to this checksum to get a number divisible by 10 (i.e. the additive inverse of the checksum, modulo 10).[8] See ISBN-13 check digit calculation for a more extensive description and algorithm. The Global Location Number(GLN) also uses the same method.
Position - weight
The weight at a specific position in the EAN code is alternating (3 or 1) in a way, that the final data digit has a weight of 3 (and thus the check digit has a weight of 1).

All Global Trade Item Number (GTIN) and Serial Shipping Container Code (SSCC) codes meet the next rule:

Numbering the positions from the right (code aligned to the right), the odd data digits are always weight of 3 and the even data digits are always weight of 1, regardless of the length of the code.

Weights for 18-digit SSCC code and GTINs (GTIN-8, GTIN-12, GTIN-13, GTIN-14):

position  17  16  15  14  13  12  11  10  9 8 7 6 5 4 3 2 1
weight  3 1 3 1 3 1 3 1 3 1 3 1 3 1 3 1 3
Weights for EAN-13 code:

position  12  11  10  9 8 7 6 5 4 3 2 1
weight  1 3 1 3 1 3 1 3 1 3 1 3
Weights for EAN-8 code:

position  7 6 5 4 3 2 1
weight  3 1 3 1 3 1 3

Calculation examples
For EAN-13 barcode 400638133393x, where x is the unknown check digit, (Stabilo Point 88 Art. No. 88/57), the check digit calculation is...
position                    12 11 10 9  8  7  6  5  4  3  2  1
first 12 digits of barcode  4  0  0  6  3  8  1  3  3  3  9  3
weight                      1  3  1  3  1  3  1  3  1  3  1  3
partial sum                 4  0  0  18 3 24  1  9  3  9  9  9
checksum                                                    89
The nearest multiple of 10 that is equal to or higher than the checksum, is 90. Subtract them: 90 - 89 = 1, which is the check digit x of the barcode.
=cut

=for comment
Please place the item in the bagging area
The EngineerMan team are working to build a new self-checkout machine, but need help determining the validity of barcodes.
It has been decided that EAN-13 barcodes will be used, and need to have their check-digits calculated so the kiosk can either reject or accept the scan.
You will be given an EAN-13 barcode number, either valid or invalid, and you need to calculate the correct check digit, and tell us if we should reject or accept the barcode.
For more information on EAN-13 and calculating check digits, read here.

Examples
Example conveniently lifted from the Wikipedia page linked above, and adapted for this contest. 
Input: 4006381333931
Digits: 4 0 0 6 3 8 1 3 3 3 9 3
Weight: 1 3 1 3 1 3 1 3 1 3 1 3
Sums  : 4 0 0 18  3 24  1 9 3 9 9 9
Checksum: 89
Check Digit: 90 - 89 = 1
Output: 1 accept

Input: 4006381333932
As above
Output: 1 reject

Test Cases

Test Case 1
Argument 1: 3799413122023
Expected Output: 3 accept

Test Case 2
Argument 1: 3232654481191
Expected Output: 6 reject

Test Case 3
Argument 1: 1162371709056
Expected Output: 6 accept

Test Case 4
Argument 1: 8942813582563
Expected Output: 9 reject

Test Case 5
Argument 1: 5158143830765
Expected Output: 5 accept

Test Case 6
Argument 1: 6130251206804
Expected Output: 8 reject

Test Case 7
Argument 1: 9770818545352
Expected Output: 2 accept

Test Case 8
Argument 1: 5869001603503
Expected Output: 5 reject
=cut

