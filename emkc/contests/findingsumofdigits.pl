#!/usr/bin/perl
#use Data::Dumper;
#perl ./findingsumofdigits.pl '487'
# 1

pop;print!$_?0:$_%9?$_%9:9;


=for comment
sub r {
print $_[0],
return($_[0])if($_[0]<10);
$b=0; 
for(split//,$_[0]){$b+=$_}
return r($b);
}
print r(pop);
=cut

=for comment
sub r_sub {
  ($a) = @_;
  @a=split//,$a;
  $b=0;
  for(@a){$b+=$_}
  if ($b>9) {
    return r_sub($b);
  } else {
    $b;
  }
}


sub s_sub {
  ($a) = @_;
  return $a%9?$a%9:9;
}


for $val (0..1000000) {
    $s = s_sub($val);
    $r = r_sub($val);
  if ($s != $r) {
    print "\$val:$val", " ", "\$s:$s", " ", "\$r:$r", "\n";
  }
}

#print r('487');
=cut

=for comment
print $_%9?$_%9:9;

$value=0;
for (1..999999) {
  $result=
  $value = $result if ($value == 0 and $value != $result); 
  print $_,":",$result," ", $value,"\n" if ($value+1 != $result);
  $value=$result;
}
=cut

exit();

__END__

$a=pop;print!$a?0:$a%9?$a%9:9;


#winner
$_=pop;print$_?$_%9||9:0

# 2nd place
$a=pop;
print$a&&~-$a%9+1;






=for comment
Finding sum of digits
Given a large (or small) number, find the sum of each digit in the number, repeat this process until the sum is less then 10 (0-9).

Sample Input 

487

Sample Output

1

Sample explained. 

4 + 8 + 7 = 19
1 + 9 = 10
1 + 0 = 1
Guidelines

The input will be between 0 and 1,000,000
The output should be a positive integer between 0 and 10
=cut