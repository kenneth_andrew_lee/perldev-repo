#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;

#perl ./pacman.pl 'bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh' 'bash,zsh,xorg,i3wm'
#bash,zsh
#perl ./pacman.pl 'lightdm=gdm,bash=sh,xorg=wayland,xfce=i3wm,sway=i3wm' 'cups,gdm,bash,wayland,sway,xorg,i3wm'
#wayland,sway,xorg,i3wm
#perl ./pacman.pl 'docker=lxc' 'docker'
#
#perl ./pacman.pl 'deb=nix,apt=nix' 'nix,apt,deb'
#nix,apt,deb

@a=split/,/,pop;
# 'bash,zsh,xorg,i3wm'
$b=pop;
# 'bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh'

for$e(0..$#a){
  $i=$a[$e];
  for(0..$#a){
    $j=$a[$_];
    $c=$i."=".$j;
    $d=$j."=".$i;
    print "Items Searched - \$c:$c", " ", "\$d:$d", " ", "\n";
    if(($b=~$c)or($b=~$d)){
      if(defined($r)&&(!($r=~$i))) {
        $r.=",".$i;
      }else{
        $r=$i;
      }
      print "found: \$c:$c", " ", "\$d:$d", " - ", "\$r:$r", " ", "\n";
    }
  }
}
print $r;
print "\n";

exit();
__END__

@a=split/,/,pop;$b=pop;for$e(0..$#a){$i=$a[$e];for(0..$#a){$j=$a[$_];$c=$i."=".$j;$d=$j."=".$i;if(($b=~$c)or($b=~$d)){if(defined($r)&&(!($r=~$i))){$r.=",".$i}else{$r=$i}}}}print $r;
print "\n";



@a=split/,/,pop;
# 'bash,zsh,xorg,i3wm'
$b=pop;
# 'bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh'

for$e(0..$#a){
  $i=$a[$e];
  for(0..$#a){
    $j=$a[$_];
    $c=$i."=".$j;
    $d=$j."=".$i;
    print "Items Searched - \$c:$c", " ", "\$d:$d", " ", "\n";
    if(($b=~$c)or($b=~$d)){
      if(defined($r)&&(!($r=~$i))) {
        $r.=",".$i;
      }else{
        $r=$i;
      }
      print "found: \$c:$c", " ", "\$d:$d", " - ", "\$r:$r", " ", "\n";
    }
  }
}
print $r;
print "\n";



@a=split/,/,pop;$b=pop;for$e(0..$#a){$i=$a[$e];for(0..$#a){$j=$a[$_];$c=$i."=".$j;$d=$j."=".$i;if(($b=~$c)or($b=~$d)){if(defined($r)){$r.=",".$i}else{$r=$i}}}}print $r;
print "\n";


@a=split/,/,pop;$b=pop;for$e(0..$#a-1){$i=$a[$e];for($e+1..$#a){$j=$a[$_];$c=$i."=".$j;$d=$j."=".$i;if(($b=~$c)or($b=~$d)){if(defined($r)){$r.=",".$i.",".$j}else{$r=$i.",".$j}}}}print $r;
print "\n";


@a=split/,/,pop;
# 'bash,zsh,xorg,i3wm'
$b=pop;
# 'bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh'

for$e(0..$#a){
  $i=$a[$e];
  for(0..$#a){
    $j=$a[$_];
    $c=$i."=".$j;
    $d=$j."=".$i;
    print "Items Searched - \$c:$c", " ", "\$d:$d", " ", "\n";
    if(($b=~$c) or ($b=~$d)) {
    #if($b=~$c) {
      if (defined($r)) {
        #$r.=",".$i.",".$j;
        $r.=",".$i;
      }else{
        #$r=$i.",".$j;
        $r=$i;
      }
      print "found: \$c:$c", " ", "\$d:$d", " - ", "\$r:$r", " ", "\n";
    #}else{
    #  print "couldn't find \$c:$c and \$d:$d in $b", " ", "\$r:$r", " ", "\n";
    }
  }
}
print $r;
print "\n";





@a=split/,/,pop;
# 'bash,zsh,xorg,i3wm'
$b=pop;
# 'bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh'

for$e(0..$#a-1){
  $i=$a[$e];
  for($e+1..$#a){
    $j=$a[$_];
    $c=$i."=".$j;
    $d=$j."=".$i;
    #print "Items Searched - \$c:$c", " ", "\$d:$d", " ", "\n";
    if(($b=~$c) or ($b=~$d)) {
      if (defined($r)) {
        $r.=",".$i.",".$j;
      }else{
        $r=$i.",".$j;
      }
      #print "found: \$c:$c", " ", "\$d:$d", " - ", "\$r:$r", " ", "\n";
    #}else{
    #  print "couldn't find \$c:$c and \$d:$d in $b", " ", "\$r:$r", " ", "\n";
    }
  }
}
print $r;
print "\n";



=for docs
=cut

=for comment
pacman
The EngineerMan team are working on a new package manager, but need help determining package conflicts.

Your first argument will be a comma (,) separated list of package pairs, which are separated by an 
equals (=), which are deemed to be conflicting.

Your second argument will be a comma separated list of packages which need to be checked for any conflict.

Your program should output a list of any dependencies which conflict, in the order they appear in the 
second argument.

Test Cases
Test Case 1
Argument 1: bash=zsh,bash=sh,bash=fish,zsh=sh,sh=fish,fish=zsh
Argument 2: bash,zsh,xorg,i3wm
Expected Output: bash,zsh
Test Case 2
Argument 1: lightdm=gdm,bash=sh,xorg=wayland,xfce=i3wm,sway=i3wm
Argument 2: cups,gdm,bash,wayland,sway,xorg,i3wm
Expected Output: wayland,sway,xorg,i3wm
Test Case 3
Argument 1: docker=lxc
Argument 2: docker
Expected Output: 
Test Case 4
Argument 1: deb=nix,apt=nix
Argument 2: nix,apt,deb
Expected Output: nix,apt,deb
=cut
