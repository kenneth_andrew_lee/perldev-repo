#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl secretmessage.pl 'The challenge is to overcome all odds' 'cool'
# ~~~~c~~~~~~~~~~~~~o~o~~~~~~~~~l~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#@a=split//,pop;push@a,'#';map{print/$a[0]/?shift@a:'~'}split//,pop;
#@a=pop=~/./gs;push@a,'#';map{print/$a[0]/?shift@a:'~'}pop=~/./gs;
#$_=pop.'#';@a=/./g;map{print/$a[0]/?shift@a:'~'}pop=~/./g;




#$_=pop.'#';
#@a=/./g;

#@a=(pop.'#')=~/./g;map{print/$a[0]/?shift@a:'~'}pop=~/./g;
#@a=pop=~/./g;print$a[0]eq$_?shift@a:'~'for pop=~/./g;
@;=pop=~/./g;pop=~s/./print$&ne$;[0]?'~':shift@;/g;


exit(0);

__END__

#winner
@;=pop=~/./g;pop=~s/./print$&ne$;[0]?'~':shift@;/g;

#2nd
@a=pop=~/./sg;print$a[0]eq$_?shift@a:'~'for pop=~/./sg


=for comment
#map{print/shift@a/?'x':'~'}pop=~/./g;
$_=<>;
s/./$&ne$F[0]||shift@F/ge

$_=<>;s/./$&ne$F[0]||shift@F/ge;


=cut

=for comment
Secret Message
Problem


Sometimes in books secret messages can be created by placing paper over a page and covering all but certain letters. The 
remaining letters that were not covered make up the secret word or words. You'll be given a string of characters and a 
secret message. Your job is to cover up all of the letters with the ~ symbol that doesn't make up the secret message and 
output that.



Example


Sample Input Argument 1 (the text)

The challenge is to overcome all odds


Sample Input Argument 2 (the secret message)

cool


Expected Output

~~~~c~~~~~~~~~~~~~o~o~~~~~~~~~l~~~~~~


Guidelines

The length of your output should be the same length as Argument 1 (the text)
Argument 1 will contain characters typically found in normal English sentences
Argument 2 will be a single lowercase word
Use the ~ symbol to cover letters
The output should show the secret message using the letters that first occur
=cut