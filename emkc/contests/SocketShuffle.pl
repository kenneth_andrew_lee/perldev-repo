#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl SocketShuffle.pl 454d0000000c48656c6c6f20576f726c6421
# Hello World!
# perl SocketShuffle.pl 454d00000009537562736372696265,454d0001000420746f20486578462c206e6f7420,454d0002000b456e67696e6565724d616e
# Subscribe to EngineerMan
# perl SocketShuffle.pl 454d0000000b646973636f72642e67672f,4d450001000e6d616e656e67696e656572696e67,454d0002000b656e67696e6565726d616e
# discord.gg/engineerman
# perl SocketShuffle.pl 454d01a40006706973746f6e,454d0000000b6769746875622e636f6d2f,454d0045000d656e67696e6565722d6d616e2f
# github.com/engineer-man/piston

#for(split/,/,pop){print substr(pack("H*",substr($_,12)),0,hex(substr($_,8,4)))}



for(split/,/,pop){
if(substr($_,0,4)=="454d"){$h{hex(substr($_,4,4))}=substr(pack("H*",substr($_,12)),0,hex(substr($_,8,4)))}
}
for(sort{$a <=> $b}keys %h){print $h{$_}}




exit();
__END__

# winner
# print map{pack'H*',substr$_,12,/^454d/*2*hex substr$_,8,4}sort split',',pop

$a=pack("H*",substr($_,0,4));
$b=hex(substr($_,4,4));
$c=hex(substr($_,8,4));
$d=pack("H*",substr($_,12));
print "\$a:$a", "\n";
print "\$b:$b", "|",  "\n";
print "\$c:$c", "\n"; # , pack("H*",$c), "|", hex($c),
print "\$d:$d", "\n";


for(split/,/,pop){
$a=pack("H*",substr($_,0,4));
$b=pack("H*",substr($_,4,4));
$c=hex(substr($_,8,4));
d=pack("H*",substr($_,12));
print "\$a:$a", "\n";
print "\$b:$b", "\n";
print "\$c:$c", "\n"; # , pack("H*",$c), "|", hex($c),
print "\$d:$d", "\n";

if(substr($_,0,4)=="454d"){
#print "\$a:", pack("H*",$a), "\n";
#print "\$b:", pack("H*",$b), "\n";
#print "\$c:$c|", pack("H*",$c), "\n";
#print "\$d:", pack("H*",$d), "\n";
#map{print pack("H*",$_)}substr($_,12)=~/../g;
#print pack("H*",substr($_,12)), "\n";
#print substr(pack("H*",substr($_,12,hex($c))),0,$c), "\n";
#print substr($d,0,$c), "\n";
print substr(pack("H*",substr($_,12)),0,hex(substr($_,8,4)));

#print "\n\n";
}
}


#for(split/,/,pop){if(substr($_,0,8)=="454d0000"){map{print pack("H*",$_)}substr($_,12)=~/../g}}
#454d0000000c48656c6c6f20576f726c6421
#454d:Protocol Identifier
#0000:Sequence Number
#000c: Message Length
#48656c6c6f20576f726c6421: Data


for(split/,/,pop){
if(substr($_,0,8)=="454d0000"){
map{print pack("H*",$_)}substr($_,12)=~/../g;
}
}

=for comment
Socket Shuffle
TCP Sockets are amazing for getting data in the right place at the right time. There is only issue, the new EM network 
controller doesn't support TCP and has instead has its own layer on top of UDP to make sure packets are put together 
correctly.

You have been tasked with writing an implementation for the network card which will reconstruct the data in the right 
order, and discard any corrupted packets.

You will be given a list of frames (comma separated, in hex), in the format specified below, which you will extract 
the relevant data from, and piece together in the right order. You will need to discard any invalid packets, which are 
those which dont start without the header.

All numbers are encoded in big-endian format.

Example Frame: 454d0000000c48656c6c6f20576f726c6421

╔═══════╦══════════════════════════════════════════════════╦═══════════════╗
║ Bytes ║                     Meaning                      ║    Example    ║
╠═══════╬══════════════════════════════════════════════════╬═══════════════╣
║ 0:1   ║ Protocol Identifier - Constant ASCII Value "EM"  ║ \x45\x4d (EM) ║
║ 2:3   ║ Sequence Number - Reconstruct in ascending order ║ \x00\x00 (0)  ║
║ 4:5   ║ Message Length (n)                               ║ \x00\x0c (12) ║
║ 5:n+5 ║ Message Data (UTF-8 encoded string)              ║ Hello World!  ║
╚═══════╩══════════════════════════════════════════════════╩═══════════════╝

45 4d - fixed
00 00 - fixed

00 0c - length
48 65 6c 6c 6f 20 57 6f 72 6c 64 21 - data

Test Cases
Test Case 1
Argument 1: 454d0000000c48656c6c6f20576f726c6421
Expected Output: Hello World!

Test Case 2
Argument 1: 454d00000009537562736372696265,454d0001000420746f20486578462c206e6f7420,454d0002000b456e67696e6565724d616e
Expected Output: Subscribe to EngineerMan

Test Case 3
Argument 1: 454d0000000b646973636f72642e67672f,4d450001000e6d616e656e67696e656572696e67,454d0002000b656e67696e6565726d616e
Expected Output: discord.gg/engineerman

Test Case 4
Argument 1: 454d01a40006706973746f6e,454d0000000b6769746875622e636f6d2f,454d0045000d656e67696e6565722d6d616e2f
Expected Output: github.com/engineer-man/piston
=cut
