#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl findaword.pl cat,dog 116,100,97,99,103,111
# 2
# perl findaword.pl mouse,horse,dog,cat 115,111,101,116,99,117,104,97,114,109
# 3
# perl findaword.pl EngineerMan,Bones,Brtwrst,HexF 110,97,105,103,110,69,110,116,114,116,77,101,114,115,101,66,119
# 2

#@n=split/,/,pop;@a=split/,/,pop;for$k(@a){for$v(split//,$k){if (grep $_ == ord($v),@n){$c++;last}}}print$c;

@n=split/,/,pop;@a=split/,/,pop;for$k(@a){$f=1;for$v(split//,$k){if(!(grep$_==ord($v),@n)){$f=0}}$c++ if($f)}print$c;

exit();
__END__
#%h=map{$_=>1}split/,/,pop;
#@n=keys %h;

@n=split/,/,pop;
@a=split/,/,pop;
print "\@n:@n\n";
for$k(@a){
  $f=1;
  for$v(split//,$k){
    if(!(grep $_ == ord($v),@n)){
      #print "ord($v):", ord($v), "\n";
      $f=0;
    }
  }
  $c++ if($f);
  #print "\$k:$k", "|", "\$c:$c", "\n";
}
print $c;




@n=split/,/,pop;
@a=split/,/,pop;
print "\@n:@n\n";
for$k(@a){
print "\$k:$k", "\n";
for$v(split//,$k){if (grep $_ == ord($v),@n){$c++;last}}}print "\$c=$c\n";




if ( grep $_ == 80, @int_array )
#@a=split/,/,pop;
#%h=map{$_=>1}split/,/,pop;
#print Dumper(%h), "\n";

#print "\@a:@a\n";
#print "\@n:@n\n";

#winner
print~~grep$`=~ord,`dd`=~/./g

# winner
print pack'B*',`dd`=~s/./$'&&0+$'!~"^$&"/ger

=for comment
Find A Word
Given two inputs both comma delimitated - input 1 is a list of words, input two represents a list of 
ascii values for letters. Find how many of those words can be constructed by the values of the letters 
given in input 2.

arg1: cat,dog
arg2: 116,100,97,99,103,111
Expected: 2

arg1: mouse,horse,dog,cat
arg2: 115,111,101,116,99,117,104,97,114,109
Expected: 3

arg1: EngineerMan,Bones,Brtwrst,HexF
arg2: 110,97,105,103,110,69,110,116,114,116,77,101,114,115,101,66,119
Expected: 2

Test Cases
Test Case 1
Argument 1: cat,dog
Argument 2: 116,100,97,99,103,111
Expected Output: 2
Test Case 2
Argument 1: mouse,horse,dog,cat
Argument 2: 115,111,101,116,99,117,104,97,114,109
Expected Output: 3
Test Case 3
Argument 1: EngineerMan,Bones,Brtwrst,HexF
Argument 2: 110,97,105,103,110,69,110,116,114,116,77,101,114,115,101,66,119
Expected Output: 2
=cut

