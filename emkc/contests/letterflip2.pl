#!/usr/bin/perl
#use Data::Dumper;
#Positive numbers are between 0 and 9, Negative numbers are between -9 and -1

#perl ./letterflip2.pl 'Ab-11-10z'
#ab1-110z

# map{y/AEIOUaeiou/aeiouAEIOU/;$_=/\d{1}/?-$_:$_;print}pop=~/-?\d{1}|\w{1}/g;

#print "$_\n";

#map{
#y/AEIOUaeiou/aeiouAEIOU/;
#print/\d{1}/?-$_:$_
#pop=~/-?\d{1}|\w{1}/g;


#map{y/AEIOUaeiou/aeiouAEIOU/;print/\d{1}/?-$_:$_}pop=~/-?./;


map{y/AEIOUaeiou/aeiouAEIOU/;print/\d/?-$_:$_}pop=~/-?./g;


#y/AEIOUaeiou/aeiouAEIOU/;
#$_=/\d{1}/?-$_:$_;



#map{
#y/AEIOUaeiou/aeiouAEIOU/;
#$_=/\d{1}/?-$_:$_;
#print
#}pop=~/-?\d{1}|\w{1}/g;






exit();
__END__

$_ = pop;
print; 
print "\n";
s/([aeiouAEIOU])/$1eq"\U$1"?"\L$1":"\U$1"/eg;
print;
print "\n";




pop=~(pop=~s/./$&.?/gr);print@-

#map{print$_ eq uc?lc:uc}pop=~/./g;

#map{print \U?\L:\U}pop=~/./g;

#s/([[:alpha:]])/ $1 eq "\U$1" ? "\L$1" : "\U$1" /eg; 

$_=pop;y/A-Za-z/a-zA-Z/;print;

#map{print uc==$_?lc:uc}pop=~/./g;
#map {/\p{Lu}/ ? lc : uc} $str;
map {/\p{Lu}/ ? lc : uc}$_=pop;

$_=pop;print y/R//-y/L//,',',y/U//-y/D//;
$_==uc$_?lc:uc
if($_==uc($_)){print lc} else {print uc}

map {printf"%c",(/!/?64:96)+length} $ARGV[0]=~/(>*.)/g


perl -le'$_ = "MiXeD CaSe ExAmPlE"; print; s/([[:alpha:]])/ $1 eq "\U$1" ? "\L$1" : "\U$1" /eg; print'


map{print \U?\L:\U}pop=~/./g

#map{$_=/[aeiou]/?uc:$_;$_=/\d{1}/?-$_:$_;print}pop=~/-?\d{1}|\w{1}/g;


#map{
#y/AEIOUaeiou/aeiouAEIOU/;
#$_=/\d{1}/?-$_:$_;
#rint
#}pop=~/-?\d{1}|\w{1}/g;
#rint"\n";



#s/^-(\d{1})//;
#y/AEIOUaeiou/aeiouAEIOU/;

#s/([1-9])/-$1/;
#s/-([1-9])/$1/;


#$_=pop;

#s/([1-9]|-([1-9]))/-$1$2/g;
#s/([1-9])/-$1/g;
#s/=/1/g;print;
#print;



#$_=~s/[aeiou]/uc$1/;
#print;


#-?\d{1}
#(-[1-9])


Letter Flip 2
This time you have not only been tasked with flipping the case of all vowel letters in the string, but also switching the sign on a number. If you are given a positive number you should return that number negative, and if the number is negative it should be returned positive. If you encounter a 0, just ignore it.



Sample Input 

Ab-11-10z


Sample Output

ab1-110z


Guidelines

The letters you should switch the case of are vowels (A, E, I, O, U)
The input will contain a string of numbers and letters
The length of the input will be between 1 and 100
Positive numbers are between 0 and 9, Negative numbers are between -9 and -1
