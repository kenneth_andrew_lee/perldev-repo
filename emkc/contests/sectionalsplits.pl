#!/usr/bin/perl
# perl sectionalsplits.pl 100,50,25,75,80
# 25
#
# perl sectionalsplits.pl 42,28
# 28
#
# perl sectionalsplits.pl 1503,3003,7375,5409,887,5050,5465,4257,7257,801
# 1792
#
# perl sectionalsplits.pl 37030,7930,43508,6956,47461,30128,351,37097,3105,37358,55613,12497,4591,5211,39507,63312,15742,31760,11964,33373
# 14386
#

#use 5.032;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

@a=sort{$a<=>$b}split/,/,pop;
if(scalar(@a)>2){
  for(1..$#a){
    push@b,($a[$_]-$a[$_-1]);
  }
  @b=sort{$a<=>$b}@b;
  print$b[-1]
}else{
  print@a[0];
}


__END__


@a=25 50 75 80 100
$v=100, $c=80, $v-$c=20
push @a, 20
@a=20 25 50 75 80
$v=80, $c=75, $v-$c=5
push @a, 5
@a=5 20 25 50 75
$v=75, $c=50, $v-$c=25
# 25 already exists


@a=28 42
$v=42, $c=28, $v-$c=14
push @a, 14
@a= 14 28
$v=28, $c=14, $v-$c=14
# 14 already exists

