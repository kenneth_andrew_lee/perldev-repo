#!/usr/bin/perl 
use Data::Dumper;
#perl ./plottingthetrail.pl 'UURRDD'
#2,0 UURRDD
#UUDD = 0 
#RR = 2

#$_=pop;printf"%s,%s",y/[R]//-y/[L]//,y/[U]//-y/[D]//;

#$_=pop;print Dumper(y/[R]//-y/[L]//,y/[U]//-y/[D]//);
#$VAR1 = 2;
#$VAR2 = 0;

$_=pop;print y/R//-y/L//,',',y/U//-y/D//;

print "\n\n$_\n";

exit(0);
__END__


#$_=pop;print 3*y/[126]//+4*y/[459]//+5*y/[378]//;


Here is the though process I went through for this problem.  I may have actually had 15 iterations, I keep
them really small to keep track of changes, but hopefully you get the idea.
```1. Solve the problem length = 96
$x=$y=0;
for (pop=~/./g){
    if(/R/){
        $x++;
    }
    if (/L/){
        $x--;
    }
    if(/U/){
        $y++;
    }
    if(/D/){
        $y--;
    }
}
print$x.",".$y;


2. Crunch it. length = 59
# some gimme's.  Don't need to intialize x and y.  perl will make them zero when first used.  
# That helps!  Compress the print"
for(pop=~/./g)
{/R/?$x++:/L/?$x--:/U/?$y++:$y--}
print"$x,$y";

2a. others
# Note:  In perl, any time you have a for loop, you replace it with a map.  This automatically saves 
# one character.  I couldn't get it too work, not sure why, but decided I was doing something fundamentally
# wrong and tried a different route.

3. new strategy brainstorming length = 60
#*** Thought, this is the same problem counting as previous Textual Number Length Contest.  Use Perls 
#    transliterate operator.  Also works with a string, gets rid of for loop. Bonus!  lots of crunching 
#    potential too.  In perl, tr/U// returns the number of occurances when used this way.

$_=pop;
print tr/[R]//-tr/[L]//;
print ',';
print tr/[U]//-tr/[D]//;


4. simplify  length=49
# tr has shortcut of y which is in perldoc.  Read these for fun.
# It took several tried to figure out that y/R// returns a value that I can string together in a print statment.
$_=pop;
print y/[R]//-y/[L]//,','tr/[U]//-tr/[D]//;

5. simplify more length=40
#don't need character class. saves eight characters.
$_=pop;print y/R//-y/L//,','tr/U//-tr/D//
```


exit(0);
__END__




for($a=~/./gs){
    />/
    ? print chr($b)
}



Plotting The Trail
Using positional movements as an input, you must write a program to determine the ending coordinates that a player is at. Each positional movement will either increment or decrement the x or y value by one, depending on the input.



Possible Movements

U = Up
R = Right
D = Down
L = Left


Sample Input Argument 1 (the positional movements)

UURRDD


Sample Output

2,0


Guidelines

The starting coordinates are 0,0
The ending coordinates can be positive or negative x and y values