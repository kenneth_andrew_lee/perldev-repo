#!/usr/bin/perl
use Data::Dumper;
#perl ./asciiconversions.pl 6,H,p,h,D,t,b,t,R,n,H,Z,u,t,q,Q,V,x,I,M,Q,K,z,j,o,6,s,u,J,4,e,4
# 2745
# 

print "\n\n";
map{$y+=/\d/?$_:ord}split/,/,pop;print$y;
print "\n\n";
exit(0);
__END__

#winner 
#$c=pop;s/./$&.?/g,$c=~$_,$r+=@-[0]for@ARGV;print$r


=for comment
$&
The string matched by the last successful pattern match (not counting any matches hidden within a BLOCK or eval() enclosed by the current BLOCK).
See "Performance issues" above for the serious performance implications of using this variable (even once) in your code.
This variable is read-only and dynamically-scoped.
Mnemonic: like & in some editors
=cut


=for comment
ASCII Conversions
Given a string of random characters containing both upper and lowercase letters and numbers ([A-Za-z][0-9]) each separated by commas:

Convert all [A-Za-z] characters to their ASCII value leaving the existing numbers in place ([0-9]).
Sum each comma separated value contained within the string and then return the sum.


Sample Input Argument

6,H,p,h,D,t,b,t,R,n,H,Z,u,t,q,Q,V,x,I,M,Q,K,z,j,o,6,s,u,J,4,e,4


Sample Output

2745
=cut