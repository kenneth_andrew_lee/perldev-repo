#!/usr/bin/perl

# perl ispalindrome.pl 'Sir, I demand, I am a maid named Iris.'
# true
#
#

# perl ispalindrome.pl 'race a car.'
# false
#
# 

# perl ispalindrome.pl 'Never odd or even.'
# true
#
# 

# perl ispalindrome.pl 'race car!'
# true
# 
# 

# perl ispalindrome.pl 'Eve, can I see bees in a cave?'
# false
# 
# 

# perl ispalindrome.pl 'A man, a plan, a canal, Panama!'
# true
# 
# 



use POSIX qw/ceil/;
#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


#'Sir, I demand, I am a maid named Iris.'

$_=pop;
print $_, "\n";
s/[^a-zA-Z]//g;
$_ = lc($s);
print length $_, "\n";
print ceil((length $_)/2), "\n";
print $_, "\n";

exit();


__END__


=for winners 
# hashkitten
$_=lc`tr -d -@`;print$_=~reverse?true:false

JoKing
$_=uc`tr -d -@`;print$_~~reverse?true:false

# hydrazer
$_=lc`dd`;s/\W//g;print$_=~reverse?tru:fals,e

# Natte
$_=lc pop;s/\W//g;print$_ eq reverse?true:false
=cut


=for problem
Is Palindrome?
A phrase is a palindrome if, after converting all upper and lowercase letters and 
removing all non-alphanumeric characters, it reads the same forward and backward.

output true if it is a palindrome, or false if not.

Test Cases

Test Case 1
Argument 1
Sir, I demand, I am a maid named Iris.
Expected Output
true

Test Case 2
Argument 1
race a car.
Expected Output
false

Test Case 3
Argument 1
Never odd or even.
Expected Output
true

Test Case 4
Argument 1
race car!
Expected Output
true

Test Case 5
Argument 1
Eve, can I see bees in a cave?
Expected Output
false

Test Case 6
Argument 1
A man, a plan, a canal, Panama!
Expected Output
true
=cut


=for testcase
Test Case 1
'Sir, I demand, I am a maid named Iris.'
true

Test Case 2
'race a car.'
false

Test Case 3
'Never odd or even.'
true

Test Case 4
'race car!'
true

Test Case 5
Argument 1
'Eve, can I see bees in a cave?'
false

Test Case 6
'A man, a plan, a canal, Panama!'
true
=cut


=for misc

=cut