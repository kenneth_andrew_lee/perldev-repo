#!/usr/bin/perl
# perl rotationalexercise.pl 'dog' 'ogd'
# 1 
#
# 

# perl rotationalexercise.pl 'soupface' 'soupface'
# 0
#
# 

# perl rotationalexercise.pl 'dinosaur' 'saurdino'
# 4
#
# 

# perl rotationalexercise.pl 'javascript' 'tjavascrip'
# 9
#
# 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$c=0;
$a=pop;
@l=split//,pop;
until(join('',@l) eq $a){
    push(@l,shift@l);
    $c++
}
print$c;


exit();


__END__


=for winners 
hashkitten
(shift()x2)=~pop;print@-
=cut


=for problem
Rotational Exercise


You'll be supplied two inputs: a word and a partially rotated version of that 
word. You'll need to output how many characters it has been rotated.



Guidelines:

The word will always rotate right to left (first letter moved to end).
The word may not rotate at all.
Test Cases

Test Case 1
Argument 1
dog
Argument 2
ogd
Expected Output
1

Test Case 2
Argument 1
soupface
Argument 2
soupface
Expected Output
0

Test Case 3
Argument 1
dinosaur
Argument 2
saurdino
Expected Output
4

Test Case 4
Argument 1
javascript
Argument 2
tjavascrip
Expected Output
9
=cut


=for testcase
Test Case 1
Argument 1
dog
Argument 2
ogd
Expected Output
1

Test Case 2
Argument 1
soupface
Argument 2
soupface
Expected Output
0

Test Case 3
Argument 1
dinosaur
Argument 2
saurdino
Expected Output
4

Test Case 4
Argument 1
javascript
Argument 2
tjavascrip
Expected Output
9
=cut


=for misc
$c=0;
$a=pop;
@l=split//,pop;
until(join('',@l) eq $a) {
    push(@l,shift@l);
    $c++;
}
print$c;
    print "$a[$i+1]", "\n";
=cut