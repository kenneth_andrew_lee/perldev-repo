#!/usr/bin/perl

$c=0;$n=pop;while($n>1){$c++;$n=$n%2?3*$n+1:$n/2};
print "\$n:$n", " ", "\$c:$c", " ", "\$_:$_", "\n";







exit();

__END__

$c=0;$n=pop;while($n>1){$c++;$n=$n%2?3*$n+1:$n/2}print$c


Winner HashKitten
$-++while~($j=$j%2?3*$j-1:$j/2-pop);print$-

2nd place Lyndon
$.--while($n=$n%2?3*$n+1:$n/2|pop)-1;print-$.


=for comment
M=001100
T=010011
A=000000
w=110000

combine and divide into groups of 8 for 8bit values
1
2631
84268421
00110001 = 1+16+32 = 49 1
00110000 =   16+32 = 48 0
00110000 =   16+32 = 48 0

Result=100

sub m {
  ($s)=@_;
  print "\$s:$s", "\n";
  @r=map{/A-Z/?ord -64:/a-z/?ord -70:ord +5;sprintf("%b",$_)}$s=~/./;
  print "\@r:@r", "\n";
  return join"",@r; 
}


=cut

=for easymoney
Easy Money
The Engineer Man Team  is running low on funds and so have decided to try to disapprove the Collatz Conjecture and 
collect the million dollar prize. The team needs your help in verifying the some of the calculations.

The way that this works is that given a number `n` the next value in the sequence will be:

`3n + 1` if `n` is odd
`n / 2` if `n` is even


Given these rules the Collatz Conjecture states that for any starting `n` the sequence will eventually reach 1.

You need to compute how many iterations it takes for the sequence to reach one for the given input (if it never reaches 
one return -1).

For example:

Input: 34

Iteration 1:  17 (34 / 2)
Iteration 2:  52 (17 * 3 + 1)
Iteration 3:  26 (52 / 2)
Iteration 4:  13 (26 / 2)
Iteration 5:  40 (13 * 3 + 1)
Iteration 6:  20 (40 / 2)
Iteration 7:  10 (20 / 2)
Iteration 8:   5 (10 / 2)
Iteration 9:  16 (5 * 3 + 1)
Iteration 10:  8 (16 / 2)
Iteration 11:  4 (8 / 2)
Iteration 12:  2 (4 / 2)
Iteration 13:  1 (2 / 2)

Output: 13

Test Cases
Test Case 1
Argument 1: 989345275647
Expected Output: 1348

Test Case 2
Argument 1: 34
Expected Output: 13

Test Case 3
Argument 1: 27
Expected Output: 111

Test Case 4
Argument 1: 3
Expected Output: 7

Test Case 5
Argument 1: 2
Expected Output: 1

Test Case 6
Argument 1: 1
Expected Output: 0
=cut

__END__
