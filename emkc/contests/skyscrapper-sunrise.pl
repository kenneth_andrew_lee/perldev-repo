#!/usr/bin/perl
# perl skyscrapper-sunrise.pl '1,2,2,1'
# 2
#
# perl skyscrapper-sunrise.pl '9,4,7,4,7'
# 1
#
# perl skyscrapper-sunrise.pl '16,1,1,15,2,11,11,2'
# 1
#
# perl skyscrapper-sunrise.pl '10,8,23,5,23,21,23,19,15,13,25,8,3'
# 3
#
# perl skyscrapper-sunrise.pl '16,32,14,33,27,2,12,32,24,24,32,14,30,22,19,19,2,34,26,5'
# 4
#
# perl skyscrapper-sunrise.pl '36,5,22,23,24,16,35,7,31,18,3,22,43,5,14,14,39,11,10,40,20,37,39,35,24,41,5,26,3'
# 2
#
# perl skyscrapper-sunrise.pl '13,12,11,57,29,53,60,28,9,35,18,59,16,30,61,44,1,42,25,3,22,25,30,17,1,18,14,2,23,21,47,18,17,54,16,25,40,40,44,28'
# 4
#
# perl skyscrapper-sunrise.pl '13,49,60,67,1,14,36,8,41,49,23,8,24,70,44,3,23,2,55,17,75,47,16,66,44,38,28,34,17,5,58,32,81,80,58,59,10,29,6,29,70,36,17,70,61,42,4,19,48,47,34,36,71'
# 7
#
# perl skyscrapper-sunrise.pl '10,1,44,74,55,49,15,44,67,39,95,76,11,18,82,73,48,14,91,61,32,44,19,59,32,26,38,66,44,67,88,33,35,81,88,61,8,99,99,47,42,70,53,27,13,66,58,27,7,92,69,36,37,25,22,24,5,13,37,17,44,43,74,38,22,91,13,34'
# 5
#
# perl skyscrapper-sunrise.pl '65,48,106,84,39,97,12,25,79,101,29,85,115,116,61,93,57,76,121,100,8,51,60,63,81,95,95,79,101,83,90,76,64,47,67,43,45,95,14,29,11,75,64,104,87,91,9,103,70,47,96,109,81,10,104,46,121,118,44,111,117,99,24,102,38,56,8,118,68,95,22,100,71,79,57,22,29,78,4,72,79,95,52,119,28'
# 5 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

map{$c++and$v=$_ and print$_," ",$v, " ", $c,"\n" if$_>$v}split/,/,pop;
print "\n$c";

exit();

__END__



=for winners
hydrazer
$_>$!&&($!=$_,++$\)for pop=~/\d+/g;print

Natte
$_>$m&&$h{$m=$_}++for eval`dd`;print%h|0

Shanethegamer
for(split",",$ARGV[0]){if($_>$m){$m=$_;$c++}}print$c
=cut



=for comment
Dependency Dilemma
The EM team have been working on material processing software. They need help counting the total amount of raw materials required for each, and if it is even possible to make the final output.

You will be given 2 inputs - the first, the product which should be made and the second, the different inputs and outputs for each process that can be run.

You will need to return the quantity of the most abundant raw material required, or 0 if it is impossible to make the product.



Each ingredient is denoted by a singular letter, A-Z in capital form. Your first input will contain one of these along with a number, indicating the number of required materials.

Each process consists of a list of ingredients and products each delimited by a "+" symbol, with "=>" separating the ingredients on the left, and the products on the right. Your inputs will contain a list of these, comma separated.





Example 1:

Input 1: 3A
Input 2: 2B+1C=>1A,3D+1B=>3C

﻿# Input 1 is indicating we are making 3A.
processes:
 - 2B+1C=>1A # 1A is made with 2B and 1C
 - 3D+1B=>3C # 3C is made with 3D and 1B

# Break up 3A into its ingredients
current ingredientes: 
  - 6B
  - 3C

# we cannot break down B any futher, but we can break down 3C.
current ingredients:
  - 7B
  - 3D

# B requires 7, whilst D requires 3. B is the most abundent resource.
Output: 7﻿﻿﻿﻿


Example 2:

Input 1: 3A
Input 2: 2B+1C=>1A,3D+1A=>3C

﻿# Input 1 is indicating we are making 3A.
processes:
 - 2B+1C=>1A # 1A is made with 2B and 1C
 - 3D+1A=>3C # 3C is made with 3D and 1A - notice 1A and not 1B like previous example
﻿
# Break up 3A into its ingredients
current ingredientes: 
  - 6B
  - 3C

# we cannot break down B any futher, but we can break down 3C.
current ingredients:
  - 6B
  - 3D
  - 1A

# Making 3A requires 1A, thus there is an circular dependency on A and we cannot produce this, so we output 0
Output: 0﻿
Test Cases
Test Case 1
Argument 1: 3A
Argument 2: 2B+1C=>1A,3D+1B=>3C
Expected Output: 7
Test Case 2
Argument 1: 3A
Argument 2: 2B+1C=>1A,3D+1A=>3C
Expected Output: 0
Test Case 3
Argument 1: 8C
Argument 2: 3B+2A=>1C,3B=>2A
Expected Output: 48
=cut

