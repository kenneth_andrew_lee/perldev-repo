#!/usr/bin/perl
#use v5.26.1;
#use Data::Dumper;

#perl ./complicatedcurrents.pl '>v<>v^>>v<X<<<<^<v^<^>>^^ ^'
# 9 
#perl ./complicatedcurrents.pl '>v<>v^>>v<><<X<^<v^<^>>^^ v'
# 1
#perl ./complicatedcurrents.pl '>>><<v^<>v>>>>vX^<<<>^<<< ^'
# 0
#

printf"%d is %b",(map{$1+$2}pop=~/(\d+),(\d+)/);

exit();

__END__


=for comment
Binary Math
Given two comma seperated numbers return the sum value of these numbers in both decimal and binary.

Test Cases
Test Case 1
Argument 1: 0,0
Expected Output: 0 is 0
Test Case 2
Argument 1: 1,0
Expected Output: 1 is 1
Test Case 3
Argument 1: 1,1
Expected Output: 2 is 10
Test Case 4
Argument 1: 10,110
Expected Output: 120 is 1111000
Test Case 5
Argument 1: 93846,65789
Expected Output: 159635 is 100110111110010011
Test Case 6
Argument 1: 10,11
Expected Output: 21 is 10101
Test Case 7
Argument 1: 99,99
Expected Output: 198 is 11000110
=cut


=for code
@a=split/,/,pop;
print "\@a:@a", "\n";
#if(!defined$l){$l='^'}
map{
$m=1;
/(.)(.)/;
if($l=$1){$m++}
if($1 eq"<"){
  $l+=$2
}else{
  $r+=$2
}

print "\$l:$l","|","\$r:$r","|","\$1:$1","|","\$2:$2","|","\$_:$_","\n";
}@a;

print "after map:","\$l:$l","|","\$r:$r","\n";
=cut