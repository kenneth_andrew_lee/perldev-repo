#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;

#perl ./birdwatching.pl >5,<9
# 9
#perl ./birdwatching.pl >2,>3,<9
# 9
#perl ./birdwatching.pl >9,>9,<9
# 18
#perl ./birdwatching.pl >3,<4,>1,>7
# 8
#

@a=split/,/,pop;
print "\@a:@a", "\n";
#if(!defined$l){$l='^'}
map{
$m=1;
/(.)(.)/;
if($l=$1){$m++}
if($1 eq"<"){
  $l+=$2
}else{
  $r+=$2
}

print "\$l:$l","|","\$r:$r","|","\$1:$1","|","\$2:$2","|","\$_:$_","\n";
}@a;

print "after map:","\$l:$l","|","\$r:$r","\n";
exit();

__END__


=for comment
Bird Watching
The Engineer Man team have recently picked up a new hobby: bird watching.

They watch birds fly between Bones' and Hex's houses, counting how many, and 
which direction they flew.

They want to be certain on how many birds flew, so if 2 birds flew from Hex's 
to Bones' there would be a total of 2 birds, but if 3 birds then flew from Bones' 
to Hex's, there would be 3 total birds.

The team have asked you to write a program to take in their tallies, and 
give a total number of birds.



Input
Input is provided in a comma separated list, each of tokens starting with 
either > or <, indicating the direction of travel, then a number from 1-9 
indicating how many birds were seen traveling that direction.

Example
>5,<9
>5    5 birds traveled from Bones -> Hex
<9    9 birds traveled from Bones <- Hex
The maximium number of birds they are certain on is 9 birds

Test Cases
Test Case 1
Argument 1: >5,<9
Expected Output: 9

Test Case 2
Argument 1: >2,>3,<9
Expected Output: 9

Test Case 3
Argument 1: >9,>9,<9
Expected Output: 18

Test Case 4
Argument 1: >3,<4,>1,>7
Expected Output: 8
=cut
