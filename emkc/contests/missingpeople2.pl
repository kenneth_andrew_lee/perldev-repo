#!/usr/bin/perl
use Data::Dumper;
#perl ./missingpeople2.pl joe 114 w1i142ziwjoae
# 10
# $_=pop;$b=pop;$b=~s/(.)/$1.?/g;print@-if/$b/


sub p{($a)=@_;$_=~($a=~s/./$&.?/gr);@-[0]}$_=pop;print p(pop)+p(pop);



#pop=~'.?'.(pop=~/./g);
#pop=~join'.?',pop=~/./g;
#print@-[0],"\n";
#w1i142ziwjoae=~.?114=~/./g);

#sub p{($a)=@_;$_=~($a=~s/./$&.?/gr);return@-[0];}$_=pop;print p(pop)+p(pop);




=for comment
print "\$a:$a", " ", "\$b:$b", " ", "\$_:$_", "\n";
$_=~($b=~s/./$&.?/gr);
print "\$a:$a", " ", "\$b:$b", " ", "\$_:$_", "\n";
print@-[0];
$_=~($a=~s/./$&.?/gr);
print "\$a:$a", " ", "\$b:$b", " ", "\$_:$_", "\n";
print@-[0];
=cut

#$_=pop;
#$_=~(pop=~s/./$&.?/gr);
#print @-[0],"\n";

#$_=~(pop=~s/./$&.?/gr);
#print @-[0],"\n";


#$_=~(pop=~s/./$&.?/gr);$x=@-[0];
#$_=~(pop=~s/./$&.?/gr);$y=@-[0];
#print $x,"\n";
#print $y,"\n";
#@-," ",$v,"\n";

#$a=pop;
#$b=pop;
#print $b, " ", $a, " ", $_, "\n";
#$_=~($b=~s/./$&.?/gr);$v=@-;$_=~($a=~s/./$&.?/gr);print@-, "\n";


#$_=~($b=~s/./$&.?/gr);print@-, "\n";
#$_=~($a=~s/./$&.?/gr);print@-, "\n";


#$_=~(pop=~s/./$&.?/gr);print@-, "\n";
#$_=~(pop=~s/./$&.?/gr);print@-, "\n";
#print index($_,$a),"\n";

#$_=~($b=~s/./$&.?/gr);print@-;


#print index($_,$a),"\n";
#print index($_,$b),"\n";



# $_:akbbussbwobbzywdawda|$b:b??o??b??b??y|@-:13|@+:14|$-[0]:13|$+[0]:14


exit(0);
__END__

#winner 
$c=pop;s/./$&.?/g,$c=~$_,$r+=@-[0]for@ARGV;print$r


$&
The string matched by the last successful pattern match (not counting any matches hidden within a BLOCK or eval() enclosed by the current BLOCK).
See "Performance issues" above for the serious performance implications of using this variable (even once) in your code.
This variable is read-only and dynamically-scoped.
Mnemonic: like & in some editors



=for comment
Missing People 2
You will be provided a persons name, an age, and a long string of letters and numbers. This time you will also be provided with a number representing the persons age. You must find the provided name in the long string and the provided age in the string. Then return the sum of there starting positions. The letters of the name can be spaced out with a random character in between. The numbers of the age can be spaced out with a single character. 



Possible combinations for a 3 letter name(X represents a random character):

joe
jXoe
joXe
jXoXe﻿


Sample Input Argument 1 (the name)

joe
Sample Input Argument 2 (the age)

114
Sample Input Argument 3 (the string of data)

w1i142ziwjoae


Sample Output

10


Explanation: The starting position for the name joe is at position 9 in the string, and the age is at position 1. 9 + 1 = 10.



Guidelines

There is a maximum of 1 random character between each letter of the name. (0 or 1)
There is a maximum of 1 random character (a-zA-Z) between each number making up the age
The name only consist of lowercase and uppercase english letters (a-zA-Z)
The age will be an integer between 0 and 200
Uppercase/Lowercase letters have to match.
The name will be between 1 and 10 characters long
The string will be between 1 and 100 characters long
Counting starts at 0
=cut