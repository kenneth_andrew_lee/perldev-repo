#!/usr/bin/perl

# perl easymoney2.pl '9835' 'A33A' 'K5K' 'JA' 'A243'
# '4'
#
#

# perl easymoney2.pl 'J5Q' '6KA5' 'Q4' 'Q55J' 'QA'
# '5'
#
# 

# perl easymoney2.pl 'J32' 'AK' '869' '5Q6' 'A3'
# '2,4'
#
# 

# perl easymoney2.pl '26' 'Q38' '2K8' '55' '73'
# '2'
# 
# 

# perl easymoney2.pl '2287' '12888' '26543' '2Q' '5JK'
# '3'
# 
# 

# perl easymoney2.pl 'Q47' 'AJ' '1J28' '97J' 'KA'
# '1,2,3,5'
# 
# 

# perl easymoney2.pl 'AQ' '27' '57' '2JJ' '489'
# '1,5'
#
# 

# perl easymoney2.pl '4AA' '79' '15' '45J' 'A82'
# '5'
# 
# 
#

# perl easymoney2.pl 'JK2' 'QJ3' '9627' 'A589' '2KQ'
# '1,2,3,4,5'
# 
# 
#


#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


for(0..4){
    $a=map{}split//,pop;
}

exit();


__END__


=for winners 

=cut


=for problem
Easy Money Pt. 2
The EngineerMan team was unable to disprove the Collatz theory and needs 
to find another way of raising funds. They have turned to running 
BlackJack games. In order to do so they must first develop software 
able to grade BlackJack hands.

The rules are simple, each card has one or more values and the people 
with the highest hand value not exceeding  21 win the round. The number 
cards (1 thru 9) are worth their face value (e.g., one is worth one, 
five is worth five), the Jack/Queen/King are worth 10, and the Ace is 
worth 1 or 11 (the player chooses).

For the following rounds, please output the winner (1 indexed).
For example:
Input: 1QJ|996|J6A6|K22|9J9
Output: 1

Evaluate the value of each hand, in this case: 21,24,23,14,28
Figure out the winning value, in this case its 21.
Find all of the players that have tied for the winning value, in this 
case only one player.
Output the index of the winning players (1 indexed), in this case: 1

If more than one player wins then list all winning players separated 
by commas. If all players bust then they tie and all "win".

Test Cases
Test Case 1
Argument 1
9835
Argument 2
A33A
Argument 3
K5K
Argument 4
JA
Argument 5
A243
Expected Output
4

Test Case 2
Argument 1
J5Q
Argument 2
6KA5
Argument 3
Q4
Argument 4
Q55J
Argument 5
QA
Expected Output
5

Test Case 3
Argument 1
J32
Argument 2
AK
Argument 3
869
Argument 4
5Q6
Argument 5
A3
Expected Output
2,4

Test Case 4
Argument 1
26
Argument 2
Q38
Argument 3
2K8
Argument 4
55
Argument 5
73
Expected Output
2

Test Case 5
Argument 1
2287
Argument 2
12888
Argument 3
26543
Argument 4
2Q
Argument 5
5JK
Expected Output
3

Test Case 6
Argument 1
Q47
Argument 2
AJ
Argument 3
1J28
Argument 4
97J
Argument 5
KA
Expected Output
1,2,3,5
Test Case 7
Argument 1
AQ
Argument 2
27
Argument 3
57
Argument 4
2JJ
Argument 5
489
Expected Output
1,5

Test Case 8
Argument 1
4AA
Argument 2
79
Argument 3
15
Argument 4
45J
Argument 5
A82
Expected Output
5

Test Case 9
Argument 1
JK2
Argument 2
QJ3
Argument 3
9627
Argument 4
A589
Argument 5
2KQ
Expected Output
1,2,3,4,5
=cut


=for testcase
Test Case 1
Argument 1
9835
Argument 2
A33A
Argument 3
K5K
Argument 4
JA
Argument 5
A243
Expected Output
4

Test Case 2
Argument 1
J5Q
Argument 2
6KA5
Argument 3
Q4
Argument 4
Q55J
Argument 5
QA
Expected Output
5

Test Case 3
Argument 1
J32
Argument 2
AK
Argument 3
869
Argument 4
5Q6
Argument 5
A3
Expected Output
2,4

Test Case 4
Argument 1
26
Argument 2
Q38
Argument 3
2K8
Argument 4
55
Argument 5
73
Expected Output
2

Test Case 5
Argument 1
2287
Argument 2
12888
Argument 3
26543
Argument 4
2Q
Argument 5
5JK
Expected Output
3

Test Case 6
Argument 1
Q47
Argument 2
AJ
Argument 3
1J28
Argument 4
97J
Argument 5
KA
Expected Output
1,2,3,5
Test Case 7
Argument 1
AQ
Argument 2
27
Argument 3
57
Argument 4
2JJ
Argument 5
489
Expected Output
1,5

Test Case 8
Argument 1
4AA
Argument 2
79
Argument 3
15
Argument 4
45J
Argument 5
A82
Expected Output
5

Test Case 9
Argument 1
JK2
Argument 2
QJ3
Argument 3
9627
Argument 4
A589
Argument 5
2KQ
Expected Output
1,2,3,4,5
=cut


=for misc

=cut