#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl jumbledstringvalues.pl a1,B2
# 166
# perl jumbledstringvalues.pl a1,b2,C3
# 13860
# perl jumbledstringvalues.pl A1,b2,c3,d4
# 162




#for(shift=~/\w+/g){$s=0;map{$s+=/[A-Za-z]/?ord:$_}/./g;@a[$c]=$s;$c++}$c==2?$t=$a[0]+$a[1]:$c==3?$t=($a[0]+$a[1])*$a[2]:$c==4?$t=int(($a[0]+$a[1])*$a[2]/$a[3]):c==5?$t=int(($a[0]+$a[1]+$a[2])*$a[3]/$a[4]):0;print$t;

#for(shift=~/\w+/g){$s=0;map{$s+=/[A-Za-z]/?ord:$_}/./g;@a[$c]=$s;$c++}$d=$a[0]+$a[1];print $c==2?$d:$c==3?$d*$a[2]:$c==4?int($d*$a[2]/$a[3]):c==5?int(($d+$a[2])*$a[3]/$a[4]):0;
#for(shift=~/\w+/g){$s=0;map{$s+=/[A-Za-z]/?ord:$_}/./g;@a[$c]=$s;$c++}$d=$a[0]+$a[1];$e=$a[2];print $c==2?$d:$c==3?$d*$e:$c==4?int($d*$e/$a[3]):c==5?int(($d+$e)*$a[3]/$a[4]):0;

#map{$s+=/[A-Za-z]/?ord:$_;if(/\d/){push@a,$s;$s=0}}shift=~/\w/g;$c=()=@a;$d=$a[0]+$a[1];print $c==2?$d:$c==3?$d*$a[2]:$c==4?int($d*$a[2]/$a[3]):c==5?int(($d+$a[2])*$a[3]/$a[4]):0;

for(shift=~/\w+/g){$s=0;map{$s+=/[A-Za-z]/?ord:$_}/./g;@a[$c]=$s;$c++}
$d=$a[0]+$a[1];
print $c==2?$d:$c==3?$d*$a[2]:$c==4?int($d*$a[2]/$a[3]):c==5?int(($d+$a[2])*$a[3]/$a[4]):0;


exit();
__END__


#Winner
s/\B/+/g,eval'$%'.qw(+= *= /=)[$-=$i++-@_/4].s/\pL/ord$&/ger for@_=<{@ARGV}>;print$%

#2nd place
$_=pop=~s/\B/+/gr.'#/*++';s/\pL/ord$&/ge;y/,//>3&&s/,/+/;s/^|,/'$-'.chop."=$'"/eeg;print$-

#3rd place
@r=map{$y=0;map{$y+=/\d/?$_:ord}split//;$y}split',',pop;
$a=~~@r>4;
print int((@r[0]+@r[1]+$a*@r[2])*(@r[2+$r]||1)/(@r[3+$r]||1))

#4th place - Me!
for(shift=~/\w+/g){$s=0;map{$s+=/[A-Za-z]/?ord:$_}/./g;@a[$c]=$s;$c++}
$d=$a[0]+$a[1];
print $c==2?$d:$c==3?$d*$a[2]:$c==4?int($d*$a[2]/$a[3]):c==5?int(($d+$a[2])*$a[3]/$a[4]):0;


=for comment
Jumbled String Values
Given a string with a minimum of 2 and maximum of 5 jumbled comma delimited strings containing letters and numbers [A-Za-z0-9]. 
Convert all [A-Za-z] characters to their ASCII value leaving the existing numerical numbers in place [0-9]. 

Step 2:
Sum each group of strings.

Step 3:
If there are two groups strings:
  Return the sum of the two strings.

If there are three groups of strings:
  Return the sum of the first two strings multiplied by the third string.

If there are four groups of strings:
  Return the sum of the first two strings muliplied by the third string, floor divided by the fourth string.

if there are five groups of strings:
  Return the sum of the first three strings multiplied by the fourth string, floor divided by the fifth string.

Examples with each case.

input : a1,B2
output: 166

input : a1,b2,C3
output: 13860

input : A1,b2,c3,d4
output: 162


input : a1,b2,c3,d4,E5
output: 421
Test Cases
Test Case 1
Argument 1: a1,B2
Expected Output: 166
Test Case 2
Argument 1: a1,b2,C3
Expected Output: 13860
Test Case 3
Argument 1: abc123,def456
Expected Output: 618
Test Case 4
Argument 1: ABC123,DEF456
Expected Output: 426
Test Case 5
Argument 1: a,b
Expected Output: 195
Test Case 6
Argument 1: lkj2H,8hS2w
Expected Output: 711
=cut