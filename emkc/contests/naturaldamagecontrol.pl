#!/usr/bin/perl
# perl naturalfirecontol.pl 'WHXXFLXXX'
# 8
# 
# 
# perl naturalfirecontol.pl 'XXXXHWLFX'
# 2
# 
# 
# perl naturalfirecontol.pl 'HXLXXWFXX'
# 89
# 
# 
# perl naturalfirecontol.pl 'XWXXHFXLX'
# 4
# 
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }





exit();

__END__

$x=pop;
for(1..$x){
  $s+=$_ if$x%$_==0;
}
print $s;



=for hints
Input and output
The field is a grid of 3x3 boxes.
The input is a string of 9 characters :

If the box is subject to a weather phenomenon, the letter used for the phenomena
If the box is not subject to a weather phenomenon, the letter used is `X`

The grid is described row per row (from 1 to 9).

╔═══════╗
║ 1 2 3 ║
║ 4 5 6 ║
║ 7 8 9 ║
╚═══════╝
You have to write to standard output the locations where the house can be built using the grid above in ascendant order.



Helpers
Here are some helping graphics showing you where you can and can't for each device :
(X is where you can't build the house)

╔═══════╗        ╔═══════╗
║ !     ║        ║ X   X ║
║       ║   =>   ║       ║
║       ║        ║ X     ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║     X ║
║       ║   =>   ║       ║
║     ! ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║       ║
║   +   ║   =>   ║   X   ║
║       ║        ║       ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║ X   X ║
║   *   ║   =>   ║   X   ║
║       ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║ *     ║   =>   ║ X     ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║   $   ║   =>   ║ X X X ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝


╔═══════╗        ╔═══════╗
║       ║        ║       ║
║       ║   =>   ║   X   ║
║   $   ║        ║ X X X ║
╚═══════╝        ╚═══════╝
=cut


=for example 
Input :

XWFLXXXHX


The corresponding grid is the following with the following devices :

╔═══════╗        ╔═══════╗
║   W F ║        ║   ! + ║
║ L     ║   =>   ║ $     ║
║   H   ║        ║   *   ║
╚═══════╝        ╚═══════╝
Now, we write a X where we can't build the house.

╔═══════╗        ╔═══════╗
║   ! + ║        ║ X X X ║
║ $     ║   =>   ║ X X X ║
║   *   ║        ║ X X   ║
╚═══════╝        ╚═══════╝
Then, we clearly see that the box 9 is the only place where we can build the house, so we output 9.
=cut

=for winners

=cut


=for comment
$_ The default input and pattern-searching space.
$" When an array or an array slice is interpolated into a double-quoted string or a similar context such as /.../, its elements are separated by this value. Default is a space.
$` The string preceding whatever was matched by the last successful pattern match, not counting any matches hidden within a BLOCK or eval enclosed by the current BLOCK.
=cut



=for comment
Natural damage control
The EngineerMan team wants to build a house in order to create a secret HQ. Unfortunately, the building land is subject to weather damage.



You are going to help design the facilities used to protect the new HQ from natural damages.



The following weather phenomena may happen :



 - Windstorm (W)

 - Flood (F)

 - Heat wave (H)

 - Landslide (L)



Each phenomenon has a letter associated to it. For each one, you can install the following devices :

 

 - For Windstorm, you can use a windbreak (!)

 - For Flood, you can use a pipe system (+)

 - For Heat Wave, you can plant trees (*)

 - For Landslide, you can install a drainage device ($)



However, each device restricts you from building the house where it is planted and sometimes around the device. The following restrictions applies :



 - A windbreak prevents building a house 2 boxes around it (non diagonals) and where the windbreak is.

 - A pipe system prevent building a house where it is.

 - Planting trees prevent building a house where they are and in diagonals boxes.

 - A drainage device prevents building a house around it (non diagonals) and on it.



Input and output


The field is a grid of 3x3 boxes.

The input is a string of 9 characters :



If the box is subject to a weather phenomenon, the letter used for the phenomena
If the box is not subject to a weather phenomenon, the letter used is `X`


The grid is described row per row (from 1 to 9).



╔═══════╗
║ 1 2 3 ║
║ 4 5 6 ║
║ 7 8 9 ║
╚═══════╝
You have to write to standard output the locations where the house can be built using the grid above in ascendant order.



Helpers


Here are some helping graphics showing you where you can and can't for each device :

(X is where you can't build the house)





╔═══════╗        ╔═══════╗
║ !     ║        ║ X   X ║
║       ║   =>   ║       ║
║       ║        ║ X     ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║     X ║
║       ║   =>   ║       ║
║     ! ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║       ║
║   +   ║   =>   ║   X   ║
║       ║        ║       ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║ X   X ║
║   *   ║   =>   ║   X   ║
║       ║        ║ X   X ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║ *     ║   =>   ║ X     ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝

╔═══════╗        ╔═══════╗
║       ║        ║   X   ║
║   $   ║   =>   ║ X X X ║
║       ║        ║   X   ║
╚═══════╝        ╚═══════╝


╔═══════╗        ╔═══════╗
║       ║        ║       ║
║       ║   =>   ║   X   ║
║   $   ║        ║ X X X ║
╚═══════╝        ╚═══════╝
Example case


Input :

XWFLXXXHX


The corresponding grid is the following with the following devices :

╔═══════╗        ╔═══════╗
║   W F ║        ║   ! + ║
║ L     ║   =>   ║ $     ║
║   H   ║        ║   *   ║
╚═══════╝        ╚═══════╝
Now, we write a X where we can't build the house.

╔═══════╗        ╔═══════╗
║   ! + ║        ║ X X X ║
║ $     ║   =>   ║ X X X ║
║   *   ║        ║ X X   ║
╚═══════╝        ╚═══════╝
Then, we clearly see that the box 9 is the only place where we can build the house, so we output 9.

Test Cases
Test Case 1
Argument 1
WHXXFLXXX
Expected Output
8
Test Case 2
Argument 1
XXXXHWLFX
Expected Output
2
Test Case 3
Argument 1
HXLXXWFXX
Expected Output
89
Test Case 4
Argument 1
XWXXHFXLX
Expected Output
4
=cut

