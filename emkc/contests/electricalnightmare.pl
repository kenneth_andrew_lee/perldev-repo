#!/usr/bin/perl
#use v5.26.1;
#use Data::Dumper;
# perl electricalnightmare.pl 123 F
# 3
# perl electricalnightmare.pl 130 8
# 4
# perl electricalnightmare.pl 100 A
# 5

#$_=sprintf("%b",hex(pop)+pop);print y/1//;
print sprintf('%b',hex(pop)+pop)=~y/1//;

#123 + 15 = 138 = 10001010 = three 1's
#perl -ple '$\="=".hex.$/'

exit();
__END__


=for comment
Electrical Nightmare
A customer is in need of some more power outlets and left a note on site with instructions on how many new outlets 
required. The electrician quickly counts the amount of outlets he can see on site for a power load check (Input 1). 
Then notices the note left for the amount of new outlets required was left in hexadecimal (Input 2).

We are to calculate the amount of switches to be left on by returning the sum of value 1 and value 2 and converting 
it to a binary string, every 1 in the binary string is a switch to be left on.

Test Cases
Test Case 1
Argument 1: 123
Argument 2: F
Expected Output: 3

Test Case 2
Argument 1: 130
Argument 2: B
Expected Output: 4

Test Case 3
Argument 1: 100
Argument 2: A
Expected Output: 5

Test Case 4
Argument 1: 123456789
Argument 2: 3AF6
Expected Output: 11

Test Case 5
Argument 1: 987654321
Argument 2: 3E6D
Expected Output: 19

Test Case 6
Argument 1: 8793845678
Argument 2: 1BE3
Expected Output: 14

Test Case 7
Argument 1: 8793945845678
Argument 2: 1BE3A7
Expected Output: 21
=cut