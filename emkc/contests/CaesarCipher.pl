#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl CaesarCipher.pl CATDOG 3
# FDWGRJ
# perl CaesarCipher.pl ENGINEERMAN 7
# LUNPULLYTHU
# perl CaesarCipher.pl URCODEISBAD 22
# QNYKZAEOXWZ


@a=map{$_}(A..Z);
print "@a\n";

#$b=pop;print map{$c=ord($_)+$b;$c>90?chr(ord($_)-(26-$b)):chr($c)}pop=~/./g;


exit();
__END__

$i=join'',('A'..'Z');
$o=substr((join'',('A'..'Z', 'A'..'Z')), pop, 26);
print eval"pop=~y/$i$/$o/r";

# winner
y//-ZA/cfor($_=shift)x pop;print

=for comment
Caesar Cipher
Problem
The Caesar Cipher is a simple encryption algorithm where the alphabet is shifted or scrambled in some way. For this problem, the alphabet will just be shifted to the right by a certain number of spaces.

Example
Sample Input Argument 1 (the plaintext to encrypt)
CATDOG

Sample Input Argument 2 (the shift amount)
3

In this case, the substitution chart would look like this:

ABCDEFGHIJKLMNOPQRSTUVWXYZ
DEFGHIJKLMNOPQRSTUVWXYZ﻿ABC


Expected Result (the encrypted text)
FDWGRJ


Guidelines

The plaintext will be only capital letters in the English alphabet
The shift will be an integer between 0 and 26
Test Cases
Test Case 1
Argument 1: CATDOG
Argument 2: 3
Expected Output: FDWGRJ

Test Case 2
Argument 1: ENGINEERMAN
Argument 2: 7
Expected Output: LUNPULLYTHU

Test Case 3
Argument 1: URCODEISBAD
Argument 2: 22
Expected Output: QNYKZAEOXWZ
=cut
