#!/usr/bin/perl
#use Data::Dumper;

#perl ./letterflip.pl 'HeLLo'
#hEllO


#map{print$_ eq uc?lc:uc}pop=~/./g;

#$s = pop;
#$s =~ tr[A-Za-z][a-zA-Z];
#print $s;

$_=pop;y/A-Za-z/a-zA-Z/;print;

#map{print$_ eq uc?lc:uc}pop=~/./g;

#map{print \U?\L:\U}pop=~/./g;

exit();
__END__


#map{print uc==$_?lc:uc}pop=~/./g;
#map {/\p{Lu}/ ? lc : uc} $str;
map {/\p{Lu}/ ? lc : uc}$_=pop;

$_=pop;print y/R//-y/L//,',',y/U//-y/D//;
$_==uc$_?lc:uc
if($_==uc($_)){print lc} else {print uc}

map {printf"%c",(/!/?64:96)+length} $ARGV[0]=~/(>*.)/g


perl -le'$_ = "MiXeD CaSe ExAmPlE"; print; s/([[:alpha:]])/ $1 eq "\U$1" ? "\L$1" : "\U$1" /eg; print'


map{print \U?\L:\U}pop=~/./g



Letter Flip
You will be given a string full of random letters. Some of the letters will be capitalized while others will be lowercase. You need to return the string with the letters that are capitalized flipped to lowercase and the ones that where lowercase should become capitalized.



Sample Input Argument 1

HeLLo


Sample Output

hEllO


Guidelines

The Input will be a string containing  only letters from the English Alphabet
The output should be the same string with the capital and lowercase letters flipped


Your Submission
It's recommended that you compose your solution separately and just paste here once you're ready. After clicking "Submit Solution" your solution will be tested with secret inputs. If successful, your solution will be saved. Be sure to choose the correct language.
