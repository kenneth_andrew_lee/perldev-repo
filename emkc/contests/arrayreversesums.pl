#!/usr/bin/perl
# perl arrayreversesums.pl '123,456,789' '0,2'
# 1308
#
# 
# perl arrayreversesums.pl '566,655,334,123' '1,2'
# 89
#
# 
# perl arrayreversesums.pl '1234,5678,8765,4321' '2,3'
# 6912
# 
#
# perl arrayreversesums.pl '12,34,56,78' '0,1,2,3'
# 216
# 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }
@b=split/,/,pop;@a=split/,/,pop;
for(@a){@c=split//;push@d,join"",reverse@c}for(@b){$t+=$d[$_]}print$t;

exit();


__END__

    print "$a[$i+1]", "\n";


=for winners 
# 1
$\+=reverse for(eval pop)[eval pop];print
=cut


=for problem
Array Reverse Sums
Given 2 x comma separated array of elements, the first input being the array of numbers, the second being the positions to reverse in the array and sum.

Provide the sum of the reversed numbers only.



Example:

Input:
  - 123,456,789
  - 0,2
Array: [123,456,789]
Selected Elements: [Array[0],Array[2]]
Selected Elements: [123,789]
Reversed Elements: [321,987]
Sum: 321 + 987
Output: 1308

Test Cases

Test Case 1
Argument 1
123,456,789
Argument 2
0,2
Expected Output
1308

Test Case 2
Argument 1
566,655,334,123
Argument 2
1,2
Expected Output
989

Test Case 3
Argument 1
1234,5678,8765,4321
Argument 2
2,3
Expected Output
6912

Test Case 4
Argument 1
12,34,56,78
Argument 2
0,1,2,3
Expected Output
216
=cut


=for testcase
Test Case 1
'123,456,789' '0,2'
1308

Test Case 2
'566,655,334,123' '1,2'
989

Test Case 3
'1234,5678,8765,4321' '2,3'
6912

Test Case 4
'12,34,56,78' '0,1,2,3'
216
=cut

