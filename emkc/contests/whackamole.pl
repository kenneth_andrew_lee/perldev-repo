#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl whackamole.pl __2_1_____
# 6
# perl whackamole.pl __________
# 0
# perl whackamole.pl 12345_____
# 4
# perl whackamole.pl __51__4_32
# 16

$s=0;@a=split//,pop;for$b(1..10){@b=grep{$a[$_]eq$b}(0..@a-1);$s+=abs($b[0]-$d)if$b[0];$d=$b[0]}print$s;


exit();
__END__

#winner
$a=pop;$\+=$a=~($_+1)*abs-$-[0]+$a=~$_*$-[0]for 0..8;print


print "@a\n";
while(($b,$c)=each@a) {
  $s+=abs($b-$p)if$c ne "_";
  print $b, "|", $p, "|", $c, "|", $s, "\n";
  $p=$b if$c ne "_";
} 
 
print$s,"\n";

@a=split//,pop;
print "b|c\n";
while(($b,$c)=each@a){
  $h{$b}=$c;
  print $b, "|", $c, "\n";
}

for(1..10){
  $v=$h{$_};
  $s+=abs($v-$p) if$v ne "_";
  print $_, "|", $v, "|", $p, "|", $s, "\n";
  $p=$v if$v ne "_";
}
print $s, "\n";



@a=split//,pop;
print "@a\n";
while(($b,$c)=each@a) {
  print $b, "|", $c, "\n";

}
_ _ 2 _ 1 _ _ _ _ _
0|_
1|_
2|2
3|_
4|1
5|_
6|_
7|_
8|_
9|_


@a=split//,pop;
while(($b,$c)=each @a) {
  print @a, "|", $b, "|", $c, "\n";

}
__2_1_____|0|_
__2_1_____|1|_
__2_1_____|2|2
__2_1_____|3|_
__2_1_____|4|1
__2_1_____|5|_
__2_1_____|6|_
__2_1_____|7|_
__2_1_____|8|_
__2_1_____|9|_






$c=$p=$s=0;
for(split//,pop){
  $s+=abs($c-$p)if$_ ne "_";
  $p=$c if$_ ne "_";
  print "\$c:$c", "|", "\$_:$_", "|", "\$p:$p", "|","\$s:$s","\n";
  $c++;
}
print$s;






$s=$p=$c=0;
for(split//,pop){
  print "\$c:$c","\n";
  $s+=abs($c-$p)if$_ ne "_";
    print "\$c:$c", "|", "\$p:$p", "|","\$s:$s","\n";
  $p=$c;
  $c++
}
print$s;


$s=0;
@a=split//,pop;
print "@a\n";
for$b(1..10){
  @b=grep{$a[$_]eq$b}(0..@a-1);
  $s+=abs($b[0]-$d)if$b[0];
  $d=$b[0];
  print $b,":",$s,":",$b[0],"\n";
}
print$s;








#for$b(1..10){
#  @b=grep{$a[$_]eq$b}(0..@a-1);
#  $s+=abs($b[0]-$d)if$b[0];
#  $d=$b[0];
#  print $b,":",$s,":",$b[0],"\n";
#}
#print$s;
$h{$c}=$_ if $_ ne "_";
while(($k,$v)=each%h){
  print $k,":",$v,"\n";
}

print $s, "\n";






=for comment
Whack-a-mole
Problem
You're playing whack-a-mole, but with code. You'll be given an input that contains ten potential mole locations and each location will potentially have a number annotating the order in which you'll whack the moles. Starting at position 1, you'll need to output the number of moves it takes to whack all of the moles.

Example
Sample Input Argument 1 (mole locations)
__2_1_____

Expected Result
6

In this example you'd move right 4 spaces to hit the first mole and left 2 spaces to hit the second, with the total being 6.

Guidelines

Your input will be exactly 10 characters long
Each character will either be an underscore (_) or a number
An underscore denotes a location where no mole will appear
A number denotes when a mole will appear at that location
Test Cases
Test Case 1
Argument 1: __2_1_____
Expected Output: 6
Test Case 2
Argument 1: __________
Expected Output: 0
Test Case 3
Argument 1: 12345_____
Expected Output: 4
Test Case 4
Argument 1: __51__4_32
Expected Output: 16
=cut
