#!/usr/bin/perl
# perl simplesummation.pl 1,2,3,4,5,6,7,8,9,10
# 55
# 
# 
# perl simplesummation.pl '854,658,898,454,654,976'
# 4494
# 
# 
# perl simplesummation.pl '123456789,987654321'
# 1111111110
# 
#


#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }


$_=pop;y/,/+/;print eval;

exit();

#map{$c+=$_}pop=~/\d+/g;print$c;

#$_=pop;
#s/,/+/ge;
#print;


#y/,/+/;
#print

#$_=pop;y/,/+/;print eval;

__END__



=for winners
# Natte
print eval`tr , +`
=cut


=for problem
Simple Summation
The EngineerMan team have been doing some accounting and need to find the total of some accounts. 



Your input will be a list of comma-separated integers. You are expected to find the sum (add all the numbers) of the list and output this.



Test Cases
Test Case 1
Argument 1
1,2,3,4,5,6,7,8,9,10
Expected Output
55
Test Case 2
Argument 1
854,658,898,454,654,976
Expected Output
4494
Test Case 3
Argument 1
123456789,987654321
Expected Output
1111111110
=cut


=for testcase
Test Case 1
Argument 1
1,2,3,4,5,6,7,8,9,10
Expected Output
55
Test Case 2
Argument 1
854,658,898,454,654,976
Expected Output
4494
Test Case 3
Argument 1
123456789,987654321
Expected Output
1111111110
=cut

