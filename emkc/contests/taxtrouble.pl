#!/usr/bin/perl
#use v5.26.1;
#use Data::Dumper;
# perl taxtrouble.pl 0xFF,245
# 500
# perl taxtrouble.pl 0b11111111,0o365,0xFF,245
# 1000
# perl taxtrouble.pl 0xFFFF,0b1111111111111111
# 131070
#;$s+=/^0/?oct($_):$_
#@a=split/,/,pop;
map{s/o//;$s+=/^0/?oct($_):$_}split/,/,pop;print$s;

#print$s;
#print "\@a:@a", "\n";
exit();
__END__


print eval pop=~y/,o/+/rd

$_=pop;y/,o/+0/;print eval

print eval<STDIN>=~y/,o/+0/r


for(split/,/,pop){
s/o//;
/^0/?oct($_):$_;
print
}

#$_=sprintf("%b",hex(pop)+pop);print y/1//;
#print sprintf('%b',hex(pop)+pop)=~y/1//;

#123 + 15 = 138 = 10001010 = three 1's
#perl -ple '$\="=".hex.$/'



=for comment
Tax Trouble
EngineerMan Co's accounting department have been lazy this Easter, and forgot to file the tax returns.

Thankfully they just need some help calculating their total spending, and have given you a list of all their transactions they need summed up.

In true software engineer fashion, they have recorded the values in all sorts of different numbering systems, some people like hex, others octal, and binary.

The engineers' are only allowed to spend up to $65535.00 per transaction, with the companies spending limit being $4294967296.

Test Cases
Test Case 1
Argument 1: 0xFF,245
Expected Output: 500
Test Case 2
Argument 1: 0b11111111,0o365,0xFF,245
Expected Output: 1000
Test Case 3
Argument 1: 0xFFFF,0b1111111111111111
Expected Output: 131070
=cut

=for comment
split/,/,pop - breaks argument into an array
map takes each element in array and runs it through what is in {}.  In this case, check if starting character is a 0.  If it is, use oct function to convert.  This function works for oct, bin, and hex values.  If value doesn't start with 0, just use the value itself, as it is decimal.  Sum these values to variable $s
lastly, print out $s
=cut