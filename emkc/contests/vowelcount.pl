#!/usr/bin/perl
# perl vowelcount.pl 'The quick brOwn fox jumps over the lazy dog'
# 11
#
# 
#

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);
#2879
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

$_=pop;print y/aeiouAEIOU//;

#print ()=pop=~/[aeiou]/g;
#print pop=~/aeiouAEIOU/g;
exit();


__END__

    print "$a[$i+1]", "\n";


=for winners 
=cut


=for problem
Vowel Count
For each input string, count the number of vowels. This includes both upper and lower case vowels so be sure to count both.

Test Cases
Test Case 1
Argument 1
The quick brOwn fox jumps over the lazy dog
Expected Output
11

Test Case 2
Argument 1
How mAny vowels do I have?
Expected Output
8

Test Case 3
Argument 1
EngineerMan
Expected Output
5

Test Case 4
Argument 1
AeIoU
Expected Output
5
=cut


=for testcase
Test Case 1
Argument 1
The quick brOwn fox jumps over the lazy dog
Expected Output
11

Test Case 2
Argument 1
How mAny vowels do I have?
Expected Output
8

Test Case 3
Argument 1
EngineerMan
Expected Output
5

Test Case 4
Argument 1
AeIoU
Expected Output
5
=cut

