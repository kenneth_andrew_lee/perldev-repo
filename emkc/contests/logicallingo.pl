#!/usr/bin/perl
#use v5.26.1;
use Data::Dumper;
# perl logicallingo.pl hF0hFFsh00oh10a
# 31
# perl logicallingo.pl h00h01h03aa
# 4
# perl logicallingo.pl h04h04s
# 0
# perl logicallingo.pl h01h02h03h04h05aaa
# 14
@a=split//,pop;
for(@a){
  if($a[$c]eq"h"){push@b,hex($a[$c+1].$a[$c+2]);$c+=2}
  elsif($a[$c]eq"o"){pop@b}
  elsif($a[$c]eq"a"){$x=pop@b;$y=pop@b;push@b,$x+$y}
  elsif($a[$c]eq"s"){$x=pop@b;$y=pop@b;push@b,$x-$y}
  $c++;
}
print pop@b;


exit();
__END__

# winner
for$i(split",",pop){($b,$r,$u,$h)=map{hex substr$i,$_,2}6,10,12,14;$b==80?push@A,substr pack("H*",substr$i,14),0,$u:$b>82?$B[$r]+=$h:0}print$A[(grep$B[$_]==(sort{$a-$b}@B)[0],0..$#B)[0]]




=for comment
Logical Lingo
The team have been working at building a very simple calculator language, which can take in a list of instructions and output a number.

The language is stack based, and has the following instructions:

h[2 hex chars] - Push the value to the stack
o - Pop 1 values off stack and discard it
a - Add 2 values off the stack together
s - Subtract 2 values from the stack 


Your program should output the top value from the stack

Example
Program: hF0hFFsh00oh10a

Initial  []
hF0 push [240]
hFF push [240, 255]
s   sub  [15]
h00 push [15, 0]
o   pop  [15]
h10 push [15, 16]
a   add  [31]

Output: 31
Test Cases
Test Case 1
Argument 1: hF0hFFsh00oh10a
Expected Output: 31

Test Case 2
Argument 1: h00h01h03aa
Expected Output: 4

Test Case 3
Argument 1: h04h04s
Expected Output: 0

Test Case 4
Argument 1: h01h02h03h04h05aaa
Expected Output: 14
=cut
