#!/usr/bin/perl
# perl basicbases.pl '1AB' 'FB'
# 386
# 
# 
# perl basicbases.pl '283' '1ce'
# 3523
# 
# 
# perl basicbases.pl '2C71' '573'
# 11377
# 
# 
# perl basicbases.pl 'Egj' '195B5'
# 37145
# 
# 
# perl basicbases.pl '1K7c' 2NU2'
# 90050
# 
# 
# perl basicbases.pl 'A62B3' '1fg3'
# 218151
# 
# 
# perl basicbases.pl '828D' '6H8I'
# 47461
# 
# 
# perl basicbases.pl 'F0DG' 'OG3O'
# 539500
# 
# 
# perl basicbases.pl 'O2Ld' 'H1XK'
# 1658366
# 
# 
# perl basicbases.pl 'FM8c' '17AD52'
# 1150502
# 
# 

#use v5.26.1;
#use warnings;
use Time::HiRes qw/gettimeofday tv_interval time/;

#use List::Util qw(pairs unpairs);
#use List::MoreUtils qw(:all);
#use List::Compare qw(:all);

#use Scalar::Util qw(refaddr);

use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print

my $start_time = [gettimeofday];
END { print "Duration: ", tv_interval($start_time)*1000, " ms\n"; }

for $b (2..64){
  print $b,"\n";
}

exit();

__END__

$x=pop;
for(1..$x){
  $s+=$_ if$x%$_==0;
}
print $s;




=for example 
Input: 10, A
Values:
  10:
    - 2 # Binary
    - 10 # base 10
    - 17 # Hex
    - 65 # base 64
  A:
    - 10 # Hex
    - 10 # base 64
# Quite clearly, 10 is common between both numbers in decimal, so we output this
Output: 10
=cut

=for winners
@n%$_ or$\+=$_ for@n=1..pop;print
hashkitten

$\+=$n%$_?0:$_ for 1..($n=pop);print
Lyndon

=cut


=for comment
$_ The default input and pattern-searching space.
$" When an array or an array slice is interpolated into a double-quoted string or a similar context such as /.../, its elements are separated by this value. Default is a space.
$` The string preceding whatever was matched by the last successful pattern match, not counting any matches hidden within a BLOCK or eval enclosed by the current BLOCK.
=cut



=for comment
Basic Bases
The EngineerMan team have been given many pairs of numbers.
They are told that the 2 values in this pair are equal, but in varying bases.
They want to know the lowest number in base-10 form which makes these 2 values equal.

You will be given a list of comma separated strings, each which are encoded in one of the bases 2 through to 64.

It is your task to find which 2 in this list are equal and convert these to base 10, outputting the value.

The character ranges used for base conversions are

0-9A-Za-z+/
0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/

Example
Input: 10, A
Values:
  10:
    - 2 # Binary
    - 10 # base 10
    - 17 # Hex
    - 65 # base 64
  A:
    - 10 # Hex
    - 10 # base 64
# Quite clearly, 10 is common between both numbers in decimal, so we output this
Output: 10
Test Cases
Test Case 1
Argument 1
1AB
Argument 2
FB
Expected Output
386
Test Case 2
Argument 1
283
Argument 2
1ce
Expected Output
3523
Test Case 3
Argument 1
2C71
Argument 2
573
Expected Output
11377
Test Case 4
Argument 1
Egj
Argument 2
195B5
Expected Output
37145
Test Case 5
Argument 1
1K7c
Argument 2
2NU2
Expected Output
90050
Test Case 6
Argument 1
A62B3
Argument 2
1fg3
Expected Output
218151
Test Case 7
Argument 1
828D
Argument 2
6H8I
Expected Output
47461
Test Case 8
Argument 1
F0DG
Argument 2
OG3O
Expected Output
539500
Test Case 9
Argument 1
O2Ld
Argument 2
H1XK
Expected Output
1658366
Test Case 10
Argument 1
FM8c
Argument 2
17AD52
Expected Output
1150502
=cut

