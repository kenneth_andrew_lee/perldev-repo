#!/usr/bin/perl
use v5.26.1;
use Data::Dumper;

#perl ./holyletters.pl ENGINEERMAN
# 18
#perl ./holyletters.pl HEXF
# 0
#perl ./holyletters.pl BONES
# 9
#perl ./holyletters.pl QWERTYUIOPASDFGHJKLZXCVBNM
# 152
#perl ./holyletters.pl BABABLACKSHEEP
# 70
#perl ./holyletters.pl 
# 0


$_=pop;print((y/B//+y/ADOPQRB//d)*y///c);


exit();
__END__

# winner hashkitten
# print s/(?=B)|[ABDO-R]//g*y///cfor pop

# 2nd JoKing
# $_=pop;s/B/OO/g;print y/QOPRAD//d*y///c


=for docs
=cut

=for comment
Holy Letters
Given a string of capital letters only. Count all the letters with holes, double holed letters 
are equal to 2 and multiply by the amount of unholy letters.

Example...
PETER PETER
^___^ _^^^_
10001 01110 : 2 x 3 = 6

Test Cases
Test Case 1
Argument 1: ENGINEERMAN
Expected Output: 18
Test Case 2
Argument 1: HEXF
Expected Output: 0
Test Case 3
Argument 1: BONES
Expected Output: 9
Test Case 4
Argument 1: QWERTYUIOPASDFGHJKLZXCVBNM
Expected Output: 152
Test Case 5
Argument 1: BABABLACKSHEEP
Expected Output: 70
Test Case 6
Argument 1: ABBA
Expected Output: 0
=cut



