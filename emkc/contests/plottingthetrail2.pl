#!/usr/bin/perl 
use Data::Dumper;
#perl ./plottingthetrail2.pl 'FWFWTLFW'
#-1,2,L
#UUDD = 0 
#RR = 2

#$_=pop;printf"%s,%s",y/[R]//-y/[L]//,y/[U]//-y/[D]//;

#$_=pop;print Dumper(y/[R]//-y/[L]//,y/[U]//-y/[D]//);
#$VAR1 = 2;
#$VAR2 = 0;

#$_=pop;print y/R//-y/L//,',',y/U//-y/D//;

#'FWFWTLFW'
#'F F L F'
#'FWFWTLFWTLBWTLBWBW'
# 
#$d=1;%r=(1,U,2,L,3,D,4,R);sub x{$v=$d%4;($v==1)?{/F/?$y++:$y--}:($v==3)?{/F/?$y--:$y++}:($v==2)?{/F/?$x--:$x++}:{/F/?$x++:$x--}}for(pop=~/./g){/L/?$d++:/R/?$d--:/[FB]/?x:''}print "$x,$y,$r{$d}";
#sub x{$v=$d%4;($v==1)?{/F/?$y+$t}:($v==3)?{/F/?$y+$t}:($v==2)?{/F/?$x+$t}:{/F/?$x+$t}}

#$d=1;%r=(1,U,2,L,3,D,4,R);for(pop=~/./g){$s=($d-1)%3;$t=($s)?-1:1;$t*=/F/?1:/B/?-1:1;$u=$d%2;/L/?$d++:/R/?$d--:/[FB]/&&$u==0?$x+=$t:/[FB]/&&$u==1?$y+=$t:''}print"$x,$y,$r{$d}";

#$x=$y=0;
#%r=(1,U,2,L,3,D,0,R);
sub p{$_=/B/?$d+2:$d;$_%=4;/1/?$y++:/2/?$x--:/3/?$y--:$x++}
$d=1;@v=(U,L,D,R);
for(pop=~/./g){/L/?$d++:/R/?$d--:/[FB]/?p:''}
print "$x,$y,$v[--$d]";



exit(0);
__END__

# winners
map/T/?$d=/L/?--$d%4:++$d%4:($d%2?$x:$y+=($d+ord)%6>1?1:-1),pop=~/../g;print"$x,$y,".(U,R,D,L)[$d]

/W/?@d[$c%4]+=/F/-/B/:($c+=3**/L/)for pop=~/../g;$,=',';print@d[1]-@d[3],@d[0]-@d[2],(U,R,D,L)[$c%4]



im^1 = 0 + 1im
im^2 = -1 + 0im
im^3 = 0 - 1im
im^4 = 1 + 0im
im^5 = 0 + 1im

let a=2,p=0
    for x in split(ARGS[],"T")
        @show p
        !y=count(y,x)

        a+=2*!"L"-1 # 1 if turn left, -1 if turn right

        p+=im^a*(!"F"-!"B")
    end
    @show p
    print("$(real(p)),$(imag(p)),$("RULD"[a%4+1])")
end

for i in 1:5
  println("im^$i = $(im^i)")
end



Plotting The Trail 2
Imagine you are driving a car. You receive directions like TR which tells you to make a 90 degree clockwise turn, or FW which tells you to move forward by one square. Your job is to write a program to interpret these instructions and return the ending position along with the direction the 'car' is facing.



Possible Movements

TL = Turn Left/CounterClockwise
TR = Turn Right/Clockwise
FW = Move forward 1 space
BW = Move backwards 1 space


Sample Input Argument 1 (the positional movements)

FWFWTLFW


Sample Output

-1,2,L


Guidelines

The starting coordinates are 0,0 and you are facing up (U)
The output can be positive or negative x and y values along with the direction you are currently facing (L, R, U, D)


Your Submission
It's recommended that you compose your solution separately and just paste here once you're ready. After clicking "Submit Solution" your solution will be tested with secret inputs. If successful, your solution will be saved. Be sure to choose the correct language.