#!/usr/bin/perl
#use v5.26.1;

# perl seasonschange.pl 2021-02-28 S
# S

$m=pop;($_)=pop=~/\d+-(\d{2})/;$a=$m=~/N/?W:S if(/12|01|02/);$a=$m=~/N/?P:A if(/03|04|05/);$a=$m=~/N/?S:W if(/06|07|08/);$a=$m=~/N/?A:P if(/09|10|11/);print $a;



exit();

__END__


=for comment
$h=pop;
($_)=pop=~/\d+-(\d{2})/;
$a=$h=~/N/?W:S if(/12|01|02/);
$a=$h=~/N/?P:A if(/03|04|05/);
$a=$h=~/N/?S:W if(/06|07|08/);
$a=$h=~/N/?A:P if(/09|10|11/);
print $a;
=cut

=for comment
          N       S
12, 01, 02 Winter  Summer
03, 04, 05 sPring  Autumn
06, 07, 08 Summer  Winter
09, 10, 11 Autumn  sPring

=cut


=for comment
Seasons Change
The EngineerMan team aren't too good at keeping track of their seasons.

They have tasked you to develop a small program for them to help them remember.



Given any date and hemisphere, you should output the meteorological season in which the date lies.

Dates are given as YYYY-MM-DD, hemisphere as either N or S, and your response should be abbreviated as one of SAWP (Summer Autumn Winter sPring)



Example Test Cases

Date       Hemisphere  Season
2021-02-28 S           S
2021-03-01 N           P

=cut

