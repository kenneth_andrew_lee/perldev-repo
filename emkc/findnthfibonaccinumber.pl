#!/usr/bin/perl
use strict;
use warnings;

my $value1 = 34;
my $value2 = 7;


# write your solution here
my $next = 1;
my $i = 0;
my $j = 1;
while ($next < $value1) {
    $i = $j;
    $j = $next;
    $next = $i + $j;
}

my @array = ();
for (my $step = 0; $step < $value2; $step++) {
    $i = $j;
    $j = $next;
    $next = $i + $j;
    push @array,$next;
}

my $str = join ',', @array;
print $str;
