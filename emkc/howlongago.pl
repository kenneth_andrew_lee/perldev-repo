use Data::Dumper;
#my $value1 = $ARGV[0];
#my $value1 = '2015-12-03,2013-12-03';
#my $value1 = '2020-10-03,2020-12-04';
#my $value1 = '2015-12-03,2015-12-01';
my $value1 = '2020-12-11,2007-03-26';

@dates=split/,/,$value1;
use Time::Piece;

sub date_ymd {
  ($_)=@_;
  $d = Time::Piece->strptime($_, '%Y-%m-%d');
  return int($d->julian_day);
}
print abs(date_ymd($dates[0])-date_ymd($dates[1]));

exit();
__END__




sub date_ymd {
  ($_)=@_;
  $year  = substr($_[0],0,4); #should use unpack
  $month = substr($_[0],5,2);
  $day   = substr($_[0],8,2);
  print "\$year:$year", "|", "\$month:$month", "|", "\$day:$day", "\n";
  return  $year*365 + 
          int(($year-1)/4)+$months[$month-1]+
          $day[$year%4==0 && $month>=3?1:0]+5;
}

@dates=split/,/,$value1;
for(@dates){
  print date_ymd($_),"\n\n";
}
if ($dates[0]>$dates[1]) {
  print date_ymd($dates[0])-date_ymd($dates[1]),"\n";
} else {
  print date_ymd($dates[1])-date_ymd($dates[0]),"\n";
}

exit(0);
__END__
#print Dumper(@dates),"\n";

print Delta_days(,$dates[0][5:6],$dates[0][7:8],$dates[1][0:4],$dates[1][5:6],$dates[1][7:8]);
#print date_ymd($dates[0])-date_ymd($dates[1]);


=for comment
The program must take input as two comma-separated dates 
in the form YYYY-MM-DD, and print the number of days that 
has passed since the second date as if today was the first 
date (if the second date would be in the future, output a 
negative number). Assume both dates are in the Gregorian 
calendar.
Inputs
value1

A comma separated separated dates in the form YYYY-MM-DD. 
Sample Test Cases
Sample Input 1

value1: 2015-12-03,2013-12-03
output: 730
=cut