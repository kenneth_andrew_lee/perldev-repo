#!/usr/bin/perl
use strict;
use warnings;

my $value1 = 'rUnNiNg ,swimming, eating,biking, climbing';
# write your solution here
my @words = map lc, split /,/, $value1;
for (@words) {
    $_ =~ s/^\s+|\s+$//g
}

my $max = 0;
for (@words) {
    $max = length($_) if length($_) > $max;
}
my $flag = 0;
for (@words) {
    if (length($_) == $max) {
        print "," if $flag;
        print;
        $flag = 1;
    }
}