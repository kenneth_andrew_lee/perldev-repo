#!/usr/bin/perl
#use strict;
#use warnings;
use Data::Dumper;



#value1 = sys.argv[1]
#my $value1 = "14467,42641";
my $value1 = "brian,airbn";
# write your solution here
my @input = split /,/, $value1;
my @ary = ();
# Get all permutations of 
my @results = permute($input[0]);
my $flag = 0;
foreach my $item (@results) {
    if ($item eq $input[1]) {
        print "Yes";
        $flag = 1;
        last;
    } 
}
if (!$flag) {
    print "No";
}


sub permute {
    my( $in, $pre ) = ( @_, '' );
    
    if( not length $in ) {
        #print $pre;
        push @ary, $pre;
    } else {
        for my $i ( 0 .. length( $in ) - 1 ) {
            permute(
                substr( $in, 0, $i ) . substr( $in, $i+1 ),
                $pre . substr( $in, $i, 1 )
            );
        }
    }
    return @ary;
}

exit(0);
