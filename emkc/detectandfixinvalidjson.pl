#!/usr/bin/perl
use strict;
use warnings;


#//const value1 = process.argv[2];
#//const value1='{"name":"em" "dog":"elmer"}';
#//const value1='{"name":"em" "dog":"elmer"';
#//const value1='"name":"em" "dog":"elmer"}';
#//const value1='"name":"em" "dog":"elmer"';
#my $value1='{"name":"em"';
my $value1='"name":"em" "dog":"elmer"';

# write your solution here
my $counter = 0;
my @words = ();
my $pattern = "(\\w+)";
my $max = 0;
while ($value1 =~ /$pattern/g) {
	push @words, $1;
	#print $1, "  ", pos $value1, "\n";
}

$counter = 0;
print "{";
foreach my $word (@words) {
	$counter++;
	if ($counter > 2 && $counter%2==1) {
		print ",";
	}  
    if ($counter%2==0) {
        print ":";  
    }
    print "\"" . $word . "\"";
}
print "}";


__END__ 
