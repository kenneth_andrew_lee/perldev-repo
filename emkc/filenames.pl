use Data::Dumper;
#my $value1 = $ARGV[0];
my $value1      = "doc(3), doc(4), doc, doc, doc, doc, doc (6), (image ), hello world, helloworld, (image )(1), (image )";
# expected output: doc(3), doc(4), doc, doc(1), doc(2), doc(5), doc (6), (image ), hello world, helloworld, (image )(1), (image )(2)
# write your solution here
my @temp = split",",$value1;
for my $i (0 .. $#temp) {
  $temp[$i] =~ s/^\s+|\s+$//g;
}

my %hash=();
for my $i (0 .. $#temp) {
  my $val = 1;
  my $key = $temp[$i];
  my $newkey = $temp[$i];

  while (defined $hash{$newkey}) {
    # change the name until a new one is found that isn't found
    if ( $newkey=~/(.*?)\((\d+)\)/ ) {
      #print "\$1:$1", " ", "\$2:$2", " ", "\$newkey:$newkey", "\n";
      #change value of key
      $val=$2;
      $val++;
      $newkey=$1."(".$val.")";
    } else {
      $newkey=$key."(".$val.")";
      $val++;
      #print "\$val:$val", " ", "\$newkey:$newkey", "\n";
    }  
  }
  #print 'defined $hash{$newkey}', " ", "\$newkey:$newkey", " ", "\$hash{$newkey}:$hash{$newkey}", "\n";
  
  $temp[$i]=$newkey;
  $hash{$temp[$i]}=1;

}
  
print join", ",@temp;  


#print Dumper(%hash), "\n";

exit(0);
__END__

for (@temp){
  if(defined $hash{$_}) {
    $hash{$_}+=1;
  } else {
    $hash{$_}=1;
  }
}





my %hash=();
for (@temp){
  if(defined $hash{$_}) {
    $hash{$_}+=1;
  } else {
    $hash{$_}=1;
  }
}

for my $key (keys %hash) {
  while ($hash{$key} > 1) {
    my $val = 1;
    my $newkey=$key."(".$val.")";
    while (defined $hash{$newkey}) {
      $val++;
      $newkey=$key."(".$val.")";
    }
    $hash{$newkey}=1;
    $hash{$key}-=1;
  }
}

@list=();
my @list = (keys %hash);
print join",",@list;  



# now fix the ones where the count > 1

#print "\@temp:@temp", "\n";
#print Dumper(%hash), "\n";

$VAR1 = ' doc';
$VAR2 = 4;
$VAR3 = ' doc (6)';
$VAR4 = 1;
$VAR5 = ' (image )(1)';
$VAR6 = 1;
$VAR7 = ' hello world';
$VAR8 = 1;
$VAR9 = ' (image )';
$VAR10 = 2;
$VAR11 = 'doc(3)';
$VAR12 = 1;
$VAR13 = ' doc(4)';
$VAR14 = 1;
$VAR15 = ' helloworld';
$VAR16 = 1;

=for comment
Instructions
You are given an string of desired filenames in the order of their creation. Since two files cannot have equal names, the one which comes later will have an addition to its name in a form of (k), where k is the smallest positive integer such that the obtained name is not used yet.

Input: "doc(3), doc(4), doc, doc, doc, doc, doc (6), (image ), hello world, helloworld, (image )(1), (image )"
doc -> doc 
doc -> doc(1) # as there is doc already 
image -> image 
doc(1) -> doc(1)(1) # as doc(1) is already there 
doc -> doc(2) # as doc and doc(1) are already there
Output: "doc(3), doc(4), doc, doc(1), doc(2), doc(5), doc (6), (image ), hello world, helloworld, (image )(1), (image )(2)"



Inputs
value1

Single string separated by commas.

Sample Test Cases
Sample Input 1

value1: doc(3), doc(4), doc, doc, doc, doc, doc (6), (image ), hello world, helloworld, (image )(1), (image )
output: doc(3), doc(4), doc, doc(1), doc(2), doc(5), doc (6), (image ), hello world, helloworld, (image )(1), (image )(2)
=cut