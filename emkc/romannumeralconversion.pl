#!/usr/bin/perl
use strict;
use warnings;

#my $value1 = $ARGV[0];
my $value1 = 'CDVII';

# write your solution here
$value1 =~ s/IV/F/g;
$value1 =~ s/IX/G/g;
$value1 =~ s/XL/H/g;
$value1 =~ s/XC/N/g;
$value1 =~ s/CD/J/g;
$value1 =~ s/CM/K/g;

my $sum = 0;
my @splitresults = split //, $value1;
foreach my $item (@splitresults) {
	if ($item eq 'I') {
		$sum += 1;
	} elsif ($item eq 'V') {
		$sum += 5;
	} elsif ($item eq 'X') {
		$sum += 10;
	} elsif ($item eq 'L') {
		$sum += 50;
	} elsif ($item eq 'C') {
		$sum += 100;
	} elsif ($item eq 'D') {
		$sum += 500;
	} elsif ($item eq 'M') {
		$sum += 1000;
	} elsif ($item eq 'F') {
		$sum += 4;
	} elsif ($item eq 'G') {
		$sum += 9;
	} elsif ($item eq 'H') {
		$sum += 40;
	} elsif ($item eq 'N') {
		$sum += 90;
	} elsif ($item eq 'J') {
		$sum += 400;
	} elsif ($item eq 'K') {
		$sum += 900;
	}
}
print $sum;
