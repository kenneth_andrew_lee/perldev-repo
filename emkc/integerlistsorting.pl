#!/usr/bin/perl
use strict;
use warnings;

my $value1 = "3,9,10,5,2,7,9,2";

# write your solution here
my @list = ();
foreach my $value (split /,/, $value1) {
    push(@list,$value);
}
my @sorted = sort { $b <=> $a } @list;
print join(',', @sorted);