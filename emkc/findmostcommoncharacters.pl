#!/usr/bin/perl
use strict;
use warnings;


#value1 = sys.argv[1]
my $value1 = "A test case";

# write your solution here
my %hash = ();
my @display_order = ();
my @results = ();
foreach my $char (split //, $value1) {
	if ($char eq ' ') {
		# nothing to do.
	} else {
		if (defined $hash{$char}) {
			$hash{$char} += 1;
		} else {
			$hash{$char} = 1;
			push @display_order, $char;
		}
	}
}

my @hashes = sort { $hash{$a} <=> $hash{$b} } keys %hash;
 
my $max = $hashes[-1];
 
#print "$max : $hash{$max}\n\n\n";
#print all the keys that that a value that equals $hash{$max}
#my $flag = 0;
foreach my $key (keys %hash) {
	if ($hash{$key} == $hash{$max}) {
		#print "," if $flag;
		push @results, $key;
		#print $key;
		#$flag = 1;
	}
}
#### Last part, they need to be in the order the appeared.
my $flag = 0;
foreach my $key (@display_order) {
	foreach my $otherkey (@results) {
		if ($otherkey eq $key) {
			print "," if $flag;
			print $key;
			$flag = 1;
		}
	}
}
