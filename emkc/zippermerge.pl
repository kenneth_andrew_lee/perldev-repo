#!/usr/bin/perl
use strict;
use warnings;

my $value1 = '9,5';
my $value2 = '7,10';

# write your solution here
my @list1 = split /,/, $value1;
my @list2 = split /,/, $value2;
my $str = "";

my $listlen = scalar @list1;
for (my $i=0;$i<$listlen;$i++) {
    if (length($str) > 0) {
        $str .= ",";  
    }
    $str .= $list1[$i] . "," . $list2[$i];
}
print $str;
