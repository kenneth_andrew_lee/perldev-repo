#!/usr/bin/perl
use strict;
use warnings;

#my $value1 = $ARGV[0];
#my $value1 = 'ig-pay atin-lay isyay usedyay inyay ools-schay o-tay each-tay anguage-lay onstructs-cay';
#my $value1 = 'engineeryay an-may';
my $value1 = 'isyay ift-sway ayay ood-gay anguage-lay';

# write your solution here
my $value2 = "";
my @words = split /\s+/, $value1; # break value1 into words

foreach my $element (@words) {
	my $word = "";
	my $ayat = "";
	my $dashat = index($element,'-');
	if ($dashat > 0) {
		$ayat = rindex($element,'ay');
		$word = substr($element,$dashat+1,$ayat-$dashat-1) . substr($element,0,$dashat);
	} else {
		$ayat = rindex($element,'yay');
		$word = substr($element,0,$ayat);
	}
	if (length($value2) > 0) {
		$word =~ s/^\s+|\s+$//g;
		$value2 = $value2 . " " . $word;
	} else {
		$word =~ s/^\s+|\s+$//g;
		$value2 = $word;
	}
};

print $value2;
