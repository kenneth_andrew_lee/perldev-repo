#!/usr/bin/perl
#
#
#

my $gi = Geo::IP->open("/usr/local/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);
my $record = $gi->record_by_name("24.24.24.24");
print $record->country_code,
        $record->country_code3,
        $record->country_name,
        $record->region,
        $record->region_name,
        $record->city,
        $record->postal_code,
        $record->latitude,
        $record->longitude;

