#!/usr/bin/perl
#
# perl WordDistanceDP.pl pots spot
#
#        P  O  T  S
#     0  1  2  3  4
#  S  1  2  3  3  3
#  P  2  1  2  3  4
#  O  3  2  1  2  3
#  T  4  3  2  1  2
# result = 2

use strict;
use warnings;
use List::Util qw(min);
use Data::Dumper;

my $s1 = shift;
my $s2 = shift;

sub print_matrix {
  my ($m_ref, $s1, $s2) = @_;
  my %m = %$m_ref;
  my $l1 = length $s1;
  my $l2 = length $s2;
  $s1 = "B".$s1;
  
  # print top Row
  printf("%3s%3s","","B");
  for my $l (0..length($s2)) {
    printf("%3s",substr($s2,$l,1))
  }
  print "\n";
  # display matrix
  for my $row (0 .. $l1) {

    printf("%3s",substr($s1,$row,1));
    for my $col (0 .. $l2) {
      my $key = "(".$row.",".$col.")";
      printf("%3d",defined($m{$key})?$m{$key}:0);
    }
    print "\n";
  }
  print "\n";
}

#        P  O  T  S
#     0  1  2  3  4
#  S  1  2  3  3  3
#  P  2  1  2  3  4
#  O  3  2  1  2  3
#  T  4  3  2  1  2

# initialize %T hash
my %T = ();  # store value in a hash
for my $i (0 .. length($s1)) {
  for my $j (0 .. length($s2)) {
    my $key = "(".$i.",".$j.")";
    $T{$key} = 0;
  }
}
#T(i,j) i=row, j=col
for my $i (0 .. length($s1)+1) { # pots:4
  for my $j (0 .. length($s2)+1) { # spot:4
    if ($i==0 and $j==0) { # nothing compared to nothing = 0
      $T{"(".$i.",".$j.")"} = 0;
    } elsif ($i==0) {
      $T{"(".$i.",".$j.")"} = $T{"(".$i.",".($j-1).")"} + 1;
    } elsif ($j==0) {
      $T{"(".$i.",".$j.")"} = $T{"(".($i-1).",".$j.")"} + 1;
    } else {
      if (substr($s1,($i-1),1) eq substr($s2,($j-1),1)) { # upper left diagonal
          $T{"(".$i.",".$j.")"} = $T{"(".($i-1).",".($j-1).")"};
      } else { # (min of up, left, upper left diagonal) + 1
          $T{"(".$i.",".$j.")"} = 
          min(
            $T{"(".$i.",".($j-1).")"}, 
            $T{"(".($i-1).",".$j.")"}, 
            $T{"(".($i-1).",".($j-1).")"} 
            ) + 1;
          #print $T{"(".$i.",".$j.")"}," ", $s1, " ", "\$i:$i", " ", substr($s1,($i-1),1), " ", $s2, " ", "\$j:$j"," ",substr($s2,($j-1),1), " ",$T{"(".$i.",".($j-1).")"}," ", $T{"(".($i-1).",".$j.")"}," ", $T{"(".($i-1).",".($j-1).")"} ,"\n";

      }
    }
  }
}                          

#print Dumper(%T), "\n";
print_matrix(\%T, $s1, $s2);
print "$s1:", length($s1), "  ", "$s2:", length $s2, "\n";
print "Solution:", $T{"(".length($s1).",".length($s2).")"}, "\n";


=for comment
words distance
What are the subproblems in this case? 
The idea is to process all characters one by one starting from either from left or right sides of both strings. 
Let us traverse from right corner, there are two possibilities for every pair of character being traversed.  

m: Length of str1 (first string)
n: Length of str2 (second string)
If last characters of two strings are same, nothing much to do. Ignore last characters and get count for remaining strings. So we recur for lengths m-1 and n-1.
Else (If last characters are not same), we consider all operations on ‘str1’, consider all three operations on last character of first string, recursively compute minimum cost for all three operations and take minimum of three values. 
Insert: Recur for m and n-1
Remove: Recur for m-1 and n
Replace: Recur for m-1 and n-1
=cut