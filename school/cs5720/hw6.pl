#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;


sub print_matrix {
  my ($m_ref, $n, $m) = @_;
  my %m = %$m_ref;
  # display matrix
  for my $row (1 .. $n) {
    for my $col (1 .. $m) {
      my $key = "(".$row.",".$col.")";
      printf("%5d",defined($m{$key})?$m{$key}:0);
    }
    print "\n";
  }
  print "\n";
}


my $n = 6;
my $m = 6;
my $i;
my $j;
my $val1;
my $val2;
my $flag;

# define coin hash
my %c = ();
for my $row (1 .. $n) {
  for my $col (1 .. $m) {
    my $key = "(".$row.",".$col.")";
    $c{$key}=0;
  }
}
$c{"(1,4)"}=1;
$c{"(2,5)"}=1;
$c{"(3,4)"}=1;
$c{"(3,6)"}=1;
$c{"(4,3)"}=1;
$c{"(4,5)"}=1;
$c{"(5,2)"}=1;
$c{"(6,1)"}=1;

print "Coins\n";
print_matrix(\%c,$n,$m);


# define Block hash
my %x = ();
$x{"(1,2)"}=1;
#$x{"(2,3)"}=1;
$x{"(2,6)"}=1;
$x{"(3,1)"}=1;
$x{"(3,3)"}=1;
$x{"(5,5)"}=1;
$x{"(6,5)"}=1;

print "Blocks\n";
print_matrix(\%x,$n,$m);

# check for starting block having a block
if (defined($x{"(1,1)"})) {
  print "No solution";
  exit();
}

# define F hash
my %f = ();
for my $row (1 .. $n) {
  for my $col (1 .. $m) {
    my $key = "(".$row.",".$col.")";
    $f{$key}=0;
  }
}


#ith row
#jth col

# first row, if one -1, the rest are -1
$flag = 0;
for $j (2 .. $m) {
  if (defined($x{"(1,".$j.")"})) {
    $f{"(1,".$j.")"} = -1;
    $flag=1;
  } elsif ($flag) {
    $f{"(1,".$j.")"} = -1;
  } else {  
    $f{"(1,".$j.")"} = $c{"(1,".$j.")"};
  }
}

# first column, if one -1, the rest are -1
$flag = 0;
for $i (2 .. $n) {
  if (defined($x{"(".$i.",1)"})) { # check for block
    $f{"(".$i.",1)"} = -1;
    $flag=1;
  } elsif ($flag) {
    $f{"(".$i.",1)"} = -1;
  } else {  
    $f{"(".$i.",1)"} = $f{"(".($i-1).",1)"} + $c{"(".$i.",1)"};
  }
}

# rest of the grid
for $i (2 .. $n) {
  for $j (2 .. $m) {
    if (defined($x{"(".$i.",".$j.")"})) {  # check for block
      $f{"(".$i.",".$j.")"} = -1;
    } elsif ($f{"(".($i-1).",".$j.")"}==-1 and $f{"(".$i.",".($j-1).")"}==-1) {
      $f{"(".$i.",".$j.")"} = -1;
    } else {
      $val1 = $f{"(".($i-1).",".$j.")"};
      $val2 = $f{"(".$i.",".($j-1).")"};
      if ($val1 > $val2) {
        $f{"(".$i.",".$j.")"} = $val1 + $c{"(".$i.",".$j.")"};
      } else {
        $f{"(".$i.",".$j.")"} = $val2 + $c{"(".$i.",".$j.")"};
      }
    }
  }
}

print "Values\n";
print_matrix(\%f,$n,$m);

my $key = "(".$n.",".$m.")";
if ($f{$key} == -1) {
  print "no solution\n";
} else {
  print "\$f{$key}", ":", $f{$key}, "\n";
}


__END__

=for comment

if C(1,1)=-1
  no solution
  return -1
F(1,1)= C(1,1)



F(i,j)= max{F(i-1,j), F(i,j-1)} + C(i,j)
=cut