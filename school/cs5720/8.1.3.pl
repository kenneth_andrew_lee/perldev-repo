#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;


sub print_matrix {
  my ($m_ref, $n, $m) = @_;
  my %m = %$m_ref;
  # display matrix
  for my $row (1 .. $n) {
    for my $col (1 .. $m) {
      my $key = "(".$row.",".$col.")";
      printf("%5d",$m{$key});
    }
    print "\n";
  }
  print "\n";
}


my $n = 5;
my $m = 6;
my $i;
my $j;
my %f = ();

my %c = ();
for my $row (1 .. $n) {
  for my $col (1 .. $m) {
    my $key = "(".$row.",".$col.")";
    $c{$key}=0;
  }
}
$c{"(1,5)"}=1;
$c{"(2,2)"}=1;
$c{"(2,4)"}=1;
$c{"(3,4)"}=1;
$c{"(3,6)"}=1;
$c{"(4,3)"}=1;
$c{"(4,6)"}=1;
$c{"(5,1)"}=1;
$c{"(5,5)"}=1;

print_matrix(\%c,$n,$m);


$f{"(1,1)"} = $c{"(1,1)"};

for my $j (2 .. $m) {
  $f{"(1,".$j.")"} = $f{"(1,".($j-1).")"} + $c{"(1,".$j.")"};
}

for my $i (2 .. $n) {
  $f{"(".$i.",1)"} = $f{"(".($i-1).",1)"} + $c{"(".$i.",1)"};
  for my $j (2 .. $m) {
    my $val1 = $f{"(".($i-1).",".$j.")"} + $c{"(".$i.",".$j.")"};
    my $val2 = $f{"(".$i.",".($j-1).")"} + $c{"(".$i.",".$j.")"};
    if ($val1 > $val2) {
      $f{"(".$i.",".$j.")"} = $val1;
    } else {
      $f{"(".$i.",".$j.")"} = $val2;
    }
  }
}


print_matrix(\%f,$n,$m);

my $key = "(".$n.",".$m.")";
print "\$f{$key}", ":", $f{$key}, "\n";


