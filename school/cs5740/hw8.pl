#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

#Setup problem

#slots
my $a=10;
my $b=7;
my $c=0;

#bidders
my $x=10;
my $y=9;
my $z=2;

#prices
my $xprice=0;
my $yprice=0;
my $zprice=0;

# payoff
my $xpayoff=0;
my $ypayoff=0;
my $zpayoff=0;


my $first="";
my $second="";
my $third="";

my $max = 0;
my $xbest= 0;
my $ybest= 0;
my $zbest= 0;
my $xmaxpayoff=0;
my $ymaxpayoff=0;
my $zmaxpayoff=0;


# loop through possible bid values
for my $x1 (1...$x){
  for my $y1 (1...$y){
    for my $z1 (0..$z){
      if ($x1 > $y1 && $x1 > $z1) {
        $first = "x";
        if ($y1 > $z1) {
          $second = "y";
          $third = "z";
        } else {
          $second = "z";
          $third = "y";
        }
      } elsif($y1 > $x1 && $y1 > $z1) {
        $first = "y";
        if ($x1 > $z1) {
          $second = "x";
          $third = "z";
        } else {
          $second = "z";
          $third = "x";
        }
      } else {
        $first = "z";
        if ($x1 > $y1) {
          $second = "x";
          $third = "y";
        } else {
          $second = "y";
          $third = "x";
        }
      }

      if ($first eq "x") {
        if ($second eq "y") {
          $xprice=$a;
          $yprice=$b;
          $zprice=$c;
        } else {
          $xprice=$a;
          $yprice=$c;
          $zprice=$b;
        }

      } elsif ($first eq "y") {
        if ($second eq "x") {
          $xprice=$b;
          $yprice=$a;
          $zprice=$c;
        } else {
          $xprice=$c;
          $yprice=$a;
          $zprice=$b;
        }

      } else { #first = "z"
        if ($second eq "y") {
          $xprice=$c;
          $yprice=$b;
          $zprice=$a;
        } else {
          $xprice=$b;
          $yprice=$c;
          $zprice=$a;
        }
      }
      $xpayoff=$x*$xprice-$x1*$xprice;
      $ypayoff=$y*$yprice-$y1*$yprice;
      $zpayoff=$z*$zprice-$z1*$zprice;
      my $total = $xpayoff+$ypayoff+$zpayoff;
      if ($total >= $max) {
        $max = $total;
        $xbest=$x1;
        $ybest=$y1;
        $zbest=$z1;
        $xmaxpayoff=$xpayoff;
        $ymaxpayoff=$ypayoff;
        $zmaxpayoff=$zpayoff;

      }
    }
  }
}

print "x:$x", " ", "xprice:$xprice", " ", "xmaxpayoff:$xmaxpayoff", " ", "xbest:$xbest", " ", "max:$max", "\n"; 
print "y:$y", " ", "yprice:$yprice", " ", "ymaxpayoff:$ymaxpayoff", " ", "ybest:$ybest", " ", "max:$max", "\n"; 
print "z:$z", " ", "zprice:$zprice", " ", "zmaxpayoff:$zmaxpayoff", " ", "zbest:$zbest", " ", "max:$max", "\n"; 


__END__

=for comment

=cut