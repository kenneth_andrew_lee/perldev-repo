#!/usr/bin/perl

use strict;
use warnings;


my $filename = $ARGV[0];

open my $fh, '<', $filename or die "Couldn't open file: $!\n";
while (<$fh>) {
    chomp;
    if ( -e $_ ) {
        print "$_\n";
    }
}
close $fh;

exit 0;


