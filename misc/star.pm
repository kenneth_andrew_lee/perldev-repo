package star;
use strict;
use warnings;

sub new {
	my ($class, %attr) = @_;
	my $ref = \%attr;
	bless $ref, $class;
	return $ref;
}

sub loc {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

sub dir {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

sub size {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

sub color {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

1;