#!/usr/bin/perl
use POSIX qw(strftime);

@weekday = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

print "gmtime:\n";

$local_time = gmtime();
$now_string = strftime "%a %b %d %H:%M:%S %Y %z", gmtime;
#$now_string = strftime "%a, %d %b %y %T %z", gmtime;
print "$now_string\n";

print "Local time = $local_time\n";
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime(time);
$year = $year + 1900;
print "Formated time = $mday/$mon/$year $hour:$min:$sec $weekday[$wday]\n\n\n";


print "localtime:\n";
$local_time = localtime;
$now_string = strftime "%a %b %d %H:%M:%S %Y %z", localtime;
#$now_string = strftime "%a, %d %b %y %T %z", localtime;  #RFC 822-compliant date format
#$now_string = strftime "%a, %d %b %Y %T %z", localtime;  #RFC 2822-compliant date format
print "$now_string\n";

print "Local time = $local_time\n";
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = $year + 1900;
print "Formated time = $mday/$mon/$year $hour:$min:$sec $weekday[$wday]\n";