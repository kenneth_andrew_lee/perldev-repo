#!/usr/bin/perl
use strict;
use warnings;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

#print "sha1_hex Digest for $ARGV[0] is ", sha1_hex($ARGV[0]), "\n";

#my $prefix = "111111";
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
my $date = $year+1900 . sprintf "%02d", $mon+1 . sprintf "%02d", $mday;
my $time =  sprintf "%-2.2d", $hour . sprintf "%-2.2d", $min . sprintf "%-2.2d", $sec;

#printf "%-2.2d\n", $hour;
#printf "%-2.2d\n", $min;
#printf "%-2.2d\n", $sec;
#printf "%-06.6d\n\n",$time;

#return;

# Create 10,000 cards and 100,000 amounts: 10000
for (1..100) {
	#my $acct = sprintf "%-16.16d", int(rand(20000000));  # 0000000000000001 - 0000000020000000   # 0000000000000001 - 0000000020000000
 	#my acct = $prefix . sprintf "%-10.10d", int(rand(20000000));
	#my $pan = sprintf "%-19.19d", int(rand(20000000));
	my $pan = int(rand(20000000));
	my $customerAccountNumber = sha1_hex($pan);
	$pan = sprintf "%-16.16d", $pan;
	#print $acct, "\n";
	#$acct = sha1_hex($acct);
	#print $acct, "\n";
	for ( 1..10) {
		my $milli = sprintf "%03d", $_;
		my $amt = (rand(997) + 2);	
		printf "%040s,%-19s,%06.2f,%08s,%06s,%03s",$customerAccountNumber, $pan,$amt, $date,$time,$milli;
		print "\n";
		#print "\n***$hour****\n";
	}
}
