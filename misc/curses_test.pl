#!/usr/bin/env perl -w

use strict;
use Curses::UI;

my @daten = (
        { "headline" => "Lorem Ipsum 1", "text" => "Lorem Ipsum 1" },
        { "headline" => "Lorem Ipsum 2", "text" => "Lorem Ipsum 2" },
        { "headline" => "Lorem Ipsum 3", "text" => "Lorem Ipsum 3" }
);

sub draw_ui {
        my $cui = new Curses::UI( -color_support => 1 );
        my $win = $cui->add("win", "Window", -border => 1, -y => 0, -title => "Title");
        my $max_height = $win->height();
        my $max_width = $win->width();
        my $lbox = $win->add("List", "Listbox", -fg => "white", -height => int($max_height / 2), -border => 1, => "Foo" );
        my $tbox = $win->add("Textbox", "TextViewer", -fg => "white", -y => ($max_height / 2), -height => int($max_height / 2), -wrapping => 1, -border => 1, -title => "Foo" );
        $lbox->onSelectionChange(sub {
                my $id = $lbox->get_active_id();
                $tbox->text($daten[$id]->{text});
        });
        $lbox->onChange(sub {
        my $id = $lbox->get_active_id();
                $cui->dialog("ID:" . $id);
        });
        $cui->set_binding( sub { exit(0); } , "\cC");
        my @headlines;
        push(@headlines, $_->{headline}) for @daten;
        $lbox->values(\@headlines);
        $lbox->focus();
        $cui->mainloop();
}

draw_ui();

