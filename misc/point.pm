package point;
use strict;
use warnings;

sub new {
	my ($class, %attr) = @_;
	my $ref = \%attr;
	bless $ref, $class;
	return $ref;
}

sub x {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

sub y {
	my $self = shift;
	$self->{y} = shift if @_;
	return $self->{y};
}

1;