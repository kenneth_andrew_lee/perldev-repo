#!/usr/bin/perl
use strict;
use warnings;

if (0) {
    print "passed in 0\n";
} else {
    print "didn't succeed for 0\n";
}


if (1) {
    print "succeeded for 1\n";
} else {
    print "didn't succeed for 1\n";
}

# Results:
# didn't succeed for 0
# succeeded for 1

#1=true
#0=false