#!/usr/bin/perl
#use JSON -support_by_pp;
use JSON;
#use JSON::Parse 'parse_json';
use Data::Dumper;               # Perl core module


my %hash = ();

my $jsondata;
while (<DATA>) {
	chomp;
	$jsondata .= $_;
}



build_hash_from_json(%hash, $jsonData);


sub build_hash_from_json() {

}





sub my_test() {
	my $json_text = decode_json( $jsondata );
	print "\$json_text is a ",ref($json_text), "\n";

	# this is an array reference
	my $array_ref = $json_text->{data};
	print "\$json_text->{data} is a ",ref($json_text->{data}), "\n";

	for my $hash_ref ( @$array_ref ) {
		# Now each item is a Hash reference
		my @keys =  keys  %$hash_ref;
		for my $key (@keys) {
			if (ref($hash_ref->{$key})) { 
				print "\$hash_ref->{$key} is a ",ref($hash_ref->{$key}), "\n";
			} else {
				print "\$hash_ref->{$key} = $hash_ref->{$key}", "\n";
			}
		}
	}
}


__DATA__
{
   "data" : [
      {
         "publisher_name" : "Harpercollins Childrens Books",
         "isbn10" : "0060248122",
         "language" : "",
         "publisher_text" : "Harpercollins Childrens Books",
         "title_latin" : "The Mrs. Piggle-Wiggle Treasury",
         "publisher_id" : "harpercollins_childrens_bo_a01",
         "notes" : "",
         "isbn13" : "9780060248123",
         "dewey_decimal" : "",
         "summary" : "",
         "physical_description_text" : "",
         "book_id" : "the_mrs_piggle_wiggle_treasury",
         "lcc_number" : "",
         "marc_enc_level" : "~",
         "title" : "The Mrs. Piggle-Wiggle Treasury",
         "subject_ids" : [
            "amazon_com_childrens_books_series_humorous_mrs_piggle_wiggle"
         ],
         "author_data" : [
            {
               "id" : "macdonald_betty",
               "name" : "MacDonald, Betty"
            },
            {
               "name" : "Knight, Hilary",
               "id" : "knight_hilary"
            }
         ],
         "dewey_normal" : "0",
         "title_long" : "",
         "urls_text" : "",
         "awards_text" : "",
         "edition_info" : "Hardcover; 1995-10-01"
      }
   ],
   "index_searched" : "isbn"
}
