#!/usr/bin/perl
#
#The header and footer records for all the data feeds contain several fixedlength
#fields, the sum of which is 35 bytes. The header and footer record files
#also contain a FILLER field that is used to make up the difference between the
#size of the fixed-length fields and the size of the batch record. For example,
#the Credit 2.3 batch record size is 902 bytes, the size of the fixed-length fields
#is 35 bytes, so the FILLER field size is 867 bytes (902 - 35 = 867).
#
#
#
use strict;
use warnings;

my $record_count = 1;
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime();
$year += 1900;

my $RECORD_TYPE_HEADER = "B"; # The value of the RECORD_TYPE field in the file header is B.
my $RECORD_TYPE_FOOTER = "E"; # The value of the RECORD_TYPE field in the file footer is E.
my $DATA_FEED_SORT_KEY_HEADER = "000000000"; # Used to sort the file without disturbing the header position.  The only valid value is all zeros.
my $DATA_FEED_SORT_KEY_FOOTER = "999999999"; # Used to sort the file without disturbing the footer position.  For the footer record, the only valid value is all nines (999999999).
my $FILLER = ' ' x 1599; # Fill with blanks such that record size matches that of all other records in file.
my $LAYOUT_VER = "1.0"; # Falcon Data Format version.  1.0 = Current value
my $FILETYPE = "EXT10"; # Describes the type of data in the file. Use the data feed name.
my $SYSTEM_ID = "PMAX"; # Must match the SYSTEM_ID field in the pmax.cnf file on the Falcon Server. The SYSTEM_ID is “PMAX”.
my $RECORD_COUNT_HEADER = "00000000"; # Identifies the number of records Falcon has processed from this file. Be sure to fill with zeros to ensure that Falcon processes all records from the batch file.
my $RECORD_COUNT_FOOTER = sprintf("%d",$record_count); # Identifies the number of records to process.

my $SEMAPHORE = "EXT10";

my $sdate = sprintf("%-4.4s",$year) . sprintf("%-2.2s",$mon) . sprintf("%-2.2s",$mday);
my $stime = sprintf("%-2.2s",$hour) . sprintf("%-2.2s",$min) . sprintf("%-2.2s",$sec);
####################### Data Definitions #######################
my $workflow = "UD17DF26";
my $recordType = "EXT10";
my $dataSpecificationVersion = "1.0";
my $clientIdFromHeader = "";
my $recordCreationDate = $sdate;
my $recordCreationTime = $stime;
my $recordCreationMilliseconds = "000";
my $gmtOffset = "000000";
my $customerIdFromHeader = "";
my $customerAcctNumber = "";
my $externalTransactionId = "";
my $serviceId = "";
my $transactionDate = "";
my $transactionTime = "";
my $validity = "";
my $entityType = "";
my $extSource = "";
my $notificationName = "";
my $notificationStatus = "";
my $score1 = "";
my $score2 = "";
my $score3 = "";
my $userData01 = "";
my $userData02 = "";
my $userData03 = "";
my $userData04 = "";
my $userData05 = "";
my $userData06 = "";
my $userData07 = "";
my $userData08 = "";
my $userData09 = "";
my $userData10 = "";
my $userData11 = "";
my $userData12 = "";
my $userData13 = "";
my $userData14 = "";
my $userData15 = "";
my $userData16 = "";
my $userData17 = "";
my $userData18 = "";
my $userData19 = "";
my $userData20 = "";
my $userData21 = "";
my $userData22 = "";
my $userData23 = "";
my $userData24 = "";
my $userData25 = "";
my $userData26 = "";
my $userData27 = "";
my $userData28 = "";
my $userData29 = "";
my $userData30 = "";
my $userData31 = "";
my $userData32 = "";
my $userData33 = "";
my $userData34 = "";

################################################################

#open (XFEED,'+>',"EXT_FEED.txt") || die "file not found :$!";
# Print Header Line
#printf XFEED "%s%-2099.2099s%s%s%s%s%-08.8d\n", $RECORD_TYPE_HEADER, $DATA_FEED_SORT_KEY_HEADER, $FILLER, $LAYOUT_VER, $FILETYPE, $SYSTEM_ID, $RECORD_COUNT_HEADER;
printf "%s%-2099.2099s%s%s%s%s%-08.8d\n", $RECORD_TYPE_HEADER, $DATA_FEED_SORT_KEY_HEADER, $FILLER, $LAYOUT_VER, $FILETYPE, $SYSTEM_ID, $RECORD_COUNT_HEADER;

# Print Body
#printf XFEED  "%-16.16s%-8.8s%-5.5s%-16.16s%-8.8s%-6.6s%-3.3s%-6.6s%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s", $workflow, $recordType, $dataSpecificationVersion, $clientIdFromHeader, $recordCreationDate, $recordCreationTime, $recordCreationMilliseconds, $gmtOffset, $customerIdFromHeader, $customerAcctNumber, $externalTransactionId, $serviceId, $transactionDate, $transactionTime, $validity, $entityType, $extSource, $notificationName, $notificationStatus, $score1, $score2, $score3, $userData01, $userData02, $userData03, $userData04, $userData05, $userData06, $userData07, $userData08, $userData09, $userData10, $userData11, $userData12, $userData13, $userData14, $userData15, $userData16, $userData17, $userData18, $userData19, $userData20, $userData21, $userData22, $userData23, $userData24, $userData25, $userData26, $userData27, $userData28, $userData29, $userData30, $userData31, $userData32, $userData33, $userData34;
printf "%-16.16s%-8.8s%-5.5s%-16.16s%-8.8s%-6.6s%-3.3s%-6.6s%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s\n", $workflow, $recordType, $dataSpecificationVersion, $clientIdFromHeader, $recordCreationDate, $recordCreationTime, $recordCreationMilliseconds, $gmtOffset, $customerIdFromHeader, $customerAcctNumber, $externalTransactionId, $serviceId, $transactionDate, $transactionTime, $validity, $entityType, $extSource, $notificationName, $notificationStatus, $score1, $score2, $score3, $userData01, $userData02, $userData03, $userData04, $userData05, $userData06, $userData07, $userData08, $userData09, $userData10, $userData11, $userData12, $userData13, $userData14, $userData15, $userData16, $userData17, $userData18, $userData19, $userData20, $userData21, $userData22, $userData23, $userData24, $userData25, $userData26, $userData27, $userData28, $userData29, $userData30, $userData31, $userData32, $userData33, $userData34;

# Print Footer Line
#printf XFEED "%s%-2099.2099s%s%s%s%s%-08.8d\n", $RECORD_TYPE_FOOTER, $DATA_FEED_SORT_KEY_FOOTER, $FILLER, $LAYOUT_VER, $FILETYPE, $SYSTEM_ID, $RECORD_COUNT_FOOTER;
printf "%s%-2099.2099s%s%s%s%s%-08.8d\n", $RECORD_TYPE_FOOTER, $DATA_FEED_SORT_KEY_FOOTER, $FILLER, $LAYOUT_VER, $FILETYPE, $SYSTEM_ID, $RECORD_COUNT_FOOTER;
#close (XFEED);

