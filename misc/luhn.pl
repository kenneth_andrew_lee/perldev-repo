#!/usr/bin/perl
#
#

use strict;
use warnings;

use Algorithm::LUHN qw(check_digit is_valid);

my $pan = shift(@ARGV) || die("Usage: $0 <pan>\n");

print "Valid Pan:$pan\n" if is_valid($pan) and length($pan)==16;

my $len = length $pan;
if ($len == 16) {
	print "checkDigit:", check_digit(substr($pan,0,15)), "\n";	
} else {
	print "checkDigit:", check_digit($pan), "\n";	
}








__END__
my $number = shift(@ARGV) || die("Usage: $0 <number>\n");
$number=~s,[^0–9],,g;
my($sum,$odd);
foreach my $n (reverse split(//,$number)) {
        $odd=!$odd;
        if($odd) {
                $sum+=$n;
        } else {
                my $x=2*$n;
                $sum+=$x>9?$x–9:$x;
        }
}
my $ok = 0+(($sum%10)==0);
exit(($sum%10)!=0);
