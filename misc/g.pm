package g;
use strict;
use warnings;

sub new {
	my ($class, %attr) = @_;
	my $ref = \%attr;
	bless $ref, $class;
	return $ref;
}

sub stars {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

sub numStars {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}
sub screen {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}
sub speed {
	my $self = shift;
	$self->{x} = shift if @_;
	return $self->{x};
}

1;