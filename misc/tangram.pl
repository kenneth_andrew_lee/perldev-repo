#!/usr/bin/perl
use strict;
use warnings;

run (4,5);

my $TEMP_COUNTER;


sub run{
	my ($row, $column) = @_;

	my $solution = [];
	try ($row, $column, $solution);
	
}

sub try {
	my ($row, $column, $solution) = @_;

	print_solution(($row*$column), $solution);
	 #if unique($solution);
	#	exit if $TEMP_COUNTER++ > 15;
	#} else {
	#	for my $column ( 0 .. $max_cell) {
	#		next if attacked ($solution, $row, $column);
	#		my $new_solution = copy($solution);
	#		put_queen_at($new_solution, $row, $column);
	#		try($max_cell, $new_solution, $row + 1);
	#	}
	#}
}

sub unique {
	return 1;
}

sub attacked {
	return 0;
}

sub put_queen_at {
	my ($solution, $row, $column) = @_;
	$solution->[$row][$column] = 'Q';
}

sub copy {
	my $solution = shift;

	my @rows;
	push @rows, [ @$_ ] for @$solution;
	return \@rows;
}

sub print_solution {
	my ($max_cell, $solution) = @_;

	for my $row (@$solution) {
		for my $column (0 .. $max_cell) {
			print $row->[$column] ? " Q " : " . ";
		}
		print "\n";
	}
	print "\n"; # Extra blank line for spaxing
}


exit();

__END__

1. 4 total pieces that will fit into a 4 x 5 grid - 20 total positions.
2. pieces have defined shapes:
      (1)  (2)   (3) (4)
      xx   xxx   xx   x
     xx     x     x   x
      x     x     xx  xx
                      x

3. Pieces can be rotated (turn), flipped, shift (up, down, left, right).

Goal.  Write a program that will solve for every posible position, and list 
all solutions.  A solution is where a piece does not overlap within the 4 x 5 
grid, and stays within the grid.
