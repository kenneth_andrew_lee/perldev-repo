#!/usr/bin/perl

use strict;
use warnings;
use feature qw( say );

sub find_solution_bfs {
    my ($passages, $entrance, $exit) = @_;
    $passages = { %$passages };  # Make a copy so we don't clobber caller's.

    my @todo = ( [ $entrance ] );
    while (@todo) {
        my $path = shift(@todo);
        my $here = $path->[-1];
        return @$path if $here == $exit;

        my $passages_from_here = delete($passages->{$here});
        push @todo,
            map { [ @$path, $_ ] } 
               grep { $passages->{$_} }  # Keep only the unvisited.
                    @$passages_from_here;
    }

    return;
}


{
    my %passages = ( 1 => [6, 2], ..., 20 => [15] );
    my $entrance = 2;
    my $exit = 5;
    if ( my @solution = find_solution_bfs(\%passages, $entrance, $exit)) {
        say "@solution";
    } else {
        say "No solution.";
    }
}