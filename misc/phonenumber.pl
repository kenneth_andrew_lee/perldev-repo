#!/usr/bin/perl
use strict;
use warnings;

# Given a phone number give all possible phone numbers.  
# Use BFS to solve.


my %hashmap = { 2=>"abc",
                3=>"def",
                4=>"ghi",
                5=>"jkl",
                6=>"mno",
                7=>"prs",
                8=>"tuv",
                9=>"wxy"}

sub lettercombinations {
    my digits = unshift;
    my @list = ();
    if (undef digits) || length digits == 0) {
        return @list;
    }
    my @templist = ();
    for (my i = 0; i < digits.length; i++) {   
        option = $hashmap{digits}
        push(@temp,
}




    l.add("");
 
    for (int i = 0; i < digits.length(); i++) {
        ArrayList<String> temp = new ArrayList<>();
        String option = map.get(digits.charAt(i));
 
        for (int j = 0; j < l.size(); j++) {
            for (int p = 0; p < option.length(); p++) {
                temp.add(new StringBuilder(l.get(j)).append(option.charAt(p)).toString());
            }
        }
 
        l.clear();
        l.addAll(temp);
    }
 
    return l;






public List<String> letterCombinations(String digits) {
    HashMap<Character, String> map = new HashMap<>();
    map.put('2', "abc");
    map.put('3', "def");
    map.put('4', "ghi");
    map.put('5', "jkl");
    map.put('6', "mno");
    map.put('7', "pqrs");
    map.put('8', "tuv");
    map.put('9', "wxyz");
 
    List<String> l = new ArrayList<>();
    if (digits == null || digits.length() == 0) {
        return l;
    }
 
    l.add("");
 
    for (int i = 0; i < digits.length(); i++) {
        ArrayList<String> temp = new ArrayList<>();
        String option = map.get(digits.charAt(i));
 
        for (int j = 0; j < l.size(); j++) {
            for (int p = 0; p < option.length(); p++) {
                temp.add(new StringBuilder(l.get(j)).append(option.charAt(p)).toString());
            }
        }
 
        l.clear();
        l.addAll(temp);
    }
 
    return l;
}























public class Solution {
    public List<String> letterCombinations(String digits) {
        List<String> res = new ArrayList<>();
        if (digits == null || digits.length() == 0) return res;
        String[] keyboard = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        res.add("");
        for (int i = 0; i < digits.length(); i++) {
            int curt = digits.charAt(i) - '0';
            while (res.get(0).length() == i) {
                String temp = res.remove(0);
                for (char c : keyboard[curt].toCharArray()) {
                    res.add(temp + c);
                }
            }
        }
        return res;
    }
}







public List<String> letterCombinations(String digits) {
    HashMap<Character, String> map = new HashMap<>();
    map.put('2', "abc");
    map.put('3', "def");
    map.put('4', "ghi");
    map.put('5', "jkl");
    map.put('6', "mno");
    map.put('7', "pqrs");
    map.put('8', "tuv");
    map.put('9', "wxyz");
 
    List<String> l = new ArrayList<>();
    if (digits == null || digits.length() == 0) {
        return l;
    }
 
    l.add("");
 
    for (int i = 0; i < digits.length(); i++) {
        ArrayList<String> temp = new ArrayList<>();
        String option = map.get(digits.charAt(i));
 
        for (int j = 0; j < l.size(); j++) {
            for (int p = 0; p < option.length(); p++) {
                temp.add(new StringBuilder(l.get(j)).append(option.charAt(p)).toString());
            }
        }
 
        l.clear();
        l.addAll(temp);
    }
 
    return l;
}