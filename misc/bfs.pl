#!perl
 
use strict;
use warnings;
 
sub find_path_between {
    my ( $start, $end, $graph ) = @_;
 
    return () unless defined $start && defined $end;
 
    my @path;     # Path so far
    my @queue;    # Vertices still to visit.
    my %seen;     # Vertices already seen.
    my $found;    # Whether we have found the wanted vertex.
    my $st = {};  # Spanning tree, used to find paths.
 
    if ( $start eq $end ) {
        push @path, $start;
        return @path;
    }
 
    push @queue, $start;
    $seen{$start}++;
 
    while (@queue) {
        my $v         = shift @queue;
        my $neighbors = get_neighbors( $v, $graph );
 
        for my $neighbor (@$neighbors) {
            next if $seen{$neighbor};
            _st_add( $v, $neighbor, $st );
            if ( $neighbor eq $end ) {
                $found++;
                @path = _st_walk( $start, $end, $st );
                return @path;
            }
            else {
                push @queue, $neighbor;
            }
            $seen{$neighbor}++;
        }
    }
    return $found ? @path : ();
}
 
sub _st_walk {
    my ( $start, $end, $st ) = @_;
 
    my @path;
 
    push @path, $end;
    my $prev = $st->{$end}->{prev};
    while (1) {
        if ( $prev eq $start ) {
            push @path, $start;
            last;
        }
        push @path, $prev;
        $prev = $st->{$prev}->{prev};
        next;
    }
    return reverse @path;
}
 
sub _st_add {
    my ( $vertex, $neighbor, $st ) = @_;
    $st->{$neighbor}->{prev} = $vertex;
}
 
sub get_neighbors {
    my ( $k, $graph ) = @_;
 
    my $index = _find_index( $k, $graph );
 
    if ( defined $index ) {
        return $graph->[$index]->[1];
    }
    else {
        return;
    }
}
 
sub _find_index {
    my ( $wanted, $graph ) = @_;
 
    # Naive linear search, for now.
    my $i = 0;
    for my $elem (@$graph) {
 
        # Definedness check here is necessary because we delete
        # elements from the graph by setting the element's index to
        # undef.  In other words, some graph indices can be undef.
        if ( defined $elem->[0] && $elem->[0] eq $wanted ) {
            return $i;
        }
        $i++;
    }
    return;
}
 
sub main {
    # $graph is an AoA (Array of Arrays)
    my $graph = [
        [ 's', [ 'a', 'd' ] ],
        [ 'a', [ 's', 'b', 'd' ] ],
        [ 'b', [ 'a', 'c', 'e' ] ],
        [ 'c', ['b'] ],
        [ 'd', [ 's', 'a', 'e' ] ],
        [ 'e', [ 'b', 'd', 'f' ] ],
        [ 'f', ['e'] ]
    ];
 
    my $start = 's';
    my $end   = 'c';
 
    my @path = find_path_between( $start, $end, $graph );
 
    print qq[Path from '$start' to '$end' is: @path\n];
 
    # Find a second path.
    $end  = 'f';
    @path = find_path_between( $start, $end, $graph );
    print qq[Path from '$start' to '$end' is: @path\n];
}
 
main();