#!/usr/bin/perl

use strict;
use warnings;
use feature qw( say );

sub find_solution_bfs {
    my ($passages, $entrance, $exit) = @_;

    my @todo = ( [ $entrance ] );
    while (@todo) {
        my $path = shift(@todo);
        my $here = $path->[-1];
        return @$path if $here == $exit;

        my %seen = map { $_ => 1 } @$path;
        push @todo,
            map { [ @$path, $_ ] } 
                grep { !$seen{$_} }
                    @{ $passages->{$here} };
    }

    return;
}

{
    my %passages = ( 1 => [6, 2], ..., 20 => [15] );
    my $entrance = 2;
    my $exit = 5;
    if ( my @solution = find_solution_bfs(\%passages, $entrance, $exit)) {
        say "@solution";
    } else {
        say "No solution.";
    }
}