#!/usr/bin/perl

use strict;
use warnings;
use Time::HiRes qw( time );


# Program Starts here
my $start  = time();

# time without memoization
print 'factorial(100) = ', factorial(100);
my $end     = time();
my $runtime = sprintf( "%.16s", ( $end - $start ) / 1000 );
print "factiorial took $runtime ms to execute\n\n";


exit(0);

sub factorial {
  my ($n)=@_;
  if ($n == 0) {
    return 1; #[by the convention that 0! = 1]
  } else {
    return factorial($n - 1) * $n; # [recursively invoke factorial with the parameter 1 less than n]
  }
}


__DATA__
