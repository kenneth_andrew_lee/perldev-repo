use 5.010;
running_sum( 5, 6 );
running_sum( 1..3 );
running_sum( 4 );
sub running_sum {
state $sum = 0;
state @numbers;
foreach my $number ( @_ ) {
push @numbers, $number;
$sum += $number;
}
say "The sum of (@numbers) is $sum";
}



#$fred = 3;
#$barney = 4;
#$wilma = &sum_of_fred_and_barney; # $wilma gets 7
#print "\$wilma is $wilma.\n";
#$betty = 3 * &sum_of_fred_and_barney; # $betty gets 21
#print "\$betty is $betty.\n";
#
#sub sum_of_fred_and_barney {
#	print "Hey, you called the sum_of_fred_and_barney subroutine!\n";
#	$fred + $barney; # That's the return value
#}





#foreach (1..10) { # Uses $_ by default
#	print "I can count to $_!\n";
#}
#
#
#foreach $rock (qw/ bedrock slate lava /) {
#	print "One rock is $rock.\n"; # Prints names of three rocks
#}