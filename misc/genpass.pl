#!/usr/bin/perl
use strict;
use warnings;

use App::Genpass;

my $generator = App::Genpass->new(
                number    => 5,
                readable  => 0,
                special   => 1,
                specials  =>  [ '!', '#', '$' ],
                verify    => 1,
                length => 14,
);

my @list = $generator->generate();

foreach ( @list ) {
    print "$_\n";
}

# specials  =>  [ '!', '@', '#', '$', '%', '^', '&', '*', '(', ')' ],