#!/usr/bin/perl
use strict;
use warnings;

use LWP::Simple;                # From CPAN
use JSON;
use Data::Dumper;               # Perl core module

# From API documentation from woot: http://api.woot.com/2
# GET http://api.woot.com/2/events.json?site=www.woot.com&eventType=Daily&key={yourkey}
#
# site=www.woot.com electronics.woot.com computers.woot.com home.woot.com tools.woot.com sport.woot.com accessories.woot.com kids.woot.com shirt.woot.com wine.woot.com sellout.woot.com
#  Daily Moofi Reckoning WootOff WootPlus
#
#my @sites = qw(www.woot.com electronics.woot.com computers.woot.com home.woot.com tools.woot.com sport.woot.com accessories.woot.com kids.woot.com shirt.woot.com wine.woot.com sellout.woot.com);
#my @eventTypes = qw(Daily Moofi Reckoning WootOff WootPlus);

# Use a small subset for testing, adding additional array elements to test additional functionality and debug.
my @sites = qw(www.woot.com);
my @eventTypes = qw(Daily WootPlus);

# hash to store key we will search.  In XML, this would be the name of a branch or leaf, not sure what it is called in JSON.
my %title;
my %title1;


#my $wooturl = "http://api.woot.com/2/events.json?key=59b2672e52314d818728bab165c575b0";
#foreach (@sites) {
#	$wooturl .= "&site=" . $_
#}
#
#foreach (@eventTypes) {
#	$wooturl .= "&eventType=" . $_
#}
#
#print $wooturl, "\n\n";

my $wooturl = "http://api.woot.com/2/events.json?key=59b2672e52314d818728bab165c575b0";
foreach (@sites) {
	$wooturl .= "&site=" . $_
}

foreach (@eventTypes) {
	$wooturl .= "&eventType=" . $_
}

$wooturl .= "&select=Title";


# 'get' is exported by LWP::Simple; install LWP from CPAN unless you have it.
# You need it or something similar (HTTP::Tiny, maybe?) to get web pages.
my $json = get($wooturl);
die "Could not get $wooturl!" unless defined $json;

print $json, "\n\n";

# Decode the entire JSON
my $data = decode_json($json);

#for my $report ( @{$data} ) {
foreach  my $report (@$data) {
	#print $report->{Title}, "\n";
	$title{$report->{Title}}++; 
}

# print distinct list
 while( my ($k, $v) = each %title ) {
        print "key: $k, value: $v.\n";
}

print "\n\n";

$wooturl = "http://api.woot.com/2/events.json?key=59b2672e52314d818728bab165c575b0";
$json = get($wooturl);
#print $json, "\n\n";
$data = decode_json($json);

foreach  my $report (@$data) {
	$title1{$report->{Title}}++; 
}

# print distinct list
 while( my ($k, $v) = each %title1 ) {
        print "key: $k, value: $v.\n";
}


