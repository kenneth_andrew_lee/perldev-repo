#!/usr/bin/perl
 
use strict;
use warnings;
 
use Time::HiRes qw(usleep);
use Curses;

my $screen_x;
my $screen_y;

my $window = Curses->new;
getmaxyx(stdscr,$screen_x, $screen_y);


system('stty sane');  # reset the environment after using ncurses.
__DATA__ 

#!/usr/bin/perl
 
use strict;
use warnings;
 
use Time::HiRes qw(usleep);
use Curses;
 
my $numstars = 100;
my $screen_x;
my $screen_y;
my $max_speed = 4;
 
my $screen = Curses->new;
getmaxyx(stdscr,$screen_x, $screen_y);

noecho;
curs_set(0);
 
my @stars;
 
foreach my $i (1 .. $numstars) {
  push @stars, {
    x => rand($screen_x),
    y => rand($screen_y),
    s => rand($max_speed) + 1,
  };
}
 
while (1) {
  $screen->clear;
 
  foreach my $star (@stars) {
    $star->{x} -= $star->{s};
    $star->{x} = $screen_x if $star->{x} < 0;
 
    $screen->addch($star->{y}, $star->{x}, '.');
  }
 
  $screen->refresh;
  usleep 50000;
}

********************************************************************************
SCALE=2000L;
   Star->Loc.x=g.Screen.x / 2-SCALE + random(SCALE * 2);
   Star->Loc.y=g.Screen.y / 2-SCALE + random(SCALE * 2);
   Star->Size=random(10)==9?2:1;

   do
      {
       Star->Dir.x=SCALE*10-random(SCALE*20);
       Star->Dir.y=SCALE*10-random(SCALE*20);
      }
   while (Star->Dir.x==0&&Star->Dir.y==0);
********************************************************************************
   for (Star=&g.Stars[g.NumStars - 1];Star>=g.Stars;Star--){
      DrawStar(Star,BLACK);

      Star->Loc.x += Star->Dir.x*g.Speed/30;
      Star->Loc.y += Star->Dir.y*g.Speed/30;

      if (Star->Loc.x<0 || Star->Loc.x>=g.Screen.x || Star->Loc.y<0 ||Star->Loc.y>=g.Screen.y){
         NewStar(Star);
      }
      else{
         DrawStar(Star,Star->Color);
         }
      }
********************************************************************************