#!/usr/bin/perl

use strict;
use warnings;
use Chart::Gnuplot;
 

# Create chart object and specify the properties of the chart
my $chart = Chart::Gnuplot->new(
    output => "simple.png",
    title  => "Simple testing",
    xlabel => "My x-axis label",
    ylabel => "My y-axis label",
    bg     => {
            color   => "#000000",
            density => 0.0,
        },    
);


# Data1 data
my @x = (-10 .. 10);
my @y = (0 .. 20);
 
 
# Create dataset object and specify the properties of the dataset
my $data1 = Chart::Gnuplot::DataSet->new(
    xdata => \@x,
    ydata => \@y,
    title => 'Arrays of x-values and y-values',
);


my @points = (
    [-7, 7],
    [-6, 6],
    [-5, 5],
    [-4, 4],
    [-3, 3],
    [-2, 4],
    [-1, 5],
    [ 0, 6],
    [ 1, 7],
    [ 2, 8],
);
my $data2 = Chart::Gnuplot::DataSet->new(
    points => \@points,
    title  => 'Array of x-y pairs',
);


 
# Plot the data set on the chart
$chart->plot2d($data1, $data2);
 
