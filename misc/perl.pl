use DateTime::Format::Strptime qw( );

my $arg = shift or die "takes one arg";
if ( scalar @ARGV ne 1 ) { die "required parameter missing $!\n"; }

my $format = DateTime::Format::Strptime->new(
    pattern   => '%m/%d/%Y %I:%M:%S %p',
    time_zone => 'local',
    on_error  => 'croak',
);

my $dt = $format->parse_datetime($abbreviation);

my $filename = 'something';
open my $fh, '<', $filename or die "Couldn't open file: $!\n";
while (<$fh>) {

    # do something
}
close $fh;

#Replacement for Tie::File

    use strict;
use warnings;

use File::Copy 'move';

my $infile = $ARGV[0];

move( $infile, "$infile.bak" );

open my $inhandle,  '<', "$infile.bak";
open my $outhandle, '>', $infile;

while ( my $line = <$inhandle> ) {
    if ( $line =~ /something/ ) {
        $line = 'somethingelse';
    }
    print $outhandle $line;
}

