#!/usr/bin/perl
#
#
use strict;
use warnings;

use POSIX qw( strftime );

my $ts = 1441058175442;
my $formated = strftime("%Y-%m-%d %H:%M:%S", localtime($ts/1000));
print "forced epoch date = " . $ts . "\n";
print "formatted =  $formated\n";
print "localtime = ", scalar localtime($ts/1000),"\n\n";


my @months = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
my ($sec, $min, $hour, $day,$month,$year) = (localtime($ts/1000))[0,1,2,3,4,5]; 

# You can use 'gmtime' for GMT/UTC dates instead of 'localtime'
$ts = (time - 125*86400)*1000;
print "epoch time (milli) for 125 Days ago: ", $ts, "\n";
print "Unix time (no milli) " . $ts/1000 . " converts to " . $months[$month] . " " . $day . ", " . ($year+1900);
print " " . $hour . ":" . $min . ":" . $sec . "\n\n";

# Go back to Now
$ts = (time)*1000;
print "time = ",time, "\n";
print "epoch time (milli) for now: ", $ts, "\n";

($sec, $min, $hour, $day,$month,$year) = (localtime($ts/1000))[0,1,2,3,4,5]; 
print "Unix time (no milli) " . $ts/1000 . " converts to " . $months[$month] . " " . $day . ", " . ($year+1900);
print " " . $hour . ":" . $min . ":" . $sec . "\n\n";

my $fmt_time = strftime("%Y%m%d", localtime(time));
print "\$fmt_time = $fmt_time\n\n";