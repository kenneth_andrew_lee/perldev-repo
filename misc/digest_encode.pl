###################################################
# Simple MD5 encode
#!/usr/bin/perl
use strict;
#use MIME::Base64 qw( encode_base64 );
use Digest::MD5 qw(md5 md5_hex md5_base64);
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

if (scalar @ARGV ne 1) {die "required parameter missing $!\n";}

#print "md5 Digest for $ARGV[0] is ", md5($ARGV[0]), "\n";
print "md5_hex Digest for $ARGV[0] is ", md5_hex($ARGV[0]), "\n";
print "md5_base64 Digest for $ARGV[0] is ", md5_base64($ARGV[0]), "\n";

print "sha1 Digest for $ARGV[0] is ", sha1($ARGV[0]), "\n";
print "sha1_hex Digest for $ARGV[0] is ", sha1_hex($ARGV[0]), "\n";
print "sha1_base64 Digest for $ARGV[0] is ", sha1_base64($ARGV[0]), "\n";
