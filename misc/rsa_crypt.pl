#!/usr/bin/perl
use strict;
use warnings;

use Crypt::OpenSSL::RSA;

my $key_string = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC7m/25l4MpLIh6VBegQhCfsTsNfTOORQ+5cQrf2gM0vHATRlpQHZfc1+16U9Hr0+B02lBlugV0B8Gktg6uDzPbHUxAWCPUEazzNONAkF9KPjw9A7haXakERpekac7g3K49ElkG3TGRu+JTAuJDXmLA4KE5xgom1+vQsK8LeQ38pedidofRfLf/s18c7Tpa55UlW5NRoZdVAgahm+Qnkx30lHvWpVgisLElkIE64PGswrAnuNtDsFQTRQEqjiio1f6INZhI4s0JdVcrtG8rIGlCVIwi19eWNSibz9xGnIez7NsYZ/lU6kF0zffzdOPbk9anyaj5m7pghg+lQUQwiN0T';
my $rsa = Crypt::OpenSSL::RSA->new_public_key($key_string);

print "private key is:\n", $rsa->get_private_key_string();
print "public key (in PKCS1 format) is:\n", $rsa->get_public_key_string();
print "public key (in X509 format) is:\n", $rsa->get_public_key_x509_string();

exit(0);

__DATA__

$rsa_pub->use_sslv23_padding(); # use_pkcs1_oaep_padding is the default
$ciphertext = $rsa->encrypt($plaintext);

$rsa_priv = Crypt::OpenSSL::RSA->new_private_key($key_string);
$plaintext = $rsa->encrypt($ciphertext);

$rsa = Crypt::OpenSSL::RSA->generate_key(1024); # or
$rsa = Crypt::OpenSSL::RSA->generate_key(1024, $prime);

print "private key is:\n", $rsa->get_private_key_string();
print "public key (in PKCS1 format) is:\n", $rsa->get_public_key_string();
print "public key (in X509 format) is:\n", $rsa->get_public_key_x509_string();

$rsa_priv->use_md5_hash(); # use_sha1_hash is the default
$signature = $rsa_priv->sign($plaintext);
print "Signed correctly\n" if ($rsa->verify($plaintext, $signature));




__DATA__
use Crypt::RSA;
#use Crypt::RSA::Key::Private;
#use Crypt::RSA::Key::Public;

my $rsa = new Crypt::RSA; 

#   my ($public, $private) = 
#       $rsa->keygen ( 
#           Identity  => 'Lord Macbeth <macbeth@glamis.com>',
#           Size      => 1024,  
#           Password  => 'A day so foul & fair', 
#           Verbosity => 1,
#       ) or die $rsa->errstr();



#"Cigital","/PA1sLbT2ywfle","kenlee@visa.com","StupidTraining!","https://elearning.cigital.com/login/signup.php"
my $message="this is a simple test";
print $message,"\n";


#my $public = 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC7m/25l4MpLIh6VBegQhCfsTsNfTOORQ+5cQrf2gM0vHATRlpQHZfc1+16U9Hr0+B02lBlugV0B8Gktg6uDzPbHUxAWCPUEazzNONAkF9KPjw9A7haXakERpekac7g3K49ElkG3TGRu+JTAuJDXmLA4KE5xgom1+vQsK8LeQ38pedidofRfLf/s18c7Tpa55UlW5NRoZdVAgahm+Qnkx30lHvWpVgisLElkIE64PGswrAnuNtDsFQTRQEqjiio1f6INZhI4s0JdVcrtG8rIGlCVIwi19eWNSibz9xGnIez7NsYZ/lU6kF0zffzdOPbk9anyaj5m7pghg+lQUQwiN0T';

#my $public;
#my $pubkey = 'C:\Users\kenlee\.ssh\id_rsa.pub';
#open FILE, "$pubkey" or die "Couldn't open file: $!"; 
#while (<FILE>){
#    $public .= $_;
#}
#close FILE;


#my $public;
#open(PUB,$pubkey) || die "$pubkey: $!";
#read(PUB,$public,-s PUB); # Suck in the whole file
#close(PUB);

#my $public = new Crypt::RSA::Key::Public (
#    Filename => 'C:/Users/kenlee/.ssh/id_rsa.pub'
#);

#my $publickey = new Crypt::RSA::Key::Public ();
#my $public = $publickey->read(Filename => 'C:/Users/kenlee/.ssh/id_rsa.pub');

my $public = Crypt::RSA::Key::Public->read( Filename => 'C:/Users/kenlee/.ssh/id_rsa.pub' );


#my $private = new Crypt::RSA::Key::Private (
#    Filename => 'C:/Users/kenlee/.ssh/id_rsa'
#);


#my $private;
#my $privatekey = 'C:\Users\kenlee\.ssh\id_rsa';
#open FILE, "$privatekey" or die "Couldn't open file: $!"; 
#while (<FILE>){
#    $private .= $_;
#}
#close FILE;


my $cyphertext = 
    $rsa->encrypt ( 
        Message    => $message,
        Key        => $public,
        Armour     => 1,
    ) || die $rsa->errstr();

print $cyphertext,"\n";

exit(0);
__DATA__
my $plaintext = 
    $rsa->decrypt ( 
        Cyphertext => $cyphertext, 
        Key        => $private,
        Armour     => 1,
    ) || die $rsa->errstr();

print $plaintext,"\n";

my $signature = 
    $rsa->sign ( 
        Message    => $message, 
        Key        => $private
    ) || die $rsa->errstr();

print $signature,"\n";

my $verify = 
    $rsa->verify (
        Message    => $message, 
        Signature  => $signature, 
        Key        => $public
    ) || die $rsa->errstr();

print $verify,"\n";

exit(0);
