#!/usr/bin/perl

#Uncomment to get full debug info
use LWP::Debug qw(+ -conns);
use LWP::Simple;
require LWP::Parallel::UserAgent;
require HTTP::Request;


#URLs must end with a /
@urls = (
"http://www.yahoo.com/",
"http://216.239.39.111/",
#"http://www.newmediamusings.com/"
);

#Array where content is going to be placed
@results;


$timeout = 1; # each request times out after 1 seconds
grab(@urls);

# This prints the contents of the page, be aware are not the same as the URLs
# but with index.html, or even with an ending /

#print $results{'http://www.fuckedcompany.com/'};
#print $results{'http://www.digitaldeliverance.com/'};
#print $results{'http://www.andrewtobias.com/'};
print $results{'http://www.fuckedcompany.com/'};
print $results{'http://216.239.39.111/'};

sub grab
{
   @results;

   $ua = LWP::Parallel::UserAgent->new();
   $ua->agent("MS Internet Explorer");
   $ua->redirect (0); # prevents automatic following of redirects
   $ua->max_hosts(5); # sets maximum number of locations accessed in parallel
   $ua->max_req  (5); # sets maximum number of parallel requests per host
   $ua->remember_failures(1);


  foreach $url (@_)
  {
        $dom=$url;
        $dom =~ m|(\w+)://([^/:]+)(:\d+)?/(.*)|;
        $results{$1."://".$2."/"}="timeout";

       $ua->register(HTTP::Request->new(GET => $url), \&callback);
  }

  $ua->wait ( $timeout );

  return %results;

}

sub callback
{
        my($data, $response, $protocol) = @_;
        #Comment this line to prevent show the url
        print $response->base."\n";

        $url=$response->base;
        $url =~ m|(\w+)://([^/:]+)(:\d+)?/(.*)|;

        if ($results{$1."://".$2."/"} eq "timeout")
        {
                $results{$1."://".$2."/"}="";
        }
        $results{$1."://".$2."/"}.=$data;

        return;
}