use strict;
use warnings;
use 5.010;
use Math::BigInt;
 
my @fibo = (Math::BigInt->new(1), Math::BigInt->new(1));
for my $f (@fibo) {
	say $f;
	#print "$f\n";
	push @fibo, Math::BigInt->new($fibo[-1]+$fibo[-2]);

	#last if $f > 500000000000000000000000000000000000000000000000000000000000000000000000000;
	last if $f > 100 ** 100
}
 
#say "@fibo";
