#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use DBD::Oracle qw(:ora_types);

my $dbh = DBI -> connect('dbi:Oracle:host="192.168.7.80";sid="ktpn";port=1521"', 'cwl', 'cwl', { RaiseError => 1, AutoCommit => 1 }) || die "Error while connecting to database : $! \n";

my $sth = $dbh->prepare(q{BEGIN CATEGORYLIST1(:p_my_cursor); END;});
my $my_cursor;
print "Executed upto this point"."\n";
$sth->bind_param_inout(":p_my_cursor", \$my_cursor, 0, { ora_type => ORA_RSET });
$sth->execute;
$sth->finish;

my @row;
while( @row = $my_cursor->fetchrow_array) {
        foreach $_ (@row) {
                print $_;
                print "\n";
        }
}
$my_cursor->finish;
$dbh->disconnect;

