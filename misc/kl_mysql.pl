use strict;
use DBI;
#my $db = DBI->connect( "dbi:mysql:database=db:host=pegasus", "bestseller", "bestseller" )
my $db = DBI->connect( "dbi:mysql:db:192.168.2.65", "bestseller", "bestseller" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;
my $SEL = "select rank, description from bestsellers where date(capture_date) = (select date(max(capture_date)) from bestsellers)";
my $sth = $db->prepare($SEL);
$sth->execute();
my $nf = $sth->{NUM_OF_FIELDS};
print "This statement returns $nf fields\n";
for ( my $i = 0; $i < $nf; $i++ ) {
    my $name = $sth->{NAME}[$i];
    my $type = $sth->{TYPE}[$i];
    my $prec = $sth->{PRECISION}[$i];
    my $scle = $sth->{SCALE}[$i];
    my $tn=$db->type_info($type)->{TYPE_NAME};
    print
        "Field number $i: name $name of type $tn with precision $prec,$scle\n";
}

print "\nReturning Data Here\n";
while ( my @row = $sth->fetchrow_array() ) {
    foreach (@row) {
        $_ = "\t" if !defined($_);
        print "$_\t";
    }
    print "\n";
}
print "\n\nThis query returned " . $sth->rows . " rows.\n";


# Collect and display the value for capture_date
$SEL = "select date(max(capture_date)) from bestsellers";
$sth = $db->prepare($SEL);
$sth->execute();
print "\nReturning Date Here\n";
while ( my @row = $sth->fetchrow_array() ) {
    foreach (@row) {
        $_ = "\t" if !defined($_);
        print "$_\t";
    }
    print "\n";
}
 


END {
    $db->disconnect if defined($db);
}

