#!/usr/bin/perl
use strict;
use warnings;

my $year;
my $month;
my $day;

use Date::Calc qw(Add_Delta_Days);
($year, $month, $day) = Add_Delta_Days(2013,2,1,-1);

print "$year $month $day\n";

printf qq{%d\n}, time/86400;