#!/usr/bin/perl
use strict;
use warnings;

#my $cash = 12; Test Case 1
#my $cash = 10; Test Case 2
#my $cash = 8;  Test Case 3

my $cash = 400000;
my $principal = 100000;
my $interest_rate = 7; # %
my $term = 20; # Years
my $total_paid = $principal * (1 + $interest_rate/100) ** $term;
#$total_paid = 10;  # Test Case

print "Value of principal + interest after ", $term, " years = ", $total_paid, "\n";
print "10% larger than \$total_paid = ", $total_paid * 1.1, "\n";
print "\$total_paid = ", $total_paid, "\n";
print "\$cash = ", $cash, "\n";

if ($cash > $total_paid * 1.1) {
	print "Approved", "\n";
} elsif ($cash >= $total_paid) {
	print "Marginal", "\n";
} else {
	print "Unacceptable", "\n";
}


# Modify this program for printing loan payments as follows:
# 
#     Declare a variable $cash and initialize it to some number.
#     Instead of printing out the total loan payment, test the value of $cash 
#     against the value of $total_paid to see whether there is enough cash to 
#     pay off the loan:
#         If $cash is more than 10% larger than the value of $total_paid, print "Approved."
#         If $cash is between 0% and 10% larger than $total_paid, print "Marginal."
#         If $cash is less than $total_paid, print "Unacceptable."
# 
# When you finish, hand in your program.
# 
# 
# 
# Here are the files you handed in:
# perl1/lesson03/evaluate2.pl
# 
# Overall Comments:
# 
# Thanks for getting this in to me, though this version is still only making the 
# comparison to 10% of $total_paid rather than 10% MORE: $cash > $total_paid * .1 
# So if $total_paid is 10, then $total_paid *.1 is only going to be 1. Thus 
# because we know that *.1 will give us 10% then we can add back $total_paid 
# to make 110%: $cash > $total_paid + $total_paid * .1; Though this is more 
# typing then is really needed as the same result can be had by *1.1 because 
# 1*$total_paid is $total_paid: $cash > $total_paid * 1.1; There is also an 
# issue with your elsif but I will leave that one for you to sort out though I 
# will give you three test cases so you can check your code: 
# if $cash is 12 and $total_paid is 10 : Approved 
# if $cash is 10 and $total_paid is 10 : Marginal 
# if $cash is 8 and $total_paid is 10 : Unacceptable 
# If you have any questions on this one please email me via the 
# messages tab in CodeRunner. -Ben 

