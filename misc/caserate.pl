#!/usr/bin/perl
use strict;
use warnings;

my (%caserate, %hour);
my @clients;

while ( <DATA> ) {
	chomp;
	if (/\Ahour/) {
		@clients = split /,/;
		shift @clients;  # Get rid of the first entry which is hour
	} else {
		my ($hour, $value) = split /,/;
		$caserate{$hour} = $value;
	}
}

print "@clients\n";


while ( my ($hours, $value) = each %caserate ) {
	my $year = substr $hours,0,4;
	my $month = substr $hours,4,2;
	my $day = substr $hours,6,2;
	my $hour = substr $hours,9,2;

	print "$hours : $year : $month : $day : $hour : $value\n";
}

__DATA__
hour,FALU0FF
20131003_00,1356
20131003_01,1113
20131003_02,981
20131003_03,738
20131003_04,775
20131003_05,918
20131003_06,993
20131003_07,1154
20131003_08,1426
20131003_09,1865
20131003_10,2238
20131003_11,2025
20131003_12,2092
20131003_13,2096
20131003_14,1996
20131003_15,2056
20131003_16,2034
20131003_17,2061
20131003_18,2011
20131003_19,1854
20131003_20,1748
20131003_21,1735
20131003_22,1570
20131003_23,1665