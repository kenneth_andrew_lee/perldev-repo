use Algorithm::FastPermute ('permute');
my @array = (1..shift());
permute {
	print "@array\n"; # Print all the permutations
} @array;
