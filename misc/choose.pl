#!/usr/bin/perl
#choose.pl

sub choose {
     my ($n, $k) = @_;
     my ($result, $j) = (1, 1);

     return 0 if $k > $n || $k < 0;
     $k = ($n - $k) if ($n - $k) < $k;

     while ( $j <= $k ) {
        $result *= $n--;
        $result /= $j++;
     }
     return $result;
}

#example:
#print choose(5,3);
print "choose(2,1):",choose(2,1),"\n";
print "choose(2,2):",choose(2,2),"\n";
print "choose(4,1):",choose(4,1),"\n";
print "choose(4,2):",choose(4,2),"\n";
print "choose(4,4):",choose(4,4),"\n";
print "choose(5,2):",choose(5,2),"\n";
print "choose(6,4):",choose(6,4),"\n";
print "choose(52,5):",choose(52,5),"\n";
