#!/usr/bin/perl
use strict;
use warnings;

my $dir = '.';
opendir my $dh, $dir or die "Couldn't open $dir: $!\n";
while ( my $file_name = readdir $dh ) {
	if ($file_name =~ /SENT_(\w{4}\d{8}\.TXT)/) {
		#next if $file_name =~ /\A\.\.?\z/;
		#print "$dir: $file_name\n";
		#rename $file_name, substr $file_name, 4;
		my $new_name = substr $file_name, 5;
		#print "$dir: $new_name\n";
		rename $file_name, $new_name;
	}
}
closedir $dh;
