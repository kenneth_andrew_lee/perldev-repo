#!/usr/bin/env perl
use strict;
use warnings;
 
use Net::LDAP;
my $server = "localhost:389";
my $ldap = Net::LDAP->new( $server ) or die $@;
$ldap->bind;
 
my $result = $ldap->search(
    base   => "dc=visa,dc=com",
    filter => "(objectClass=groupOfNames)",
);
#filter => "(&(cn=Jame*) (sn=Woodw*))",
 
die $result->error if $result->code;
 
printf "COUNT: %s\n", $result->count;
 
foreach my $entry ($result->entries) {
    $entry->dump;
}
print "===============================================\n";
 
foreach my $entry ($result->entries) {
    #printf "%s %s <%s>\n",
    #    $entry->get_value("givenName"),
    #    $entry->get_value("sn"),
    #    ($entry->get_value("mail") || '');
}
 
$ldap->unbind;

__END__
ssh into sl73ffldapd001, then tunnel to get values.  Trying to use server 
name from workstation does not work.

For LDAP Browser- 
Base DN- dc=visa,dc=com
port- 389
host – localhost ( Have tunneling set on 389)
Directory Admin - cn=FalconAdmin,cn=Administrators,cn=dscc 
DirectoryAdmin Password- falc0n
