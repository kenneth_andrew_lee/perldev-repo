#!/usr/bin/perl
#
# scrub.pm
#
#






	private static final int maxWidth = 6;
	private static final int ENCRYPT = 1;
	private static final int DECRYPT = 0;
	private static final Logger LOG = Logger.getLogger(Scrub.class);
	private static final boolean traceEnabled = LOG.isTraceEnabled();
	private static final boolean debugEnabled = LOG.isDebugEnabled();
	private int[][] substTable, inverseTable;

	public Scrub(String keyFile) {
		init(keyFile);
	}

	public String encrypt(String pan, int binWidth) {
		if (debugEnabled) {
			LOG.debug("Encrypt PAN " + pan + ", binWidth = " + binWidth);
		}

		return process(pan, binWidth, ENCRYPT, 1, true);
	}

	public String encrypt(String pan, int binWidth, int breaker, boolean validate) {
		if (debugEnabled) {
			LOG.debug("Encrypt PAN " + pan + ", binWidth = " + binWidth);
		}

		return process(pan, binWidth, ENCRYPT, breaker, validate);
	}

	public String decrypt(String pan, int binWidth) {
		if (debugEnabled) {
			LOG.debug("Decrypt PAN " + pan + ", binWidth = " + binWidth);
		}

		return process(pan, binWidth, DECRYPT, 0, true);
	}

	public String decrypt(String pan, int binWidth, boolean validate) {
		if (debugEnabled) {
			LOG.debug("Decrypt PAN " + pan + ", binWidth = " + binWidth);
		}

		return process(pan, binWidth, DECRYPT, 0, validate);
	}

	public static int checkLuhn(String pan) {
		int len = pan.length();
		int pos = len;
		int sum = 0;

		for (int i = 0; i < len; i++) {
			pos--;
			int n = pan.charAt(pos) - '0';
			if ((i & 1) != 0)
				sum += n / 5 + (n % 5) * 2;
			else
				sum += n;
		}

		return sum % 10;
	}

	private void init(String keyFile) {
		try {
			LOG.info("Initializing Scrub");
			if (debugEnabled) {
				LOG.debug("key file: " + keyFile);
			}

			if (Files.exists(Paths.get(keyFile))) {
				readKey(keyFile);
			} else {
				LOG.info("key file " + keyFile + " does not exist");
				newKey(keyFile);
			}

			LOG.info("Scrub has been initialized");
		} catch (Exception e) {
			LOG.error("Scrub failed to initialize", e);
			throw new RuntimeException("Scrub failed to initialize", e);
		}
	}

	private void readKey(String keyFile) throws IOException, NumberFormatException {
		LOG.info("Reading the substitution tables from the key file");
		if (debugEnabled) {
			LOG.debug("key file: " + keyFile);
		}

		BufferedReader br = Files.newBufferedReader(Paths.get(keyFile), StandardCharsets.US_ASCII);

		substTable = new int[maxWidth][];
		inverseTable = new int[maxWidth][];

		for (int i = 0; i < maxWidth; i++) {
			int sz = (int) Math.round(Math.pow(10, i + 1));
			substTable[i] = new int[sz];

			for (int j = 0; j < sz; j++) {
				substTable[i][j] = Integer.parseInt(br.readLine());
			}

			inverseTable[i] = new int[sz];

			for (int j = 0; j < sz; j++) {
				int val = substTable[i][j];
				inverseTable[i][val] = j;
			}

			if (debugEnabled) {
				LOG.debug("Loaded substTable and inverseTable for width " + (i + 1));
			}
		}

		br.close();

		LOG.info("Loaded all substitution tables");
	}

	private void newKey(String keyFile) throws IOException {
		LOG.info("Generating new substitution tables");
		if (debugEnabled) {
			LOG.debug("key file: " + keyFile);
		}

		Path keyPath = Paths.get(keyFile);
		BufferedWriter bw = Files.newBufferedWriter(keyPath, StandardCharsets.US_ASCII);

		substTable = new int[maxWidth][];
		inverseTable = new int[maxWidth][];

		SecureRandom random = new SecureRandom();

		for (int i = 0; i < maxWidth; i++) {
			int sz = (int) Math.round(Math.pow(10, i + 1));
			substTable[i] = new int[sz];

			for (int j = 0; j < sz; j++) {
				substTable[i][j] = j;
			}

			for (int j = 0; j < sz; j++) {
				int k = (int) (((double) random.nextInt() - (double) Integer.MIN_VALUE)
						/ (((double) Integer.MAX_VALUE - (double) Integer.MIN_VALUE + 1.0) / sz));
				int t = substTable[i][j];
				substTable[i][j] = substTable[i][k];
				substTable[i][k] = t;
			}

			for (int j = 0; j < sz; j++) {
				bw.write(substTable[i][j] + "\n");
			}

			inverseTable[i] = new int[sz];

			for (int j = 0; j < sz; j++) {
				int val = substTable[i][j];
				inverseTable[i][val] = j;
			}

			if (debugEnabled) {
				LOG.debug("Generated substTable and inverseTable for width " + (i + 1));
			}
		}

		bw.close();
		Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		Files.setPosixFilePermissions(keyPath, perms);

		LOG.info("All substitution tables have been written to the key file");
	}

	private String makeValidLuhn(String pan) {
		int checksum = checkLuhn(pan);
		LOG.info("checksum = " + checksum);
		if (checksum == 0)
			return pan;
		else
			return pan.substring(0, pan.length() - 1) + Integer.toString(10 - checksum);
	}

	private String makeInvalidLuhn(String pan, int breaker) {
		int checksum = (checkLuhn(pan) + 10 - breaker) % 10;

		if (checksum == 0)
			return pan;
		else
			return pan.substring(0, pan.length() - 1) + Integer.toString(10 - checksum);
	}

	private String cmc(String s, int t, int mode) {
		int m, p0width;
		int sLength = s.length();
		if (sLength % maxWidth == 0) {
			m = sLength / maxWidth;
			p0width = maxWidth;
		} else {
			m = sLength / maxWidth + 1;
			p0width = sLength % maxWidth;
		}

		if (traceEnabled) {
			LOG.trace("s = " + s);
			LOG.trace("t = " + t);
			LOG.trace("sLength = " + sLength);
			LOG.trace("m = " + m);
			LOG.trace("p0width = " + p0width);
		}

		int[] width = new int[m];
		int[] p = new int[m];
		int[] ppp = new int[m];
		int[] ccc = new int[m];
		int[] c = new int[m];

		switch (mode) {
		case ENCRYPT:
			width[0] = p0width;
			for (int i = 1; i < m; i++) {
				width[i] = maxWidth;
			}
			break;
		case DECRYPT:
			for (int i = 0; i < m - 1; i++) {
				width[i] = maxWidth;
			}
			width[m - 1] = p0width;
			break;
		default:
			LOG.error("unknown CMC mode " + mode);
			throw new RuntimeException("unknown CMC mode " + mode);
		}

		int pos = 0;
		for (int i = 0; i < m; i++) {
			p[i] = Integer.parseInt(s.substring(pos, pos + width[i]));
			pos += width[i];

			if (traceEnabled) {
				LOG.trace("width[" + i + "] = " + width[i]);
				LOG.trace("p[" + i + "] = " + p[i]);
			}
		}

		ppp[0] = blockFunction(p[0], width[0], mode);
		if (traceEnabled) {
			LOG.trace("ppp[0] = " + ppp[0]);
		}

		for (int i = 1; i < m; i++) {
			ppp[i] = blockFunction(add(p[i], ppp[i - 1], width[i]), width[i], mode);

			if (traceEnabled) {
				LOG.trace("ppp[" + i + "] = " + ppp[i]);
			}
		}

		int mc = blockFunction(add(ppp[m - 1], t, width[m - 1]), width[m - 1], mode);
		int tmp = add(ppp[0], -mc, p0width);
		int mask = add(tmp, tmp, p0width);
		int mp = add(ppp[0], mask, width[0]);

		ccc[0] = add(mc, mask, width[m - 1]);

		if (traceEnabled) {
			LOG.trace("mc = " + mc);
			LOG.trace("mask = " + mask);
			LOG.trace("mp = " + mp);
			LOG.trace("ccc[0] = " + ccc[0]);
		}

		for (int i = 1; i < m - 1; i++) {
			ccc[i] = add(ppp[m - i - 1], mask, width[m - 1 - i]);

			if (traceEnabled) {
				LOG.trace("ccc[" + i + "] = " + ccc[i]);
			}
		}
		ccc[m - 1] = add(blockFunction(mp, width[0], mode), -t, width[0]);

		c[0] = blockFunction(ccc[0], width[m - 1], mode);

		if (traceEnabled) {
			LOG.trace("ccc[" + (m - 1) + "] = " + ccc[m - 1]);
			LOG.trace("c[0] = " + c[0]);
		}

		for (int i = 1; i < m; i++) {
			c[i] = add(blockFunction(ccc[i], width[m - 1 - i], mode), -ccc[i - 1], width[m - 1 - i]);

			if (traceEnabled) {
				LOG.trace("c[" + i + "] = " + c[i]);
			}
		}

		StringBuilder r = new StringBuilder();
		for (int i = 0; i < m; i++) {
			int cLen = 0;
			for (int v = c[i]; v > 0; cLen++) {
				v /= 10;
			}
			for (int j = 0; j < width[m - 1 - i] - cLen; j++) {
				r.append('0');
			}
			r.append(c[i]);
		}

		return r.toString();
	}

	private int add(int a, int b, int n) {
		int adj = (b < 0) ? 10 : 0;
		int c = 0;
		int m = 1;
		int a1 = a;
		int b1 = b;

		for (int i = 0; i < n; i++) {
			int c1 = (a1 % 10) + (b1 % 10) + adj;
			if (c1 >= 10)
				c1 -= 10;
			c += c1 * m;
			a1 /= 10;
			b1 /= 10;
			m *= 10;
		}

		if (traceEnabled) {
			LOG.trace(a + " + " + b + " = " + c + " (n = " + n + ")");
		}

		return c;
	}

	private int blockFunction(int input, int width, int mode) {
		int output = 0;

		switch (mode) {
		case ENCRYPT:
			output = substTable[width - 1][input];
			if (traceEnabled) {
				LOG.trace("ENCRYPT " + input + " >>> " + output);
			}
			break;
		case DECRYPT:
			output = inverseTable[width - 1][input];
			if (traceEnabled) {
				LOG.trace("DECRYPT " + input + " >>> " + output);
			}
			break;
		default:
			LOG.error("unknown mode " + mode);
			throw new RuntimeException("unknown mode " + mode);
		}

		return output;
	}

	private String process(String pan, int binWidth, int mode, int breaker, boolean validate) {
		String s;
		int sLength;

		if (validate) {
			s = pan.trim();

			if (!s.matches("[0-9]+")) {
				LOG.error("the input must contain only digits: " + s);
				throw new RuntimeException("the input must contain only digits: " + s);
			}

			if (binWidth < 0) {
				LOG.error("the length of BIN cannot be negative: " + binWidth);
				throw new RuntimeException("the length of BIN cannot be negative: " + binWidth);
			}

			sLength = s.length();
			if (binWidth > sLength) {
				LOG.error("the length of BIN (" + binWidth + ") cannot exceed the length of PAN (" + sLength + ")");
				throw new RuntimeException(
						"the length of BIN (" + binWidth + ") cannot exceed the length of PAN (" + sLength + ")");
			}

			if ((mode == ENCRYPT) && (checkLuhn(s) != 0)) {
				LOG.error("the input fails MOD10 check, the checksum is " + checkLuhn(s));
				throw new RuntimeException("the input fails MOD10 check, the checksum is " + checkLuhn(s));
			}
		} else {
			s = pan;
			sLength = s.length();
		}

		int tweak;
		String bin, out;

		if (binWidth > 0) {
			bin = s.substring(0, binWidth);
			tweak = Integer.parseInt(bin) % 888887;
		} else {
			bin = new String("");
			tweak = 0;
		}

		String in = s.substring(binWidth, sLength - 1);

		int inLen = in.length();
		if (debugEnabled) {
			LOG.debug("bin: " + bin);
			LOG.debug("tweak: " + tweak);
			LOG.debug("in: " + in);
			LOG.debug("inLen: " + inLen);
		}
		if (inLen <= 6) {
			int sign = (mode == ENCRYPT) ? 1 : -1;
			int inInt = Integer.parseInt(in);
			int tmp = add(blockFunction(inInt, inLen, mode), sign * tweak, inLen);
			int outInt = blockFunction(tmp, inLen, mode);
			String fmt = "%0" + Integer.toString(inLen) + "d";
			out = String.format(fmt, outInt);
		} else {
			out = cmc(in, tweak, mode);
		}

		if (debugEnabled) {
			LOG.debug("out: " + out);
		}

		if ((mode == DECRYPT) || (breaker == 0)) {
			return makeValidLuhn(bin + out + '0');
		} else {
			return makeInvalidLuhn(bin + out + '0', breaker);
		}
	}
}
