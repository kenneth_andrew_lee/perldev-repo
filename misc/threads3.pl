#!/usr/bin/perl
# prime-pthread, courtesy of Tom Christiansen
use strict;
use warnings;

use threads;
use Thread::Queue;

sub check_num {
    my ($upstream, $cur_prime) = @_;
    my $kid;
    my $downstream = Thread::Queue->new();
    while (my $num = $upstream->dequeue()) {
        next unless ($num % $cur_prime);
        if ($kid) {
            $downstream->enqueue($num);
        } else {
            print("Found prime: $num\n");
            $kid = threads->create(\&check_num, $downstream, $num);
            if (! $kid) {
                warn("Sorry.  Ran out of threads.\n");
                last;
            }
        }
    }
    if ($kid) {
        $downstream->enqueue(undef);
        $kid->join();
    }
}

my $stream = Thread::Queue->new(3..1000, undef);
check_num($stream, 2);


__END__
This program uses the pipeline model to generate prime numbers. Each thread in the pipeline has an input queue that feeds numbers to be checked, a prime number that it's responsible for, and an output queue into which it funnels numbers that have failed the check. If the thread has a number that's failed its check and there's no child thread, then the thread must have found a new prime number. In that case, a new child thread is created for that prime and stuck on the end of the pipeline.
This probably sounds a bit more confusing than it really is, so let's go through this program piece by piece and see what it does. (For those of you who might be trying to remember exactly what a prime number is, it's a number that's only evenly divisible by itself and 1.)

The bulk of the work is done by the "check_num()" subroutine, which takes a reference to its input queue and a prime number that it's responsible for. After pulling in the input queue and the prime that the subroutine is checking (line 11), we create a new queue (line 13) and reserve a scalar for the thread that we're likely to create later (line 12).

The while loop from line 14 to line 26 grabs a scalar off the input queue and checks against the prime this thread is responsible for. Line 15 checks to see if there's a remainder when we divide the number to be checked by our prime. If there is one, the number must not be evenly divisible by our prime, so we need to either pass it on to the next thread if we've created one (line 17) or create a new thread if we haven't.

The new thread creation is line 20. We pass on to it a reference to the queue we've created, and the prime number we've found. In lines 21 through 24, we check to make sure that our new thread got created, and if not, we stop checking any remaining numbers in the queue.

Finally, once the loop terminates (because we got a 0 or "undef" in the queue, which serves as a note to terminate), we pass on the notice to our child, and wait for it to exit if we've created a child (lines 27 and 30).

Meanwhile, back in the main thread, we first create a queue (line 33) and queue up all the numbers from 3 to 1000 for checking, plus a termination notice. Then all we have to do to get the ball rolling is pass the queue and the first prime to the "check_num()" subroutine (line 34).

That's how it works. It's pretty simple; as with many Perl programs, the explanation is much longer than the program.



