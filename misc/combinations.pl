use strict;
use warnings;

#my $strings = [qw(AAA BBB CCC DDD EEE)];
my $strings = [qw(A B C D E)];

#sub combin;

print "@$_\n" for combine($strings, 3);


# requires a reference to a list and the number of combinations 
sub combine {
  my ($list, $n) = @_;
  die "Insufficient list members" if $n > @$list;

  return map [$_], @$list if $n <= 1;

  my @comb;

  for (my $i = 0; $i+$n <= @$list; ++$i) {
    my $val  = $list->[$i];
    my @rest = @$list[$i+1..$#$list];
    push @comb, [$val, @$_] for combine(\@rest, $n-1);
  }

  return @comb;
}