#!/bin/perl

use DateTime;
use DateTime::TimeZone;

my $tz = DateTime::TimeZone->new( name => 'America/Denver' );

my $dt = DateTime->now();
my $offset = $tz->offset_for_datetime($dt);

print "$tz, $dt, $offset\n\n";