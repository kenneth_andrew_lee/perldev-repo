#!/usr/bin/perl
use strict;
use warnings;

use DBI;
use DBD::Oracle qw(:ora_types);

# Pull parameters off of command line
my $database = 'DBI:Oracle:FALDBQ1';
my $username = 'dpsvru';
my $password = 'dpsvru';
my $in_phone = '3033897993';
my $in_schema = 'FALUCALL';
my $in_prc = 'PRCC95';
my $curref;

# connection to the database
my $dbh = DBI->connect($database, $username, $password) or die "Can't make database connect: $DBI::errstr\n";    
my $sth_cursor;
my $sth = $dbh -> prepare("begin nuance.get_account_from_phone_sp(:in_phone,:in_schema,:in_prc,:curref); end;");

$sth->bind_param(":in_phone", $in_phone);
$sth->bind_param(":in_schema", $in_schema);
$sth->bind_param(":in_prc", $in_prc);
$sth->bind_param_inout(":curref", \$sth_cursor, 0, {ora_type => ORA_RSET });
$sth->execute();

### Retrieve the returned rows of data
my @row;
while(@row = map { defined($_) ? $_ : ' *** No Data *** '} $sth_cursor->fetchrow_array()) {
	print "Row: @row\n";
}
warn "Data fetching terminated early by error: $DBI::errstr\n" if $DBI::err;


# Close Connection
$dbh->disconnect();
