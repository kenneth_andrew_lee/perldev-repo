use strict;
use warnings;
use feature qw( say );

sub find_solution_dfs {
    my ($passages, $entrance, $exit) = @_;

    my @todo = ( [ $entrance ] );
    while (@todo) {
        my $path = shift(@todo);
        my $here = $path->[-1];
        return @$path if $here eq $exit;

        my %seen = map { $_ => 1 } @$path;
        unshift @todo,
            map { [ @$path, $_ ] } 
                grep { !$seen{$_} }
                    @{ $passages->{$here} };
    }

    return;
}

{
    my %passages = ( start => ['A', 'b'], A => ['c', 'b', 'end'], b => ['d', 'end'] );
    my $entrance = 'start';
    my $exit = 'end';
    if ( my @solution = find_solution_dfs(\%passages, $entrance, $exit)) {
        say "@solution";
    } else {
        say "No solution.";
    }
}

__END__
start-A
start-b
A-c
A-b
b-d
A-end
b-end
