#!/usr/bin/perl
use strict;
use warnings;

run (8);

my $TEMP_COUNTER;


#exit();

#__END__


sub run{
	my $size = shift;

	my $solution = [];
	my $row = 0;
	try ($size-1, $solution, $row);
	
}

sub try {
	my ($max_cell, $solution, $row) = @_;

	if ($row > $max_cell) {
		print_solution($max_cell, $solution) if unique($solution);
		exit if $TEMP_COUNTER++ > 15;
	} else {
		for my $column ( 0 .. $max_cell) {
			next if attacked ($solution, $row, $column);
			my $new_solution = copy($solution);
			put_queen_at($new_solution, $row, $column);
			try($max_cell, $new_solution, $row + 1);
		}
	}
}

sub unique {
	return 1;
}

sub attacked {
	return 0;
}

sub put_queen_at {
	my ($solution, $row, $column) = @_;
	$solution->[$row][$column] = 'Q';
}

sub copy {
	my $solution = shift;

	my @rows;
	push @rows, [ @$_ ] for @$solution;
	return \@rows;
}

sub print_solution {
	my ($max_cell, $solution) = @_;

	for my $row (@$solution) {
		for my $column (0 .. $max_cell) {
			print $row->[$column] ? " Q " : " . ";
		}
		print "\n";
	}
	print "\n"; # Extra blank line for spaxing
}