#!/usr/bin/perl

# use strict;
# use warnings;

use Data::Dumper;
use POSIX qw(strftime);
use Time::HiRes qw/gettimeofday/;

# Generating unique random hexadecimal number using sha1_hex.
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

# Getting input from the user
print "Enter the starting number for generation :\n";
my $x = <STDIN>;
chomp $x;

print "Enter the total number of data records to be generated :\n";
my $n = <STDIN>;
chomp $n;

print "Enter the number of data records to be generated in a file:\n";
my $m = <STDIN>;
chomp $m;

$c = ($x + $m);
my $rec_cnt = 0;
# my $x = 1;
my $y = 1;

while ( $x <= $n ) 
{

# my $rec_cnt = 0;
open (FH,'+>',"EXT$y.txt") || die "file not found :$!";     

# Print Header Line
printf FH "B000000000%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $rec_cnt; 
&datagen();
# Print the footer.
printf FH "E999999999%-1599.1599s2.0EXT10   PMAX  %-08.8d\n", '' x 1599, $rec_cnt; 
close (FH);

$command = "touch EXT$y.txt.EXT10; EXT$y touch status 2>&1";
system (`$command`);

$y++;
$c = ($c + $m);
$x = ($x + $m);
$rec_cnt = 0;

}

sub datagen {

# $rec_cnt = 0;
for ( my $i = $x; $i < $c; $i++ ) {

# Generating 40bytes hex values.
my $digest = sha1_hex($i);

# Generating random values.

$current_time = strftime "%H%M%S", localtime; 
$gmt_date = strftime "%Y%m%d", gmtime;
$gmt_time = strftime "%H%M%S", gmtime;
$recordCreationDate = $gmt_date;
$recordCreationTime = $gmt_time;

# calculate millisecond upto two decimal places(centiSeconds)
my $ms = substr(gettimeofday, 11,2); 
$recordCreationMilliseconds = $ms;

# calculate gmtoffset
$gmt_offset_diff = (substr($gmt_time,0,2) - substr($current_time,0,2));

if ($gmt_offset_diff > 12) {
$gmtOffset = $gmt_offset_diff - 24;
}
else {
$gmtOffset = "-$gmt_offset_diff\n";
}

$transactionDate = strftime "%Y%m%d", localtime;
$transactionTime = strftime "%H%M%S", localtime;



# Initialising values to the variable function.
    $customerIdFromHeader = int(rand(10**15-1))+(10**15-1);
    $clientIdFromHeader = 'PRC365';
                $customerAcctNumber = $digest;
                $serviceId = $digest;
                $validity = '####';
                $userData05 = '$$$$';
                $userData33 = "This is a test message for External message feed version 1.0 for Falcon 6.4. The user_data_33 field contains the 256 byte lenght for the message associated with the particular case linked to the PAN.";
                $userData34 = "This is a test message for External message feed version 1.0 for Falcon 6.4. The user_data_34 field contains the 256 byte lenght for the message associated with the particular case linked to the PAN.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
# Formatting the output print.
  printf FH "DUD17DF26        EXT10   1.0  %-16.16s%-8.8s%-6.6s%02d0%06.2f%-20.20s%-40.40s%-32.32s%-19.19s%-8.8s%-6.6s%-4.4s%-4.4s%-48.48s%-48.48s%-10.10s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-4.4s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-8.8s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-16.16s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-32.32s%-64.64s%-64.64s%-64.64s%-64.64s%-64.64s%-255.255s%-255.255s\n",
  $clientIdFromHeader, $recordCreationDate, $recordCreationTime, $recordCreationMilliseconds,
  $gmtOffset, $customerIdFromHeader, $customerAcctNumber, $externalTransactionId, $serviceId, $transactionDate, $transactionTime, $validity,
  $entityType, $extSource, $notificationName, $notificationStatus, $score1, $score2, $score3, $userData01, $userData02, $userData03, $userData04,
  $userData05, $userData06, $userData07, $userData08, $userData09, $userData10, $userData11, $userData12, $userData13, $userData14, $userData15,
  $userData16, $userData17, $userData18, $userData19, $userData20, $userData21, $userData22, $userData23, $userData24, $userData25, $userData26, 
  $userData27, $userData28, $userData29, $userData30, $userData31, $userData32, $userData33, $userData34;
  $rec_cnt++;
}
}
