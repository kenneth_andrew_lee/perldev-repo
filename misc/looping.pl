#!/usr/bin/perl
use strict;
use warnings;
#printf "%x %b %d %1\$c\n", $_, $_, $_ for 32..127;
printf "%4x  %15b  %5d \n", $_, $_, $_ for 0..32767;

# ffff  1111111111111111  65535