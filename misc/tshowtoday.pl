#!/usr/bin/perl

use strict;
use warnings;

use Switch;

my $val = time/86400%8;
my $result;

switch ($val) {
	case "0" { $result = 'FALT002B'}
	case "1" { $result = 'FALT002C'}
	case "2" { $result = 'FALT002D'}
	case "3" { $result = 'FALT002E'}
	case "4" { $result = 'FALT002F'}
	case "5" { $result = 'FALT002G'}
	case "6" { $result = 'FALT002H'}
	case "7" { $result = 'FALT002A'}
	case "8" { $result = 'FALT002B'}
	else { $result = 'INVALID'}
}

print "Today's rat file is $result\n";



__END__
#my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
#print "\$sec,\$min,\$hour,\$mday,\$mon,\$year,\$wday,\$yday,\$isdst = $sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst","\n";
#my $val = $yday % 8;

