#!/usr/bin/perl
use JSON;
use Switch;
use DBI;
use Data::Dumper;               # Perl core module

my %hash = ();
my $hash_ref = \%hash;
my $jsondata;


my $dbh = DBI->connect( "dbi:mysql:db:192.168.2.65", "bestseller", "bestseller" ) || die( $DBI::errstr . "\n" );
$dbh->{AutoCommit}    = 0;
$dbh->{RaiseError}    = 1;
$dbh->{ora_check_sql} = 0;
$dbh->{RowCacheSize}  = 16;

my $sth = $dbh->prepare(qq{
	select k, v from isbn_list where k = ?
});
$sth->execute('0060248122');
my @row = $sth->fetchrow_array();
my $jsondata = $row[1];
#$dbh->commit or die $db->errstr;
$sth->finish;
$dbh->disconnect if defined($dbh);

my $json_text = decode_json( $jsondata );

build_hash_from_json($hash_ref, $json_text);

print Dumper($hash_ref, $json_text), "\n";

sub build_hash_from_json {
	my $hash_ref = shift;
	my $jsontext = shift;
	if (!defined($jsontext)) {
		return;
	}
	my $ref_value = ref($json_text);

	switch ( $ref_value ) {
		case ('SCALAR') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
		case ('ARRAY') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('HASH') {
			print "\$jsontext is a ",ref($jsontext), "\n";
			my @keys =  keys %$json_text;
			for my $key (@keys) {
				#build_hash_from_json($hash_ref, $json_text->{$key});
				print "key = $key", "\n";
				#print "ref\(\$json_text->{$key}\) = ", ref($json_text->{$key}), "\n";
				#print "length\(ref\(\$json_text->{$key}\)\) = ", length(ref($json_text->{$key})), "\n";
				if (length(ref($json_text->{$key})) == 0) {
					print "\$json_text->{$key} = $json_text->{$key}","\n";
					# add value to hash ref
					#print "value = ", $json_text->{$key}, "\n";
					${$hash_ref}{$key} = $json_text->{$key};
				} else {
					print "ref\(\$json_text->{$key}\) = ", ref($json_text->{$key}), "\n";
					# call this routine, which is an endless loop.
					#build_hash_from_json($hash_ref, $json_text->{$key});
				}
			}
		}
    	case ('CODE') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('REF') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('GLOB') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('LVALUE') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('FORMAT') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('IO') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('VSTRING') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
    	case ('Regexp') {
			print "\$jsontext is a ",ref($jsontext), "\n";
		}
		else {
			print "\$jsontext is a ",ref($jsontext), "\n";
			print 'Dumper($jsontext = ', "Dumper($jsontext)";
		}
	}
	return;
}



__END__
#use JSON -support_by_pp;
#use JSON::Parse 'parse_json';

#print "\$json_text is a ",ref($json_text), "\n";



# print a list of hash keys
#my @keys =  keys %$json_text;
#for my $key (@keys) {
#	print "\$key = $key\n";
#}

## this is an array reference
#my $array_ref = $json_text->{data};
#print "\$json_text->{data} is a ",ref($json_text->{data}), "\n";


#sub my_test {
#	# this is a hash reference
#	my $json_text = decode_json( $jsondata );
#	print "\$json_text is a ",ref($json_text), "\n";
#
#	# this is an array reference
#	my $array_ref = $json_text->{data};
#	print "\$json_text->{data} is a ",ref($json_text->{data}), "\n";
#
#	for my $hash_ref ( @$array_ref ) {
#		# Now each item is a Hash reference
#		my @keys =  keys  %$hash_ref;
#		for my $key (@keys) {
#			if (ref($hash_ref->{$key})) { 
#				print "\$hash_ref->{$key} is a ",ref($hash_ref->{$key}), "\n";
#			} else {
#				print "\$hash_ref->{$key} = $hash_ref->{$key}", "\n";
#			}
#		}
#	}
#}

#while ( <DATA> ) {
#	chomp;
#	$jsondata .= $_;
#}



{
   "data" : [
      {
         "publisher_name" : "Harpercollins Childrens Books",
         "isbn10" : "0060248122",
         "language" : "",
         "publisher_text" : "Harpercollins Childrens Books",
         "title_latin" : "The Mrs. Piggle-Wiggle Treasury",
         "publisher_id" : "harpercollins_childrens_bo_a01",
         "notes" : "",
         "isbn13" : "9780060248123",
         "dewey_decimal" : "",
         "summary" : "",
         "physical_description_text" : "",
         "book_id" : "the_mrs_piggle_wiggle_treasury",
         "lcc_number" : "",
         "marc_enc_level" : "~",
         "title" : "The Mrs. Piggle-Wiggle Treasury",
         "subject_ids" : [
            "amazon_com_childrens_books_series_humorous_mrs_piggle_wiggle"
         ],
         "author_data" : [
            {
               "id" : "macdonald_betty",
               "name" : "MacDonald, Betty"
            },
            {
               "name" : "Knight, Hilary",
               "id" : "knight_hilary"
            }
         ],
         "dewey_normal" : "0",
         "title_long" : "",
         "urls_text" : "",
         "awards_text" : "",
         "edition_info" : "Hardcover; 1995-10-01"
      }
   ],
   "index_searched" : "isbn"
}