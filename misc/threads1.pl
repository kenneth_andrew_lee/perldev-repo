#!/usr/bin/perl
use strict;
use warnings;
use threads;
use Thread::Queue;

my $process_q = Thread::Queue -> new(1,2,3,4,5,6,7,8,9,10);

sub worker {
    while ( $process_q -> dequeue() ) {
        my $retVar=threads -> self() -> tid();
        return $retVar;
    }
}

$process_q -> end();

#start some threads
for ( 1..10 )    {
    my $tmp = threads -> create ( \&worker );
}

#Wait for threads to all finish processing.
foreach my $thr ( threads -> list() ) {
    my $sum=$thr -> join;
    print "sum=$sum\n";
}


__END__
sum=1
sum=2
