#!/usr/bin/perl

use strict;
use warnings;
use threads;
use Thread::Queue qw ( ); 

my $num_threads = 10; 

my $work_q = Thread::Queue -> new;
my $output_q = Thread::Queue -> new; 

sub worker {
   my $count = 0; 
   print "Worker: ", threads -> self -> tid, " started and waiting for input\n";
   while ( my $item = $work_q -> dequeue ) { 
      $output_q -> enqueue ( threads -> self -> tid. ": ". $count++. " items: $item");
       print "Processing: $item";
   }
   print "Worker: ", threads -> self -> tid, " exiting\n";
}

sub serialise_output {

   print "Serialiser waiting for things to print: ", threads -> self -> tid,"\n";
   while ( my $output_item = $output_q -> dequeue ) { 
     print $output_item,"\n";
   }
}

my @workers = map { threads -> create ( \&worker ) } 1..$num_threads; 
my $serialiser = threads -> create ( \&serialise_output ); 


open ( my $input, '<', 'log.txt' ) or die $!;
while ( my $line = <$input> ) {
  chomp ( $line ) ;
  $work_q -> enqueue ( $line ); 
}
close ( $input );

$work_q -> end; #tell the threads that's all of it, so they exit. 
$_ -> join for @workers; #wait for all the workers to exit. 

$output_q -> end; #need to wait for workers to finish, so we don't create race condition. 
$serialiser -> join;


print "All threads done\n";

__END__
# This is the data in log.txt
sum=1
sum=2
sum=3
sum=4
sum=5
sum=6
sum=7
sum=8
sum=9
sum=10