#!/usr/bin/perl
 
use strict;
use warnings;
 
use Time::HiRes qw(usleep);
use Curses;
my $numstars = 100;
my $screen_x = 80;
my $screen_y = 24;
my $max_speed = 4;
 
my $screen = Curses->new;
noecho;
curs_set(0);
 
my @stars;
 
foreach my $i (1 .. $numstars) {
  push @stars, {
    x => rand($screen_x),
    y => rand($screen_y),
    s => rand($max_speed) + 1,
  };
}
 
while (1) {
  $screen->clear;
 
  foreach my $star (@stars) {
    $star->{x} -= $star->{s};
    $star->{x} = $screen_x if $star->{x} < 0;
 
    $screen->addch($star->{y}, $star->{x}, '.');
  }
 
  $screen->refresh;
  usleep 50000;
}
