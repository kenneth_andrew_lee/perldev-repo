#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
$Data::Dumper::Terse = 1;        # don't output names where feasible
$Data::Dumper::Indent = 0;       # turn off all pretty print
use Clone qw(clone);



sub combine;
sub build_available;
sub printer;
sub loaddata;
sub print_structure;
sub find_next;

my %current=();
my @available=();
my @queue=();
my %next=();
my $elevator = 1;
my $stepcounter = 0;

my $file = "/home/klee/srccode/bitbucket-repo/adventofcode/2016/data/test-11.txt";
#my $file = "/home/klee/srccode/bitbucket-repo/adventofcode/2016/data/google-11.txt";

loaddata($file);
#printer();
#print_structure();
push @queue,%current;
build_available();


exit();


# pretty print %current
sub printer {
  my @keys = reverse sort keys %current;
  for (@keys) {
    print "e:".$_."|";

    for my $g ($current{$_}{G}) {
      print join(',', map{$_.' gen'}@$g);
    }
    print "|";
    for my $m ($current{$_}{M}) {
      print join(',', map{$_.' chip'}@$m);
    }
    print "|\n";
  }
}

# print structure of %current using Data::Dumper
sub print_structure {
  print Dumper(%current),"\n";
}

# load data from file into $current
sub loaddata {
  my ($f)=@_;
  open(my $fh, $f) or die("Could not open file.");

  for (<$fh>)  {
    chomp;
    my @floor = (m/The (\w+) floor/);
    my @gens = (m/a (\w+) generator/g);
    my @mc = (m/a (\w+)-compatible/g);
    my @nothing = (m/(contains nothing)/);
    
    my @b=qw(zero first second third fourth);
    my ($f) = grep { $b[$_] eq $floor[0]}(0 .. @b-1);
    
    $current{$f}{G}=\@gens;
    $current{$f}{M}=\@mc;
  }
  close($fh);
}



# fill @available with states that are valid
# @available is an array filed with structures 
#   matching %current
#
sub build_available() {
  if ($elevator==1) {
    # Can only go up
    # '2' => {'M' => [],'G' => ['hydrogen']};
    # '1' => {'M' => ['hydrogen','lithium'],'G' => []};

    #1. build list of all single items and all groups of two items on current floor.
    my $chip_ref = $current{$elevator}{M};
    my $gens_ref = $current{$elevator}{G};
    my @both = ();
    push @both,@$chip_ref;
    push @both,@$gens_ref;
    print "\@both:@both","\n";
    for my $item_ref (combine(\@both,1)) {
      my $temp_ref = clone(\%current);

      print "\@\$item_ref:@$item_ref", "\n";
      print "temp_ref:\n";
      print Dumper(%$temp_ref), "\n";
      #print "\$_:@$_","\n";
      #move the single item from current elevator level up 1
    }
    for (combine(\@both,2)) {
      my $temp_ref = clone(\%current);
      #print "\$_:@$_","\n";
    }


  } elsif ($elevator==4) {
    # can only go down
  } else {
    # must check both up and down modes
  }

}

sub find_next() {

}


# requires a reference to a list and the number of combinations 
sub combine {
  my ($list, $n) = @_;
  die "Insufficient list members" if $n > @$list;

  return map [$_], @$list if $n <= 1;

  my @comb;

  for (my $i = 0; $i+$n <= @$list; ++$i) {
    my $val  = $list->[$i];
    my @rest = @$list[$i+1..$#$list];
    push @comb, [$val, @$_] for combine(\@rest, $n-1);
  }

  return @comb;
}




=for test structure
'4' => {'M' => [],'G' => []};
'3' => {'M' => [],'G' => ['lithium']};
'2' => {'M' => [],'G' => ['hydrogen']};
'1' => {'M' => ['hydrogen','lithium'],'G' => []};
=cut


=for data structure
'4' => {'M' => [],'G' => []};
'3' => {'M' => ['thulium'],'G' => []};
'2' => {'M' => ['ruthenium','curium'],'G' => ['thulium','ruthenium','curium']};
'1' => {'M' => ['strontium','plutonium'],'G' => ['strontium','plutonium']};
=cut





=for comment
The first floor contains a strontium generator, a strontium-compatible microchip, a plutonium generator, and a plutonium-compatible microchip.
The second floor contains a thulium generator, a ruthenium generator, a ruthenium-compatible microchip, a curium generator, and a curium-compatible microchip.
The third floor contains a thulium-compatible microchip.
The fourth floor contains nothing relevant.
=cut

__DATA__
The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant.
