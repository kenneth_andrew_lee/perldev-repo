%HoH = (
    flintstones => {
        husband   => "fred",
        pal       => "barney",
    },
    jetsons => {
        husband   => "george",
        wife      => "jane",
        "his boy" => "elroy",  # Key quotes needed.
    },
    simpsons => {
        husband   => "homer",
        wife      => "marge",
        kid       => "bart",
    },
);

while ( ($family, $roles) = each %HoH ) {
    print "$family: ";
    while ( ($role, $person) = each %$roles ) {
        print "$role=$person ";
    }
    print "\n";
}



#%tgs = (
#    'articles' =>  {
#                       'vim' => '20 awesome articles posted',
#                       'awk' => '9 awesome articles posted',
#                       'sed' => '10 awesome articles posted'
#                   },
#    'ebooks'   =>  {
#                       'linux 101'    => 'Practical Examples to Build a Strong Foundation in Linux',
#                       'nagios core'  => 'Monitor Everything, Be Proactive, and Sleep Well'
#                   }
#);

#To access a single element from hash, do the following.
#$print $tgs{'ebooks'}{'linux 101'};
#for $key1 (keys %tgs) {
#	print "key1 = $key1\n";
#	for $key2 (keys %{$tgs{$key1}}) {
#		print "key2 = $key2\n";
#	}
#}




#%tgs = (
#    'top 5' =>  [ 'Best linux OS', 'Best System Monitoring', 'Best Linux Text editors' ],
#    '15 example' => [ 'rpm command', 'crontab command', 'Yum command', 'grep command' ],
#);
#
#To access all elements one by one, do the following.
#
#foreach my $key ( keys %tgs )  {
#    print "Articles in group $key are: ";
#    foreach ( @{$tgs{$key}} )  {
#        print $_;
#    }
#}


