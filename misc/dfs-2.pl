use strict;
use warnings;

use Graph::Undirected;
use Graph::Traversal::DFS;

my $graph = Graph::Undirected->new;


my %maze = (
    start => ['A', 'b'], 
    A => ['c', 'b', 'start', 'end'], 
    b => ['A', 'start', 'd', 'end'],
    c => ['A'],
    d => ['b'],
    end => ['A', 'b'],
);

for my $node (keys %maze) {
    $graph->add_edge($node, $_) for @{ $maze{$node} };
}

my $traversal = Graph::Traversal::DFS->new($graph,
    start => 'start',
    pre => sub {
        my ($v, $self) = @_;
        print "$v\n";
        $self->terminate if $v eq 'end';
    }
);

$traversal->dfs;