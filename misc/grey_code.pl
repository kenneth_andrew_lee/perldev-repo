#!/usr/bin/perl
use Data::Dumper;


sub gray {
  ($n) = @_;
    @set = (0..$n-1);
    
    ## Run a loop for printing all 2^n
    ## subsets one by one
    for ($i = 0; $i < (1 << $n); $i++) {
      print "{ ";

      ## Print current subset
      for ($j = 0; $j < $n; $j++) {
        ## (1<<j) is a number with jth bit 1
        ## so when we 'and' them with the
        ## subset number we get which numbers
        ## are present in the subset and which
        ## are not
        if (($i & (1 << $j)) > 0) {
          print $set[$j],+ " ";
        }
      }
      print "}","\n";
    }
    print 2**$n," sets", "\n";
}
print "\n";

gray(5);


=for results
{ }
{ 1 }
{ 2 }
{ 1 2 }
{ 3 }
{ 1 3 }
{ 2 3 }
{ 1 2 3 }
8 sets
=cut