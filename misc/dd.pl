#!/usr/bin/perl
#
#
#

$path_data="M35,7.333C19.168,7.333,6.334,20.167,6.334,36c0,15.833,12.834,28.667,28.666,28.667 c15.833,0,28.667-12.834,28.667-28.667C63.667,20.167,50.833,7.333,35,7.333z M53,27h-9v-1.791l-2.795,3.525l-1.285-1.816 L27.236,35H38v-1.922l6,2.244V32h9v9h-9v-3.323l-6,2.245V38H27.238l12.799,8.081l1.168-1.878L44,47.791V46h9v9h-9v-3.598 l-6.218-1.496l0.85-1.805L23,37.989V44H13V29h10v6.01l15.748-10.112l-0.965-1.804L44,21.597V18h9V27z";

# Split path string on alpha command character. Retain character using zero-width look ahead
for $cmd ( split(/(?=[[:alpha:]])/, $path_data) ) {

	# Add space between command character and parameters (zero-width look behind)
	# [used look behind, because look ahead placed command character after the space
	#  not sure why]
	$cmd =~ s/(?<=[[:alpha:]])/$1 /;

	# Use spaces instead of commas to separate parameters
	$cmd =~ s/\,/ /g;

	# Add space in front of negative parameters
	$cmd =~ s/\-/ \-/g;

	print "$cmd\n";
}
