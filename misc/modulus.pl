To check if a number $num is divisible by $divisor, use the modulus operator, %:

if ($num % $divisor) {
    # does not divide cleanly
} else {
    # does.
}