##Example 2a
use strict;
use DBI;
my $db = DBI->connect( "dbi:Oracle:LEE", "scott", "tiger" )
    || die( $DBI::errstr . "\n" );
$db->{AutoCommit}    = 0;
$db->{RaiseError}    = 1;
$db->{ora_check_sql} = 0;
$db->{RowCacheSize}  = 16;
my $SEL = "SELECT * FROM EMP";
my $sth = $db->prepare($SEL);
$sth->execute();
my $nf = $sth->{NUM_OF_FIELDS};
print "This statement returns $nf fields\n";
for ( my $i = 0; $i < $nf; $i++ ) {
    my $name = $sth->{NAME}[$i];
    my $type = $sth->{TYPE}[$i];
    my $prec = $sth->{PRECISION}[$i];
    my $scle = $sth->{SCALE}[$i];
    my $tn=$db->type_info($type)->{TYPE_NAME};
    print
        "Field number $i: name $name of type $tn with precision $prec,$scle\n";
}

print "\nReturning Data Here\n";
while ( my @row = $sth->fetchrow_array() ) {
    foreach (@row) {
        $_ = "\t" if !defined($_);
        print "$_\t";
    }
    print "\n";
}
print "\n\nThis query returned " . $sth->rows . " rows.\n";
 
END {
    $db->disconnect if defined($db);
}


#





## Example 2
#use strict;
#use DBI;
#my $db = DBI->connect( "dbi:Oracle:LEE", "scott", "tiger" )
#    || die( $DBI::errstr . "\n" );
#$db->{AutoCommit}    = 0;
#$db->{RaiseError}    = 1;
#$db->{ora_check_sql} = 0;
#$db->{RowCacheSize}  = 16;
#my $SEL = "SELECT * FROM EMP";
#my $sth = $db->prepare($SEL);
#$sth->execute();
# 
#while ( my @row = $sth->fetchrow_array() ) {
#    foreach (@row) {
#        $_ = "\t" if !defined($_);
#        print "$_\t";
#    }
#    print "\n";
#}
# 
#END {
#    $db->disconnect if defined($db);
#}



## Example 1
#use strict;
#use DBI;
#my $db = DBI->connect( "dbi:Oracle:LEE", "scott", "tiger" )
#    || die( $DBI::errstr . "\n" );
#$db->{AutoCommit}    = 0;
#$db->{RaiseError}    = 1;
#$db->{ora_check_sql} = 0;
#$db->{RowCacheSize}  = 16;
#my $SEL = "invalid SQL statement";
#my $sth = $db->prepare($SEL);
#print "If you see this, parse phase succeeded without a problem.\n";
#$sth->execute();
#print "If you see this, execute phase succeeded without a problem.\n";
#END {
#    $db->disconnect if defined($db);
#}



#for (my $test = 0; $test < 10; $test++) {
#	print "Hello World\n";
#}

#use DBI; 
#print $DBI::VERSION,"\n";
##1.623

#use DBD::Oracle; 
#print $DBD::Oracle::VERSION,"\n";
##1.56


##Example 4
#use strict;
#use DBI;
# 
## Note inclusion of DBD::Oracle data types. Without it, we cannot
## work with REF Cursor variables.
# 
#use DBD::Oracle qw(:ora_types);
# 
#my $db = DBI->connect( "dbi:Oracle:LEE", "scott", "tiger" )
#    || die( $DBI::errstr . "\n" );
#$db->{AutoCommit}    = 0;
#$db->{RaiseError}    = 1;
#$db->{ora_check_sql} = 0;
#$db->{RowCacheSize}  = 16;
# 
#my $dno=10;
#my $csr;
#my ($ename, $job, $sal, $hiredate);
# 
#my $SEL = "begin
#                               dbi.test(:DNO,:CSR);
#           end;";
# 
#my $sth = $db->prepare($SEL);
# 
# 
#$sth->bind_param_inout( ":DNO", \$dno, 20);
## Now comes the important part.....
#$sth->bind_param_inout( ":CSR", \$csr, 0, { ora_type => ORA_RSET } );
#$sth->execute();
#print "\nNow, we have a valid handle....\n\n";
## Observe that here we are fetching from csr, a handle which was
## prepared and executed by the DBI.TEST procedure.
#while ( ($ename, $job, $sal, $hiredate)=$csr->fetchrow_array()) {
#        write;
#}
#no strict;
#format STDOUT_TOP =
#Employee         Job        Hiredate       Salary
#----------------------------------------------------
#.
# 
#format STDOUT =
#@<<<<<<<<<<      @<<<<<<<   @<<<<<<<<<     @<<<<<<<
#$ename,          $job,      $hiredate,     $sal
#.
# 
#END {
#    $db->disconnect if defined($db);
#}



###Example 3
#use strict;
#use DBI;
#my $db = DBI->connect( "dbi:Oracle:LEE", "scott", "tiger" )
#                || die( $DBI::errstr . "\n" );
#$db->{AutoCommit}    = 0;
#$db->{RaiseError}    = 1;
#$db->{ora_check_sql} = 0;
#$db->{RowCacheSize}  = 16;
#my $SEL = "SELECT e.ename,e.job,d.dname,e.sal
#                      FROM   emp e, dept d
#                      WHERE e.deptno=d.deptno AND
#                                      e.empno=?";
#my $sth = $db->prepare($SEL);
# 
#print "Enter EMPNO:";
#my $empno = <STDIN>;
#chomp($empno);
# 
#$sth->bind_param( 1, $empno );
#$sth->execute();
#my ( $ename, $job, $dept, $sal ) = $sth->fetchrow_array();
#write if  $sth->rows>0;
# 
#no strict;

