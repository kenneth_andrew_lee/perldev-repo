#!/usr/bin/perl
use strict;
use warnings;

my $DATA_FILE = 'billing.csv';
my $fh;
open $fh, '>', $DATA_FILE or die "cant open $DATA_FILE";

my $date = 0;
my $year;
my $month;
my $day;
use Date::Calc qw(Add_Delta_Days);
while (<>) {
	chomp;
	if (substr($_,0,9) eq "DPSHEADER") {
		#$date = substr($_,36,8);
		$year = substr($_,36,4);
		$month = substr($_,40,2);
		$day = substr($_,42,2);
		#print "$year . $month . $day\n";
		($year, $month, $day) = Add_Delta_Days($year,$month,$day,-1);
		$date = sprintf("%04.4s%02.2s%02.2s",$year,$month,$day);
	}
	if (substr($_,0,6) eq "DPSBIL") {
		$_ =~ s/\s+/,/g;
		print {$fh} "$_$date\n";
	}
}


# Sample Call
# perl groupfiles.pl falb20130701.txt falb20130702.txt falb20130703.txt falb20130704.txt falb20130705.txt falb20130706.txt falb20130707.txt falb20130708.txt falb20130709.txt falb20130710.txt falb20130711.txt falb20130712.txt falb20130713.txt falb20130714.txt falb20130715.txt falb20130716.txt falb20130717.txt falb20130718.txt falb20130719.txt falb20130720.txt falb20130721.txt falb20130722.txt falb20130723.txt falb20130724.txt falb20130725.txt falb20130726.txt falb20130727.txt falb20130728.txt
