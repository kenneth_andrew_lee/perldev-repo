#!/usr/bin/perl
use strict;
use warnings;
use DBI;
use DateTime::Format::Strptime qw( );
#use Data::Dumper;
 
my %errors;
my @names;

my $format = DateTime::Format::Strptime->new(
   pattern   => '%m/%d/%Y %I:%M:%S %p',
   time_zone => 'local',
   on_error  => 'croak',
);
my $dt='';



## Database commands
#my $dbname = "XE";
#my $username="kalee";
#my $passwd = "kalee";
#my $db = DBI->connect( "dbi:Oracle:$dbname", $username, $passwd ) or die( $DBI::errstr . "\n" );
#$db->{AutoCommit}    = 0;
#$db->{RaiseError}    = 1;
#$db->{ora_check_sql} = 0;
#$db->{RowCacheSize}  = 16;

my $filename = 'c:\temp\gfalconerrors.txt';
#my $filename = 'c:\temp\outlookemail.txt';
open my $fh, '<', $filename or die "Couldn't open file: $!\n";
while (<$fh>) {
  my ($abbreviation, @fields) =  split /\s*\t\s*/;
  @fields = map {chomp; s/! //g; s/!//g; s/\.\./\./g; s/: /:/g; s/ :/:/g; $_; } @fields;
  
  ## Debug code
  #for (@fields) {
  #  print "$_\n";
  #}

  if (defined $fields[2] and $fields[2] ne "") {
    my $date = substr($fields[2],0,10);
    my $time = substr($fields[2],11);
    $time =~ s/\s//g;
    $fields[2] =  $date . " " . $time; 
  }

  @names = @fields and next if $abbreviation eq 'Schema';
  if ((scalar @fields == 0) and (length $abbreviation > 0)) {
    # 6/11/2014 2:07:41 PM
    #and (2 == () = $abbreviation =~ /:/g;)
    # Your am/pm value (PM) does not match your hour (2) at importdata.pl line 42. 
    my $x = ":";
    my $c = () = $abbreviation =~ /$x/g;
    $x = "/";
    my $d = () = $abbreviation =~ /$x/g;    
    if ($c == 2 and $d == 2) {
      $dt = $format->parse_datetime($abbreviation);
    }
  } elsif ((substr($abbreviation,0,4) eq "FALU") and (scalar @fields == 5)) {
    @{ $errors{$abbreviation} }{@names} = @fields;
    # Remove all whitespace
    #while (my ($key, $value) = each ($errors{$abbreviation})) {
    #  $value = $errors{$abbreviation}{$key};
    #  $value =~ s/\s//g;
    #  $errors{$abbreviation}{$key} = $value;
    #}
    #

    #Schema  
    #Curr_Date_GMT 
    #Cur_Mst_Date  
    #af_max_Date 
    #Delay_hours_from_GMT  
    #Delay_hours_from_mst  
    #2014-11-20T21:10:09|FALU899|STAND|2014-11-21 04:09:15|2014-11-20 21:09:15|2014-11-20 16:36:23|4.55
    #my $sth = $db->prepare("INSERT INTO GFALCON_ERRORS
    #                       (SCHEMA, CURR_DATE_GMT, CUR_MST_DATE, AF_MAX_DATE, DELAY_HOURS_FROM_MST)
    #                        values
    #                       ('$abbreviation', 
    #                        to_date('$errors{$abbreviation}{$names[1]}','yyyy-mm-dd-hh24:mi:ss'),
    #                        to_date('$errors{$abbreviation}{$names[2]}','yyyy-mm-dd-hh24:mi:ss'),
    #                        to_date('$errors{$abbreviation}{$names[3]}','yyyy-mm-dd-hh24:mi:ss'),
    #                        $errors{$abbreviation}{$names[4]})");
    #$sth->execute() or die $DBI::errstr;
    #$sth->finish();
    print "$dt|$abbreviation|$errors{$abbreviation}{$names[0]}|$errors{$abbreviation}{$names[1]}|$errors{$abbreviation}{$names[2]}|$errors{$abbreviation}{$names[3]}|$errors{$abbreviation}{$names[4]}\n";
  }
  #print "\n";
}
close $fh;

#$db->commit or die $DBI::errstr;
